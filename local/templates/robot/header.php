<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="Современные технологии - Внедрение в три этапа 🔷  ➤➤ ✔️ CRM системы ✔️ Автоматизация бизнес процесов ✔️ Маркировка ✔️ Домены ✔️ Хостинг сайтов и DNS-серверов ✔️ Облачные VDS/VPS ✔️ Выделенные серверы в аренду ✔️ Конструктор сайтов и CMS ✔️ Сайты и Интернет магазины ✔️Продажа SSL-сертификатов ✔️ Почта для доменов ✔️ Разработка программного обеспечения ✔️ Услуги системного администрирования  Настройка сетевого оборудования ✔️ Обучение пользователей" />

	<meta name="og:url" content="https://intercalation.ru" />
	<meta name="og:title" content="Современные технологии - Внедрение в три этапа" />
	<meta name="og:image" content="<?=SITE_TEMPLATE_PATH?>/img/ss.png" />
	<meta name="og:image:width" content="150"/>
	<meta name="og:image:height" content="50"/>


	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="yandex-verification" content="246f82aa51171df2" />

	<link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/img/favicon.png" type="image/x-icon">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/style.css">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/pop-ups.css">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/quiz.css">

	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/slick/slick.css"/>
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/slick/slick-theme.css"/>
	
	<title><?$APPLICATION->ShowTitle("Современные технологии - Внедрение в три этапа");?></title>
	<?$APPLICATION->ShowHead();?>

	<!-- Yandex.Metrika counter 
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(65279116, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/65279116" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
/Yandex.Metrika counter -->


<!-- Global site tag (gtag.js) - Google Analytics 
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145272881-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-145272881-2');
</script>
 /Google Analytics counter -->
</head>

<body>

<div id="panel">
	<?$APPLICATION->ShowPanel();?>
</div>

<?
	/* Создаем CAPTCHA
	include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/captcha.php");
	$cpt = new CCaptcha();
	$captchaPass = COption::GetOptionString("main", "captcha_password", "");
	if(strlen($captchaPass) <= 0)
	{
	    $captchaPass = randString(10);
	    COption::SetOptionString("main", "captcha_password", $captchaPass);
	}
	$cpt->SetCodeCrypt($captchaPass);
*/
?>

<form action="<?=SITE_TEMPLATE_PATH?>/send.php" method="post">
	<div id="send-form" class="bg-popup">
		<div class="bg-flex">
			<div class="form-popup">
				<img id="send-close" src="<?=SITE_TEMPLATE_PATH?>/img/close.png" alt="" class="close">
				<div class="form-wrap">
					<img src="<?=SITE_TEMPLATE_PATH?>/img/rocket.png" class="rocket" alt="">
					<input type="text" name="fio" required placeholder="Иванов Иван Иванович">
					<input type="text" name="tel" required placeholder="Телефон">
					<input type="email" name="email" required placeholder="Email">

					<!--<input name="captcha_code" value="<?/*=htmlspecialchars($cpt->GetCodeCrypt());*/?>" type="hidden">
<img src="/bitrix/tools/captcha.php?captcha_code=<?/*=htmlspecialchars($cpt->GetCodeCrypt());*/?>">
<input id="captcha_word" name="captcha_word" type="text" required placeholder="Введите текст с картинки..."> -->

					<textarea name="content" id="" cols="10" rows="5" placeholder="Введите текст..."></textarea>

					<button id="send" class="center">Отправить</button>
				</div>
			</div>
		</div>
	</div>
</form>

<!--<form action="<?=SITE_TEMPLATE_PATH?>/send2.php" method="post">-->
	<div id="send2-form" class="bg-popup">
		<div class="bg-flex">
			<div class="form-popup">
				<img id="send2-close" src="<?=SITE_TEMPLATE_PATH?>/img/close.png" alt="" class="close">
				<div class="form-wrap">
					<h3 class="title-job">Оставьте данные для связи</h3>
					<img src="<?=SITE_TEMPLATE_PATH?>/img/rocket.png" class="rocket" alt="">
					<input type="text" name="fio-job" required placeholder="Иванов Иван Иванович">
					<input type="text" name="tel-job" required placeholder="Телефон">
					<input id="job" type="hidden" name="job" value="Вакансия"/>
					<button id="send2" class="center">Откликнуться</button>
				</div>
			</div>
		</div>
	</div>
<!--</form>-->

	<div id="send3-form" class="bg-popup">
		<div class="image">
    		<div class="image-quiz">
    			<img srcset="<?=SITE_TEMPLATE_PATH?>/img/begin-quiz-mini.jpg 375w, <?=SITE_TEMPLATE_PATH?>/img/begin-quiz-mini.jpg 425w, <?=SITE_TEMPLATE_PATH?>/img/begin-quiz-mini.jpg 500w, <?=SITE_TEMPLATE_PATH?>/img/begin-quiz.png 1280w," src="<?=SITE_TEMPLATE_PATH?>/img/begin-quiz.png" id="img1">
				<img src="<?=SITE_TEMPLATE_PATH?>/img/begin-quiz-mini.jpg" id="img3">
        		<img srcset="<?=SITE_TEMPLATE_PATH?>/img/end-quiz-mini.jpg 375w, <?=SITE_TEMPLATE_PATH?>/img/end-quiz-mini.jpg 425w, <?=SITE_TEMPLATE_PATH?>/img/end-quiz-mini.jpg 500w, <?=SITE_TEMPLATE_PATH?>/img/end-quiz.png 1280w," src="<?=SITE_TEMPLATE_PATH?>/img/end-quiz.png" id="img2">
				<img src="<?=SITE_TEMPLATE_PATH?>/img/end-quiz-mini.jpg" id="img4">
    		</div>
		</div>
    	<div class="bg-flex">
        	<div class="form-popup quiz">
				<img id="send3-close" src="<?=SITE_TEMPLATE_PATH?>/img/close.png" alt="" class="close">
            	<div class="form-wrap wrap-quiz">
					<img src="<?=SITE_TEMPLATE_PATH?>/img/rocket.png" class="rocket rocket-quiz" alt="">
					<!--<form action="<?=SITE_TEMPLATE_PATH?>/send3.php" method="post">-->
                	<div class="quiz-header" id="header">
                    	<!-- Заголовок вопроса -->
                    	<h2 id="title-quiz">Загружаем вопрос...</h2>
        
                    	<!-- Результаты викторины -->
                    	<input type="text" placeholder="" value="Иванов Иван Иванович">
                    	<input type="text" placeholder="Телефон">
                    	<button id="send3" class="center">Откликнуться</button>

                        <h2>%title%</h2>
                        <h3>%tel%</h3>
                               
                	</div>
					<!--</form>-->
                	<ul class="quiz-list" id="list">
                    	<li>
                        	<label>
                            	<input type="radio" class="answer" name="answer" />
                            	<span>Ответ...</span>
                        	</label>
                    	</li>
                    	<li>
                        	<label>
                            	<input type="radio" class="answer" name="answer" />
                            	<span>Ответ...</span>
                        	</label>
                    	</li>
                	</ul>
                	<img src="<?=SITE_TEMPLATE_PATH?>/img/quiz-back.png" class="quiz-submit" alt="">
                	<img src="<?=SITE_TEMPLATE_PATH?>/img/quiz-for.png" class="quiz-submit2" alt="">

            	</div>
        	</div>
    	</div>
	</div>


	<div id="auth-form" class="bg-popup">
		<div class="bg-flex">
			<div class="form-popup">
				<img id="auth-close" src="<?=SITE_TEMPLATE_PATH?>/img/close.png" alt="" class="close">
				<div class="form-wrap">
					<h1>Авторизация</h1>
					<input type="text" placeholder="Логин">
					<input type="text" placeholder="Пароль">
					<p><a id="regisr-link" class="form-link">Регистрация</a></p>
					<p><a id="btn-restore-form" class="form-link">Забыли пароль?</a></p> <!-- href="" -->
					<button id="auth" class="center">Войти</button> 
				</div>
			</div>
		</div>
	</div>

	<div id="regisr-form" class="bg-popup">
		<div class="bg-flex">
			<div class="form-popup">
				<img id="regisr-close" src="<?=SITE_TEMPLATE_PATH?>/img/close.png" alt="" class="close">
				<div class="form-wrap">
					<h1>Регистрация</h1>
					<input type="text" placeholder="Логин">
					<input type="text" placeholder="Пароль">
					<input type="text" placeholder="Email">
					<input type="text" placeholder="Телефон">
					<p><a id="auth-link" class="form-link">Авторизация</a></p>
					<button id="regisr" class="center">Регистрация</button>
				</div>
			</div>
		</div>
	</div>

	<div id="restore-form" class="bg-popup">
		<div class="bg-flex">
			<div class="form-popup">
				<img id="restore-close" src="<?=SITE_TEMPLATE_PATH?>/img/close.png" alt="" class="close">
				<div class="form-wrap">
					<img id="restore-back" src="<?=SITE_TEMPLATE_PATH?>/img/back.png" alt="" class="back">
					<h1>Восстановление пароля</h1>
					<input type="text" placeholder="Логин">
					<input type="text" placeholder="Email">
					<input type="text" placeholder="Номер телефона">
					<p>&nbsp;</p><p>&nbsp;</p>
					<button id="restore" class="center">Восстановить</button>
				</div>
			</div>
		</div> 
	</div>

	<div class="babls">
		<header>
			
			<div class="logo">
				<a href="https://intercalation.ru">
			<?/*	<h1><a href="">Внедрение</a></h1>*/?>
				<img src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" 
					alt=""
					height = "38px"
					width = "210px">
				</a>
			</div>
			

<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"top.multi", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "3",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "Y",
		"COMPONENT_TEMPLATE" => "top.multi",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>

<?/*

				<div id="socials" class="socials">
					<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/img/vk.png" alt=""></a>
					<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/img/inst.png" alt=""></a>
					<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/img/mail.png" alt=""></a>
				</div>


*/?>


<?/*
			<nav>
				<div>
					<a href="" class="nav-link__active">Главная</a>
				</div>
				<div>
					<a class="nav-link">Подменю</a>
					<div class="sub-menu">
						<div class="bx"><div></div></div>
						<img src="<?=SITE_TEMPLATE_PATH?>/img/back.png" alt="" class="back">
						<div class="sub-link">Текст</div>
						<div class="sub-link">Текст</div>
						<div class="sub-link">Текст1</div>
						<div class="sub2-menu">
							<div class="bx2"><div></div></div>
							<img src="<?=SITE_TEMPLATE_PATH?>/img/back.png" alt="" class="back">
							<div class="sub2-link">подТекст1</div>
							<div class="sub2-link">подТекст1</div>
							<div class="sub2-link">подТекст1</div>	
						</div>
						<div class="sub-link">Текст</div>
						<div class="sub-link">Текст2</div>
						<div class="sub2-menu">
							<div class="bx2"><div></div></div>
							<img src="<?=SITE_TEMPLATE_PATH?>/img/back.png" alt="" class="back">
							<div class="sub2-link">подТекст2</div>
							<div class="sub2-link">подТекст2</div>
							<div class="sub2-link">подТекст2</div>
							<div class="sub2-link">подТекст2</div>
							<div class="sub2-link">подТекст2</div>
							<div class="sub2-link">подТекст2</div>
							<div class="sub2-link">подТекст2</div>
						</div>
					</div>
				</div>
				<div>
					<a href="" class="nav-link">Левый</a>
				</div>
				<div>
					<a href="" class="nav-link">Правый</a>
				</div>
				<div>
					<a href="" class="nav-link">Два</a>
				</div>
				<div>
					<a href="" class="nav-link">Нету</a>
				</div>
				<div id="socials" class="socials">
					<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/img/vk.png" alt=""></a>
					<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/img/inst.png" alt=""></a>
					<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/img/mail.png" alt=""></a>
				</div>
			</nav>

*/?>

			<div class="auth">
				<img id="burger" src="<?=SITE_TEMPLATE_PATH?>/img/burger.png" alt="">
				<button id="btn-auth-form">Войти</button>
			</div>


