const questions = [
	{
		question: "Пройдите короткий опрос",
		answers: [
			"CRM-системы", 
			"Сайты и Интернет-магазин", 
			"Разработка ПО", 
			"Системное администрирование" ],

	},
	{
		question: "CRM-системы",
		answers: [
			"Первичное внедрение",
			"Настройка или доработка существующей CRM"
		],

	},
	{
		question: "Сайты и Интернет-магазин",
		answers: [
			"Сайт Визитка",
			"Лендинг",
			"Интернет магазин"
		],

	},
	{
		question: "Разработка ПО",
		answers: [
			"Интеграция между системами по API",
			"Разработка приложения к CRM",
			"Интеграция с 1С",
			"Разработка квизов"
		],

	},
	{
		question: "Системное администрирование",
		answers: [
			"Настройка сетевого оборудования",
			"Настройка серверов",
			"Настройка прочего оборудования",
			"Прокладка сетевых кабелей"
		],

	},
	{
		question: "Какое решение?",
		answers: [
			"Индивидуальное решение",
			"Шаблонное решение"
		],

	},

];

let questionIndex = 0;

const headerContainer = document.querySelector('#header');
const listContainer = document.querySelector('#list');
const forBtn = document.querySelector('.quiz-submit2');
const backBtn = document.querySelector('.quiz-submit');

 if (document.querySelector('.slogan')){
	setTimeout(function() {
		var screenWidth = window.screen.width;
		console.log(screenWidth);
		document.getElementById('send3-form').style.display = 'block';
		document.querySelector('.form-popup').style.display = 'none';
		document.querySelector('.image').style.display = 'block';
		if (screenWidth <= 768){		
			document.getElementById('img3').style.display = 'block';
			document.getElementById('img1').style.display = 'none';
		}
		else {
		document.getElementById('img1').style.display = 'block';
		document.getElementById('img3').style.display = 'none';
		}
	 }, 2000);
	 setTimeout(function() {
		document.getElementById('img1').style.display = 'none';
		document.querySelector('.image').style.display = 'none';
		document.querySelector('.form-popup').style.display = 'block';
	 }, 4000);
 }
 else console.log("Слогана нет");

clearPage();
showQuestion();
forBtn.onclick = checkAnswer;
backBtn.onclick = backAnswer;
//listContainer.onclick = checkAnswer; //для перехода без кнопки вперед

function clearPage(){
	headerContainer.innerHTML = '';
	listContainer.innerHTML = '';
}

function showQuestion(){
	console.log('showQuestion');
	//Вопрос
	const headerTemplate = `<h2 id="title-quiz">%title%</h2>`;//шаблон
	const title = headerTemplate.replace('%title%', questions[questionIndex]['question']);//заменяем шаблон

	headerContainer.innerHTML = title;//применяем

	//Ответы
	let answerNumber = 1;

	for (answerText of questions[questionIndex]['answers']){
		console.log(answerText);

			const questionTemplate = 
			`<li>
				<label>
					<input type="radio" value="%number%" class="answer" name="answer"/>
					<span>%answer%</span>
				</label>
			</li>
			`;


		const answerHTML = questionTemplate
							.replace('%answer%', answerText)
							.replace('%number%', answerNumber);

		listContainer.innerHTML = listContainer.innerHTML + answerHTML;
		if (questionIndex === 0) {backBtn.style.display = 'none';} else {backBtn.style.display = 'block';}
		answerNumber++;
	}
}

function checkAnswer(){
	const checkedRadio = listContainer.querySelector('input[type="radio"]:checked'); //поиск выбранной радиокнопки
	console.log(checkedRadio);

	//если ответ не выбран, ничего не делаем, выход из функции
	if (!checkedRadio){
		submitBtn.blur();
		return
	}

	//узнаем номер ответа пользователя
	let userAnswer = parseInt(checkedRadio.value);
	console.log(userAnswer);

	console.log(questions[questionIndex]);
	if (questionIndex !== questions.length){

		if (questions[questionIndex]['question'] === 'Пройдите короткий опрос'){
			//document.querySelector('#img1').style.display = "block";
			switch (userAnswer){
				case 1: console.log("Выбран вопрос 1 и ответ CRM"); v1 = "CRM,"; questionIndex++; break;
				case 2: console.log("Выбран вопрос 1 и ответ Сайты"); v1 = "Сайты,"; questionIndex = questionIndex + 2; break;
				case 3: console.log("Выбран вопрос 1 и ответ ПО"); v1 = "ПО,"; questionIndex = questionIndex + 3; break;
				case 4: console.log("Выбран вопрос 1 и ответ Администрирование"); v1 = "Администрирование,"; questionIndex = questionIndex + 4; break;
			}
			v3 = "";
			console.log("Выбор", v1);
			clearPage();
			showQuestion();
		}

		else if (questions[questionIndex]['question'] === 'CRM-системы'){			
			switch (userAnswer){	
				case 1: console.log("Выбран вопрос 2 и ответ Первичное внедрение"); v2 = " Первичное внедрение"; questionIndex = questionIndex + 4; break;
				case 2: console.log("Выбран вопрос 2 и ответ Настройка или доработка существующей CRM"); v2 = " Настройка или доработка существующей CRM"; questionIndex = questionIndex + 4; break;
			}
			v3 = "";
			console.log("Выбор", v1, v2, v3);
			clearPage();
			showResults();
		}

		else if (questions[questionIndex]['question'] === 'Сайты и Интернет-магазин'){
			switch (userAnswer){	
				case 1: console.log("Выбран вопрос 2 и ответ Сайт Визитка"); v2 = " Сайт Визитка"; questionIndex = questionIndex + 3; break;
				case 2: console.log("Выбран вопрос 2 и ответ Лендинг"); v2 = " Лендинг"; questionIndex = questionIndex + 3; break;
				case 3: console.log("Выбран вопрос 2 и ответ Интернет магазин"); v2 = " Интернет магазин"; questionIndex = questionIndex + 3; break;
			}
			console.log("Выбор", v1, v2);
			clearPage();
			showQuestion();
		}

		else if (questions[questionIndex]['question'] === 'Разработка ПО'){
			switch (userAnswer){	
				case 1: console.log("Выбран вопрос 2 и ответ Интеграция между системами по API"); v2 = " Интеграция между системами по API"; questionIndex = questionIndex + 2; break;
				case 2: console.log("Выбран вопрос 2 и ответ Разработка приложения к CRM"); v2 = " Разработка приложения к CRM"; questionIndex = questionIndex + 2; break;
				case 3: console.log("Выбран вопрос 2 и ответ Интеграция с 1С"); v2 = " Интеграция с 1С"; questionIndex = questionIndex + 2; break;
				case 4: console.log("Выбран вопрос 2 и ответ Разработка квизов"); v2 = " Разработка квизов"; questionIndex = questionIndex + 2; break;
			}
			v3 = "";
			console.log("Выбор", v1, v2, v3);
			clearPage();
			showResults();
		}

		else if (questions[questionIndex]['question'] === 'Системное администрирование'){
			switch (userAnswer){	
				case 1: console.log("Выбран вопрос 2 и ответ Настройка сетевого оборудования"); v2 = " Настройка сетевого оборудования"; questionIndex++; break;
				case 2: console.log("Выбран вопрос 2 и ответ Настройка серверов"); v2 = "Настройка серверов"; questionIndex++; break;
				case 3: console.log("Выбран вопрос 2 и ответ Настройка прочего оборудования"); v2 = " Настройка прочего оборудования"; questionIndex++; break;
				case 4: console.log("Выбран вопрос 2 и ответ Прокладка сетевых кабелей"); v2 = " Прокладка сетевых кабелей"; questionIndex++; break;
			}
			v3 = "";
			console.log("Выбор", v1, v2, v3);
			clearPage();
			showResults();
		}

		else if (questions[questionIndex]['question'] === 'Какое решение?'){
			switch (userAnswer){	
				case 1: console.log("Выбран вопрос 3 и ответ Индивидуальное решение"); v3 = ", Индивидуальное решение"; questionIndex = questionIndex + 1; break;
				case 2: console.log("Выбран вопрос 3 и ответ Шаблонное решение"); v3 = ", Шаблонное решение"; questionIndex = questionIndex + 1; break;
			}
			console.log("Выбор", v1, v2, v3);
			clearPage();
			showResults();
		}

	}

	else {
		clearPage();
		showResults();
	}

}

function backAnswer(){

	forBtn.style.display = 'block';
	if (questionIndex !== questions.length - 6){

		if (questions[questionIndex]['question'] === 'CRM-системы'){
			 questionIndex = questionIndex - 1;
			
			//console.log("Выбор", v1, v2);
		}
		
		else if (questions[questionIndex]['question'] === 'Сайты и Интернет-магазин'){
			 questionIndex = questionIndex - 2;
			
			//console.log("Выбор", v1, v2);
		}

		else if (questions[questionIndex]['question'] === 'Разработка ПО'){
			 questionIndex = questionIndex - 3;
		
		}

		else if (questions[questionIndex]['question'] === 'Системное администрирование'){

			questionIndex = questionIndex - 4;
		}

		else if (questions[questionIndex]['question'] === 'Какое решение?'){
			if (v1 === "CRM"){
				questionIndex = questionIndex - 4;
			}
			else if (v1 === "Сайты"){
				questionIndex = questionIndex - 3;
			}
			else if (v1 === "ПО"){
				questionIndex = questionIndex - 2;
			}
			else questionIndex--;
		}

		clearPage();
		showQuestion();


	}

	else {
		
		clearPage();
		showResults();
	}

}

function showResults(){
	console.log('showResults start');
	let result = v1 + ' ' + v2 + ' ' + v3;
	const resultsTemplate = 
	`	
		<h3>Оставьте данные</h3><br>
		<input type="text" id="fio-quiz" name="fio-quiz" required placeholder="Иванов Иван Иванович"><br>
		<input type="text" id="tel-quiz" name="tel-quiz" required placeholder="Телефон"><br>
		<input id="poll" type="hidden" name="poll" value="Poll"/>
		<button id="send3" class="center">Отправить</button>

	`;
	const final = resultsTemplate.replace('%result%', result);
	headerContainer.innerHTML = final;
	document.getElementById('poll').value = result;
	let d = document.getElementById('poll').value;
	forBtn.style.display = 'none';
	backBtn.style.display = 'none';

	var res = document.querySelector("#label"),
	text = res.lastChild.textContent.trim();

	const submit3Btn = document.querySelector('#send3');
	console.log("Нашел send3");
	console.log(text);
	submit3Btn.onclick = function(){
		$.ajax({
			url: '/local/templates/robot/send3.php',
			type: 'POST',
			data: {text: d},
			success: function(data){
				console.log(data);
				console.log("Peredana v json");
				var screenWidth = window.screen.width;
				document.getElementById('send3-form').style.display = 'block';
				document.querySelector('.form-popup').style.display = 'none';
				document.querySelector('.image').style.display = 'block';
				if (screenWidth <= 768){
					document.getElementById('img4').style.display = 'block';
					document.getElementById('img3').style.display = 'none';
					document.getElementById('img2').style.display = 'none';
				}
				else {
				document.getElementById('img2').style.display = 'block';
				document.getElementById('img4').style.display = 'none';
				}
				setTimeout(function() {					
					document.querySelector('.image').style.display = 'none';
					document.getElementById('img4').style.display = 'none';
					document.getElementById('img2').style.display = 'none';
					document.getElementById('send3-form').style.display = 'none';
				}, 2000);
			}, error: function(){
			console.log('ERROR');
				}
		})

	}

}