//test

$('#btn-send-form, #fbtn-send-form, .sidebare-send-form').click(function(e)
{
	$('#send-form').fadeIn();	
});
$('#send, #send-close').click(function(e)
{
	$('#send-form').fadeOut();	
});

$('.btn-send2-form').click(function(e)
{
	$('#send2-form').fadeIn();	
});
$('#send2, #send2-close').click(function(e)
{
	$('#send2-form').fadeOut();	
});

//Кнопка закрыть для квиза
$('#send3-close').click(function(e)
{
	$('#send3-form').fadeOut();	
});

$('#btn-auth-form, #auth-danger, .fat-btn, #auth-link').click(function(e)
{
	$('#auth-form').fadeIn();
	$('#regisr-form').fadeOut();
	//$('.bg-left-menu, .bg-right-menu').fadeOut();
	//$('#side-btn-l, #side-btn-r').show();
});
$('#auth, #auth-close').click(function(e)
{
	$('#auth-form').fadeOut();	
});

$('#btn-regisr-form, #regisr-link').click(function(e)
{
	$('#regisr-form').fadeIn();	
	$('#auth-form').fadeOut();
});
$('#regisr, #regisr-close').click(function(e)
{
	$('#regisr-form').fadeOut();	
});

$('#btn-restore-form').click(function(e)
{
	$('#restore-form').fadeIn();	
});
$('#restore, #restore-close, #restore-back').click(function(e)
{
	$('#restore-form').fadeOut();	
});

// MENU TOP
$('#burger').click(function()
{
	$('nav').slideToggle();
	$('header').removeClass('out-y');
});

$('.nav-link').click(function()
{
	if($(window).width() <= 900) {
		$(this).next('.sub-menu').css('display', 'block');
	}
});

$('.nav-link__active').click(function()
{
	if($(window).width() <= 900) {
		$(this).next('.sub-menu').css('display', 'block');
	}
});

$('.sub-menu > .back').click(function()
{
	$(this).parent().css('display', 'none');
	$('.sub2-menu').css('display', 'none');
});

$('.sub2-menu > .back').click(function()
{
	$(this).parent().css('display', 'none');
});

$('.sub-link').click(function()
{
	if($(window).width() <= 900) {
	$(this).next('.sub2-menu').css('display', 'block');
	}
});

// MENU SIDE-LEFT
$('#side-btn-l').click(function()
{
	$('.bg-left-menu').show();
	$(this).hide();
});
$('#side-long-l').click(function()
{
	$('.bg-left-menu').fadeOut();
	$('#side-btn-l').show();
});

$('#side-btn-r').click(function()
{
	$('.bg-right-menu').show(); 
	$(this).hide();
});
$('#side-long-r').click(function()
{
	$('.bg-right-menu').fadeOut();
	$('#side-btn-r').show();
});

$(document).ready(function()
{
	if($(window).width() <= 900) { $('.sub-menu, .sub2-menu').hide(); }

	/*$('.sub-link').last().css({'border': 0, 'margin': 0, 'pading': 0});

	$('.sub-link').hover(function()
	{
		let pos = $(this).position().top;
		$(this).next().children('.bx2').css('top', pos - 25);
	});*/


		if($(window).width() <= 375)
		{
			var ctrPdg = '4%';
			var k = 1;
			var d = true;
		}
		else if($(window).width() <= 768)
		{
			var ctrPdg = '27%';
			var k = 2;
			var d = true;
		}
		else
		{
			var ctrPdg = '1%';
			var k = 3;
			var d = false;
		}

		$('.slider-3d').slick({
			dots: d,
			infinite: true,
			accessibility: false,
			centerMode: true,
			centerPadding: ctrPdg,
			slidesToShow: 3,
			speed: 500,
		responsive: [{

		      breakpoint: 992,
		      settings: {
		        slidesToShow: 1,
		        arrows: false
		      }

		    }]
		});

		$('.footer-center').slick({
			dots: d,
			infinite: true,
			accessibility: false,
			centerMode: true,
			centerPadding: 0,
			slidesToShow: 3,
			speed: 500,
		responsive: [{

		      breakpoint: 992,
		      settings: {
		        slidesToShow: k,
		        arrows: false
		      }

		    }]
		});

		$('.slider').slick({
	      dots: true,
	      accessibility: true,
	      autoplay: true,
       	  autoplaySpeed: 5000,
	      prevArrow: "<img class='arrow-left' src='../local/templates/robot/img/arrow-left.png' class='prev' alt='left'>",
		  nextArrow: "<img class='arrow-right' src='../local/templates/robot/img/arrow-right.png' class='next' alt='right'>",
	    });
	
	var fItemCount = $('.footer-item').length,
		fItem = $('.footer-item'),
		k = 0;

	for(let i = 0; i < fItemCount; i++)
	{
		fItem.eq(i).children('p').each(function()
		{
			k++;

			if(k > 4)
			{
				$(this).addClass('f-hide');
			}
		});

		k = 0;
	}

	$('.down').click(function()
	{
		if($('.f-hide').css('display') == 'none')
		{
			$('.f-hide').css('display', 'block');
			$(this).css('transform', 'rotate(180deg)');
		}
		else
		{
			$('.f-hide').css('display', 'none');
			$(this).css('transform', 'rotate(0)');
		}
	});

	function setTS()
	{
		if($(window).scrollTop() > 75)
		{
			$('header').addClass('out-y');
			$('.danger').addClass('out-x');
		}
	}

	$(window).scroll(function()
	{
		$('header').removeClass('out-y');
		$('.danger').removeClass('out-x');
		
		if($(window).scrollTop() > 75)
		{
			$('header').addClass('out-y');
			$('.danger').addClass('out-x');
		}
		else
		{
			$('header').removeClass('out-y');
			$('.danger').removeClass('out-x');
		}
	});
	
});