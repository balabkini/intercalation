<?
/*
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');
if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');
*/

require_once dirname(__FILE__) . '/Classes/PHPExcel.php';
?>
<?php
	$result = CRest::call('crm.'.$item.'.fields');
    $res = $result['result'];
?>
<?
	$u_l1 = [];
	$enumeration1 = field_id_value($res[$field1], $u_l1);
	$u_l2 = [];
	$enumeration2 = field_id_value($res[$field2], $u_l2);
?>
<?php
$next = 0;
$finish = false;
$res = [];
while (!$finish)
{
	$result = CRest::call('crm.'.$item.'.list',
		[
			'order' => [
				$field2 => "ASC",
				$field1 => "ASC"
		],
			'filter' => [
				'>=DATE_CREATE' => $calendar1,
				'<=DATE_CREATE' => $calendar2,
		],
			'select' => [
				'DATE_CREATE',
				$field1,
				$field2,
				'SOURCE_ID',
		],
			'start' => $next,
	]);

	$res = array_merge($res, $result['result']);
	if(isset($result['next']))
	$next = $result['next'];
	else
	$finish = true;
}


$f1= array();
$f2= array();
$f = array();
$s = array();
foreach ($res as $key => $value){
	//Категории
	if(in_array($value[$field1], $f1)==false)
	$f1[] = $value[$field1];

	//Разделение по дате
	$dar = date_parse($value['DATE_CREATE']);
	$d = $dar['day'].'.'.$dar['month'].'.'.$dar['year'];
	if (!isset($f2[$d]))
	$f2[$d] = [];

	//Разделение по времени в датах
	if (isset($f2[$d][$value[$field2]]))
	$f2[$d][$value[$field2]]++;
	else
	$f2[$d][$value[$field2]]=1;

	//Разделение по всем параметрам
	if (isset($f[$value[$field1]][$d][$value[$field2]])){
		$f[$value[$field1]][$d][$value[$field2]]++;
	}else{
		$f[$value[$field1]][$d][$value[$field2]] = 1;
	}

	//Разделение по источникам
	if (isset($s[$value['SOURCE_ID']][$d]))
	$s[$value['SOURCE_ID']][$d]++;
	else
	$s[$value['SOURCE_ID']][$d]=1;
}


$result = CRest::call('crm.status.list',
[
	'order' => [
		'SORT' => "ASC",
	],
	'filter' => [ 
		"ENTITY_ID" => "SOURCE",
	],
]);
$source = $result['result'];


?>
<?php
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("Мир спорта")
							 ->setLastModifiedBy("Мир спорта")
							 ->setTitle("Мир спорта")
							 ->setSubject("Мир спорта")
							 ->setDescription("Мир спорта")
							 ->setKeywords("Мир спорта")
							 ->setCategory("Мир спорта");


$len = 0;
$sum = 0;
//echo "<table class='table-bordered text-center table-lg'><caption>".$tablename."</caption>";
//echo "<tr><th class='table-primary' rowspan=2>".$catname."</th>";
		$i = 'A';
		$j = 1;
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($i.$j, $catname);

		$i = 'B';
		foreach ($f2 as $k => $v){
			//echo "<th colspan=".count($v).">".$k."</th>";
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($i.$j, $k);
			for ($c = 1; $c <= count($v); $c++)
			$i++;
		}
//echo "</tr>";

//echo "<tr>";
		$i = 'B';
		$j = 2;
		foreach ($f2 as $k => $v){
			foreach ($v as $k1 => $v1){
				if($enumeration2 && isset($u_l2[$k1]))
				$int = $u_l2[$k1];
				else
				$int = $k1;

				//echo "<th style='width: 6%;'>".$int."</th>";
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($i.$j, $int);
				$i++;
			}
		}
//echo "</tr>";

	foreach ($s as $k => $v){
		if(!empty($k)){
			$key = array_search($k, array_column($source, 'STATUS_ID'));
			$n = $source[$key]['NAME'];
		}else{
			$n = 'Источника нет';
		}
		$i = 'A';
		$j++;
		//echo "<tr><th class='table-primary'>".$n."</th>";
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($i.$j, $n);
			$i = 'B';
			foreach ($f2 as $k2 => $v2){

				$ks = array_keys($f2);
				$last_key = array_pop($ks);

				if($k2 == $last_key)
				$tc = 'table-danger';
				else
				$tc = 'table-success';

				//echo "<th class='".$tc."' colspan=".count($v2).">";
					if(isset($v[$k2]))
						//echo $v[$k2];
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue($i.$j, $v[$k2]);
					else
						//echo "0";
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue($i.$j, 0);
				//echo "</th>";
				for ($c = 1; $c <= count($v2); $c++)
				$i++;
			}
		//echo "</tr>";
	}

//echo "<tr>";
//echo "</tr>";

		foreach ($f as $key => $row){

			$ks = array_keys($f2);
			$last_key0 = array_pop($ks);

			//echo "<tr>";
			if($enumeration1 && isset($u_l1[$key]))
			$cat = $u_l1[$key];
			else
			$cat = $key;
			//echo "<th class='table-primary'>".$cat."</th>";
			$i = 'A';
			$j++;
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($i.$j, $cat);
			$i = 'B';
			foreach ($f2 as $k0 => $v0){

				$ks = array_keys($f2[$k0]);
				$last_key = array_pop($ks);

				//$i = 'B';
				//$j++;
				foreach ($v0 as $k => $v){

					if($k == $last_key)
					$tc = 'table-active';
					else{
						if($k0 == $last_key0)
						$tc = 'table-danger';
						else
						$tc = 'table-success';
					}

					if (array_key_exists($k0, $row)){
					if (array_key_exists($k, $row[$k0])) {
						//echo "<td class='".$tc."'>".$row[$k0][$k]."</td>";
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue($i.$j, $row[$k0][$k]);
					}else{
						//echo "<td class='".$tc."'></td>";
					}}else{
						//echo "<td class='".$tc."'></td>";
					}
					$i++;
				}
			}

			//echo "</tr>";
		}


//echo "<tr>";
//echo "<td>Всего</td>";
	$i = 'A';
	$j++;
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($i.$j, 'Всего');
	$i = 'B';
	foreach ($f2 as $k1 => $v1)
	foreach ($v1 as $k => $v){
		$sum=$sum+$v;
		$len++;
		//echo "<td>".$v."</td>";
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($i.$j, $v);
		$i++;
	}
//echo "</tr>";
//echo "<tr><td>Общее кол-во</td><td colspan=".$len.">".$sum."</td></tr>";
	$i = 'A';
	$j++;
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($i.$j, 'Общее кол-во');
	$i = 'B';
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($i.$j, $sum);
//echo "</table>";

$objPHPExcel->setActiveSheetIndex(0);


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Отчет по лидам и сделкам.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
?>