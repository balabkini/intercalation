<?
require_once (__DIR__.'/crest.php');
?>
<?
ignore_user_abort(true);
set_time_limit(180);
?>
<?
function field_id_value($field, &$u_l){
	if ($field['type'] === 'enumeration'){
		foreach($field['items'] as $key => $value){
			$u_l[$value['ID']]=$value['VALUE'];
		}
		return true;
	}elseif ($field['type'] === 'iblock_element'){
		$ib_id = $field['settings']['IBLOCK_ID'];
		$l = CRest::call('lists.field.get', [
			'IBLOCK_TYPE_ID' => 'lists',
			'IBLOCK_ID' => $ib_id,
		]);
		$r = $l['result'];
		unset($r['NAME']);
		$kf = array_keys($r);
		$f_id = $kf[0];
		$l = CRest::call('lists.element.get', [
			'IBLOCK_TYPE_ID' => 'lists',
			'IBLOCK_ID' => $ib_id,
		]);
		$r = $l['result'];
		forEach($r as $key => $value){
			$u_l += $value[$f_id];
		};
		return true;
	}elseif ($field['type'] === 'employee'){

		$next = 0;
		$finish = false;
		$r = [];
		while (!$finish)
		{
			$u = CRest::call('user.get', ['start' => $next]);
			$r = array_merge($r, $u['result']);
			if(isset($u['next']))
			$next = $u['next'];
			else
			$finish = true;
		}

		foreach($r as $key => $value){
			$u_l[$value['ID']]=$value['EMAIL']."<br>".$value['NAME'];
		}
		return true;
	}else
	return false;
}
?>
<?
$item = "lead";
$field1 = "UF_CRM_1556318808794";
$field2 = "UF_CRM_1634722315";
$calendar1 = date('Y-m-d\T').'00:00';
$calendar2 = date('Y-m-d\T').'23:59';
$tablename = "Отчет";
$catname = "Категории";

if(isset($_POST["report"])){
    $item = $_POST["item"];
    switch ($item) {
        case "lead":
            $tablename = "Отчет по лидам";
		$catname = "Лиды по категориям";
            break;
        case "deal":
            $tablename = "Отчет по сделкам";
		$catname = "Сделки по категориям";
            break;
        default:
		$tablename = "Отчет";
    }
	if (isset($_POST["field1"]))
    $field1 = $_POST["field1"];
	else
	$field1 = 'ID';
	if (isset($_POST["field2"]))
    $field2 = $_POST["field2"];
	else
	$field2 = 'ID';
    $calendar1 = $_POST["calendar1"];
    $calendar2 = $_POST["calendar2"];

	if($_POST["report"]=="reportexcel"){
		include(dirname(__FILE__) . '/to_excel.php');
		return;
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<title>Отчет</title>
</head>
<body style="background-color: #ffffff;">
<section class="content">

<div style="background-color: #288ee2; padding-left: 5%; margin-bottom: 3%; padding-top: 1%; padding-bottom: 1%; color:azure"><h3>Фильтр</h3></div>
        <div class="filter" style="margin-left: 30px;">

<form action="" method="post" name="report">
	<input type="hidden" name="report" value="report">
    <div class="form-group">
		<label for="item">Тип фильтра</label>
        <select name="item" id="item" class="col-md-3" tabindex="1" autofocus required>
            <option value="lead" <?=($item == "lead" ? ' selected="selected"' : '')?>>Лиды</option>
            <option value="deal" <?=($item == "deal" ? ' selected="selected"' : '')?>>Сделки</option>
        </select>
    </div>

    <div class="form-group">
		<label for="cal1">Дата начала</label>
        <input type="datetime-local" name="calendar1" id="cal1" value=<?=$calendar1?> tabindex="2" required>
	</div>
    <div class="form-group">
		<label for="cal2">Дата окончания</label>
        <input type="datetime-local" name="calendar2" id="cal2" value=<?=$calendar2?> tabindex="3" required>
    </div>

    <div class="form-group">
		<label for="field1">Поле категорий</label>
        <select name="field1" id="field1" class="col-md-3" tabindex="4" autofocus required>

        <?php

            $result = CRest::call('crm.'.$item.'.fields');
            $res = $result['result'];

            foreach ($res as $key => $value){
				$pos = strripos($key, 'UF_CRM_');
				if ($pos === false) 
                echo "<option value=\"".$key."\"".($field1 == $key ? ' selected="selected"' : '').">".$value['title']."</option>";
				else
				echo "<option value=\"".$key."\"".($field1 == $key ? ' selected="selected"' : '').">".$value['listLabel']."</option>";
            }
           
         ?>
        </select>
    </div>

    <div class="form-group">
		<label for="field2">Поле интервала</label>
        <select name="field2" id="field2" class="col-md-3" tabindex="5" autofocus required>
        <?php
            foreach ($res as $key => $value){
                $pos = strripos($key, 'UF_CRM_');
				if ($pos === false) 
                echo "<option value=\"".$key."\"".($field2 == $key ? ' selected="selected"' : '').">".$value['title']."</option>";
				else
				echo "<option value=\"".$key."\"".($field2 == $key ? ' selected="selected"' : '').">".$value['listLabel']."</option>";
            }
	    ?>        
        </select>
    </div>
	<div class="form-group">
		<input class="btn btn-primary" type="submit" value="Применить">
	</div>
</form>

</div>

<?php
/*
echo '<pre>';
print_r($res[$field1]);
echo '</pre>';
*/
//Выбираем нужные элементы определенного списка
$type_lead = [];
$field = 'UF_CRM_1595397205732';
$type_lead = ['4647', '5916'];
/*
echo '<pre>';
print_r($type_lead);
echo '</pre>';
*/

$u_l1 = [];
$enumeration1 = field_id_value($res[$field1], $u_l1);
$u_l2 = [];
$enumeration2 = field_id_value($res[$field2], $u_l2);
?>

<?php
$next = 0;
$finish = false;
$res = [];
while (!$finish)
{
	$result = CRest::call('crm.'.$item.'.list',
		[
			'order' => [
				$field2 => "ASC",
				$field1 => "ASC"
		],
			'filter' => [
				'>=DATE_CREATE' => $calendar1.'+03:00',
				'<=DATE_CREATE' => $calendar2.'+03:00',
				'='.$field => $type_lead,
		],
			'select' => [
				'DATE_CREATE',
				$field1,
				$field2,
				'SOURCE_ID',
				'ASSIGNED_BY_ID',
		],
			'start' => $next,
	]);

	if(!$result['error']){
		$res = array_merge($res, $result['result']);
		if(isset($result['next']))
		$next = $result['next'];
		else
		$finish = true;
		//$finish = true;
	}else{
		$finish = true;
		echo '<pre style="color: red;">'.$result['error'].'</pre>';
	}
}
/*
echo '<pre>';
print_r($res);
echo '</pre>';
*/


$f1= array();
$f2= array();
$f = array();
$s = array();
$u = array();
foreach ($res as $key => $value){
	//Категории
	if(in_array($value[$field1], $f1)==false)
	$f1[] = $value[$field1];

	//Разделение по дате
	$dar = date_parse($value['DATE_CREATE']);
	$d = $dar['day'].'.'.$dar['month'].'.'.$dar['year'];
	if (!isset($f2[$d]))
	$f2[$d] = [];

	//Разделение по времени в датах
	if (isset($f2[$d][$value[$field2]]))
	$f2[$d][$value[$field2]]++;
	else
	$f2[$d][$value[$field2]]=1;

	//Разделение по всем параметрам
	if (isset($f[$value[$field1]][$d][$value[$field2]])){
		$f[$value[$field1]][$d][$value[$field2]]++;
	}else{
		$f[$value[$field1]][$d][$value[$field2]] = 1;
	}

	//Разделение по источникам
	if (isset($s[$value['SOURCE_ID']][$d]))
	$s[$value['SOURCE_ID']][$d]++;
	else
	$s[$value['SOURCE_ID']][$d]=1;

	//Разделение по менеджерам
	if (isset($u[$value['ASSIGNED_BY_ID']][$d]))
	$u[$value['ASSIGNED_BY_ID']][$d]++;
	else
	$u[$value['ASSIGNED_BY_ID']][$d]=1;
}

function cmp($a, $b){
    if ($a == $b) {
        return 0;
    }
    return (strtotime($a) < strtotime($b)) ? -1 : 1;
}

uksort($f2, "cmp");
foreach ($f as $key => $value){
	uksort($f[$key], "cmp");
};

/*
echo '<pre>';
print_r($f1);
print_r($f2);
print_r($f);
print_r($s);
print_r($u);
echo '</pre>';
*/

$result = CRest::call('crm.status.list',
[
	'order' => [
		'SORT' => 'ASC',
	],
	'filter' => [ 
		'ENTITY_ID' => 'SOURCE',
	],
]);
$source = $result['result'];
/*
echo '<pre>';
print_r($source);
$key = array_search('CALL', array_column($source, 'STATUS_ID'));
print_r($source[$key]['NAME']);
echo '</pre>';
*/

$next = 0;
$finish = false;
$res = [];
while (!$finish)
{
	$result = CRest::call('user.get',
	[
		'sort'	=> 'ID',
		'order' => 'ASC',
		'filter' => [
			'USER_TYPE' => 'employee',
		],
		'start' => $next,
	]);
	
	if(!$result['error']){
		$res = array_merge($res, $result['result']);
		if(isset($result['next']))
		$next = $result['next'];
		else
		$finish = true;
		//$finish = true;
	}else{
		$finish = true;
		echo '<pre style="color: red;">'.$result['error'].'</pre>';
	}
}
$users = $res;
/*
echo '<pre>';
print_r($users);
$key = array_search(1, array_column($users, 'ID'));
print_r($users[$key]['NAME'].' '.$users[$key]['LAST_NAME']);
echo '</pre>';
*/
?>

<div style="background-color: #288ee2; padding-left: 5%; margin-bottom: 3%; padding-top: 1%; padding-bottom: 1%; color:azure"><h3>Отчет по категории</h3></div>
<div class="report" style="margin-left: 30px; margin-bottom: 5%;">

<?php
$len = 0;
$sum = 0;
echo "<table class='table-bordered text-center table-lg'><caption>".$tablename."</caption>";
echo "<thead class='sticky-top bg-white'>";
	echo "<tr><th class='table-primary headcol' rowspan=2>".$catname."</th>";
		foreach ($f2 as $k => $v){
			echo "<th colspan=".(count($v)+1).">".$k."</th>";
		}
	echo "</tr>";

	echo "<tr>";
		foreach ($f2 as $k => $v){
			foreach ($v as $k1 => $v1){
				if($enumeration2 && isset($u_l2[$k1]))
				$int = $u_l2[$k1];
				else
				$int = $k1;
				echo "<th style='width: 6%;'>".$int."</th>";
			}
			echo "<th style='width: 6%;'>Всего</th>";//***
		}
	echo "</tr>";
echo "</thead>";

	foreach ($s as $k => $v){
		if(!empty($k)){
			$key = array_search($k, array_column($source, 'STATUS_ID'));
			$n = $source[$key]['NAME'];
		}else{
			$n = 'Источника нет';
		}
		echo "<tr><th class='table-info headcol'>".$n."</th>";
			foreach ($f2 as $k2 => $v2){
				$ks = array_keys($f2);
				$last_key = array_pop($ks);
				$tc = 'table-warning';
				echo "<th class='".$tc."' colspan=".(count($v2)+1).">";
					if(isset($v[$k2]))
					echo $v[$k2];
					else
					echo "0";
				echo "</th>";
			}
		echo "</tr>";
	}

	echo "<tr>";
	echo "</tr>";

		foreach ($f as $key => $row){
			$ks = array_keys($f2);
			$last_key0 = array_pop($ks);
			echo "<tr>";
			if($enumeration1 && isset($u_l1[$key]))
			$cat = $u_l1[$key];
			else
			$cat = $key;
			echo "<th class='table-primary headcol'>".$cat."</th>";
			foreach ($f2 as $k0 => $v0){
				$ks = array_keys($f2[$k0]);
				$last_key = array_pop($ks);
				$c = 0;
				foreach ($v0 as $k => $v){
					$tc = 'table-success';
					if (array_key_exists($k0, $row)){
						if (array_key_exists($k, $row[$k0])) {
							echo "<td class='".$tc."'>".$row[$k0][$k]."</td>";
							$c = $c + $row[$k0][$k];
						}else{
							echo "<td class='".$tc."'></td>";
						}
					}else{
						echo "<td class='".$tc."'></td>";
					}
				}
				$tc = 'table-active';
				echo "<td class='".$tc."'>".$c."</td>";//***
			}
			echo "</tr>";
		}


	echo "<tr>";
	echo "<td class='headcol'>Всего по интервалам</td>";
foreach ($f2 as $k1 => $v1){
	$c = 0;
	foreach ($v1 as $k => $v){
		$sum=$sum+$v;
		$len++;
		echo "<td>".$v."</td>";
		$c = $c + $v;
	}
	echo "<td class='table-active'>".$c."</td>";//***
	$len++;
}
	echo "</tr>";

/*
	echo "<tr>";
	echo "<td class='headcol'>Всего по дням</td>";
	foreach ($f2 as $k1 => $v1){
		$l = 0;
		$s = 0;
		foreach ($v1 as $k => $v){
			$s=$s+$v;
			$l++;
		}
		echo "<td colspan=".$l.">".$s."</td>";
		echo "<td class='table-active'></td>";
	}
	echo "</tr>";
*/

	echo "<tr><td class='headcol'>Общее кол-во</td><td colspan=".$len.">".$sum."</td></tr>";
	
	
	foreach ($u as $k => $v){
		if(!empty($k)){
			$key = array_search($k, array_column($users, 'ID'));
			if($key!==false)
			$n = $users[$key]['LAST_NAME'].' '.$users[$key]['NAME'];
			else
			$n = 'Менеджер не найден';
		}else{
			$n = 'Менеджера нет';
		}
		echo "<tr><th class='table-info headcol'>".$n."</th>";
			foreach ($f2 as $k2 => $v2){
				$ks = array_keys($f2);
				$last_key = array_pop($ks);
				$tc = 'table-warning';
				echo "<th class='".$tc."' colspan=".(count($v2)+1).">";
					if(isset($v[$k2]))
					echo $v[$k2];
					else
					echo "0";
				echo "</th>";
			}
		echo "</tr>";
	}
	
	
echo "</table>";
?>
<!--
<form target="_blank" action="" method="post" name="reportexcel">
	<input type="hidden" name="report" value="reportexcel">
    <input type='hidden' name="item" value="<?=$item?>">
    <input type="hidden" name="calendar1" value=<?=$calendar1?>>
    <input type="hidden" name="calendar2" value=<?=$calendar2?>>
	<input type='hidden' name="field1" value="<?=$field1?>">
	<input type='hidden' name="field2" value="<?=$field2?>">
	<div class="form-group">
		<input class="btn btn-primary" type="submit" value="Excel">
	</div>
</form>
-->


</div>
</section>

</body>
</html>