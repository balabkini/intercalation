<?php
error_reporting(0);

define('URL_BASE', 'https://127.bitrix24.ru/rest/98/hudidopespbly9c3/');
define('DEBUG_FILE_NAME', 'debug.txt');
define('CONTROL_PREFIX_UPDATE', 'c_p_u.txt');

writeToLog($_REQUEST, 'update prefix');

$cl = 0;

switch ($_REQUEST['event']) {
	case ('ONCRMLEADUPDATE'):
		$item = 'lead';
		$cl = 0;
		break;
	case ('ONCRMDEALUPDATE'):
		$item = 'deal';
		$cl = 0;
		break;
	case ('ONCRMCONTACTUPDATE'):
		$item = 'contact';
		$cl = 0;
		break;
}

if (c_p_u($cl)){
	writeToLog('true', 'update prefix');
	$id = intval($_REQUEST['data']['FIELDS']['ID']);
	
	$result = restCommand('crm.'.$item.'.get',
		[
			'id' => $id,
		]
	);
	writeToLog($result, 'crm.'.$item.'.get');
	$data = $result['result'];
	
	if($item == 'contact'){
		if($data['ID']){
			$es = ['lead', 'deal'];
			foreach ($es as $k => $e){
				$r = restCommand('crm.'.$e.'.list',
				[
					'filter' => ['=CONTACT_ID' => $data['ID']],
					'select' => ['ID'],
				]); 
				$elements = $r['result'];
				if(count($elements) > 0){
					c_p_u_add(count($elements));
					foreach ($elements as $key => $value){			
						$r = restCommand('crm.'.$e.'.update',
						[
							'id' => $value['ID'],
							'fields' =>
							[
								'UF_CRM_LIST_PREF' => $data['UF_CRM_LIST_PREF'],
							]
						]);
						writeToLog($r, 'crm.'.$e.'.update');
						if(!$r['result'])
						c_p_u_dec();
					};
				};
			};
		};
	};
	
	if(($item == 'deal')||($item == 'lead')){
		if($data['CONTACT_ID']){
			c_p_u_add(1);
			$r = restCommand('crm.contact.update',
			[
				'id' => $data['CONTACT_ID'],
				'fields' =>
				[
					'UF_CRM_LIST_PREF' => $data['UF_CRM_LIST_PREF'],
				]
			]);
			writeToLog($r, 'crm.contact.update');
			if(!$r['result'])
			c_p_u_dec();
		
			$es = ['lead' => 'deal', 'deal' => 'lead'];
			$e = $es[$item];
			$r = restCommand('crm.'.$e.'.list',
			[
				'filter' => ['=CONTACT_ID' => $data['CONTACT_ID']],
				'select' => ['ID'],
			]); 
			$elements = $r['result'];
			if(count($elements) > 0){
				c_p_u_add(count($elements));
				foreach ($elements as $key => $value){
					
					$r = restCommand('crm.'.$e.'.update',
					[
						'id' => $value['ID'],
						'fields' =>
						[
							'UF_CRM_LIST_PREF' => $data['UF_CRM_LIST_PREF'],
						]
					]);
					writeToLog($r, 'crm.'.$e.'.update');
					if(!$r['result'])
					c_p_u_dec();
				};
			};
			
		};
		
	}
	
}else{
	writeToLog('false', 'update prefix');
};

function writeToLog($data, $title = '')
{
	if (!DEBUG_FILE_NAME)
	return false;

	$log = "\n------------------------\n";
	$log .= date("Y.m.d G:i:s")."\n";
	$log .= (strlen($title) > 0 ? $title : 'DEBUG')."\n";
	$log .= print_r($data, 1);
	$log .= "\n------------------------\n";

	file_put_contents(__DIR__."/".DEBUG_FILE_NAME, $log, FILE_APPEND);

	return true;
}

function c_p_u_dec()
{
	if (!CONTROL_PREFIX_UPDATE)
	return false;

	$current = file_get_contents(__DIR__."/".CONTROL_PREFIX_UPDATE);

	if(isset($current)){
		$num = intval($current);
		$num--;
	}else{
		$num = 0;
	}

	file_put_contents(__DIR__."/".CONTROL_PREFIX_UPDATE, $num);

	return true;
}

function c_p_u_add($c)
{
	if (!CONTROL_PREFIX_UPDATE)
	return false;

	$current = file_get_contents(__DIR__."/".CONTROL_PREFIX_UPDATE);

	if(isset($current)){
		$num = intval($current);
		$num = $num + $c;
	}else{
		$num = $c;
	}

	file_put_contents(__DIR__."/".CONTROL_PREFIX_UPDATE, $num);

	return true;
}

function c_p_u($c)
{
	if (!CONTROL_PREFIX_UPDATE)
	return false;

	$current = file_get_contents(__DIR__."/".CONTROL_PREFIX_UPDATE);

	if(isset($current)){
		$num = intval($current);
		if($num == 0){
			$num = $c;
			$r = true;
		}
		else
		{
			$num--;
			$r = false;
		}
	}else{
		$num = $c;
		$r = true;
	}

	file_put_contents(__DIR__."/".CONTROL_PREFIX_UPDATE, $num);

	return $r;
}

function restCommand($method, $params = array()) {
	$queryUrl = URL_BASE . $method . '.json';
	$queryData = http_build_query($params);
	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_SSL_VERIFYPEER => 0,
		CURLOPT_POST => 1,
		CURLOPT_HEADER => 0,
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => $queryUrl,
		CURLOPT_POSTFIELDS => $queryData,
	));
		
	$result = curl_exec($curl);
	curl_close($curl);

	return json_decode($result, true);
}
?>