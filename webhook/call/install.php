<?require_once (__DIR__.'/crest.php');?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Звонилка SIP</title>
	<script src="//api.bitrix24.com/api/v1/"></script>
	<link rel="stylesheet" href="style.css">
</head>
<body>

<?php

$result = CRest::installApp();
if($result['rest_only'] === false):
	if($result['install'] == true):
		echo 'установка завершена';
	else:
		echo 'ошибка установки';
	endif;
endif;

?>

	<h1>Установка</h1>
    <div id="name"></div>
	<hr>
	<span class="button install" onclick="regApp()">Зарегистрировать событие на звонок</span>
	<span class="button uinstall" onclick="unregApp()">Удалить событие на звонок</span>
	<hr>
	<label>Путь к приложению<input style="width: 300px;" type="text" size="40" id="pathApp"></label>
	<span class="button install" onclick="placeApp()">Встроить в карточку</span>
	<span class="button uinstall" onclick="unplaceApp()">Удалить из карточки</span>
	<hr>
	<span class="button install" onclick="addPrefList()">Создать списки</span>
	<span class="button uinstall" onclick="deleteList()">Удалить списки</span>
	<hr>
	<span class="button end" onclick="closeApp()">Завершить установку</span>
	<hr>
    <script>
		//alert('welcom');
		
        BX24.init();
		
		function deleteUserField(item){
			var items = ['lead', 'deal', 'contact'];
			
			var params = {
				'ORDER': {'SORT': 'ASC'}, 
				'FILTER': {'FIELD_NAME': 'UF_CRM_LIST_PREF'},
			};
			
			BX24.callMethod('crm.'+items[item]+'.userfield.list', params, function(res) 
			{
				if(res.error())
					console.error(res.error());
				else{
					console.dir(res.data());
					var r = res.data();
					console.log(r[0].ID);

					var id = r[0].ID;
					BX24.callMethod('crm.'+items[item]+'.userfield.delete', {'id': id}, function(res)
					{
						var name = document.getElementById('name');
						if(result.error()){
							name.innerHTML = name.innerHTML + 'Error: ' + res.error() + '<br>';
							if(item<2)
							deleteUserField(item+1);
						}else{
							name.innerHTML = name.innerHTML + 'Поле префиксов удалено из '+items[item]+'<br>';
							if(item<2)
							deleteUserField(item+1);
						}
					});

				}
			});
		};
		
		function deleteList()
		{
			var params = {
				'IBLOCK_TYPE_ID': 'lists',
				'IBLOCK_CODE': 'list_for_call_sip'
			};
			BX24.callMethod('lists.delete',	params,	function(res)
			{
				var name = document.getElementById('name');
				if(res.error())
				name.innerHTML = name.innerHTML + 'Error: ' + res.error() + '<br>';
				else
				name.innerHTML = name.innerHTML + 'Список для звонков удален<br>';
			
			
				params = {
					'IBLOCK_TYPE_ID': 'lists',
					'IBLOCK_CODE': 'prefix_list'
				};
				BX24.callMethod('lists.delete',	params,	function(res)
				{
					var name = document.getElementById('name');
					if(res.error())
					name.innerHTML = name.innerHTML + 'Error: ' + res.error() + '<br>';
					else
					name.innerHTML = name.innerHTML + 'Список префиксов удален<br>';
					
					deleteUserField(0);
				});
				
			
			});
		};
		
		function addPrefList()
		{
			var params = {
				'IBLOCK_TYPE_ID' : 'lists',
				'IBLOCK_CODE' : 'prefix_list',
				'FIELDS' : {
					'NAME' : 'Список префиксов',
					'DESCRIPTION' : 'Список префиксов',
					'SORT' : '10',
					'BIZPROC' : 'Y'
				},
				'MESSAGES' : {
					'ELEMENT_NAME' : 'Префикс',
					'ELEMENTS_NAME' : 'Префиксы',
					'ELEMENT_ADD' : 'Добавить префикс',
					'ELEMENT_EDIT' : 'Редактировать префикс',
					'ELEMENT_DELETE' : 'Удалить префикс',
					'SECTION_NAME' : 'Раздел',
					'SECTIONS_NAME' : 'Разделы',
					'SECTION_ADD' : 'Добавить раздел',
					'SECTION_EDIT' : 'Редактировать раздел',
					'SECTION_DELETE' : 'Удалить раздел'
				},
				'RIGHTS' : {
					'SG4_A' : 'W',
					'SG4_E' : 'W',
					'SG4_K' : 'W',
					'AU' : 'D',
					'G2' : 'D'
				}
			};
			BX24.callMethod('lists.add', params, function(res){
					var name = document.getElementById('name');
					if(res.error()){
						name.innerHTML = name.innerHTML + 'Error: ' + res.error() + '<br>';
						addCallList();
					}
					else{
						name.innerHTML = name.innerHTML + 'Список префиксов добавлен<br>';
						addFieldPref();
					}
				}
			);
		};
		
		function addFieldPref()
		{	
			var params = {
				'IBLOCK_TYPE_ID' : 'lists',
				'IBLOCK_CODE' : 'prefix_list',
				'FIELDS' : {
					'NAME' : 'Префикс',
					'IS_REQUIRED' : 'Y',
					'MULTIPLE' : 'N',
					'TYPE' : 'S',
					'SORT' : '20',
					'CODE' : 'prefix',
					'SETTINGS' : {
						'SHOW_ADD_FORM' : 'Y',
						'SHOW_EDIT_FORM' : 'Y',
						'ADD_READ_ONLY_FIELD' : 'N',
						'EDIT_READ_ONLY_FIELD' : 'N',
						'SHOW_FIELD_PREVIEW' : 'N'
					}
				}
			};
			BX24.callMethod('lists.field.add', params, function(res){
					var name = document.getElementById('name');
					if(res.error())
					name.innerHTML = name.innerHTML + 'Error: ' + res.error() + '<br>';
					else{
						name.innerHTML = name.innerHTML + 'Поле [Префикс] добавлено '+res.data()+'<br>';
						addElemensPref(res.data(), 0);
					}
				}
			);	
		};
		
		function addElemensPref(prop, n)
		{		
			var elements = [['01 Москва', '01'], ['02 Воронеж', '02'], ['03 Краснодар', '03'], ['04 Уфа', '04'], ['05 Омск', '05'], ['06 Новосибирск', '06']];
						
			var params = {
				'IBLOCK_TYPE_ID': 'lists',
				'IBLOCK_CODE': 'prefix_list',
				'ELEMENT_CODE': 'element_'+(n+1),
				'FIELDS': {
					'NAME': elements[n][0],
				}
			};
			params.FIELDS[prop]=elements[n][1];
				
			BX24.callMethod('lists.element.add', params, function(res){
				var name = document.getElementById('name');
				if(res.error())
				name.innerHTML = name.innerHTML + 'Error: ' + res.error() + '<br>';
				else{
					name.innerHTML = name.innerHTML + 'Элемент ['+elements[n][0]+'] добавлен<br>';
					if(n<5)
					addElemensPref(prop, n+1);
					else{
						addCallList();
					}
				}
			});	
			
		};		
		
		function addCallList()
		{
			var params = {
				'IBLOCK_TYPE_ID' : 'lists',
				'IBLOCK_CODE' : 'list_for_call_sip',
				'FIELDS' : {
					'NAME' : 'Список для звонков',
					'DESCRIPTION' : 'Список для звонков',
					'SORT' : '10',
					'BIZPROC' : 'Y'
				},
				'MESSAGES' : {
					'ELEMENT_NAME' : 'Сервер',
					'ELEMENTS_NAME' : 'Сервера',
					'ELEMENT_ADD' : 'Добавить сервер',
					'ELEMENT_EDIT' : 'Редактировать сервер',
					'ELEMENT_DELETE' : 'Удалить сервер',
					'SECTION_NAME' : 'Раздел',
					'SECTIONS_NAME' : 'Разделы',
					'SECTION_ADD' : 'Добавить раздел',
					'SECTION_EDIT' : 'Редактировать раздел',
					'SECTION_DELETE' : 'Удалить раздел'
				},
				'RIGHTS' : {
					'SG4_A' : 'W',
					'SG4_E' : 'W',
					'SG4_K' : 'W',
					'AU' : 'D',
					'G2' : 'D'
				}
			};
			BX24.callMethod('lists.add', params, function(res){
					var name = document.getElementById('name');
					if(res.error()){
						name.innerHTML = name.innerHTML + 'Error: ' + res.error() + '<br>';
						addIblock();
					}
					else{
						name.innerHTML = name.innerHTML + 'Список для звонков добавлен<br>';
						addFieldServ(0);
					}
				}
			);
		};
		
		function addFieldServ(n, iblock_id = false)
		{
			var fields = [['Логин', 'login', 'S', '20'],
						['Пароль', 'pass', 'S', '30'],
						['IP адрес', 'ip', 'S', '40'],
						['Номер линии', 'outgoing_uri', 'N', '50'],
						['Пользователь', 'user', 'S:employee', '60'],
						['Префикс по умолчанию', 'def_pref', 'E:EList', '70']];
			var field = fields[n];
			var params = {
				'IBLOCK_TYPE_ID' : 'lists',
				'IBLOCK_CODE' : 'list_for_call_sip',
				'FIELDS' : {
					'NAME' : field[0],
					'IS_REQUIRED' : 'Y',
					'MULTIPLE' : 'N',
					'TYPE' : field[2],
					'SORT' : field[3],
					'CODE' : field[1],
					'LINK_IBLOCK_ID' : iblock_id,
					'SETTINGS' : {
						'SHOW_ADD_FORM' : 'Y',
						'SHOW_EDIT_FORM' : 'Y',
						'ADD_READ_ONLY_FIELD' : 'N',
						'EDIT_READ_ONLY_FIELD' : 'N',
						'SHOW_FIELD_PREVIEW' : 'N'
					}
				}
			};
			BX24.callMethod('lists.field.add', params, function(res){
				var name = document.getElementById('name');
				if(res.error())
				name.innerHTML = name.innerHTML + 'Error: ' + res.error() + '<br>';
				else
				name.innerHTML = name.innerHTML + 'Поле ['+field[0]+'] добавлено '+res.data()+'<br>';
				if(n<4)
				addFieldServ(n+1);
				else
				if(!iblock_id)
				addIblock();
			});	
		};
		
		function addIblock()
		{
			var params = {
				'IBLOCK_TYPE_ID': 'lists',
				'IBLOCK_CODE': 'prefix_list'
			};
			BX24.callMethod('lists.get', params, function(res)
			{
				var name = document.getElementById('name');
				if(res.error())
				name.innerHTML = name.innerHTML + 'Error: ' + res.error() + '<br>';
				else{
					name.innerHTML = name.innerHTML + 'Добавление IdBlock '+res.data()+'<br>';
					addUserField(res.answer.result[0].ID, 0);
				}
			});
		};
		
		function addUserField(iblock_id, item)
		{
			var items = ['lead', 'deal', 'contact'];
			var params = {
				'FIELDS' :
				{
					'FIELD_NAME' : 'LIST_PREF',
					'EDIT_FORM_LABEL' : 'Префиксы',
					'LIST_COLUMN_LABEL' : 'Префиксы',
					'USER_TYPE_ID' : 'iblock_element',
					'IBLOCK_TYPE' : 'lists',
					'XML_ID' : 'LIST_PREF',
					'SETTINGS' : {
						'LIST_HEIGHT' : 1,
						'ACTIVE_FILTER' : "N",
						'DEFAULT_VALUE' : "",
						'DISPLAY' : "LIST",
						'IBLOCK_ID' : iblock_id,
					}
				}
			};
			BX24.callMethod('crm.'+items[item]+'.userfield.add', params, function(res){
					var name = document.getElementById('name');
					if(res.error()){
						name.innerHTML = name.innerHTML + 'Error: ' + res.error() + '<br>';
						if(item<2)
						addUserField(iblock_id, item+1);
						else
						addFieldServ(5, iblock_id);
					}
					else{
						name.innerHTML = name.innerHTML + 'В '+items[item]+' добавлено поле [Префиксы] '+res.data()+'<br>';
						if(item<2)
						addUserField(iblock_id, item+1);
						else
						addFieldServ(5, iblock_id);
					}
				}
			);	
		};
		
		function closeApp()
		{
			BX24.installFinish(function(res){
				var name = document.getElementById('name');
				if(res.answer.result='install')
				name.innerHTML = name.innerHTML + 'Установлено<br>';
				else
				name.innerHTML = name.innerHTML + 'Что-то пошло не так<br>';
				console.log(res);
			});
		};
		
		function unplaceApp()
		{
			var name = document.getElementById('pathApp');
			handler = name.value;
			BX24.callMethod('placement.unbind', {'placement': 'CALL_CARD'}, function(res){
				var name = document.getElementById('name');
				if(res.answer.result.count > 0)
				name.innerHTML = name.innerHTML + 'Карточка удалена ('+res.answer.result.count+')<br>';
				else
				name.innerHTML = name.innerHTML + 'Не найдено зарегестрированных событий для карточки<br>';
                console.log(res);
            });
		};
		
		function placeApp()
		{
			var name = document.getElementById('pathApp');
			handler = name.value;
			BX24.callMethod('placement.bind', {'placement': 'CALL_CARD', 'handler': handler, 'title': 'Звонок SIP', 'description': 'Звонок SIP'}, function(res){
				var name = document.getElementById('name');
				if(res.answer.result)
				name.innerHTML = name.innerHTML + 'Карточка вставлена<br>';
				else
				name.innerHTML = name.innerHTML + 'Что-то пошло не так<br>';
                console.log(res);
            });
		};
		
		function regApp()
		{
			//BX24.callMethod('event.bind', {'event':'OnVoximplantCallInit', 'event_type':'offline'/*, 'auth_connector':'OneC'*/}, function(res){
			BX24.callMethod('event.bind', {'event':'ONEXTERNALCALLSTART', 'event_type':'offline'/*, 'auth_connector':'OneC'*/}, function(res){
				var name = document.getElementById('name');
				if(res.answer.result)
				name.innerHTML = name.innerHTML + 'Регистрация события на звонок сделана<br>';
				else
				name.innerHTML = name.innerHTML + 'Регистрация события на звонок возможно была сделана ранее<br>';
                console.log(res);
            });
		};
		
		function unregApp()
		{
			BX24.callMethod('event.unbind', {'event':'ONEXTERNALCALLSTART', 'event_type':'offline'/*, 'auth_connector':'OneC'*/}, function(res){
				var name = document.getElementById('name');
				if(res.answer.result.count > 0)
				name.innerHTML = name.innerHTML + 'Регистрация события на звонок снята('+res.answer.result.count+')<br>';
				else
				name.innerHTML = name.innerHTML + 'Не найдено зарегестрированных событий<br>';
                console.log(res);
            });
		};
    </script>	
</body>
</html>