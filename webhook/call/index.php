<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Звонилка SIP</title><!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Звонилка SIP</title>
	<script src="//api.bitrix24.com/api/v1/"></script>
	<link rel="stylesheet" href="style.css">
</head>
<body onload="startFunction()">

<div id="message"></div>

<div class="div_form">
	<div>
		<div class="s">
			<div>
				<input type='text' value='' id='name'>
				<label class="t" for="name">Имя</label>
			</div>
		</div>
		<div class="s">
			<div id='phones'>
				<label class="t">Телефона</label>
			</div>
			<div>
				<span class="button_add" title='Добавить номер' onclick="add_input('phones', 'phone')">Добавить Телефон</span>
			</div>
		</div>
		<!--
		<div class="s">
			<div id='emails'>
				<label class="t">E-mail</label>
			</div>
			<div>
				<span class="button_add" id="i" title='Добавить email' onclick="add_input('emails', 'email')">Добавить E-mail</span>
			</div>
		</div>
		<div class="s">
			<div>
				<textarea id='comment' rows='4'></textarea>
				<label class="t" for="comment">Комментарий</label>
			</div>
		</div>
		-->
	</div>
</div>
	<div class="div_submit">
		<button type="button" class="btn" onclick="contact_update(entity_type, entity_id)">Сохранить</button>
	</div>

    <div id="log" style="display: none;"></div>
	
    <script>
		var entity_type = '';
		var entity_id = '';
		
        BX24.init(function(){
			/*
			BX24.callMethod(
				'event.offline.get', {'order':{'ID':'DESC'}, 'filter':{'EVENT_NAME':'ONEXTERNALCALLSTART'}, 'limit':1, 'clear': 1}, function(res){
                var log = document.getElementById('log');
				log.innerHTML = res.result;
                console.log(res);
            });
			*/
        });
		
		var field = '';
		var element = '';
		var timerId = 0;
		
		function SIPcall(url, userPass){
			var log = document.getElementById('log');
			//log.innerHTML = log.innerHTML + url + '<br>';
			/*
			var xmlHttp = new XMLHttpRequest();
			xmlHttp.onreadystatechange = function() {
				if (xmlHttp.readyState === 4){
					if (xmlHttp.status === 200){
						log.innerHTML = log.innerHTML + 'Call OK (200)' + '<br>';
					}else if (xmlHttp.status === 403) {
                        log.innerHTML = log.innerHTML + 'Неверный логин/пароль в настройках или ваш ip не внесен в белый список на телефоне (403)' + '<br>';
                    }else{
						log.innerHTML = log.innerHTML + 'Call Error (' + xmlHttp.status + ')<br>';
					}
				}
			};
			xmlHttp.open('GET', url, true);
			xmlHttp.setRequestHeader("Authorization", "Basic " + btoa(userPass));
			xmlHttp.timeout = 5000;
			xmlHttp.send();
			*/
			
			var callWindow = window.open(url, "_blank", "toolbar=no,scrollbars=no,resizable=no,top=50,left=50,width=200,height=300");
			setTimeout(function(){
				callWindow.close();
			}, 1000);
		}
		
		function build_url_call(phone_num, entity_id, entity_type, user_id){
			
			BX24.callMethod('lists.field.get', {'IBLOCK_TYPE_ID':'lists', 'IBLOCK_CODE':'list_for_call_sip'}, function(res){
				var prop_user = '';
				filter = {};
				r = res.answer.result;
				Object.keys(r).forEach(function(value, key){
					if (r[value].CODE === 'user'){
						prop_user = value;
						return;
					}
				});
				
				filter[prop_user] = user_id;
			
				BX24.callBatch({
						'get_field':['lists.field.get', {'IBLOCK_TYPE_ID':'lists', 'IBLOCK_CODE':'list_for_call_sip'}],
						'get_element':['lists.element.get', {'IBLOCK_TYPE_ID':'lists', 'IBLOCK_CODE':'list_for_call_sip', 'filter':filter}],
						'get_item':['crm.'+entity_type+'.get', {'ID':entity_id}],
						'get_pref_f':['lists.field.get', {'IBLOCK_TYPE_ID' : 'lists', 'IBLOCK_CODE' : 'prefix_list'}],
						'get_pref':['lists.element.get', {'IBLOCK_TYPE_ID' : 'lists', 'IBLOCK_CODE' : 'prefix_list','filter':{'ID' : '$result[get_item][UF_CRM_LIST_PREF]'}}],
					}, 
					function(res){
					var lfield = res.get_field.answer.result;
					var lelement = res.get_element.answer.result;
					var item = res.get_item.data();
					var pref_f = res.get_pref_f.answer.result;
					var pref = res.get_pref.answer.result;

					var data = {};
					var sdata = {};
					var u_id = 0;
					Object.keys(lelement).forEach(function(value, key){
						var el = lelement[value];
						Object.keys(lfield).forEach( function(fval, fkey){
							var fi = lfield[fval];
							if(typeof(el[fval]) != "undefined" && el[fval] !== null){
								var arr=el[fval];
								if (fi['TYPE']=='NAME'){
									data[fi['NAME']]=arr;
								}else if ((fi['TYPE']=='S')||(fi['TYPE']=='N')){
									Object.keys(arr).forEach( function(v, k){
										data[fi['NAME']]=arr[v];
									});
								}else if (fi['TYPE']=='L'){
									Object.keys(arr).forEach( function(v, k){
										data[fi['NAME']]=fi['DISPLAY_VALUES_FORM'][arr[v]];
									});
								}else if (fi['TYPE']=='S:employee'){
									Object.keys(arr).forEach( function(v, k){
										data[fi['NAME']]=arr[v];
										u_id = arr[v];
									});
								}else if (fi['TYPE']=='E:EList'){
									Object.keys(arr).forEach( function(v, k){
										data[fi['NAME']]=arr[v];
									});
								};
							};
						});
					});
					
					if(u_id === user_id){
						sdata = data;

						sdata['phone'] = phone_num;
						
						delete pref_f.NAME;
						var fs = Object.keys(pref_f);
						var prefix_id = item['UF_CRM_LIST_PREF'];
						
						if(prefix_id){
							try{
								var idp = Object.keys(pref[0][fs[0]]);
								var prefix = pref[0][fs[0]][idp];
								sdata['prefix'] = prefix;
							}catch(e){
								console.log(e);
								sdata['prefix'] = '';
							}
						}else{
							Object.keys(pref).forEach(function(value, key){
								if(pref[value].ID === sdata['Префикс по умолчанию']){
									try{
										var idp = Object.keys(pref[value][fs[0]]);
										var prefix = pref[value][fs[0]][idp];
										sdata['prefix'] = prefix;
									}catch(e){
										console.log(e);
										sdata['prefix'] = '';
									}
								}
							});
						}
						
						var url = 'http://'+sdata['Логин']+':'+sdata['Пароль']+'@'+sdata['IP адрес']+'/servlet?number='+sdata['prefix']+sdata['phone']+'&outgoing_uri='+sdata['Номер линии'];
						//var url = 'http://'+sdata['IP адрес']+'/servlet?number='+sdata['prefix']+sdata['phone']+'&outgoing_uri='+sdata['Номер линии'];
						SIPcall(url, sdata['Логин']+':'+sdata['Пароль']);
					}else{
						var message = document.getElementById('message');
						message.innerHTML = '<div>У вас нет данных для совершения звонков, обратитесь к администратору</div>';
					}
					
					
					var phones = item.PHONE;
					var emails = item.EMAIL;
					var name = item.NAME;
					var comment = item.COMMENTS;
					document.getElementById('name').value = name;
					//document.getElementById('comment').value = comment;
					Object.keys(phones).forEach(function(val, key){
						add_input('phones', 'phone', phones[val].VALUE, phones[val].ID);
					});
					/*
					Object.keys(emails).forEach(function(val, key){
						add_input('emails', 'email', emails[val].VALUE, phones[val].ID);
					});
					*/
					
				});
				
			});			
				
		};
		
		//43rgl973f7trbnas27ocdc4w23t4c22v
		function load_event(){
			var url = 'get_event_offline.php';
			var xmlHttp = new XMLHttpRequest();
			xmlHttp.onreadystatechange = function() {
				if (xmlHttp.readyState === 4){
					if (xmlHttp.status === 200){
						var log = document.getElementById('log');
						log.innerHTML = log.innerHTML + xmlHttp.response + '<br>';
						var res = JSON.parse(xmlHttp.response);
						console.log(res);
						if(res.result.events.length>0){
							data=res.result.events[0].EVENT_DATA;
							log.innerHTML = log.innerHTML + data.PHONE_NUMBER + '<br>';
							//log.innerHTML = log.innerHTML + data.USER_ID + '<br>';
							log.innerHTML = log.innerHTML + data.CRM_ENTITY_TYPE + '<br>';
							//log.innerHTML = log.innerHTML + data.CRM_ENTITY_ID + '<br>';
							clearTimeout(timerId);
							entity_type = data.CRM_ENTITY_TYPE;
							entity_id = data.CRM_ENTITY_ID;
							build_url_call(data.PHONE_NUMBER, data.CRM_ENTITY_ID, data.CRM_ENTITY_TYPE, data.USER_ID);
						}else{
							log.innerHTML = log.innerHTML + 'Нет данных<br>';
							clearTimeout(timerId);
						}
						
					}else if (xmlHttp.status === 403) {
                        log.innerHTML = log.innerHTML + 'Ошибка (403)' + '<br>';
                    }else{
						log.innerHTML = log.innerHTML + 'Ошибка (' + xmlHttp.status + ')<br>';
					}
				}
			};
			xmlHttp.open('GET', url, true);
			xmlHttp.timeout = 5000;
			xmlHttp.send();
		};
		
		function startFunction(){
			var log = document.getElementById('log');
			log.innerHTML = 'Звонок...<br>';
			
			timerId = setInterval('load_event()',3000);
			load_event();
        };  

//Редактирование контакта

		function add_input(p, t, v='', eid='') {
			var pp = document.getElementById(p) ;
			var new_input = document.createElement('input') ;
			new_input.type = t ;
			new_input.name = t+'[]' ;
			new_input.value = v ;
			new_input.setAttribute('data-e_id', eid);
			var div = document.createElement('div') ;
			div.appendChild(new_input) ;
			pp.appendChild(div) ;
		} ;

		function getContacts(el_id){
			var es = document.getElementById(el_id) ;
			var el =  es.querySelectorAll('input');
			var fe = [];
			for (var i=0, length = el.length; i < length; i++) {
				fe.push({'ID':el[i].dataset.e_id, 'VALUE':el[i].value});
			};
			return fe;
		};
		
		function getInf(el_id){
			var e = document.getElementById(el_id) ;
			return e.value;
		};

		function contact_update(type, id){
			var fp = getContacts('phones');
			//var fe = getContacts('emails');
			var n = getInf('name');
			//var c = getInf('comment');
			var fields = {'PHONE': fp, /*'EMAIL': fe,*/ 'NAME': n, /*'COMMENTS': c*/};
			console.info(fields);
			BX24.callMethod('crm.'+type+'.update', {'id': id, 'fields': fields},	function(res){
				if(result.error())
				console.error(res.error());
				else
				console.info(res.data());
			});
		};
	</script>

</body>
</html>