<?require_once (__DIR__.'/crest.php');?>
<?
$result = CRest::call('event.offline.get',
	[
		'order' => ['ID' => 'DESC'], 
		'filter' => ['EVENT_NAME' => 'ONEXTERNALCALLSTART'], 
		'limit' => 1, 
		'clear' => 0
	]
);
$data_string = json_encode($result);
//header('Content-Type: application/json; charset=utf-8');
//header('Content-Length: ' . strlen($data_string));
echo $data_string;
?>
<?
	//file_put_contents('log.txt', print_r($_REQUEST, true), FILE_APPEND);
?>