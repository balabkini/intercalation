<?
//Инициализируем библиотеку
require_once (__DIR__.'/crest.php');

//Данные при работе с активити
//$ID_deal = $_REQUEST["properties"]["Id_deal"]; 
//$ID_contact = $_REQUEST["properties"]["Id_contact"]; 


//Данные при запуске вэб хука
$ID_deal = $_GET['ID_DEAL']; 
$ID_contact = $_GET['ID_CONTACT']; 
$nameIFNS = $_GET['Name_IFNS'];
//Списки
$ID_svedeniyaOShet = 49;
$ID_svedeniyaOCredit = 41;
$ID_dvisghIm = 47;
$ID_nedvisghIm = 43;
$ID_creditor = 37;
$ID_obyazPlat = 65;

//Шаблоны документов
$ID_doc_svedeniyaOCredit = 27;
$ID_doc_Opis = 29;
$ID_doc_Zayavlenie = 31;

//функция удаления дублей в массиве по ключу
function removeDouble($arr, $key) {
    $len = count($arr);
    for ($i = 0; $i < $len; $i++){
        for ($j=$i+1; $j <= $len; $j++){
            if ($arr[$i][$key]==$arr[$j][$key]) {
               unset($arr[$j]);
            }
        }
    }
    return $arr;
}

/*
$arCreditor = CRest::call('lists.element.get', array(
        "IBLOCK_TYPE_ID" => "bitrix_processes",
        "IBLOCK_ID" => $ID_creditor,
));
*/

//Начало Документ Опись имущества

//Список счетов
$arSpisok = CRest::call('lists.element.get', array(
    "IBLOCK_TYPE_ID" => "bitrix_processes",
    "IBLOCK_ID" => $ID_svedeniyaOShet,
        "FILTER" => array(
            "NAME" => $ID_deal
            ),
));

echo '<pre>';
//print_r($arSpisok);
echo '</pre>';

$i=0;

foreach ($arSpisok["result"] as $value){

    foreach ($value["PROPERTY_177"] as $value1){
        $creditorID = $value1;}

    $arCreditor = CRest::call('lists.element.get', array(
        "IBLOCK_TYPE_ID" => "bitrix_processes",
        "IBLOCK_ID" => $ID_creditor,
        "FILTER" => array(
            "ID" => $creditorID
            ),
        ));    
    
    $arCreditorName = $arCreditor["result"]["0"]["NAME"];        
    foreach ($value["PROPERTY_203"] as $value1) $vidOffSchet = $value1;
    foreach ($value["PROPERTY_201"] as $value1) $vidOffMany = $value1;    
    foreach ($value["PROPERTY_183"] as $value1) $date = $value1;
    foreach ($value["PROPERTY_185"] as $value1) $ostatok = $value1;
    
        $i++;

        $arrFilterSpisokSchet[] = [
            "d0" => "3.{$i}", 
            "d1" => $arCreditorName,
            "d2" => $vidOffSchet,
            "d3" => $date,
            "d4" => $ostatok,
            
            ];
    }

//Конец списка счетов

if (!$arrFilterSpisokSchet){
    $arrFilterSpisokSchet = [];
    $t1 = "не";
    $arrFilterSpisokSchet[0] = [
        "d0" => "-", 
        "d1" => "-",
        "d2" => "-",
        "d3" => "-",
        "d4" => "-",
        
        ];
}

//Движимое имущество
$arSpisok = CRest::call('lists.element.get', array(
    "IBLOCK_TYPE_ID" => "bitrix_processes",
    "IBLOCK_ID" => $ID_dvisghIm,
    "FILTER" => array(
            "NAME" => $ID_deal
            ),
));

//Подготовка массива данных по сделке
$i=0;

foreach ($arSpisok["result"] as $value){
    
    foreach ($value["PROPERTY_161"] as $value1)$nameNoDvIm = $value1;
    foreach ($value["PROPERTY_195"] as $value1)$vidSobNoDvIm = $value1;
    foreach ($value["PROPERTY_167"] as $value1)$adressNoDvIm = $value1;
    foreach ($value["PROPERTY_169"] as $value1)$priceNoDvIm = $value1;
    foreach ($value["PROPERTY_171"] as $value1)$zalogNoDvIm = $value1;
    foreach ($value["PROPERTY_283"] as $value1)$IdNumNoDvIm = $value1;

        $i++;

        $arrFilterSpisokDvIm[] = [
            "b0" => "2.{$i}", 
            "b1" => $nameNoDvIm,
            "b2" => $IdNumNoDvIm,
            "b3" => $vidSobNoDvIm,
            "b4" => $adressNoDvIm,
            "b5" => $priceNoDvIm,
            "b6" => $zalogNoDvIm,
            ];
    }


if (!$arrFilterSpisokDvIm){
    $arrFilterSpisokDvIm = [];
    $t2 = "не";
    $arrFilterSpisokDvIm[0] = [
        "b0" => "", 
        "b1" => "",
        "b2" => "",
        "b3" => "",
        "b4" => "",
        "b5" => "",
        "b6" => "",
        ];
    }

//конец движимое имущество



//Недвижимое имущество
$arSpisok = CRest::call('lists.element.get', array(
    "IBLOCK_TYPE_ID" => "bitrix_processes",
    "IBLOCK_ID" => $ID_nedvisghIm,
    "FILTER" => array(
            "NAME" => $ID_deal
            ),
));

//Подготовка массива данных по сделке

$i=0;

foreach ($arSpisok["result"] as $value){
    
    foreach ($value["PROPERTY_197"] as $value1) $nameIm = $value1;
    foreach ($value["PROPERTY_199"] as $value1)$vidIm = $value1;
    foreach ($value["PROPERTY_147"] as $value1)$adressIm = $value1;
    foreach ($value["PROPERTY_149"] as $value1)$squreIm = $value1;
    foreach ($value["PROPERTY_153"] as $value1)$priceIm = $value1;
    foreach ($value["PROPERTY_151"] as $value1)$osnIm = $value1;    
    foreach ($value["PROPERTY_155"] as $value1)$zalogIm = $value1;   

    $i++;

    $arrFilterSpisokNoDvIm[] = [
        "f0" => "1.{$i}", 
        "f1" => $nameIm,
        "f2" => $vidIm,
        "f3" => $adressIm,
        "f4" => $squreIm,
        "f5" => "{$osnIm} {$priceIm}",
        "f6" => $zalogIm,
        ];
}

if (!$arrFilterSpisokNoDvIm){
    $arrFilterSpisokNoDvIm = [];
    $t4 = "не";
    $arrFilterSpisokNoDvIm[0] = [
        "f0" => "", 
        "f1" => "",
        "f2" => "",
        "f3" => "",
        "f4" => "",
        "f5" => "",
        "f6" => ""
        ];
}

//конец недвижимое имущество

//конец подготовки данных

//Создание документа
/*
$documentgeneratorOpis = CRest::call('crm.documentgenerator.document.add', array(
    "templateId" => $ID_doc_Opis,
    "entityTypeId" => 2, 
    "entityId" => $ID_deal, 
    "values" => [
        
        'TableNoDvIm' => $arrFilterSpisokNoDvIm,
        'f0' => 'TableNoDvIm.Item.f0',
        'f1' => 'TableNoDvIm.Item.f1',
        'f2' => 'TableNoDvIm.Item.f2',
        'f3' => 'TableNoDvIm.Item.f3',
        'f4' => 'TableNoDvIm.Item.f4',
        'f5' => 'TableNoDvIm.Item.f5',
        'f6' => 'TableNoDvIm.Item.f6',
        'TableDvIm' => $arrFilterSpisokDvIm,
        'b0' => 'TableDvIm.Item.b0',
        'b1' => 'TableDvIm.Item.b1',
        'b2' => 'TableDvIm.Item.b2',
        'b3' => 'TableDvIm.Item.b3',
        'b4' => 'TableDvIm.Item.b4',
        'b5' => 'TableDvIm.Item.b5',
        'b6' => 'TableDvIm.Item.b6',
        'TableSchet' => $arrFilterSpisokSchet,
        'd0' => 'TableSchet.Item.d0',
        'd1' => 'TableSchet.Item.d1',
        'd2' => 'TableSchet.Item.d2',
        'd3' => 'TableSchet.Item.d3',
        'd4' => 'TableSchet.Item.d4',
        //'d5' => 'TableSchet.Item.d5',
        //'d6' => 'TableSchet.Item.d6',
     ],
    'fields' => [
        'TableNoDvIm' => [
            'PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\ArrayDataProvider',
            'OPTIONS' => [
                'ITEM_NAME' => 'Item',
                'ITEM_PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\HashDataProvider',
            ],
        ],
        'TableDvIm' => [
            'PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\ArrayDataProvider',
            'OPTIONS' => [
                'ITEM_NAME' => 'Item',
                'ITEM_PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\HashDataProvider',
            ],
        ],
        'TableSchet' => [
            'PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\ArrayDataProvider',
            'OPTIONS' => [
                'ITEM_NAME' => 'Item',
                'ITEM_PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\HashDataProvider',
            ],
        ],
    ]


));
*/

$idDocOpis = $documentgeneratorOpis["result"]["document"]["id"];

$idFileonDiskOpis = $documentgeneratorOpis["result"]["document"]["emailDiskFile"];

$downloadFileOpis = CRest::call('disk.file.get', array("id"=> $idFileonDiskOpis));

$publCurlOpis = CRest::call('crm.documentgenerator.document.enablepublicurl', array(
	"id" => $idDocOpis, 
	"status" => 1
));

//Конец Документ опись имущества

//Начало Документ Список кредиторов
$arSpisok = CRest::call('lists.element.get', array(
    "IBLOCK_TYPE_ID" => "bitrix_processes",
    "IBLOCK_ID" => $ID_svedeniyaOCredit,
        "FILTER" => array(
            "NAME" => $ID_deal
            ),
));
echo '<pre>';
//print_r($arSpisok);
echo '</pre>';

//Подготовка массива данных по сделке
$i=0;
$summDolga=0;

foreach ($arSpisok["result"] as $value) {
    
    foreach ($value["PROPERTY_133"] as $value1) $creditorID = $value1;
    foreach ($value["PROPERTY_135"] as $value1) $creditorOsn = $value1;
    //foreach ($value["PROPERTY_249"] as $value1) $creditorDate = $value1; 
    foreach ($value["PROPERTY_277"] as $value1) $creditorSodOb = $value1; 
       
    $arCreditor = CRest::call('lists.element.get', array(
        "IBLOCK_TYPE_ID" => "bitrix_processes",
        "IBLOCK_ID" => $ID_creditor,
        "FILTER" => array(
            "ID" => $creditorID
            ),
        ));

    $arCreditorName = $arCreditor["result"]["0"]["NAME"]; 
    foreach ($arCreditor["result"]["0"]["PROPERTY_129"] as $value1) $arCreditorAdress = $value1;
    foreach ($arCreditor["result"]["0"]["PROPERTY_193"] as $value1) $arCreditorAdress = "$value1"." "."$arCreditorAdress";
        
    if (!empty($value["PROPERTY_299"])){
        foreach ($value["PROPERTY_299"] as $value1) $cessiaID = $value1;
        $creditorSumm = 0;

        $arCessia = CRest::call('lists.element.get', array(
            "IBLOCK_TYPE_ID" => "bitrix_processes",
            "IBLOCK_ID" => $ID_creditor,
            "FILTER" => array(
                "ID" => $cessiaID
                ),
            )); 

            $arCessiaName = $arCessia['result']['0']['NAME'];

        $textCessia = "Кредитные обязательства перед "."$arCreditorName "." по договору цессии переданы в "."$arCessiaName".".";              
    }else{
        foreach ($value["PROPERTY_137"] as $value1) $creditorSumm = "{$value1}";
        $textCessia = "";
    }; 
    
    $summDolga=$summDolga+$creditorSumm;

    $i++;

    $arrFilterSpisokCreditor[] = [
        "a0" => "1.{$i}",  
        "a1" => $creditorSodOb,
        "a2" => $arCreditorName,
        "a3" => $arCreditorAdress,
        "a4" => $creditorOsn,
        "a5" => $creditorSumm,
        "a6" => $textCessia,
        "a7" => $creditorDate
        ];
}

echo '<pre>';
//print_r($arrFilterSpisokCreditor);
echo '</pre>';

if(!$arrFilterSpisokCreditor){
    $arrFilterSpisokCreditor = [];
    $arrFilterSpisokCreditor[0] = [
        "a0" => "1.1",  
        "a1" => "-",
        "a2" => "-",
        "a3" => "-",
        "a4" => "-",
        "a5" => "-",
        "a6" => "",
        "a7" => ""
    ];
}

// Обязательные платежи
$i=0;
$arSpisok = CRest::call('lists.element.get', array(
    "IBLOCK_TYPE_ID" => "bitrix_processes",
    "IBLOCK_ID" => $ID_obyazPlat,
        "FILTER" => array(
            "NAME" => $ID_deal
            ),
));


foreach ($arSpisok["result"] as $value) {
    
    foreach ($value["PROPERTY_287"] as $value1) $obyazPlatName = $value1;
    foreach ($value["PROPERTY_289"] as $value1) $obyazPlatNedoimka = $value1;
    foreach ($value["PROPERTY_291"] as $value1) $obyazPlatShtraf = $value1;
        
        $i++;

    $arrFilterSpisokObyazPlat[] = [
        "g0" => $obyazPlatName,  
        "g1" => $obyazPlatNedoimka,
        "g2" => $obyazPlatShtraf,
        "g3" => "2.{$i}"
        ];
}

echo '<pre>';
//print_r($arrFilterSpisokObyazPlat);
echo '</pre>';

/* if(!$arrFilterSpisokObyazPlat){
    $arrFilterSpisokObyazPlat = [];
    $arrFilterSpisokObyazPlat[0] = [
        "g0" => "-",  
        "g1" => "-",
        "g2" => "-",
        "g3" => "-"
    ];
} */

//Создание документа
/*
$documentgeneratorCredit = CRest::call('crm.documentgenerator.document.add', array(
    "templateId" => $ID_doc_svedeniyaOCredit,
    "entityTypeId" => 2, 
    "entityId" => $ID_deal, 
    "values" => [
        'TableCredit' => $arrFilterSpisokCreditor,
        'a0' => 'TableCredit.Item.a0',
        'a1' => 'TableCredit.Item.a1',
        'a2' => 'TableCredit.Item.a2',
        'a3' => 'TableCredit.Item.a3',
        'a4' => 'TableCredit.Item.a4',
        'a5' => 'TableCredit.Item.a5',
        'a6' => 'TableCredit.Item.a6',
        'a7' => 'TableCredit.Item.a7',
        'TableObyazPlat' => $arrFilterSpisokObyazPlat,
        'g0' => 'TableObyazPlat.Item.g0',
        'g1' => 'TableObyazPlat.Item.g1',
        'g2' => 'TableObyazPlat.Item.g2',
        'g3' => 'TableObyazPlat.Item.g3',
    ],
    'fields' => [
        'TableCredit' => [
                'PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\ArrayDataProvider',
                'OPTIONS' => [
                    'ITEM_NAME' => 'Item',
                    'ITEM_PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\HashDataProvider',
                ],
            ],
        'TableObyazPlat' => [
                'PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\ArrayDataProvider',
                'OPTIONS' => [
                    'ITEM_NAME' => 'Item',
                    'ITEM_PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\HashDataProvider',
                ],
            ],
        ]
));
*/
$idDocCredit = $documentgeneratorCredit["result"]["document"]["id"];

$idFileonDiskCredit = $documentgeneratorCredit["result"]["document"]["emailDiskFile"];

$downloadFileCredit = CRest::call('disk.file.get', array("id"=> $idFileonDiskCredit));

$publCurlCredit = CRest::call('crm.documentgenerator.document.enablepublicurl', array(
	"id" => $idDocCredit, 
	"status" => 1
));

//Конец Документ список кредиторов

echo '<pre>';
//print_r($arrFilterSpisokCreditor);
echo '</pre>';

//Заявление
$arrFilterSpisokCreditorNotDouble = $arrFilterSpisokCreditor;
$arrFilterSpisokCreditorNotDouble = removeDouble($arrFilterSpisokCreditorNotDouble,"a2");
echo '<pre>';
//print_r($arrFilterSpisokCreditorNotDouble);
echo '</pre>';
foreach ($arrFilterSpisokCreditorNotDouble as $key=>$val)
    $arrFilterSpisokCreditorNotDouble[$key]=array_combine(['m0','m1','m2','m3','m4','m5', 'm6', 'm7'], $val);


      
//Создание документа
$documentgeneratorZayavlenie = CRest::call('crm.documentgenerator.document.add', array(
    "templateId" => $ID_doc_Zayavlenie,
    "entityTypeId" => 2, 
    "entityId" => $ID_deal, 
    "values" => [
        't1' => $t1,
        't2' => $t2,
        't3' => $summDolga,
        't4' => $t4,
        't5'=> $nameIFNS,       
        'TableNoDvIm' => $arrFilterSpisokNoDvIm,
        'f0' => 'TableNoDvIm.Item.f0',
        'f1' => 'TableNoDvIm.Item.f1',
        'f2' => 'TableNoDvIm.Item.f2',
        'f3' => 'TableNoDvIm.Item.f3',
        'f4' => 'TableNoDvIm.Item.f4',
        'f5' => 'TableNoDvIm.Item.f5',
        'f6' => 'TableNoDvIm.Item.f6',
        'TableDvIm' => $arrFilterSpisokDvIm,
        'b0' => 'TableDvIm.Item.b0',
        'b1' => 'TableDvIm.Item.b1',
        'b2' => 'TableDvIm.Item.b2',
        'b3' => 'TableDvIm.Item.b3',
        'b4' => 'TableDvIm.Item.b4',
        'b5' => 'TableDvIm.Item.b5',
        'b6' => 'TableDvIm.Item.b6',
        'TableSchet' => $arrFilterSpisokSchet,
        'd0' => 'TableSchet.Item.d0',
        'd1' => 'TableSchet.Item.d1',
        'd2' => 'TableSchet.Item.d2',
        'd3' => 'TableSchet.Item.d3',
        'd4' => 'TableSchet.Item.d4',
        'TableCredit' => $arrFilterSpisokCreditor,
        'a0' => 'TableCredit.Item.a0',
        'a1' => 'TableCredit.Item.a1',
        'a2' => 'TableCredit.Item.a2',
        'a3' => 'TableCredit.Item.a3',
        'a4' => 'TableCredit.Item.a4',
        'a5' => 'TableCredit.Item.a5',
        'a6' => 'TableCredit.Item.a6',
        'a7' => 'TableCredit.Item.a7',
        'TableCreditNotDouble' => $arrFilterSpisokCreditorNotDouble,
        'm2' => 'TableCreditNotDouble.Item.m2',
        'm3' => 'TableCreditNotDouble.Item.m3',
    ],
      'fields' => [
        'TableNoDvIm' => [
            'PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\ArrayDataProvider',
            'OPTIONS' => [
                'ITEM_NAME' => 'Item',
                'ITEM_PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\HashDataProvider',
            ],
        ],
        'TableDvIm' => [
            'PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\ArrayDataProvider',
            'OPTIONS' => [
                'ITEM_NAME' => 'Item',
                'ITEM_PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\HashDataProvider',
            ],
        ],
        'TableSchet' => [
            'PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\ArrayDataProvider',
            'OPTIONS' => [
                'ITEM_NAME' => 'Item',
                'ITEM_PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\HashDataProvider',
            ],
        ],
        'TableCredit' => [
            'PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\ArrayDataProvider',
            'OPTIONS' => [
                'ITEM_NAME' => 'Item',
                'ITEM_PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\HashDataProvider',
            ],
        ],
        'TableCreditNotDouble' => [
            'PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\ArrayDataProvider',
            'OPTIONS' => [
                'ITEM_NAME' => 'Item',
                'ITEM_PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\HashDataProvider',
            ],
        ],
    ]

));
/*
$idDocZayavlenie = $documentgeneratorZayavlenie["result"]["document"]["id"];

$idFileonDiskZayavlenie = $documentgeneratorZayavlenie["result"]["document"]["emailDiskFile"];

$downloadFileZayavlenie = CRest::call('disk.file.get', array("id"=> $idFileonDiskZayavlenie));

$publCurlZayavlenie = CRest::call('crm.documentgenerator.document.enablepublicurl', array(
	"id" => $idDocZayavlenie, 
	"status" => 1
));*/

echo '<pre>';
//print_r($documentgeneratorZayavlenie);
echo '</pre>';

//конец заявления

//Комментарий в сделку
$product_query = array(
    "fields"=> array(
        "POST_TITLE"=>"Документы",
        "MESSAGE"=> "",
        "ENTITYTYPEID"=>2,
        "ENTITYID"=>$ID_deal,
        )
    );

$result = CRest::call('crm.livefeedmessage.add',$product_query);      

ini_set('display_errors', 1);
error_reporting(E_ALL);
$start = microtime(true);

echo "<pre>";
print_r($result);
echo "</pre>";

?>



<?/*

Для прилождения
возвращает 
файл сформированного документа и публичную ссылку
 
$bizproc_send = CRest::call('bizproc.event.send', array(
    "auth" => $_REQUEST["auth"]["access_token"],
    "event_token" => $_REQUEST["event_token"],
    "log_message" => "OK!!@!#!@!@#!!!@@#!@#!!!",
    "return_values" => array(
        "downloadUrlMachine" => $downloadFileCredit["result"]["FILE_ID"],
        "publCurlCredit" => $publ_curl["result"]["publicUrl"],
        
    )
));
 
*/?>



