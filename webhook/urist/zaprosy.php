<?
//Инициализируем библиотеку
require_once (__DIR__.'/crest.php');

//Данные при работе с активити
//$ID_deal = $_REQUEST["properties"]["Id_deal"]; //223
//$ID_contact = $_REQUEST["properties"]["Id_contact"]; //7903


//Данные при запуске вэб хука
$ID_deal = $_GET['ID_DEAL']; //233
$ID_contact = $_GET['ID_CONTACT']; //7903
//Списки
$ID_creditor = 37;
$ID_svedeniyaOCredit = 41;
//Шаблоны документов
$ID_doc_Zapros = 19;


//Начало Документ Список кредиторов/

$arSpisok = CRest::call('lists.element.get', array(
    "IBLOCK_TYPE_ID" => "bitrix_processes",
    "IBLOCK_ID" => $ID_svedeniyaOCredit,
        "FILTER" => array(
            "NAME" => $ID_deal
            ),
));

//Подготовка массива данных по сделке
foreach ($arSpisok["result"] as $value){
    
    foreach ($value["PROPERTY_133"] as $value1){$creditorID = $value1;}
    foreach ($value["PROPERTY_277"] as $value1){$arCreditorSodOb = $value1;}
    foreach ($value["PROPERTY_135"] as $value1){$arCreditorOsnovVozn = $value1;}
    foreach ($value["PROPERTY_249"] as $value1){$arCreditorDate = $value1;}  
    $arCreditor = CRest::call('lists.element.get', array(
        "IBLOCK_TYPE_ID" => "bitrix_processes",
        "IBLOCK_ID" => $ID_creditor,
        "FILTER" => array(
            "ID" => $creditorID
            ),
        ));
        
    foreach  ($arCreditor['result'] as $creditor){
        $arCreditorName = $creditor["NAME"]; 
        foreach ($creditor["PROPERTY_129"] as $value1){$arCreditorAdress = $value1;}
        foreach ($creditor["PROPERTY_193"] as $value1){$arCreditorAdress = "$value1"." "."$arCreditorAdress";}
    }
    
    $documentgeneratorZapros["result"]["document"]["title"] = "запрос кредиторy ".$arCreditorName;
    $documentgeneratorZapros = CRest::call('crm.documentgenerator.document.add', array(
        "templateId" => $ID_doc_Zapros,
        "entityTypeId" => 2, 
        "entityId" => $ID_deal, 
        "values" => [
            'a2' => $arCreditorName,
            'a3' => $arCreditorAdress,
            'a4'=> $arCreditorSodOb,
            'a5'=> $arCreditorOsnovVozn,
            'a6'=> $arCreditorDate
            ]
        ));
    //Создание документа
        $idDocZapros = $documentgeneratorZapros["result"]["document"]["id"];
            
    $idFileonDiskZapros = $documentgeneratorZapros["result"]["document"]["emailDiskFile"];
    $downloadFileZapros = CRest::call('disk.file.get', array("id"=> $idFileonDiskZapros));
    $publCurlZapros = CRest::call('crm.documentgenerator.document.enablepublicurl', array(
        "id" => $idDocZapros, 
        "status" => 1
    ));
       
    
}
//Конец Документ 
?>