<?
$queryUrl = 'https://sporina.bitrix24.ru/rest/1/u375bxvqpn93f4zk/crm.lead.add.json';
$queryData = http_build_query(array(
    'FIELDS' => array(
        "TITLE" => "Добавлено из хука 6",
        "NAME" => "",
        "EMAIL" => "",
        "PHONE" => array( array("VALUE" => "", "VALUE_TYPE" => "WORK") )
    )
));

$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_SSL_VERIFYPEER => 0,
    CURLOPT_POST => 1,
    CURLOPT_HEADER => 0,
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $queryUrl,
    CURLOPT_POSTFIELDS => $queryData,
));

$result = curl_exec($curl);
curl_close($curl);


