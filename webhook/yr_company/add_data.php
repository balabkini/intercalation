<?require_once (__DIR__.'/crest.php');?>
<?

$data = add_data();
echo '<pre>';
print_r($data);
echo '</pre>';

function add_data(){
	$r = '';
	
	$result = CRest::call('entity.item.add',
		[
			'ENTITY' => 'finance_rec',
			'NAME' => time(),
			'PROPERTY_VALUES' => 
			[
				'provodca' => 'ДЕНЬГИ',
				'planfact' => 'ПЛАН',
				'date' => time(),
				'prihodrashod' => 'ПРИХОД',
				'summa' => '100',
				'count' => '1',
				'price' => '100',
				'company' => 'Наша фирма',
				'subdivision' => 'Подразделение',
				'employee' => 'Сотрудник',
				'deal' => 'Сделка',
				'contact' => 'Контакт',
				'counterparty' => 'Контрагент',
				'period' => '202201',
				'payment_method' => 'БЕЗНАЛИЧНЫЙ',
				'cipher' => 'Шифр',
				'reason' => 'Основание',
			],
		]
	);
	
	if(!$result['error'])
	$r = $result['result'];
	else
	$r = $result['error'];

	return $r;
}
?>