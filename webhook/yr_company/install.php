<?require_once (__DIR__.'/crest.php');?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Компания</title>
	<script src="//api.bitrix24.com/api/v1/"></script>
	<link rel="stylesheet" href="style.css">
</head>
<body>

<?php

$result = CRest::installApp();
if($result['rest_only'] === false):
	if($result['install'] == true):
		echo 'установка завершена';
	else:
		echo 'ошибка установки';
	endif;
endif;

?>

	<h1>Настройка</h1>
    <div id="log"></div>
	<hr>
	<span class="button install" onclick="createTable()">Создать таблицу</span>
	<span class="button uinstall" onclick="unregApp()">Удалить таблицу</span>
	<span class="button end" onclick="closeApp()">Завершить настройку</span>
	<hr>
    <script>
		//alert('welcom');
		
        BX24.init();
		
		function call(url){
			var log = document.getElementById('log');
			//log.innerHTML = log.innerHTML + url + '<br>';

			var xmlHttp = new XMLHttpRequest();
			xmlHttp.onreadystatechange = function() {
				if (xmlHttp.readyState === 4){
					if (xmlHttp.status === 200){
						log.innerHTML = log.innerHTML + 'Call OK (200)' + '<br>';
						log.innerHTML = log.innerHTML + xmlHttp.response + '<br>';
						console.log(xmlHttp.response);
                    }else{
						log.innerHTML = log.innerHTML + 'Call Error (' + xmlHttp.status + ')<br>';
					}
				}
			};
			xmlHttp.open('GET', url, true);
			xmlHttp.timeout = 5000;
			xmlHttp.send();
		}
		
		function closeApp()
		{
			BX24.installFinish(function(res){
				var log = document.getElementById('log');
				if(res.answer.result='install')
				log.innerHTML = log.innerHTML + 'Установлено<br>';
				else
				log.innerHTML = log.innerHTML + 'Что-то пошло не так<br>';
				console.log(res);
			});
		};
		
		function createTable()
		{
			var log = document.getElementById('log');
			log.innerHTML = log.innerHTML + 'Создание таблици<br>';
			call('create_table.php?action=create');
		};
		
		function unregApp()
		{
			var log = document.getElementById('log');
			log.innerHTML = log.innerHTML + 'Удаление таблици<br>';
		};
    </script>	
</body>
</html>