<?require_once (__DIR__.'/crest.php');?>
<?
if($_REQUEST['action'] == 'create'){
	$r = create_table();
	return print_r($r);
};

echo 'действие не найдено';

function create_table(){
	$r = '';
	
	$result = CRest::call('entity.add',
		[
			'ENTITY' => 'finance_rec',
			'NAME' => 'Финансовые записи',
			'ACCESS' => ['U1' => 'W', 'AU' => 'R']
		]
	);
	
	if(!$result['error'])
	$r .= 'Создание хранилища finance_rec: '.$result['result'].'<br>';
	else
	$r .= 'Создание хранилища finance_rec: '.$result['error'].'<br>';

	$params = [
		['provodca', 'Вид проводки', 'S'],	//ДЕНЬГИ/НАЧИСЛЕНИЕ
		['planfact', 'План-Факт', 'S'],	//ПЛАН/ФАКТ
		['date', 'Дата', 'S'],
		['prihodrashod', 'Приход-Расход', 'S'],	//ПРИХОД/РАСХОД
		['summa', 'Сумма', 'N'],
		['count', 'Количество', 'N'],
		['price', 'Цена', 'N'],
		['company', 'Наша фирма', 'S'],
		['subdivision', 'Подразделение', 'S'],
		['employee', 'Сотрудник', 'S'],
		['deal', 'Сделка', 'S'],
		['contact', 'Контакт', 'S'],
		['counterparty', 'Контрагент', 'S'],
		['period', 'Период', 'S'],
		['payment_method', 'Метод оплаты', 'S'],	//НАЛИЧНЫЙ/БЕЗНАЛИЧНЫЙ/ВЗАИМОЗАЧЕТ/НАЧИСЛЕНИЕ
		['cipher', 'Шифр', 'S'],
		['reason', 'Основание', 'S'],
	];

	foreach ($params as $param){
		$result = CRest::call('entity.item.property.add',
			[
				'ENTITY' => 'finance_rec',
				'PROPERTY' => $param[0],
				'NAME' => $param[1],
				'TYPE' => $param[2],
			]
		);
		
		if(!$result['error'])
		$r .= 'Создание свойства хранилища '.$param[0].': '.$result['result'].'<br>';
		else
		$r .= 'Создание свойства хранилища '.$param[0].': '.$result['error'].'<br>';
		
	};
	return $r;
}
?>