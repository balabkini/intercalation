<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Учет финансов</title>
	<script src="//api.bitrix24.com/api/v1/"></script>
	<link rel="stylesheet" href="style.css">
</head>
<body>

<div id="message"></div>

<div class="div_form">
	<div>
		<div class="s">
			<div>
				<input type='text' value='' id='name'>
				<label class="t" for="name">Имя</label>
			</div>
		</div>
		<div class="s">
			<div id='phones'>
				<label class="t">Учет финансов</label>
			</div>
			<div>
				<span class="button_add" title='Добавить номер' onclick="add_input('phones', 'phone')">Добавить Телефон</span>
			</div>
		</div>
	</div>
</div>
	<div class="div_submit">
		<button type="button" class="btn" onclick="contact_update(entity_type, entity_id)">Сохранить</button>
	</div>

    <div id="log" style="display: block;"></div>
	<div id="ctable"></div>
	
    <script>
        //BX24.init();
		
		load_data();
		//c_table();
		function c_table(data){
			let table = document.createElement('table');
			let thead = document.createElement('thead');
			let tbody = document.createElement('tbody');

			table.appendChild(thead);
			table.appendChild(tbody);

			document.getElementById('ctable').appendChild(table);
			
			for (i = 0; i < 10; i++)
			{
				var property = data.PROPERTY_VALUES;
				
				let row = document.createElement('tr');
				let row_data_1  = document.createElement('th');
				row_data_1 .innerHTML = data.ID;
				let row_data_2  = document.createElement('th');
				row_data_2 .innerHTML = property.provodca;
				let row_data_3  = document.createElement('th');
				row_data_3 .innerHTML = property.planfact;

				row.appendChild(row_data_1 );
				row.appendChild(row_data_2 );
				row.appendChild(row_data_3 );
				tbody.appendChild(row);
			}
		};
	
		function load_data(){
			var url = 'get_data.php';
			var xmlHttp = new XMLHttpRequest();
			xmlHttp.onreadystatechange = function() {
				if (xmlHttp.readyState === 4){
					if (xmlHttp.status === 200){
						var log = document.getElementById('log');
						log.innerHTML = log.innerHTML + xmlHttp.response + '<br>';
						var res = JSON.parse(xmlHttp.response);
						console.log(res);
						if(res.length>0){
							log.innerHTML = log.innerHTML + res +'<br>';
							c_table(res[0]);
						}else{
							log.innerHTML = log.innerHTML + 'Нет данных<br>';
						}
						
					}else if (xmlHttp.status === 403) {
                        log.innerHTML = log.innerHTML + 'Ошибка (403)' + '<br>';
                    }else{
						log.innerHTML = log.innerHTML + 'Ошибка (' + xmlHttp.status + ')<br>';
					}
				}
			};
			xmlHttp.open('GET', url, true);
			xmlHttp.timeout = 5000;
			xmlHttp.send();
		}; 

//Редактирование контакта

		function add_input(p, t, v='', eid='') {
			var pp = document.getElementById(p) ;
			var new_input = document.createElement('input') ;
			new_input.type = t ;
			new_input.name = t+'[]' ;
			new_input.value = v ;
			new_input.setAttribute('data-e_id', eid);
			var div = document.createElement('div') ;
			div.appendChild(new_input) ;
			pp.appendChild(div) ;
		} ;

		function getContacts(el_id){
			var es = document.getElementById(el_id) ;
			var el =  es.querySelectorAll('input');
			var fe = [];
			for (var i=0, length = el.length; i < length; i++) {
				fe.push({'ID':el[i].dataset.e_id, 'VALUE':el[i].value});
			};
			return fe;
		};
		
		function getInf(el_id){
			var e = document.getElementById(el_id) ;
			return e.value;
		};

		function contact_update(type, id){
			var fp = getContacts('phones');
			//var fe = getContacts('emails');
			var n = getInf('name');
			//var c = getInf('comment');
			var fields = {'PHONE': fp, /*'EMAIL': fe,*/ 'NAME': n, /*'COMMENTS': c*/};
			console.info(fields);
			BX24.callMethod('crm.'+type+'.update', {'id': id, 'fields': fields},	function(res){
				if(result.error())
				console.error(res.error());
				else
				console.info(res.data());
			});
		};
	</script>

</body>
</html>