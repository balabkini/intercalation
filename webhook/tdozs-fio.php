<?php?>

<script type='text/javascript'> 
var id = prompt("Введите ID");
var label = prompt("Введите новое название");
BX24.callMethod(
    "crm.requisite.userfield.update", 
    { 
        id: id,
        fields: 
        {
            "EDIT_FORM_LABEL": label,
            "LIST_COLUMN_LABEL": label
        }
    }, 
    function(result) 
    {
        if(result.error())
            console.error(result.error());
        else
        {
            console.dir(result.data()); 			
            if(result.more())
                result.next();						
        }
    }


);

</script>
