<?php


postToChat(print_r(json_decode($result, true), true));

//postToChat("Test");


function postToChat($message, $attach = array()) {

    $queryUrl = 'https://intercalation.site/rest/1/920qvukar78tf87y/im.message.add.json';
    $queryData = http_build_query(
        array(
            "USER_ID" => 1,
            "MESSAGE" => $message,
            "ATTACH" => $attach
        )
    );

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $queryUrl,
        CURLOPT_POSTFIELDS => $queryData,
    ));

    $result = curl_exec($curl);
    curl_close($curl);

    return json_decode($result, true);

}