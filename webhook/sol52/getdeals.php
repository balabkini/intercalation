<?php

/*

$deals = executeREST('crm.deal.get', array('ID'=>  2625));

*/

// Отображает все поля Сделки 

$fieldDeals = executeREST('crm.deal.fields', []);


echo "<pre>";
//print_r($fieldDeals);
echo "</pre>";

//Окончание отображения полей сделки


// тело программы

$dealID = 0;
$finish = false;
$result = [];

while (!$finish)
{

$deals = executeREST('crm.deal.list', array(
    'order' => array('ID' => 'ASC'),
    'filter'=>  array(
        '>ID' => $dealID,
        'STAGE_ID' => array('NEW', 'PREPARATION')
    ),
    'start' => -1,
    'select' => array('*', 'UF_*')
));

$result = array_merge($result, $deals['result']);

if (count($deals['result']) > 0)
   {
      foreach ($deals['result'] as $deal)
      {
         $dealID = $deal['ID'];
      }
      

      // Do something
   }
   else
   {
      $finish = true;
   }
}

// вывод массива сделки с полями 
echo "<pre>";
//print_r($result);
echo "</pre>";


//Проверка на пользовательские поля типа список, логические
foreach ($result  as $key1 => $value) {

    foreach ($value as $key => $value1) {
                                
        if ($fieldDeals['result'][$key]['type'] == "enumeration") {
                                
            foreach ($fieldDeals['result'][$key]['items'] as $value2) {
                if ($value2['ID'] == $value1) $result[$key1][$key] = $value2['VALUE'];
            }
        }
    }   
}

//$result[72]['UF_CRM_5BD6D41ACE508'] = 'test';
// вывод массива сделки с полями 
echo "<pre>";
print_r($result);
echo "</pre>";

//конец тела программы

function executeREST($method, $params) {

    $queryUrl = 'https://sol52.bitrix24.ru/rest/109/6wj1pimy2p81mly1/'.$method.'.json';
    $queryData = http_build_query($params);

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $queryUrl,
        CURLOPT_POSTFIELDS => $queryData,
    ));

    $result = curl_exec($curl);
    curl_close($curl);

    return json_decode($result, true);

}