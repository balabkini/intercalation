<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);

$gruzOtprPort = $_GET['gruzOtprPort']; //"Ningbo (China)"
$gruzPolPort = $_GET['gruzPolPort']; //"СПБ"
$os20 = $_GET['os20']; //"0"
$os40 = $_GET['os40']; //"1"
$idDeal = $_GET['idDeal']; //"7014"

require_once __DIR__.'/SimpleXLSX.php';

if($arResultSea = SimpleXLSX::parse('seaSPB.xlsx')) {
    $arResultSea = $arResultSea->rows();
    $Res = [];
    for($i = 1; $i < (count($arResultSea)); $i++){
        $Res[$i-1] = array_combine($arResultSea[0], $arResultSea[$i]);
            if(($Res[$i-1]['Грузоотправитель Порт'] == $gruzOtprPort) && ($Res[$i-1]['Грузополучатель Порт'] == $gruzPolPort)){
                if($os20 && $os40) $message = "Ставка морского фрахта из порта ".$gruzOtprPort." в порт ".$gruzPolPort." составляет на 20'DV: ".$Res[$i-1]["20'DV"]."$".", на 40'HC: ".$Res[$i-1]["40'HC"]."$"; 
                if ($os20 && !$os40) $message = "Ставка морского фрахта из порта ".$gruzOtprPort." в порт ".$gruzPolPort." составляет на 20'DV: ".$Res[$i-1]["20'DV"]."$";
                if (!$os20 && $os40) $message = "Ставка морского фрахта из порта ".$gruzOtprPort." в порт ".$gruzPolPort." составляет на 40'HC: ".$Res[$i-1]["40'HC"]."$";
                //sendComment($idDeal, $message);
            }
    }
   
}else{
    echo SimpleXLSX::parseError();
}

if($arResultZshd = SimpleXLSX::parse('zshdVSK.xlsx')) {
    $arResultZshd = $arResultZshd->rows();
     $Res = [];
    for($i = 1; $i < (count($arResultZshd)); $i++){
        $Res[$i-1] = array_combine($arResultZshd[0], $arResultZshd[$i]);
            if($Res[$i-1]['Грузоотправитель Станция'] == $gruzPolPort){
                if($os20 && $os40) $message .= " Ставка железнодорожной доставки от станции ".$gruzPolPort." до станции ".$Res[$i-1]["Грузополучатель Станция"]." составляет на 20'DV: ".$Res[$i-1]["20'DV"]." рублей".", на 40'HC: ".$Res[$i-1]["40'HC"]." рублей"; 
                if ($os20 && !$os40) $message .= " Ставка железнодорожной доставки от станции ".$gruzPolPort." до станции ".$Res[$i-1]["Грузополучатель Станция"]." составляет на 20'DV: ".$Res[$i-1]["20'DV"]." рублей";
                if (!$os20 && $os40) $message .= " Ставка железнодорожной доставки от станции ".$gruzPolPort." до станции ".$Res[$i-1]["Грузополучатель Станция"]." составляет на 40'HC: ".$Res[$i-1]["40'HC"]." рублей";
                //sendComment($idDeal, $message);
                
            }
    }
 
}else{
    echo SimpleXLSX::parseError();
}

sendComment($idDeal, $message);


function executeREST($method, $params) {

    $queryUrl = 'https://kredo-in.bitrix24.ru/rest/106/ouh6emi3h2twnrbo/'.$method.'.json';
    $queryData = http_build_query($params);

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $queryUrl,
        CURLOPT_POSTFIELDS => $queryData,
    ));

    $result = curl_exec($curl);
    curl_close($curl);

    return json_decode($result, true);

}

function sendComment($id, $message) {

$product_query = array(
    "fields"=> array(
        "POST_TITLE"=>"Результат автоматического просчета",
        "MESSAGE"=> $message,
        "ENTITYTYPEID"=>2,
        "ENTITYID"=>$id,
        )
    );

executeREST('crm.livefeedmessage.add',$product_query);
   
}





