<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);

require_once __DIR__.'/SimpleXLSX.php';

if($contact = SimpleXLSX::parse('contact.xlsx')) {
    print_r($contact->rows());
    //echo $contact->toHTML();
}else{
    echo SimpleXLSX::parseError();
}



if($company = SimpleXLSX::parse('company.xlsx')) {
    print_r($company->rows());
    //echo $company->toHTML();
}else{
    echo SimpleXLSX::parseError();
}    