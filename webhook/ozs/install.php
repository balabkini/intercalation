<?php
require_once (__DIR__.'/crest.php');

$result = CRest::call(
	'bizproc.activity.add',
	[
		"CODE" => "a2nck8azuqcpg9kuiwc736vmtr17h0dbmy0y7x6ey9jlqk9j1h",
		"HANDLER" => "https://intercalation.ru/webhook/ozs/ozs.php",
		"USE_SUBSCRIPTION" => "Y",
		"NAME" => "Коммерческое предложение и счет с подсчетом веса",
		"DESCRIPTION" => "Создание коммерческого предложения или счета",
		"PROPERTIES" => [
			"Id_deal" => [
				"Name" => "Индификатор сделки",
				"Type" => "int",
				"Required" => "Y",
				"Multiple" => "N"
			],
			"Id_template" => [
				"Name" => "Идентефикатор шаблона",
				"Type" => "select",
				"Options" => [
					"86" => "Коммерческое предложение",
					"88" => "Счет ТД ОЗС",
					"90" => "Счет ОЗС",
					"92" => "Счет ОЗС ПАО ВТБ",
				],
				"Required" => "Y",
				"Multiple" => "N"
			],
		],
		"RETURN_PROPERTIES" => [
			"downloadUrlMachine" => [
				"Name" => "Документ в docx",
				"Type" => "string",
				"Multiple" => "N",
				"Default" => null
			],
			"public_url" => [
				"Name" => "Публичная ссылка на документ в docx",
				"Type" => "string",
				"Multiple" => "N",
				"Default" => null
			],
			"pdfUrlMachine" => [
				"Name" => "Документ в pdf",
				"Type" => "string",
				"Multiple" => "N",
				"Default" => null
			],
			"number" => [
				"Name" => "Номер документа",
				"Type" => "string",
				"Multiple" => "N",
				"Default" => null
			],
		],
	]
);

echo '<pre>';
print_r($result);
echo '</pre>';

$result = CRest::installApp();
if($result['rest_only'] === false):?>
<head>
	<script src="//api.bitrix24.com/api/v1/"></script>
	<?if($result['install'] == true):?>
	<script>
		BX24.init(function(){
			BX24.installFinish();
		});
	</script>
	<?endif;?>
</head>
<body>
	<?if($result['install'] == true):?>
		installation has been finished
	<?else:?>
		installation error
	<?endif;?>
</body>
<?endif;