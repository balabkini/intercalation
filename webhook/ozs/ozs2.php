<?

//Инициализируем библиотеку
require_once (__DIR__.'/crest.php');

//Переменные
$ID_template = $_REQUEST["properties"]["Id_template"]; 
$ID_deal = $_REQUEST["properties"]["Id_deal"];
//$ID_deal = 18770;
//$ID_template = 86;

$deal_product = CRest::call('crm.deal.productrows.get', array("ID" => $ID_deal));
$deal_get = CRest::call('crm.deal.get',  array("ID" => $ID_deal));
$company_get = CRest::call('crm.company.get', array("ID" => $deal_get["result"]["COMPANY_ID"]));
//$company_get = CRest::call('crm.company.fields');

$ELEMENT_ID = $company_get["result"]["UF_CRM_1588418516"];

$postav_get = CRest::call('lists.element.get', array(
    "IBLOCK_TYPE_ID" => "bitrix_processes",
    "IBLOCK_ID" => 38,
    "ELEMENT_ID" => $company_get["result"]["UF_CRM_1588418516"]
));

foreach ($postav_get["result"][0]["PROPERTY_112"] as $value)
        $MyCompanyRequisiteRegisteredAddressText = $value;

foreach ($postav_get["result"][0]["PROPERTY_114"] as $value)
        $MyCompanyBankDetailRqBankName = $value;

foreach ($postav_get["result"][0]["PROPERTY_116"] as $value)
        $MyCompanyBankDetailRqBik = $value;
   
foreach ($postav_get["result"][0]["PROPERTY_122"] as $value)
        $MyCompanyRequisiteRqInn = $value;
   
foreach ($postav_get["result"][0]["PROPERTY_124"] as $value)
        $MyCompanyRequisiteRqKpp = $value;
   
foreach ($postav_get["result"][0]["PROPERTY_130"] as $value)
        $MyCompanyBankDetailRqAccNum = $value;
    
foreach ($postav_get["result"][0]["PROPERTY_132"] as $value)
        $MyCompanyBankDetailRqCorAccNum = $value;

foreach ($postav_get["result"][0]["PROPERTY_134"] as $value)
        $MyCompanyRequisiteRqDirector = $value;

foreach ($postav_get["result"][0]["PROPERTY_776"] as $value)
        $Stamp = $value;

foreach ($postav_get["result"][0]["PROPERTY_778"] as $value)
       $Image = $value;
    


$arrSpisok = [];
$TotalSum = 0;
$TaxesTaxValue = 0;
$ves = 0;
$colvo = 0;
$obem = 0;
$i = 0;

//Заполнение массива 
foreach($deal_product["result"] as $value)
{
    $product = CRest::call('crm.product.get', array("ID" => $value["PRODUCT_ID"]));
    
    $i++;
    
    $arrSpisok[] = [
        "a0" => $i,
        "a1" => $value["PRODUCT_NAME"],
        "a2" => $product["result"]["PROPERTY_708"]["value"]*$value["QUANTITY"],
        "a3" => $value["QUANTITY"],
        "a4" => $value["MEASURE_NAME"],
        "a5" => $value["PRICE"] - round($value["PRICE"]/(100 + $value["TAX_RATE"])*$value["TAX_RATE"],2) ,
        "a6" => $value["TAX_RATE"],
        "a7" => round($value["QUANTITY"]*$value["PRICE"] - round($value["QUANTITY"]*$value["PRICE"]/(100 + $value["TAX_RATE"])*$value["TAX_RATE"],2),2),
   ];

    $TotalSum = $TotalSum + $value["QUANTITY"]*$value["PRICE"];
    $TaxesTaxValue = $TaxesTaxValue+ (round($value["QUANTITY"]*$value["PRICE"]/(100 + $value["TAX_RATE"])*$value["TAX_RATE"],2));
    $ves = $ves + $product["result"]["PROPERTY_708"]["value"]*$value["QUANTITY"];
    $colvo = $colvo + $value["QUANTITY"];
    
}
/*
echo '<pre>';
print_r($arrSpisok);
echo '</pre>';
die;
*/

$obem = $colvo*0.01;
//$TaxesTaxValue = $TotalSum - $TaxesTaxValue;
/*
$a = CRest::call('crm.documentgenerator.template.get', array(
    "id" => $ID_template));
$a = CRest::call('crm.documentgenerator.numerator.get', array(
    "id" => $a["result"]["template"]["numeratorId"]));
 */ 

//Создание документа
$documentgenerator_update = CRest::call('crm.documentgenerator.document.add', array(
    "templateId" => $ID_template, 
    "entityTypeId" => 2, 
    "entityId" => $ID_deal, 
    "values" => [
        'MyCompanyRequisiteRqCompanyName' => $postav_get["result"][0]["NAME"],
        'MyCompanyRequisiteRegisteredAddressText' => $MyCompanyRequisiteRegisteredAddressText,
        'MyCompanyBankDetailRqBankName' => $MyCompanyBankDetailRqBankName,
        'MyCompanyBankDetailRqBik' => $MyCompanyBankDetailRqBik,
        'MyCompanyRequisiteRqInn' => $MyCompanyRequisiteRqInn,
        'MyCompanyRequisiteRqKpp' => $MyCompanyRequisiteRqKpp,
        'MyCompanyBankDetailRqAccNum' => $MyCompanyBankDetailRqAccNum,
        'MyCompanyBankDetailRqCorAccNum' => $MyCompanyBankDetailRqCorAccNum,
        'MyCompanyRequisiteRqDirector' => $MyCompanyRequisiteRqDirector,
        'Stamp' => $Stamp,
        'Image' => $Image,
        'ves' => $ves,
        'colvo' => $colvo,
        'obem' => $obem,
        'TotalSum' => $TotalSum,
        'TaxesTaxValue' => round($TaxesTaxValue,2),
        'Table' => $arrSpisok,
        'a0' => 'Table.Item.a0',
        'a1' => 'Table.Item.a1',
        'a2' => 'Table.Item.a2',
        'a3' => 'Table.Item.a3',
        'a4' => 'Table.Item.a4',
        'a5' => 'Table.Item.a5',
        'a6' => 'Table.Item.a6',
        'a7' => 'Table.Item.a7',
     ],
    'fields' => [
        'Stamp' => ['TYPE' => 'IMAGE'], // тип поля - печать
        'Image' => ['TYPE' => 'IMAGE'], // тип поля - изображение
        'Table' => [
            'PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\ArrayDataProvider',
            'OPTIONS' => [
                'ITEM_NAME' => 'Item',
                'ITEM_PROVIDER' => 'Bitrix\\DocumentGenerator\\DataProvider\\HashDataProvider',
            ],
        ],
    ]
));

$id_doc = $documentgenerator_update["result"]["document"]["id"];
//sleep(30);
$a =CRest::call('crm.documentgenerator.document.get', array("id" => $id_doc));

echo '<pre>';
//print_r($documentgenerator_update);
echo '</pre>';

$id_fileonDisk = $documentgenerator_update["result"]["document"]["emailDiskFile"];

$download_file = CRest::call('disk.file.get', array("id"=> $id_fileonDisk));

$publ_curl =CRest::call('crm.documentgenerator.document.enablepublicurl', array("id" => $id_doc, "status" => 1));

$bizproc_send = CRest::call('bizproc.event.send', array(
    "auth" => $_REQUEST["auth"]["access_token"],
    "event_token" => $_REQUEST["event_token"],
    "log_message" => "OK!!@!#!@!@#!!!@@#!@#!!!",
    "return_values" => array(
        "downloadUrlMachine" => $download_file["result"]["FILE_ID"],
        "public_url" => $publ_curl["result"]["publicUrl"],
        "number" => $a["result"]["document"]["number"],
        "pdfUrlMachine" => $download_file["result"]["FILE_ID"]
    )
));





