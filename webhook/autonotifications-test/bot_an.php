<?php
error_reporting(0);
//Создайте чатбота, который будет следить за перепиской в чате и сможет реагировать на ключевые слова
define('BOT_ID', 7);
define('CLIENT_ID', 'b4wc07rj8fz027n3g63fsjbjo9tizbcn');
define('URL_BASE', 'https://b24-nfwki3.bitrix24.ru/rest/1/7oiyg29ssmz26dhi/');
define('DATA_FILE_NAME', 'data_deal.txt');

$answers = [
	//(Если оценка 9 или 10) 
	//(Прикрепить сертификат с условиями заказа от 5000р.)
	0 => 'Мы рады сотрудничеству с вами! И будем благодарны за рекомендацию друзьям и знакомым! Дарим вам сертификат на 1000 визиток в подарок к следующему заказу.',
	//(если меньше 9)
	//(Прикрепить сертификат с условиями заказа от 5000р.)
	1 => 'Спасибо, что ответили на вопрос, мы стремимся улучшать качество наших услуг! Дарим вам сертификат на 1000 визиток в подарок к следующему заказу.',
	//(если не ответил)
	//(Прикрепить сертификат с условиями заказа на 5000р.)
	2 => 'Спасибо за заказ. Мы рады быть Вам полезными! Дарим Вам сертификат на 1000 визиток в подарок к следующему заказу.',
];

//postToChat(print_r($_REQUEST, true), 1);

    function postToChat($message, $dialog_id){
        $params = array(
            'MESSAGE' => $message,
			'DIALOG_ID' => $dialog_id,
        );
		$result = imbotCommand('imbot.message.add', $params);
		return $result;
    }

	switch ($_REQUEST['event']) {
		case ('ONAPPINSTALL'):
			//postToChat('ONAPPINSTALL', 1);
			break;
		case ('ONAPPUPDATE'):
			//postToChat('ONAPPUPDATE', 1);
			break;
		case ('ONIMBOTJOINCHAT'):
			//postToChat('ONIMBOTJOINCHAT', 1);
			break;
		case ('ONIMBOTMESSAGEADD'):
			$dialog_id = $_REQUEST['data']['PARAMS']['DIALOG_ID'];
			$mess = $_REQUEST['data']['PARAMS']['MESSAGE'];
			
			//Оцените качество работы нашей компании от 1 до 10
			
			$ar_data = array();
			if (file_exists(__DIR__."/".DATA_FILE_NAME)){
				$d = file_get_contents(__DIR__."/".DATA_FILE_NAME);
				$ar_data = unserialize($d);
			}
			
			if(count($ar_data)>0){
			
				$user_id = $_REQUEST['data']['PARAMS']['FROM_USER_ID'];
				$result = imbotCommand('imbot.dialog.get', array('DIALOG_ID' => $dialog_id));
				$entity = $result['result']['entity_data_1'] . PHP_EOL . $result['result']['entity_data_2'] . PHP_EOL . $result['result']['entity_data_3'];
				
				$entitydata = explode("|", $result['result']['entity_data_2']);
				$items = array('LEAD', 'COMPANY', 'CONTACT', 'DEAL');
				$entityes = array();
				foreach ($items as $val){
					$i = array_search($val, $entitydata);
					if($i !== false)
					$entityes[$val] = $entitydata[$i+1];
				}
				
				$sms = 'Нет в списке';
				foreach ($ar_data as $key => $value){
					if(($key == $entityes['DEAL']) || ($ar_data[$key]['lead'] == $entityes['LEAD'])){
						$sms = 'Есть в списке';
						unset($ar_data[$key]);
						
						$d = serialize($ar_data);
						file_put_contents(__DIR__."/".DATA_FILE_NAME, $d);
						
						$url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
						$ub = dirname($url);
						$url_ser = $ub . '/images/sertificat-na-1000-vizitok-v-podarok_200x200.jpg';
						
						$amess = '';
						preg_match("/[\d]+/", $mess, $match);
						if (strlen($match[0])>0){
							if (intval($match[0])<9)
							$amess = $answers[1].' '.$url_ser;
							else
							$amess = $answers[0].' '.$url_ser;
						};
						
						crmFieldGradeUpdate($key, $mess);
						postToChat($amess, $dialog_id);
						
						break;
					}
				}
				//postToChat('[' . PHP_EOL . 'user_id: ' . $user_id . PHP_EOL . ' диалог: ' . $dialog_id . PHP_EOL . 'entity: ' . $entity . PHP_EOL . $d . PHP_EOL . $sms . PHP_EOL . ']', $dialog_id);
				
			}else{
				//postToChat($mess . PHP_EOL . 'user_id: ' . $user_id . PHP_EOL . ' диалог: ' . $dialog_id . PHP_EOL . 'entity: ' . $entity . PHP_EOL . print_r($ar_data, true) . PHP_EOL . $sms, $dialog_id);
			}
			
			break;
		// если не ответили
		case ('no_answer'):
			postToChat($answers[2], $dialog_id);
			break;
		// отмечаем сделку с ожиданием оценки https://intercalation.ru/webhook/autonotifications/bot_an.php?event=set_deal
		case ('set_deal'):
			set_deal($_REQUEST);
			break;
		case ('ONCRMDEALADD'):
			add_deal($_REQUEST);
			break;
		// ручная отправка https://intercalation.ru/webhook/autonotifications/bot_an.php?event=adduserfield
		case ('adduserfield'):
			//postToChat('adduserfield', 1);
			addUserField();
			break;
		default:
			//postToChat('O_O '.$_REQUEST['event'], 1);
			break;
	}
	
function add_deal($data)
{
	//postToChat($_REQUEST['event'].print_r($data['data']['FIELDS']['ID'], true), 1);
	
	$id = $data['data']['FIELDS']['ID'];
	
	$params = [
		'ID' => $id,
	];
	$res = imbotCommand('crm.deal.get', $params);
	
	$contact_id = $res['result']['CONTACT_ID'];
	
	//postToChat(print_r($contact_id, true), 1);
	
	if(isset($contact_id)AND($contact_id>0)){
		$params = [
			'FILTER' => ['=CONTACT_ID' => $contact_id, '=STAGE_ID' => 'WON'],
			'SELECT' => ['ID', 'STAGE_ID'],
		];
		$res = imbotCommand('crm.deal.list', $params);
		
		$r = $res['result'];
		foreach($r as $v){
			//postToChat(print_r($v['ID'], true), 1);
			crmFieldSMSSendingUpdate($v['ID']);
		}
	}
};

function crmFieldSMSSendingUpdate($id)
{
	$params = [
		'ID' => $id,
		'FIELDS' =>
		[
			'UF_CRM_SMS_SENDING' => false,
		]
	];
	$res = imbotCommand('crm.deal.update', $params);
};	
	
function crmFieldGradeUpdate($id, $mess)
{
	$params = [
		'ID' => $id,
		'FIELDS' =>
		[
			'UF_CRM_GRADE' => $mess,
			'UF_CRM_ANSWER_GRADE' => true,
		]
	];
	$res = imbotCommand('crm.deal.update', $params);
};
	
function addUserField()
{
	$params = [
		'FIELDS' =>
		[
			'FIELD_NAME' => 'GRADE',
			'EDIT_FORM_LABEL' => 'Оценка заказчика',
			'LIST_COLUMN_LABEL' => 'Оценка заказчика',
			'USER_TYPE_ID' => 'string',
			'XML_ID' => 'GRADE',
			'SETTINGS' => [
				'DEFAULT_VALUE' => "",
			]
		]
	];
	$res = imbotCommand('crm.deal.userfield.add', $params);	
	//postToChat('crm.deal.userfield.add '.print_r($res, true), 1);
	
	$params = [
		'FIELDS' =>
		[
			'FIELD_NAME' => 'ANSWER_GRADE',
			'EDIT_FORM_LABEL' => 'Был ли ответ',
			'LIST_COLUMN_LABEL' => 'Был ли ответ',
			'USER_TYPE_ID' => 'boolean',
			'XML_ID' => 'ANSWER_GRADE',
			'SETTINGS' => [
				'DEFAULT_VALUE' => "Нет",
			]
		]
	];
	$res = imbotCommand('crm.deal.userfield.add', $params);	
	//postToChat('crm.deal.userfield.add '.print_r($res, true), 1);
	
	$params = [
		'FIELDS' =>
		[
			'FIELD_NAME' => 'SMS_SENDING',
			'EDIT_FORM_LABEL' => 'Рассылка по завершению',
			'LIST_COLUMN_LABEL' => 'Рассылка по завершению',
			'USER_TYPE_ID' => 'boolean',
			'XML_ID' => 'SMS_SENDING',
			'SETTINGS' => [
				'DEFAULT_VALUE' => true,
			]
		]
	];
	$res = imbotCommand('crm.deal.userfield.add', $params);	
	//postToChat('crm.deal.userfield.add '.print_r($res, true), 1);
};

function set_deal($data)
{
	if (!DATA_FILE_NAME)
	return false;

	$ar_data = [];

	if (file_exists(__DIR__."/".DATA_FILE_NAME)){
		$d = file_get_contents(__DIR__."/".DATA_FILE_NAME);
		$ar_data = unserialize($d);
	}

	$ar_data[$data['id']] = ['lead' => $data['lead'], 'time' => time()];
	$d = serialize($ar_data);

	file_put_contents(__DIR__."/".DATA_FILE_NAME, $d);

	return true;
}

function imbotCommand($method, array $params = Array())
{
	$queryUrl = URL_BASE.$method.'.json';
	$queryData = http_build_query(array_merge($params, array('CLIENT_ID' => CLIENT_ID, 'BOT_ID' => BOT_ID)));

	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_POST => 1,
		CURLOPT_HEADER => 0,
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_SSL_VERIFYPEER => 1,
		CURLOPT_URL => $queryUrl,
		CURLOPT_POSTFIELDS => $queryData,
	));

	$result = curl_exec($curl);
	curl_close($curl);

	$result = json_decode($result, true);

	return $result;
}

?>