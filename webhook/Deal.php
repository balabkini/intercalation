<?
use Bitrix\Main\Loader;
\Bitrix\Main\Loader::includeModule('rest');
$client = new \Bitrix\Main\Web\HttpClient();
$webHookUrl = 'https://intercalation.ru/webhook/';
$prefix = 'documentgenerator';

$data = [
'fields' => [
'name' => 'Rest Numerator',
'template' => '{NUMBER}',
'settings' => [
'Bitrix_Main_Numerator_Generator_SequentNumberGenerator' => [
'start' => '0',
'step' => '1',
],
],
],
];

$url = $webHookUrl.$prefix.'.numerator.add/';
$answer = $client->post($url, $data);
try
{
$result = \Bitrix\Main\Web\Json::decode($answer);
}
catch(Exception $e)
{
var_dump($answer);
}

print_r($result);
