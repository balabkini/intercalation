<?
//Инициализируем библиотеку
require_once (__DIR__.'/crest.php');

// получение полей лида
$arFields = CRest::call('crm.lead.fields');
echo '<pre>';
//print_r($arFields);
echo '</pre>';

$arOrder = [];
//для теста
$arOrder['UF_CRM_1628249365065'] = "Доставка от Dantone Home";
$arOrder['UF_CRM_1628249091112'] = "Рассрочка от Тинькофф";

//Состав заказ: артикул, наименование, цена, кол-во, сумма
$sostavZakaza = array(
  "0" => array("артикул" => "1", "наименование" => "1", "цена" => "1", "кол-во" => "1", "сумма" => "1"),
  "1" => array("артикул" => "2", "наименование" => "2", "цена" => "2", "кол-во" => "2", "сумма" => "2"),
  "2" => array("артикул" => "3", "наименование" => "3", "цена" => "3", "кол-во" => "3", "сумма" => "3")
 );

 $str = "";
 for ($i=0; $i<count($sostavZakaza); $i++) { 
   $str .= implode("_", $sostavZakaza[$i]);
   $str .= ";";
 }   
 
// поиск ID по названию
//Способ доставки
for ($i=0; $i<count($arFields['result']['UF_CRM_1628249365065']['items']); $i++) {
  if ($arFields['result']['UF_CRM_1628249365065']['items'][$i]['VALUE'] == $arOrder['UF_CRM_1628249365065']) $SD = $arOrder['UF_CRM_1628249365065'] = $arFields['result']['UF_CRM_1628249365065']['items'][$i]['ID'];  
};
//Способ оплаты
for ($i=0; $i<count($arFields['result']['UF_CRM_1628249091112']['items']); $i++) {
  if ($arFields['result']['UF_CRM_1628249091112']['items'][$i]['VALUE'] == $arOrder['UF_CRM_1628249091112']) $SO= $arOrder['UF_CRM_1628249091112'] = $arFields['result']['UF_CRM_1628249091112']['items'][$i]['ID']; 
};

$arOrder = array (
    "TITLE" => "тест_1", //название лида - думаю номер заказа само то
    "UF_CRM_1627028058702" => 1234, //Номер заказа на сайте
    "NAME" => "Имя",
    "LAST_NAME" => "Фамилия",
    "EMAIL" => array( array("VALUE" => "email@email.ru", "VALUE_TYPE" => "WORK") ),
    "PHONE" => array( array("VALUE" => "1234567890", "VALUE_TYPE" => "WORK") ),
    "UF_CRM_1627028368156" => "Город",
    "ADDRESS_2" => "Адрес (набор полей из вкладки доставка склеивается и передается в поле Адрес в Б24)",
    "UF_CRM_1628701754218" => "Комментарий (со страницы Доставка)",
    "UF_CRM_1628249365065" => $SD, //способ доставки
    "UF_CRM_1628249091112" =>  $SO, //способ оплаты
    "OPPORTUNITY" => 100,//Сумма заказа
    "UF_CRM_1628701708240" => $str,
    "UF_CRM_1628701725598" => "Комментарий (со страницы Оплата)",
    "UF_CRM_1628702403704" => "Менеджер (из инструмента по созданию ссылок на оплату",
    "UTM_SOURCE" => "utm метки",
    "UTM_MEDIUM" => "utm метки",
    "UTM_CAMPAIGN" => "utm метки",
    "UTM_CONTENT" => "utm метки",
    "UTM_TERM" => "utm метки",
    "CURRENCY_ID" => "RUB",
    "SOURCE_ID" => 74212529061, //не изменять это id - источник оформление заказа
    "ASSIGNED_BY_ID" => 9 //назначение ответственным Марат Михтифидинов
);

$arLead = CRest::call('crm.lead.add', array(
  "FIELDS" => $arOrder
));

echo("Лид создан");
?>