<?php
namespace Bitrix24\Bitrix24lead;
use Bitrix24\Bitrix24Entity;
use Bitrix24\Bitrix24Exception;

class Bitrix24lead extends Bitrix24Entity
{





    public function fields()
    {

        $result = $this->client->call('crm.lead.fields');


        return $result;
    }

    /**
     * Get list of fields entity user
     * @param $SORT - field name to sort by them
     * @param $ORDER - sort direction? must be set to ASC or DESC
     * @param $FILTER - list of fields user entity to filter result
     * @return array
     */
    public function userfieldList($SORT, $ORDER, $FILTER)
    {

        $result = $this->client->call('crm.lead.userfield.list',
            array(
            'SORT' => $SORT,
            'ORDER' => $ORDER,
            'FILTER'=> $FILTER)
        );


        return $result;
    }
}