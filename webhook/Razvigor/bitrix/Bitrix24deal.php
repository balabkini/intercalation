<?php
namespace Bitrix24\Bitrix24deal;
use Bitrix24\Bitrix24Entity;
use Bitrix24\Bitrix24Exception;

class Bitrix24deal extends Bitrix24Entity
{





    public function fields()
    {

        $result = $this->client->call('crm.deal.fields');


        return $result;
    }

    public function dealList()
    {

        $result = $this->client->call('crm.deal.list');


        return $result;
    }
    /**
     * Get list of fields entity user
     * @param $SORT - field name to sort by them
     * @param $ORDER - sort direction? must be set to ASC or DESC
     * @param $FILTER - list of fields user entity to filter result
     * @return array
     */
    public function userfieldList($SORT, $ORDER, $FILTER)
    {

        $result = $this->client->call('crm.deal.userfield.list',
            array(
                'SORT' => $SORT,
                'ORDER' => $ORDER,
                'FILTER'=> $FILTER)
        );


        return $result;
    }

    public function updateDeal($id, $fields,$params)
    {

        $result = $this->client->call('crm.deal.update',
            array(
                'id' => $id,
                'fields' => $fields,
                'params'=> $params)
        );


        return $result;
    }

}