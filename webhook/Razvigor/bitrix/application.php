<?

require_once("tools.php");
require_once("log.php");

class CApplication
{
    public $arB24App;
    public $arAccessParams = array();
    public $arRatingUsers = array();
    public $currentUser = 0;
    private $b24_error = '';
    public $is_ajax_mode = false;
    public $is_background_mode = false;
    public $currentRating = 0;
    public $allCountDeal=0;
    private function checkB24Auth()
    {

        // проверяем актуальность доступа
        $isTokenRefreshed = false;

        // $arAccessParams['access_token'] = '123';
        // $arAccessParams['refresh_token'] = '333';

        $this->arB24App = getBitrix24($this->arAccessParams, $isTokenRefreshed, $this->b24_error);
        return $this->b24_error === true;
    }

    private function returnJSONResult($answer)
    {

        ob_start();
        ob_end_clean();
        Header('Cache-Control: no-cache');
        Header('Pragma: no-cache');
        echo json_encode($answer);
        die();
    }

    private function getYesterday()
    {
        $result = new DateTime();
        $result->add(DateInterval::createFromDateString('yesterday'));
        return $result->format('Y-m-d');
    }

    public function manageAjax($operation, $params)
    {
        switch ($operation) {
            case 'getAllDeal':
                $obB24Batch = new \Bitrix24\Bitrix24Batch\Bitrix24Batch($this->arB24App);
                $obB24Batch->addStatusListCall(0,
                    array(),
                    array("ENTITY_ID" => "DEAL_STAGE")
                );
                $res = $obB24Batch->call();
                foreach ($res[0]["data"] as $status) {
                    if ($status['EXTRA']['SEMANTICS'] == 'failure' or $status['EXTRA']['SEMANTICS'] == 'apology') {
                        $errorStatus['TypeName'][$status['STATUS_ID']] = $status['NAME'];
                        $errorStatus['statusTypeCount'][$status['STATUS_ID']] = 0;
                        $errorStatus['statusType'][] = $status['STATUS_ID'];
                    }
                }
                $arDealFilter = array(
                    "CLOSED" => 'Y',
                    ">=DATE_CREATE" => $params['dateStart'],
                    "<=DATE_CREATE" => $params['dateEnd'],
                    "STAGE_ID" => $errorStatus['statusType']
                );
                $obB24Batch->addDealListCall(0,
                    array("DATE_CREATE" => "ASC"),
                    array(),
                    $arDealFilter
                );
                $res = $obB24Batch->call();
                $result = array();
                foreach ($res[0]["data"] as $k=>$value) {
                    foreach ($errorStatus['statusType'] as $key=>$val) {
                        if ($value['STAGE_ID'] == $val) {
                            $errorStatus['statusTypeCount'][$value['STAGE_ID']] = $errorStatus['statusTypeCount'][$value['STAGE_ID']] + 1;
                        }
                    }
                }
             foreach ($errorStatus['statusTypeCount'] as $count){
                 $this->allCountDeal = $this->allCountDeal+$count;
             }
                foreach ($errorStatus['statusTypeCount'] as $key => $vv) {
                    foreach ($errorStatus['TypeName'] as $k => $val) {
                        if ($key == $k) {
                            $endResult['row'][] = array('name' => $val, 'count' => $vv,'percent'=>$this->getPercent($vv));
                        }
                    }
                }
                $endResult['allcount'] = $this->allCountDeal;
                $this->returnJSONResult(array('status' => 'succes', 'result' => $endResult));
                break;
            case 'getAllLead':
                $obB24Batch = new \Bitrix24\Bitrix24Batch\Bitrix24Batch($this->arB24App);
                $obB24Batch->addStatusListCall(0,
                    array(),
                    array("ENTITY_ID" => "STATUS")
                );
                $res = $obB24Batch->call();

                foreach ($res[0]["data"] as $status) {
                    if ($status['EXTRA']['SEMANTICS'] == 'failure' or $status['EXTRA']['SEMANTICS'] == 'apology') {
                        $errorStatus['TypeName'][$status['STATUS_ID']] = $status['NAME'];
                        $errorStatus['statusTypeCount'][$status['STATUS_ID']] = 0;
                        $errorStatus['statusType'][] = $status['STATUS_ID'];
                    }
                }

                $arDealFilter = array(
                    ">=DATE_CREATE" => $params['dateStart'],
                    "<=DATE_CREATE" => $params['dateEnd'],
                    "STATUS_ID" => $errorStatus['statusType']
                );
                $obB24Batch->addLeadListCall(0,
                    array("DATE_CREATE" => "ASC"),
                    array(),
                    $arDealFilter
                );
                $res = $obB24Batch->call();
                $result = array();
                foreach ($res[0]["data"] as $k=>$value) {
                    foreach ($errorStatus['statusType'] as $key=>$val) {
                        if ($value['STATUS_ID'] == $val) {
                            $errorStatus['statusTypeCount'][$value['STATUS_ID']] = $errorStatus['statusTypeCount'][$value['STATUS_ID']] + 1;
                        }
                    }
                }
                foreach ($errorStatus['statusTypeCount'] as $count){
                    $this->allCountDeal = $this->allCountDeal+$count;
                }
                foreach ($errorStatus['statusTypeCount'] as $key => $vv) {
                    foreach ($errorStatus['TypeName'] as $k => $val) {
                        if ($key == $k) {
                            $endResult['row'][] = array('name' => $val, 'count' => $vv,'percent'=>$this->getPercent($vv));
                        }
                    }
                }
                $endResult['allcount'] = $this->allCountDeal;
                $this->returnJSONResult(array('status' => 'succes', 'result' => $endResult));
                break;
            default:
                $this->returnJSONResult(array('status' => 'error', 'result' => 'unknown operation'));
        }
    }


    public function getPercent($num){

       $result = ($num/$this->allCountDeal)*100;
        $result = number_format($result, 1, ',', ' ');
return $result;
    }

    public function getData()
    {

        $obB24User = new \Bitrix24\Bitrix24User\Bitrix24User($this->arB24App);
        $arCurrentB24User = $obB24User->current();
        $this->currentUser = $arCurrentB24User["result"]["ID"];

        $users = $this->getUsers();

        $arDealFilter = array(
            "ASSIGNED_BY_ID" => $this->currentUser,
            "CLOSED" => 'Y'/*,
			">=DATE_MODIFY": yesterday,
			"<DATE_MODIFY": today */
        );

        $obB24Batch = new \Bitrix24\Bitrix24Batch\Bitrix24Batch($this->arB24App);
        $obB24Batch->addDealListCall(0,
            array("DATE_CREATE" => "ASC"),
            array("ID", "TITLE", "OPPORTUNITY", "ASSIGNED_BY_ID"),
            $arDealFilter
        );

        $arUserIDs = array();
        foreach ($users as $arUser)
            $arUserIDs[] = $arUser["ID_USER"];

        $obB24Batch->addUserListCall(1,
            array("ID" => $arUserIDs)
        );

        $res = $obB24Batch->call();

        foreach ($users as $key => $arUser)
            $this->arRatingUsers[$arUser["ID_USER"]] = $arUser;

        $this->currentRating = isset($this->arRatingUsers[$this->currentUser]["ID_RATING_ITEM"]) ? $this->arRatingUsers[$this->currentUser]["ID_RATING_ITEM"] : 0;

        foreach ($res[1]["data"] as $arUser)
            $this->arRatingUsers[$arUser["ID"]] = array_merge(
                $this->arRatingUsers[$arUser["ID"]],
                array(
                    "FIRST_NAME" => $arUser["NAME"],
                    "LAST_NAME" => $arUser["LAST_NAME"],
                    "SECOND_NAME" => $arUser["SECOND_NAME"],
                    "PERSONAL_PHOTO" => $arUser["PERSONAL_PHOTO"])
            );

        $new_deal_sum = 0;

        foreach ($res[0]["data"] as $arDeal)
            $new_deal_sum += $arDeal["OPPORTUNITY"];

        $this->arRatingUsers[$this->currentUser]["RATE_SUM"] = $new_deal_sum;

    }


    public function start()
    {

        $this->is_ajax_mode = isset($_REQUEST['action']);
        $this->is_background_mode = isset($_REQUEST['background']);

        if ($this->is_background_mode) $this->getAuthFromDB();
        else {
            if (!$this->is_ajax_mode)
                $this->arAccessParams = prepareFromRequest($_REQUEST['auth']);
            else

                $this->arAccessParams = $_REQUEST['auth'];

            $this->b24_error = $this->checkB24Auth();

            if ($this->b24_error != '') {
                if ($this->is_ajax_mode)
                    $this->returnJSONResult(array('status' => 'error', 'result' => $this->b24_error));
                else
                    echo "B24 error: " . $this->b24_error;

                die;
            }
        }

    }
}

$application = new CApplication();

if (!empty($_REQUEST)) {

    $application->start();

    if ($application->is_ajax_mode) $application->manageAjax($_REQUEST['action'], $_REQUEST);
    else {
        if ($application->is_background_mode) $application->processBackgroundData();
        else $application->getData();
    }
}
?>