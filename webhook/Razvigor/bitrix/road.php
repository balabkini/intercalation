<?

require_once("tools.php");
require_once("log.php");

class CApplication
{
    public $arB24App;
    public $arAccessParams = array();
    public $currentUser = 0;
    private $b24_error = '';
    public $is_ajax_mode = false;
    public $is_background_mode = false;
    public $allCountDeal = 0;
    public $allCountDealAll = 0;
    public $marsh = array();
    public $roadtype = 0;
public $prioryDeal = 0;

    function __construct()
    {
        //Симферополь - Керчь
        $this->marsh[1] = array(
            'Симферополь',
            'Белогорск',
            'Богатое',
            'Тополевка',
            'Грушевка',
            'Старый Крым',
            'Первомайское',
            'Насыпное',
            'Феодосия',
            'ПервомайскоеКерч',
            'Батальное',
            'Ерофеево',
            'Луговое',
            'Ленино',
            'Керчь'
        );
        //Симферополь - Ялта

        $this->marsh[2] = array(
            'Симферополь',
            'Пионерское',
            'Доброе',
            'Перевальное',
            'Привольное',
            'Верхняя Кутузовка',
            'Нижняя Кутузовка',
            'Изобильное',
            'Алушта',
            'Виноградный',
            'Малый маяк',
            'Запрудное',
            'Партенит',
            'Краснокаменка',
            'Гурзуф',
            'Никита',
            'Восход',
            'Отрадное',
            'Массандра',
            'Ялта'
        );
        //Симферополь - Севастополь
        $this->marsh[3] = array(
            'Симферополь',
            'Чистенькое',
            'Почтовое',
            'Скалистое',
            'Бахчисарай',
            'Железнодорожное',
            'Холмовка',
            'Инкерман',
            'Севастополь',
        );
        //Симферополь - Евпатория

        $this->marsh[4] = array(
            'Симферополь',
            'Родниково',
            'Новый Мир',
            'Школьное',
            'Скворцово',
            'Чеботарка',
            'Орехово',
            'Саки',
            'Прибрежное',
            'Евпатория',
        );
        //Симферополь - Джанкой
        $this->marsh[5] = array(
            'Симферополь',
            'Комсомольское',
            'Журавлёвка',
            'Новоандреевка',
            'Амурское',
            'Октябрьское',
            'Пятихатка',
            'Полтавка',
            'Красный партизан',
            'Некрасово',
            'Марьяновка',
            'Щербаково',
            'Красногвардейское',
            'Петровка',
            'Вольное',
            'Краснодольское',
            'Серноводское',
            'Тимирязево',
            'Рощино',
            'Ближнегородское',
            'Тимофеево',
            'Новостепное',
            'Константиновка',
            'Джанкой'
        );

        //Симферополь - Армянск
        $this->marsh[6] = array(
            'Симферополь',
            'Журавлевка',
            'Трактовое',
            'Сизовка ',
            'Стахановка',
            'Каштановка',
            'Черново',
            'Войково',
            'Сары-Баш',
            'Гвардейское',
            'Гришино',
            'Степное',
            'ПервомайскоеАрмянск',
            'Ильинка',
            'Воронцовка',
            'Красноперекопск',
            'Армянск',
        );
        $this->marsh[7] = array(
            'СимферопольМ'
        );
    }


    private function checkB24Auth()
    {

        // проверяем актуальность доступа
        $isTokenRefreshed = false;

        // $arAccessParams['access_token'] = '123';
        // $arAccessParams['refresh_token'] = '333';

        $this->arB24App = getBitrix24($this->arAccessParams, $isTokenRefreshed, $this->b24_error);
        return $this->b24_error === true;
    }

    private function returnJSONResult($answer)
    {

        ob_start();
        ob_end_clean();
        Header('Cache-Control: no-cache');
        Header('Pragma: no-cache');
        echo json_encode($answer);
        die();
    }

    private function getYesterday()
    {
        $result = new DateTime();
        $result->add(DateInterval::createFromDateString('yesterday'));
        return $result->format('Y-m-d');
    }


    public function cmd($a, $b)
    {

        if ($this->prioryDeal>0) {
            $k = array_search($a["city"], $this->marsh[$this->roadtype]); // $key = 2;
            $k2 = array_search($b["city"], $this->marsh[$this->roadtype]); // $key = 2;

            return ($k2 > $k) ? +1 : -1;
        }
        else{
            $k = array_search($a["city"], $this->marsh[$this->roadtype]); // $key = 2;
            $k2 = array_search($b["city"], $this->marsh[$this->roadtype]); // $key = 2;

            return ($k2 > $k) ? -1 : +1;
        }



    }

    public function manageAjax($operation, $params)
    {
        switch ($operation) {

            case 'updateDeal':

         
                if($params['dealList']){
                    $obB24Deal = new \Bitrix24\Bitrix24deal\Bitrix24deal($this->arB24App);

                    foreach ($params['dealList'] as $deal){
                       $result[]= $obB24Deal->updateDeal($deal,array('STAGE_ID'=>2));
                    }
                }


                file_put_contents(
                    "event.log",
                    var_export($result, 1)."\n Время".time()."\n все что пришло \n",
                    FILE_APPEND
                );

                $this->returnJSONResult(array('status' => 'succes', 'result' => $result));


                break;

            case 'getRoad':
                $this->roadtype = $params['roadtype'];

                $obB24Batch = new \Bitrix24\Bitrix24Batch\Bitrix24Batch($this->arB24App);
                $arDealFilter = array(
                    "STAGE_ID" => "1",
                );
                $obB24Batch->addDealListCall(0,
                    array(),
                    array('UF_CRM_1482242797', 'UF_CRM_1482241679', 'TITLE', 'UF_CRM_1482481266', 'ASSIGNED_BY_ID', 'COMPANY_ID', 'OPPORTUNITY'),
                    $arDealFilter
                );
                $res = $obB24Batch->call();
                $totalSumm = 0;
                foreach ($res[0]['data'] as $deal) {
                    $companyList[] = $deal['COMPANY_ID'];
                    $AssignedList[] = $deal['ASSIGNED_BY_ID'];

                }
                if ($companyList) {
                    $obB24Batch->addCompanyListCall(1,
                        array(),
                        array('PHONE','ADDRESS_CITY','ADDRESS','TITLE'),
                        array('ID' => $companyList)
                    );
                    $res = $obB24Batch->call();

                    foreach ($res[1]['data'] as $comp) {
                        $dealCity[] = $comp['ADDRESS_CITY'];


                        foreach ($res[0]['data'] as $k => $deal) {

                            if ($deal['COMPANY_ID'] == $comp['ID']) {
                                if ($deal['ID'] == $params['prioryDeal']) {

                                    $lastKey = array_search($comp['ADDRESS_CITY'], $this->marsh[$this->roadtype]); // $key = 2;

                                }

                                if (in_array($comp['ADDRESS_CITY'], $this->marsh[$this->roadtype])) {

                                    if ($params['prioryDeal'] > 0) {
                                        $this->prioryDeal = $params['prioryDeal'];
                                        $nowKey = array_search($comp['ADDRESS_CITY'], $this->marsh[$this->roadtype]);

                                        if ($nowKey <= $lastKey) {
                                            if($params['prioryDeal']==$deal['ID']){
                                                $res[0]['data'][$k]['priory'] = 'Высокий';
                                            }
                                            else{
                                                $res[0]['data'][$k]['priory'] = 'средний';
                                            }
                                            $totalSumm = $totalSumm + $deal['OPPORTUNITY'];
                                            $res[0]['data'][$k]['city'] = $comp['ADDRESS_CITY'];
                                            $res[0]['data'][$k]['CompanyName'] = $comp['TITLE'];
                                            $res[0]['data'][$k]['phone'] = $comp['PHONE'][0]['VALUE'];
                                            $res[0]['data'][$k]['address'] = $comp['ADDRESS'];
                                            $res[0]['data'][$k]['contFace'] = $deal['UF_CRM_1482241679'];
                                            //$res[0]['data'][$k]['OPPORTUNITY'] = $deal['OPPORTUNITY'];
                                            $dealList[] = $res[0]['data'][$k];

                                        }


                                    } else {
                                        $totalSumm = $totalSumm + $deal['OPPORTUNITY'];
                                        $res[0]['data'][$k]['city'] = $comp['ADDRESS_CITY'];
                                        $res[0]['data'][$k]['CompanyName'] = $comp['TITLE'];
                                        $res[0]['data'][$k]['phone'] = $comp['PHONE'][0]['VALUE'];
                                        $res[0]['data'][$k]['address'] = $comp['ADDRESS'];
                                        $res[0]['data'][$k]['priory'] = 'средний';
                                        $res[0]['data'][$k]['contFace'] = $deal['UF_CRM_1482241679'];
                                        //$res[0]['data'][$k]['OPPORTUNITY'] = $deal['OPPORTUNITY'];
                                        $dealList[] = $res[0]['data'][$k];
                                    }


                                }


                            }

                        }
                    }

                }
                if ($AssignedList) {
                    $obB24Batch->addUserListCall(2,
                        array(),
                        array()
                    );
                    $res = $obB24Batch->call();

                    foreach ($res[2]['data'] as $user) {
                        foreach ($dealList as $k => $deal) {
                            if ($deal['ASSIGNED_BY_ID'] == $user['ID']) {
                                $dealList[$k]['assignet_name'] = $user['NAME'];
                            }

                        }
                    }
                }


                usort($dealList, array("CApplication", "cmd"));


                $this->returnJSONResult(array('status' => 'succes', 'totalSumm' => $totalSumm, 'result' => $dealList));

                break;



                break;
            default:
                $this->returnJSONResult(array('status' => 'error', 'result' => 'unknown operation'));
        }
    }

    public function getConversion($all, $val)
    {
        $result = ($val / $all) * 100;
        $result = number_format($result, 1, ',', ' ');
        return $result;
    }


    public function getPercent($num)
    {

        $result = ($num / $this->allCountDeal) * 100;
        $result = number_format($result, 1, ',', ' ');
        return $result;
    }

    public function getData()
    {

        $obB24User = new \Bitrix24\Bitrix24User\Bitrix24User($this->arB24App);
        $arCurrentB24User = $obB24User->current();
        $this->currentUser = $arCurrentB24User["result"]["ID"];

        $users = $this->getUsers();

        $arDealFilter = array(
            "ASSIGNED_BY_ID" => $this->currentUser,
            "CLOSED" => 'Y'/*,
			">=DATE_MODIFY": yesterday,
			"<DATE_MODIFY": today */
        );

        $obB24Batch = new \Bitrix24\Bitrix24Batch\Bitrix24Batch($this->arB24App);
        $obB24Batch->addDealListCall(0,
            array("DATE_CREATE" => "ASC"),
            array("ID", "TITLE", "OPPORTUNITY", "ASSIGNED_BY_ID"),
            $arDealFilter
        );

        $arUserIDs = array();
        foreach ($users as $arUser)
            $arUserIDs[] = $arUser["ID_USER"];

        $obB24Batch->addUserListCall(1,
            array("ID" => $arUserIDs)
        );

        $res = $obB24Batch->call();

        foreach ($users as $key => $arUser)
            $this->arRatingUsers[$arUser["ID_USER"]] = $arUser;

        $this->currentRating = isset($this->arRatingUsers[$this->currentUser]["ID_RATING_ITEM"]) ? $this->arRatingUsers[$this->currentUser]["ID_RATING_ITEM"] : 0;

        foreach ($res[1]["data"] as $arUser)
            $this->arRatingUsers[$arUser["ID"]] = array_merge(
                $this->arRatingUsers[$arUser["ID"]],
                array(
                    "FIRST_NAME" => $arUser["NAME"],
                    "LAST_NAME" => $arUser["LAST_NAME"],
                    "SECOND_NAME" => $arUser["SECOND_NAME"],
                    "PERSONAL_PHOTO" => $arUser["PERSONAL_PHOTO"])
            );

        $new_deal_sum = 0;

        foreach ($res[0]["data"] as $arDeal)
            $new_deal_sum += $arDeal["OPPORTUNITY"];

        $this->arRatingUsers[$this->currentUser]["RATE_SUM"] = $new_deal_sum;

    }


    public function start()
    {

        $this->is_ajax_mode = isset($_REQUEST['action']);
        $this->is_background_mode = isset($_REQUEST['background']);

        if ($this->is_background_mode) $this->getAuthFromDB();
        else {
            if (!$this->is_ajax_mode)
                $this->arAccessParams = prepareFromRequest($_REQUEST['auth']);
            else

                $this->arAccessParams = $_REQUEST['auth'];

            $this->b24_error = $this->checkB24Auth();

            if ($this->b24_error != '') {
                if ($this->is_ajax_mode)
                    $this->returnJSONResult(array('status' => 'error', 'result' => $this->b24_error));
                else
                    echo "B24 error: " . $this->b24_error;

                die;
            }
        }

    }
}

$application = new CApplication();

if (!empty($_REQUEST)) {

    $application->start();

    if ($application->is_ajax_mode) $application->manageAjax($_REQUEST['action'], $_REQUEST);
    else {
        if ($application->is_background_mode) $application->processBackgroundData();
        else $application->getData();
    }
}
?>