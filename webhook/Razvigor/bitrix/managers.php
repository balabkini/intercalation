<?

require_once("tools.php");
require_once("log.php");

class CApplication
{
    public $arB24App;
    public $arAccessParams = array();
    public $arRatingUsers = array();
    public $currentUser = 0;
    private $b24_error = '';
    public $is_ajax_mode = false;
    public $is_background_mode = false;
    public $currentRating = 0;
    public $allCountDeal = 0;
    public $allCountDealAll = 0;

    private function checkB24Auth()
    {

        // проверяем актуальность доступа
        $isTokenRefreshed = false;

        // $arAccessParams['access_token'] = '123';
        // $arAccessParams['refresh_token'] = '333';

        $this->arB24App = getBitrix24($this->arAccessParams, $isTokenRefreshed, $this->b24_error);
        return $this->b24_error === true;
    }

    private function returnJSONResult($answer)
    {

        ob_start();
        ob_end_clean();
        Header('Cache-Control: no-cache');
        Header('Pragma: no-cache');
        echo json_encode($answer);
        die();
    }

    private function getYesterday()
    {
        $result = new DateTime();
        $result->add(DateInterval::createFromDateString('yesterday'));
        return $result->format('Y-m-d');
    }

    public function manageAjax($operation, $params)
    {
        switch ($operation) {
            case 'getAllDeal':
                $obB24Batch = new \Bitrix24\Bitrix24Batch\Bitrix24Batch($this->arB24App);
                $arDealFilter = array(
                    ">=DATE_CREATE" => $params['dateStart'],
                    "<=DATE_CREATE" => $params['dateEnd'],
                    "STAGE_ID" => "WON",
                    //">LEAD_ID"=>0
                );

                $obB24Batch->addDealListCall(0,
                    array(),
                    array(),
                    $arDealFilter
                );
                $arDealFilter2 = array(
                    ">=DATE_CREATE" => $params['dateStart'],
                    "<=DATE_CREATE" => $params['dateEnd'],
                );
                $obB24Batch->addDealListCall(1,
                    array(),
                    array(),
                    $arDealFilter2
                );
                $obB24Batch->addStatusListCall(2,
                    array(),
                    array("ENTITY_ID" => "DEAL_STAGE")
                );
                $res = $obB24Batch->call();





                $leadAssigned = array();
//die(print_r($res[2]["data"]));
                foreach ($res[2]["data"] as $status) {
                    if ($status['EXTRA']['SEMANTICS'] == 'failure' or $status['EXTRA']['SEMANTICS'] == 'apology') {
                        $Statuses['error'][] = $status['STATUS_ID'];
                    } elseif ($status['EXTRA']['SEMANTICS'] == 'process') {
                        $Statuses['process'][] = $status['STATUS_ID'];
                    }
                }


                foreach ($res[0]["data"] as $assigned) {
                    $leadAssigned['assignedIdCount'][$assigned['ASSIGNED_BY_ID']] = $leadAssigned['assignedIdCount'][$assigned['ASSIGNED_BY_ID']] + 1;
                    $DealIds[] =  $assigned['LEAD_ID'];
                }

                foreach ($res[1]["data"] as $assigned) {
                    $leadAssigned['assignedIdCountAll'][$assigned['ASSIGNED_BY_ID']] = $leadAssigned['assignedIdCountAll'][$assigned['ASSIGNED_BY_ID']] + 1;
                    if(!$res[0]["data"]){
                        $leadAssigned['assignedIdCount'][$assigned['ASSIGNED_BY_ID']] = 0;
                    
                    }

                    if (in_array($assigned['STAGE_ID'], $Statuses['process'])) {
                        $leadAssigned['assignedIdCountProcess'][$assigned['ASSIGNED_BY_ID']] = $leadAssigned['assignedIdCountProcess'][$assigned['ASSIGNED_BY_ID']] + 1;

                    } elseif (in_array($assigned['STAGE_ID'], $Statuses['error'])) {
                        $leadAssigned['assignedIdCountError'][$assigned['ASSIGNED_BY_ID']] = $leadAssigned['assignedIdCountError'][$assigned['ASSIGNED_BY_ID']] + 1;

                    }
                }
                $keyDiff =  array_diff_ukey($leadAssigned['assignedIdCountAll'],$leadAssigned['assignedIdCount'],'key_compare_func');

                foreach ($keyDiff as $key=>$val){
                    $leadAssigned['assignedIdCount'][$key] = $val;
                }
                foreach ($leadAssigned['assignedIdCount'] as $key => $user) {
                    $this->allCountDeal = $this->allCountDeal + $user;
                    $filterUsers[] = $key;
                }
                foreach ($leadAssigned['assignedIdCountAll'] as $key => $user) {
                    foreach ($leadAssigned['assignedIdCount'] as $k => $val) {
                        if ($key == $k) {

                            $conversionUsers[$k] = $this->getConversion($user, $val);
                            $assignedIdCountAll[$key] = $user;
                        }


                    }

                }

                $obB24Batch->addUserListCall(0,
                    array("ID" => $filterUsers)
                );
                $res = $obB24Batch->call();

                foreach ($res[0]['data'] as $user) {
                    foreach ($leadAssigned['assignedIdCount'] as $key => $userId) {
                        if ($user["ID"] == $key) {
                            $conversion = $conversionUsers[$key];
                            ($processStatus = $leadAssigned['assignedIdCountProcess'][$key]) ? : $processStatus=0;
                            ($errorStatus = $leadAssigned['assignedIdCountError'][$key]) ? : $errorStatus=0;
                            ($assignedIdCountAll[$key]>0) ? : $assignedIdCountAll[$key]=0;
                            ($conversion>0) ? : $conversion=0;



                            $endResult['row'][] = array('name' => $user["NAME"] . ' ' . $user["LAST_NAME"], 'error' => $errorStatus, 'process' => $processStatus, 'allcount' => $assignedIdCountAll[$key], 'count' => $userId, 'conversion' => $conversion, 'percent' => $this->getPercent($userId));
                        }
                    }
                }

                $endResult['allcount'] = $this->allCountDeal;
                $endResult['allcountAll'] = count($res[1]["data"]);
                $endResult['iddeals'] = $DealIds;
                if(!count($endResult['row'])>0){
                    $endResult['row'] = '';
                }
                $this->returnJSONResult(array('status' => 'succes', 'result' => $endResult));




                function key_compare_func($key1, $key2)
                {
                    if ($key1 == $key2)
                        return 0;
                    else if ($key1 > $key2)
                        return 1;
                    else
                        return -1;
                }

                break;
            case 'getAllLead':
                function key_compare_func($key1, $key2)
                {
                    if ($key1 == $key2)
                        return 0;
                    else if ($key1 > $key2)
                        return 1;
                    else
                        return -1;
                }
                $obB24Batch = new \Bitrix24\Bitrix24Batch\Bitrix24Batch($this->arB24App);
                $arLeadFilter = array(
                    ">=DATE_CLOSED" => $params['dateStart'],
                    "<=DATE_CLOSED" => $params['dateEnd'],
                    "STATUS_ID" => "CONVERTED"
                );
                $arLeadFilter2 = array(
                    ">=DATE_CLOSED" => $params['dateStart'],
                    "<=DATE_CLOSED" => $params['dateEnd'],
                );


                $obB24Batch->addLeadListCall(0,
                    array(),
                    array(),
                    $arLeadFilter
                );
                $obB24Batch->addLeadListCall(1,
                    array(),
                    array(),
                    $arLeadFilter2
                );
                $obB24Batch->addStatusListCall(2,
                    array(),
                    array("ENTITY_ID" => "STATUS")
                );
                $res = $obB24Batch->call();
           
                $leadAssigned = array();

                foreach ($res[2]["data"] as $status) {
                    if ($status['EXTRA']['SEMANTICS'] == 'failure' or $status['EXTRA']['SEMANTICS'] == 'apology') {
                        $Statuses['error'][] = $status['STATUS_ID'];
                    } elseif ($status['EXTRA']['SEMANTICS'] == 'process') {
                        $Statuses['process'][] = $status['STATUS_ID'];
                    }
                }

                foreach ($res[0]["data"] as $assigned) {
                    $leadAssigned['assignedIdCount'][$assigned['ASSIGNED_BY_ID']] = $leadAssigned['assignedIdCount'][$assigned['ASSIGNED_BY_ID']] + 1;
                    $leadIds[] = $assigned['ID'];

                }

                foreach ($res[1]["data"] as $assigned) {
                    $leadAssigned['assignedIdCountAll'][$assigned['ASSIGNED_BY_ID']] = $leadAssigned['assignedIdCountAll'][$assigned['ASSIGNED_BY_ID']] + 1;
                  if($assigned['STATUS_ID']=="WIN" or $assigned['STATUS_ID']=="CONVERTED"){
                      $leadAssigned['assignedIdCountWin'][$assigned['ASSIGNED_BY_ID']] = $leadAssigned['assignedIdCountWin'][$assigned['ASSIGNED_BY_ID']] + 1;
                  }
                    if (in_array($assigned['STATUS_ID'], $Statuses['process'])) {
                        $leadAssigned['assignedIdCountProcess'][$assigned['ASSIGNED_BY_ID']] = $leadAssigned['assignedIdCountProcess'][$assigned['ASSIGNED_BY_ID']] + 1;

                    } elseif (in_array($assigned['STATUS_ID'], $Statuses['error'])) {
                        $leadAssigned['assignedIdCountError'][$assigned['ASSIGNED_BY_ID']] = $leadAssigned['assignedIdCountError'][$assigned['ASSIGNED_BY_ID']] + 1;

                    }
                }

                $keyDiff =  array_diff_ukey($leadAssigned['assignedIdCountAll'],$leadAssigned['assignedIdCount'],'key_compare_func');

                foreach ($keyDiff as $key=>$val){
                    $leadAssigned['assignedIdCount'][$key] = $val;
                }
                foreach ($leadAssigned['assignedIdCount'] as $key => $user) {
                    $this->allCountDeal = $this->allCountDeal + $user;
                    $filterUsers[] = $key;
                }

                foreach ($leadAssigned['assignedIdCountAll'] as $key => $user) {
                    foreach ($leadAssigned['assignedIdCount'] as $k => $val) {
                        if ($key == $k) {

                            $conversionUsers[$k] = $this->getConversion($user, $val);
                            $assignedIdCountAll[$key] = $user;
                        }


                    }

                }


                $obB24Batch->addUserListCall(0,
                    array("ID" => $filterUsers)
                );
                $res = $obB24Batch->call();


                foreach ($res[0]['data'] as $user) {
                    foreach ($leadAssigned['assignedIdCount'] as $key => $userId) {
                        if ($user["ID"] == $key) {
                            $conversion = $conversionUsers[$key];
                            ($processStatus = $leadAssigned['assignedIdCountProcess'][$key]) ? : $processStatus=0;
                            ($errorStatus = $leadAssigned['assignedIdCountError'][$key]) ? : $errorStatus=0;
                            ($Win = $leadAssigned['assignedIdCountWin'][$key]) ? : $Win=0;
                            $endResult['row'][] = array('name' => $user["NAME"] . ' ' . $user["LAST_NAME"], 'error' => $errorStatus, 'process' => $processStatus, 'allcount' => $assignedIdCountAll[$key], 'count' => $Win, 'conversion' => $conversion, 'percent' => $this->getPercent($userId));
                        }
                    }
                }

if(!count($endResult['row'])>0){
    $endResult['row'] = '';
}
                $endResult['allcount'] = $this->allCountDeal;
                $endResult['allcountAll'] = count($res[1]["data"]);
                $endResult['idleads'] = $leadIds;

                $this->returnJSONResult(array('status' => 'succes', 'result' => $endResult));

                break;
            default:
                $this->returnJSONResult(array('status' => 'error', 'result' => 'unknown operation'));
        }
    }

    public function getConversion($all, $val)
    {
        $result = ($val / $all) * 100;
        $result = number_format($result, 1, ',', ' ');
        return $result;
    }




    public function getPercent($num)
    {

        $result = ($num / $this->allCountDeal) * 100;
        $result = number_format($result, 1, ',', ' ');
        return $result;
    }

    public function getData()
    {

        $obB24User = new \Bitrix24\Bitrix24User\Bitrix24User($this->arB24App);
        $arCurrentB24User = $obB24User->current();
        $this->currentUser = $arCurrentB24User["result"]["ID"];

        $users = $this->getUsers();

        $arDealFilter = array(
            "ASSIGNED_BY_ID" => $this->currentUser,
            "CLOSED" => 'Y'/*,
			">=DATE_MODIFY": yesterday,
			"<DATE_MODIFY": today */
        );

        $obB24Batch = new \Bitrix24\Bitrix24Batch\Bitrix24Batch($this->arB24App);
        $obB24Batch->addDealListCall(0,
            array("DATE_CREATE" => "ASC"),
            array("ID", "TITLE", "OPPORTUNITY", "ASSIGNED_BY_ID"),
            $arDealFilter
        );

        $arUserIDs = array();
        foreach ($users as $arUser)
            $arUserIDs[] = $arUser["ID_USER"];

        $obB24Batch->addUserListCall(1,
            array("ID" => $arUserIDs)
        );

        $res = $obB24Batch->call();

        foreach ($users as $key => $arUser)
            $this->arRatingUsers[$arUser["ID_USER"]] = $arUser;

        $this->currentRating = isset($this->arRatingUsers[$this->currentUser]["ID_RATING_ITEM"]) ? $this->arRatingUsers[$this->currentUser]["ID_RATING_ITEM"] : 0;

        foreach ($res[1]["data"] as $arUser)
            $this->arRatingUsers[$arUser["ID"]] = array_merge(
                $this->arRatingUsers[$arUser["ID"]],
                array(
                    "FIRST_NAME" => $arUser["NAME"],
                    "LAST_NAME" => $arUser["LAST_NAME"],
                    "SECOND_NAME" => $arUser["SECOND_NAME"],
                    "PERSONAL_PHOTO" => $arUser["PERSONAL_PHOTO"])
            );

        $new_deal_sum = 0;

        foreach ($res[0]["data"] as $arDeal)
            $new_deal_sum += $arDeal["OPPORTUNITY"];

        $this->arRatingUsers[$this->currentUser]["RATE_SUM"] = $new_deal_sum;

    }


    public function start()
    {

        $this->is_ajax_mode = isset($_REQUEST['action']);
        $this->is_background_mode = isset($_REQUEST['background']);

        if ($this->is_background_mode) $this->getAuthFromDB();
        else {
            if (!$this->is_ajax_mode)
                $this->arAccessParams = prepareFromRequest($_REQUEST['auth']);
            else

                $this->arAccessParams = $_REQUEST['auth'];

            $this->b24_error = $this->checkB24Auth();

            if ($this->b24_error != '') {
                if ($this->is_ajax_mode)
                    $this->returnJSONResult(array('status' => 'error', 'result' => $this->b24_error));
                else
                    echo "B24 error: " . $this->b24_error;

                die;
            }
        }

    }
}

$application = new CApplication();

if (!empty($_REQUEST)) {

    $application->start();

    if ($application->is_ajax_mode) $application->manageAjax($_REQUEST['action'], $_REQUEST);
    else {
        if ($application->is_background_mode) $application->processBackgroundData();
        else $application->getData();
    }
}
?>