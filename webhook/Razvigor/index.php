<?php
?>
<!DOCTYPE html>
    <html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="css/manager.css?6">
    <link rel="stylesheet" href="css/font-awesome-4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="plugins/bootstrap/daterangepicker/daterangepicker.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <style media='print' type='text/css'>
        #navbar-iframe {display: none; height: 0px; visibility: hidden;}
        .noprint {display: none;}
        body {background:#FFF; color:#000;}
        a {text-decoration: underline; color:#00F;}
        }
    </style>
</head>
<body>


<div class="col-md-12 noprint">
        <label class="spNam" for="">Выберите маршрут</label>
        <div class="inputsBlock col-md-4">
            <div class="inner-addon right-addon">
                <select name="road" id="road">
                    <option value="7">СимферопольМ</option>
                    <option value="1">Симферополь - Керчь</option>
                    <option value="2">Симферополь - Ялта</option>
                    <option value="3">Симферополь - Севастополь</option>
                    <option value="4">Симферополь - Евпатория</option>
                    <option value="5">Симферополь - Джанкой</option>
                    <option value="6">Симферополь - Армянск</option>
                </select>
            </div>

        </div>
    <label for="dealnumber">Укажите приоритетную сделку</label> <input name="dealnumber" id="dealnumber" type="text">
<!--    <label for="CompanyList">Укажите компанию</label>-->
<!--    <select name="CompanyList" id="CompanyList">-->
<!--        <option value="1"></option>-->
<!--        <option value="2"></option>-->
<!--        <option value="3"></option>-->
<!--        <option value="4"></option>-->
<!--        <option value="5"></option>-->
<!--        <option value="6"></option>-->
<!--    </select>-->



<!--    <div class="col-md-3">-->
        <button class="btn btn-success" onclick="road.getList()" id="getDate">Сформировать список</button>
<!--    </div>-->
</div>
<div class="col-md-12">
    <hr>
    <h2 class="LostTitle">Маршрут</h2>
    <div class="totalBlock">
        На сумму <span class="totalsumm"></span>
    </div>

    <table class="table" id="roadList">
<thead>
<th>Приоритет</th><th>Сделка</th><th>Город</th><th>Компания</th><th>Контактное лицо</th><th>Ответственный за заявку</th><th>Комментарий</th><th>Товары</th>
</thead>
        <tbody>

        </tbody>
    </table>
    <textarea name="" id="" cols="50" placeholder="Комментарий" rows="5"></textarea>
    <a onclick="road.updateDeal()" href='javascript:window.print(); void 0;'>Печать</a>
</div>






<script src="//api.bitrix24.com/api/v1/"></script>
<script src="plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="js/moment.min.js"></script>
<script src="js/road.js?7"></script>



<script>
    $(document).ready(function () {
//        var windwoSize = BX24.getScrollSize();
//        BX24.resizeWindow(windwoSize.scrollWidth,900);
        BX24.init(function(){

            road = new roadmap();

            road.auth =BX24.getAuth();
            road.init();

        });
    });
</script>


</body>
</html>
