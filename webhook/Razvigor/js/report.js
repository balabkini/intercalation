function report() {
this.AjaxResult ='';
 this.url ='bitrix/application.php';
 this.preloader = $('#page-preloader');

}


report.prototype.init = function () {
 $('#startDate').daterangepicker({
  "autoApply": true,
  "locale": {
   "format": "DD.MM.YYYY",
   "separator": " - ",
   "applyLabel": "Выбрать",
   "cancelLabel": "Закрыть",
   "fromLabel": "От",
   "toLabel": "В",
   "customRangeLabel": "свободная",
   "weekLabel": "W",
   "daysOfWeek": [
    "Вс",
    "Пн",
    "Вт",
    "Ср",
    "Чт",
    "Пт",
    "Сб"
   ],
   "monthNames": [
    "Январь",
    "Февраль",
    "Март",
    "Апрель",
    "Май",
    "Июнь",
    "Июль",
    "Август",
    "Сентябрь",
    "Октябрь",
    "Ноябрь",
    "Декабрь"
   ],
   "firstDay": 1
  },
 });
 $('#endDate').daterangepicker({
  "autoApply": true,
  "locale": {
   "format": "DD.MM.YYYY",
   "separator": " - ",
   "applyLabel": "Выбрать",
   "cancelLabel": "Закрыть",
   "fromLabel": "От",
   "toLabel": "В",
   "customRangeLabel": "свободная",
   "weekLabel": "W",
   "daysOfWeek": [
    "Вс",
    "Пн",
    "Вт",
    "Ср",
    "Чт",
    "Пт",
    "Сб"
   ],
   "monthNames": [
    "Январь",
    "Февраль",
    "Март",
    "Апрель",
    "Май",
    "Июнь",
    "Июль",
    "Август",
    "Сентябрь",
    "Октябрь",
    "Ноябрь",
    "Декабрь"
   ],
   "firstDay": 1
  },
  "opens": "left",
 });


 if(this.auth){
  this.defaultDate();
 }
};



report.prototype.defaultDate =function () {
 var dateStart = moment().subtract('months', 2);
 dateStart =  dateStart.date(1);
 dateStart = moment(dateStart).format("DD.MM.YYYY");
 var endDate = moment().subtract('months', 1);
 endDate.date(1);
 // endDate.subtract(1, 'day');
 endDate = moment(endDate).format("DD.MM.YYYY");
 $('#startDate').val(dateStart+'-'+endDate);
  var prePreMonth = rep.getDataPeriod(endDate,dateStart);
 var prePreMonthDeal = rep.getDataPeriodDeal(endDate,dateStart);
 dateStart = moment().subtract('months', 1);
 dateStart =  dateStart.date(1);
 dateStart = moment(dateStart).format("DD.MM.YYYY");
 endDate = moment();
 endDate.date(1);
 // endDate.subtract(1, 'day');
 endDate = moment(endDate).format("DD.MM.YYYY")
 $('#endDate').val(dateStart+'-'+endDate);
 var preMonth = rep.getDataPeriod(endDate,dateStart);
 var preMonthDeal = rep.getDataPeriodDeal(endDate,dateStart);
rep.renderMainTable('mainLeadStats',prePreMonth,preMonth);
 rep.renderMainTable('mainDealStats',prePreMonthDeal,preMonthDeal);
 rep.renderChart('LostChartLead','LostChartPieLead',prePreMonth,preMonth);
 rep.renderChart('LostChartDeal','LostChartPieDeal',prePreMonthDeal,preMonthDeal);
 $('#endDate').val(dateStart+'-'+endDate);

 rep.preloader.delay(1000).fadeOut('slow');
}

report.prototype.renderMainTable =function (table,prePreMonth,preMonth) {
 $('.'+table+' tbody').empty();
 $('.'+table+' tfoot').empty();
 var td ='';
 $.each(prePreMonth.row,function (index,value) {
  td+='<tr><td>'+value.name+'</td><td>'+value.count+'</td><td>'+value.percent+'%</td><td>'+preMonth.row[index].count+'</td><td>'+preMonth.row[index].percent+'%</td><td>'+rep.getDinamoc(preMonth.row[index].count,value.count)+'</td></tr>'
 });
 var total='<tr><td>Итого</td><td>'+prePreMonth.allcount+'</td><td>100%</td><td>'+preMonth.allcount+'</td><td>100%</td><td>'+rep.getDinamoc(preMonth.allcount,prePreMonth.allcount)+'</td></tr>'
 $('.'+table+' tbody').append(td);
 $('.'+table+' tfoot').append(total);
 $('.'+table).trigger("update");
 var sorting = [[4,1]];
 $('.'+table).trigger("sorton", [sorting]);
};

report.prototype.getDinamoc =function (count,parentCount) {
 var result =parseInt(((count/parentCount)*100)-100);
 if(isNaN(result)){
  result = '-'
 }
 if(result>0){
  result= '+'+result;
  result = result+'% <i class="arrow red fa fa-arrow-down" aria-hidden="true"></i>'
 }
 else if(result=='-'){

 }
 else if(result==0){
  result = '-';
 }
 else{
  result = result+'% <i class="arrow green fa fa-arrow-up" aria-hidden="true"></i>'
 }

 return result;
}


report.prototype.getDataPeriod = function (endDate,dateStart) {
 if (!dateStart && !dateEnd) {
 }
 else {

  var param= {
   action: 'getAllLead',
   dateStart: dateStart,
   dateEnd: endDate,
   auth:rep.auth,
  }
  this.sendAjax(param)
  return this.AjaxResult.result;
 }
};
report.prototype.getDataPeriodDeal = function (endDate,dateStart) {
 if (!dateStart && !dateEnd) {
 }
 else {
  var param= {
   action: 'getAllDeal',
   dateStart: dateStart,
   dateEnd: endDate,
   auth:rep.auth,
  }
  this.sendAjax(param)
  return this.AjaxResult.result;
 }
};



report.prototype.dateChanged = function (picker,endDate,dateStart) {
 $('.tablerender'+picker+' tbody').empty();
 $('.count'+picker).empty();
 $(this).datepicker('hide');
 if (!dateStart && !dateEnd) {
 }
 else {
  var param= {
   action: 'getAllLead',
   dateStart: dateStart,
   dateEnd: endDate,
   auth:rep.auth,
  }
  this.sendAjax(param)
  var td ='';
  $.each(this.AjaxResult.result.row,function (ind,val) {
   td+='<tr><td>'+val.name+'</td><td>'+val.count+'</td><td>'+val.percent+' </td></tr>'
  });
  $('.tablerender'+picker+' tbody').append(td);
  $('.count'+picker).append('Итого: '+this.AjaxResult.result.allcount);
  $('.tablerender'+picker).trigger("update");
  var sorting = [[2,1]];
  $('.tablerender'+picker).trigger("sorton", [sorting]);
 }
};
report.prototype.sendAjax = function (param) {
 var result = '';
 $.ajax({
  url: this.url, // куда отправляем
  type: "post", // метод передачи
  dataType: "json", // тип передачи данных
  async: false,
  beforeSend: function () {
  },
  data: param,
  success: function (data) {
   result = data;
  }
 });
 this.AjaxResult = result;
 return false;
};


report.prototype.renderChart = function (chartBlock,pieBlock,prePreData,preData) {
 var data =[];
$.each(prePreData.row,function (index,value) {
 data.push({'name':value.name,'count1':value.count,'count2':preData.row[index].count})
});

 var chart = AmCharts.makeChart(chartBlock, {
  "theme": "light",
  "type": "serial",
  "dataProvider": data,
  "valueAxes": [{
   "guides": [{
    "dashLength": 6,
    "inside": true,
    "label": "",
    "lineAlpha": 1,
    "value": 0
   }],
   "position": "left"
  }],
  "startDuration": 1,
  "graphs": [{
   "balloonText": "[[category]]: <b>[[value]]</b>",
   "fillAlphas": 0.9,
   "lineAlpha": 0.2,
   "title": "2004",
   "type": "column",
   "valueField": "count1"
  }, {
   "balloonText": "[[category]]: <b>[[value]]</b>",
   "fillAlphas": 0.9,
   "lineAlpha": 0.2,
   "title": "2005",
   "type": "column",
   "clustered":false,
   "columnWidth":0.5,
   "valueField": "count2"
  }],
  "plotAreaFillAlphas": 0.1,
  "categoryField": "name",
  "categoryAxis": {
   "labelRotation": 30,
   "gridPosition": "start"
  },
  "export": {
   "enabled": true
  }
 });
 var chart = AmCharts.makeChart(pieBlock, {
  "type": "pie",
  "theme": "light",
  "labelsEnabled": false,
  "autoMargins": false,
  "pullOutRadius": 15,
  "startDuration": 0.5,
  "innerRadius": "70%",
  "dataProvider": data,
  "valueField": "count2",
  "titleField": "name",

 });
};

