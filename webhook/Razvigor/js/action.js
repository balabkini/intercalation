$(document).on("click", "#getDate", function () {
    man.preloader.fadeIn();
        function func() {
        var firstPeriod = $('#startDate').val();
        var secondPeriod = $('#endDate').val();
        firstPeriod = firstPeriod.split("-");
        firstPeriod[0] = firstPeriod[0].replace(" ", "");
        firstPeriod[1] = firstPeriod[1].replace(" ", "");
        firstPeriod[1] = moment(firstPeriod[1], "DD.MM.YYYY").add('days', 1);
        firstPeriod[1] = firstPeriod[1].format("DD.MM.YYYY");
        secondPeriod = secondPeriod.split("-");
        secondPeriod[0] = secondPeriod[0].replace(" ", "");
        secondPeriod[1] = secondPeriod[1].replace(" ", "");
        secondPeriod[1] = moment(secondPeriod[1], "DD.MM.YYYY").add('days', 1);
        secondPeriod[1] = secondPeriod[1].format("DD.MM.YYYY");
        var prePreMonth = man.getDataPeriod(firstPeriod[1], firstPeriod[0]);
        var preMonth = man.getDataPeriod(secondPeriod[1], secondPeriod[0]);
        var prePreMonthDeal = man.getDataPeriodDeal(firstPeriod[1], firstPeriod[0]);
        var preMonthDeal = man.getDataPeriodDeal(secondPeriod[1], secondPeriod[0]);
            man.renderMainTable('mainLeadStats', prePreMonth, preMonth);
            man.renderMainTable('mainDealStats', prePreMonthDeal, preMonthDeal);
            man.renderChart('LostChartLead', 'LostChartPieLead', prePreMonth, preMonth);
            man.renderChart('LostChartDeal', 'LostChartPieDeal', prePreMonthDeal, preMonthDeal);
            man.preloader.fadeOut('slow');
    }

    setTimeout(func, 400);
    
    
});
