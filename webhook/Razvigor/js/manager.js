function manager() {
    this.AjaxResult ='';
    this.url ='bitrix/managers.php';
    this.preloader = $('#page-preloader');

}


manager.prototype.init = function () {

    $('#endDate').daterangepicker({
        "autoApply": true,
        "locale": {
            "format": "DD.MM.YYYY",
            "separator": " - ",
            "applyLabel": "Выбрать",
            "cancelLabel": "Закрыть",
            "fromLabel": "От",
            "toLabel": "В",
            "customRangeLabel": "свободная",
            "weekLabel": "W",
            "daysOfWeek": [
                "Вс",
                "Пн",
                "Вт",
                "Ср",
                "Чт",
                "Пт",
                "Сб"
            ],
            "monthNames": [
                "Январь",
                "Февраль",
                "Март",
                "Апрель",
                "Май",
                "Июнь",
                "Июль",
                "Август",
                "Сентябрь",
                "Октябрь",
                "Ноябрь",
                "Декабрь"
            ],
            "firstDay": 1
        },
        "opens": "left",
    });


    if(this.auth){
        this.defaultDate();
    }
};



manager.prototype.defaultDate =function () {
    var dateStart = moment().subtract('months', 2);
    dateStart =  dateStart.date(1);
    dateStart = moment(dateStart).format("DD.MM.YYYY");
    var endDate = moment().subtract('months', 1);
    endDate.date(1);
    // endDate.subtract(1, 'day');
    endDate = moment(endDate).format("DD.MM.YYYY");
    $('#startDate').val(dateStart+'-'+endDate);
    var prePreMonth = man.getDataPeriod(endDate,dateStart);
    var prePreMonthDeal = man.getDataPeriodDeal(endDate,dateStart);
    dateStart = moment().subtract('months', 1);
    dateStart =  dateStart.date(1);
    dateStart = moment(dateStart).format("DD.MM.YYYY");
    endDate = moment();
    endDate.date(1);
    // endDate.subtract(1, 'day');
    endDate = moment(endDate).format("DD.MM.YYYY")
    $('#endDate').val(dateStart+'-'+endDate);
    var preMonth = man.getDataPeriod(endDate,dateStart);
   var preMonthDeal = man.getDataPeriodDeal(endDate,dateStart);
    man.renderMainTable('mainLeadStats',prePreMonth,preMonth);
     man.renderMainTable('mainDealStats',prePreMonthDeal,preMonthDeal);
    man.renderChart('LostChartLead','LostChartPieLead',prePreMonth,preMonth);
     man.renderChart('LostChartDeal','LostChartPieDeal',prePreMonthDeal,preMonthDeal);

    $('.greentd').append('<i class="arrow green fa fa-arrow-up" aria-hiddenrow="true"></i>');
    $('.redtd').append('<i class="arrow red fa fa-arrow-down" aria-hiddenrow="true"></i>');



    $('#endDate').val(dateStart+'-'+endDate);

    man.preloader.delay(1000).fadeOut('slow');
}


manager.prototype.Intersec =function (a1,a2) {
    var o1={}, o2={}, diff=[], i, len, k;
    for (i=0, len=a1.length; i<len; i++) { o1[a1[i]] = true; }
    for (i=0, len=a2.length; i<len; i++) { o2[a2[i]] = true; }
    for (k in o1) { if (!(k in o2)) { diff.push(k); } }
    for (k in o2) { if (!(k in o1)) { diff.push(k); } }
    return diff;
}





manager.prototype.renderMainTable =function (table,prePreMonth,preMonth) {

    var alllead='';
   // var Intersec = this.Intersec(prePreMonth.idleads,prePreMonthDeal.iddeals);
   //  console.log(Intersec);
   //  $.each(Intersec,function (indx,vals) {
   //       alllead = '<a href="https://kino-house-msk.bitrix24.ru/crm/lead/show/' + vals + '/" target="_blank">'+vals+' </a>';
   //      $('.leadStat').append(alllead);
   //  });

    $('.'+table+' tbody').empty();
    $('.'+table+' tfoot').empty();
    var td ='';
var isManager =[];

console.log(isManager);
  // $.merge(preMonth.row, prePreMonth.row);

    console.log(prePreMonth)
    console.log(preMonth);
    if(prePreMonth.row.length==0 || preMonth.row.length==0 ){
        $('.'+table+' tbody').append('<tr><td>За выбранный период данных нет</td></tr>');
return false;

    }
    $.each(prePreMonth.row,function (index,value) {
        $.each(preMonth.row,function (index2,value2) {
            if(value2.name==value.name){
                conversion1 = conversion1+value.count;
                conversion2 = conversion2+value2.count;
                process1 =process1+value.process
                process2 =process2+value2.process;
                error1 = error1+value.error
                error2 = error2+value2.error
                convers = convers+parseFloat(value2.conversion);
                convers2 = convers2+parseFloat(value.conversion);
                percentError2 =  parseFloat(man.percent(value2.error,value2.allcount));
                percentError1 =  parseFloat(man.percent(value.error,value.allcount));
                totalPercentError1 = totalPercentError1+parseFloat(percentError1);
                totalPercentError2 = totalPercentError2+parseFloat(percentError2);

                percentProcess2 =  parseFloat(man.percent(value2.process,value2.allcount));
                percentProcess1 =  parseFloat(man.percent(value.process,value.allcount));
                totalpercentProcess1 = percentProcess1+parseFloat(percentProcess1);
                totalpercentProcess2 = percentProcess2+parseFloat(percentProcess2);
                td+='<tr><td><a href="#" class="toggle">'+value2.name+'</a></td><td class="'+man.getColorResult(value2.allcount,value.allcount)+'">'+value2.allcount+'</td><td class="'+man.getColorResult(value2.count,value.count)+'">'+value2.count+'</td><td class="'+man.getColorResult(value2.conversion,value.conversion)+'">'+value2.conversion+'%</td></td><td  class="'+man.getColorResult(percentProcess2,percentProcess1)+'">'+percentProcess2+'% / '+value2.process+'</td><td class="'+man.getColorResult(percentError1,percentError2)+'">'+percentError2+'% / '+value2.error+'</td><td class="'+man.getColorResult(value2.percent,value.percent)+'">'+value2.percent+'%</td></tr>'
                isManager.push(value2.name);
                td+='<tr class="tablesorter-childRow hidden"><td>Было</td><td class="'+man.getColorResult(value.allcount,value2.allcount)+'">'+value.allcount+'</td><td class="'+man.getColorResult(value.count,value2.count)+'">'+value.count+'</td><td class="'+man.getColorResult(value.conversion,value2.conversion)+'">'+value.conversion+'%</td><td class="'+man.getColorResult(percentProcess1,percentProcess2)+'">'+percentProcess1+'% / '+value.process+'</td><td class="'+man.getColorResult(percentError2,percentError1)+'">'+percentError1+'% / '+value.error+'</td><td class="'+man.getColorResult(value.percent,value2.percent)+'">'+value.percent+'%</td></tr>'
            }
        });
    });
//<td>'+man.getDinamoc(value.count,value.count)+'</td>
    //<td>'+man.getDinamoc(value2.count,value.count)+'</td>
    $.each(prePreMonth.row,function (index,value) {
        $.each(preMonth.row,function (index2,value2) {

            if ($.inArray(value2.name, isManager) == -1) {
                    console.log(value2);


                conversion2 = conversion2+value2.count;

                process2 =process2+value2.process;

                error2 = error2+value2.error
                convers = convers+parseFloat(value2.conversion);

                percentError2 =  parseFloat(man.percent(value2.error,value2.allcount));


                totalPercentError2 = totalPercentError2+parseFloat(percentError2);




                percentProcess2 =  parseFloat(man.percent(value2.process,value2.allcount));


                //totalpercentProcess2 = percentProcess2+parseFloat(percentProcess2);



                td+='<tr><td><a href="#" class="toggle">'+value2.name+'</a></td><td class="greentd">'+value2.allcount+'</td><td  class="greentd">'+value2.count+'</td><td  class="greentd">'+value2.conversion+'%</td></td><td  class="greentd">'+man.percent(value2.process,value2.allcount)+'% / '+value2.process+'</td><td  class="redtd">'+man.percent(value2.error,value2.allcount)+'% / '+value2.error+'</td><td class="greentd">'+value2.percent+'%</td></tr>'

                td+='<tr class="tablesorter-childRow hidden"><td>Было</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>'

                isManager.push(value2.name);
            }
                if ($.inArray(value.name, isManager) == -1) {
                console.log(value);
                conversion1 = conversion1+value.count;

                process1 =process1+value.process

                error1 = error1+value.error
                    convers2 = convers2+parseFloat(value.conversion);


                    percentError1 =  parseFloat(man.percent(value.error,value.allcount));
                    totalPercentError1 = totalPercentError1+parseFloat(percentError1);


                    percentProcess1 =  parseFloat(man.percent(value.process,value.allcount));
                    totalpercentProcess1 = percentProcess1+parseFloat(percentProcess1);


                td+='<tr><td><a href="#" class="toggle">'+value.name+'</a></td ><td >0</td><td >0</td><td>0%</td><td>0</td><td>0</td><td>0%</td></tr>'
                isManager.push(value.name);
                td+='<tr class="tablesorter-childRow hidden"><td>Было</td><td class="greentd">'+value.allcount+'</td><td class="greentd">'+value.count+'</td><td class="greentd">'+value.conversion+'%</td><td class="greentd">'+man.percent(value.process,value.allcount)+'% / '+value.process+'</td><td class="redtd">'+man.percent(value.error,value.allcount)+'('+value.error+')</td><td class="greentd">'+value.percent+'%</td></tr>'


            }
        });
    });



    console.log(preMonth.row.length);
    convers = (convers/preMonth.row.length);
    convers2 = (convers2/prePreMonth.row.length);

    totalPercentError1 = (totalPercentError1/prePreMonth.row.length);
    totalPercentError2 = (totalPercentError2/preMonth.row.length);
    convers= convers.toFixed(2);
    convers2 = convers2.toFixed(2);
    totalPercentError1= totalPercentError1.toFixed(2);
    totalPercentError2 = totalPercentError2.toFixed(2);


    totalpercentProcess1 = (totalpercentProcess1/prePreMonth.row.length);
    totalpercentProcess2 = (totalpercentProcess2/preMonth.row.length);

    totalpercentProcess1 = totalpercentProcess1.toFixed(2);
    totalpercentProcess2 = totalpercentProcess2.toFixed(2);
console.log(prePreMonth);
    console.log(prePreMonth.allcount);







    var total='<tr class="totalPre"><td>Итого</td><td class="'+this.getColorResult(preMonth.allcountAll,prePreMonth.allcountAll)+'">'+preMonth.allcountAll+'</td><td class="'+this.getColorResult(conversion2,conversion1)+'">'+conversion2+'</td><td class="'+this.getColorResult(convers,convers2)+'">'+convers+'%</td><td  class="'+this.getColorResult(totalpercentProcess2,totalpercentProcess1)+'">'+totalpercentProcess2+'%</td><td  class="'+this.getColorResult(totalPercentError1,totalPercentError2)+'">'+totalPercentError2+'%</td><td>100%</td></tr>'
        total+='<tr class="totalPrePre"><td>Было</td><td class="'+this.getColorResult(prePreMonth.allcountAll,preMonth.allcountAll)+'">'+prePreMonth.allcountAll+'</td><td class="'+this.getColorResult(conversion1,conversion2)+'">'+prePreMonth.allcount+'</td><td class="'+this.getColorResult(convers2,convers)+'">'+convers2+'%</td><td class="'+this.getColorResult(totalpercentProcess1,totalpercentProcess2)+'">'+totalpercentProcess1+'%</td><td class="'+this.getColorResult(totalPercentError2,totalPercentError1)+'">'+totalPercentError1+'%</td><td>100%</td></tr>'
    $('.'+table+' tbody').append(td);
    $('.'+table+' tfoot').append(total);


    
    
    $('.'+table).trigger("update");
    // var sorting = [[4,1]];
    // $('.'+table).trigger("sorton", [sorting]);









};


manager.prototype.getColorResult = function (before,after) {
    var result =''
    if(before>after){
        result = 'greentd';
    }
    else if(before==after){
        result = '';
    }
    else{
        result = 'redtd';
    }
    return result;
    
}


manager.prototype.percent =function (count,allcount) {
    var result =parseFloat(((count/allcount)*100));


    if(isNaN(result)){
        result = ''
    }
if(result>0){
    result = result.toFixed(2);
}

    return result;
}


manager.prototype.getDinamoc =function (count,parentCount) {
    var result =parseInt(((count/parentCount)*100)-100);
    if(isNaN(result)){
        result = '-'
    }
    if(result>0){
        result= '+'+result;
        result = result+'% <i class="arrow green fa fa-arrow-up" aria-hiddenrow="true"></i>'
    }
    else if(result=='-'){

    }
    else if(result==0){
        result = '-';
    }
    else{
        result = result+'% <i class="arrow red fa fa-arrow-down" aria-hiddenrow="true"></i>'
    }

    return result;
}


manager.prototype.getDataPeriod = function (endDate,dateStart) {
    if (!dateStart && !dateEnd) {
    }
    else {

        var param= {
            action: 'getAllLead',
            dateStart: dateStart,
            dateEnd: endDate,
            auth:man.auth,
        }
        this.sendAjax(param)
        return this.AjaxResult.result;
    }
};
manager.prototype.getDataPeriodDeal = function (endDate,dateStart) {
    if (!dateStart && !dateEnd) {
    }
    else {
        var param= {
            action: 'getAllDeal',
            dateStart: dateStart,
            dateEnd: endDate,
            auth:man.auth,
        }
        this.sendAjax(param)
        return this.AjaxResult.result;
    }
};



manager.prototype.dateChanged = function (picker,endDate,dateStart) {
    $('.tablerender'+picker+' tbody').empty();
    $('.count'+picker).empty();
    $(this).datepicker('hide');
    if (!dateStart && !dateEnd) {
    }
    else {
        var param= {
            action: 'getAllLead',
            dateStart: dateStart,
            dateEnd: endDate,
            auth:man.auth,
        }
        this.sendAjax(param)
        var td ='';
        $.each(this.AjaxResult.result.row,function (ind,val) {
            td+='<tr><td>'+val.name+'</td><td>'+val.count+'</td><td>'+val.percent+' </td></tr>'
        });
        $('.tablerender'+picker+' tbody').append(td);
        $('.count'+picker).append('Итого: '+this.AjaxResult.result.allcount);
        $('.tablerender'+picker).trigger("update");
        var sorting = [[2,1]];
        $('.tablerender'+picker).trigger("sorton", [sorting]);
    }
};
manager.prototype.sendAjax = function (param) {
    var result = '';
    $.ajax({
        url: this.url, // куда отправляем
        type: "post", // метод передачи
        dataType: "json", // тип передачи данных
        async: false,
        beforeSend: function () {
        },
        data: param,
        success: function (data) {
            result = data;
        }
    });
    this.AjaxResult = result;
    return false;
};


manager.prototype.renderChart = function (chartBlock,pieBlock,prePreData,preData) {
    var data =[];
    var isdata=[]
    console.log(prePreData)

    if(prePreData.allcountAll==0 || preData.allcountAll==0 ){

        return false;

    }
    $.each(prePreData.row,function (index,value) {
        $.each(preData.row,function (index2,value2) {
            if(value2.name==value.name){
                data.push({'name':value.name,'count1':value.count,'count2':value2.count})
                isdata.push(value.name);
            }
        });
    });

    $.each(prePreData.row,function (index,value) {
        $.each(preData.row,function (index2,value2) {
            if ($.inArray(value2.name, isdata) == -1) {
                data.push({'name':value2.name,'count1':0,'count2':value2.count})

                isdata.push(value2.name);
            }
             if ($.inArray(value.name, isdata) == -1) {
                data.push({'name':value.name,'count1':value.count,'count2':0})
                isdata.push(value.name);
            }
        });
    });



    var chart = AmCharts.makeChart(chartBlock, {
        "theme": "light",
        "type": "serial",
        "dataProvider": data,
        "valueAxes": [{
            "guides": [{
                "dashLength": 6,
                "inside": true,
                "label": "",
                "lineAlpha": 1,
                "value": 0
            }],
            "position": "left"
        }],
        "startDuration": 1,
        "graphs": [{
            "balloonText": "<b></b> [[category]]: <b>[[value]]</b>",
            "fillAlphas": 0.9,
            "lineAlpha": 0.2,
            "title": "2004",
            "type": "column",
            "valueField": "count1"
        }, {
            "balloonText": "<b></b> [[category]]: <b>[[value]]</b>",
            "fillAlphas": 0.9,
            "lineAlpha": 0.2,
            "title": "2005",
            "type": "column",
            "clustered":false,
            "columnWidth":0.5,
            "valueField": "count2"
        }],
        "plotAreaFillAlphas": 0.1,
        "categoryField": "name",
        "categoryAxis": {
            "labelRotation": 30,
            "gridPosition": "start"
        },
        "export": {
            "enabled": true
        }
    });
    var chart = AmCharts.makeChart(pieBlock, {
        "type": "pie",
        "theme": "light",
        "labelsEnabled": false,
        "autoMargins": false,
        "pullOutRadius": 15,
        "startDuration": 0.5,
        "innerRadius": "70%",
        "dataProvider": data,
        "valueField": "count2",
        "titleField": "name",

    });
};


$(document).on('click', '.totalPre', function () {
    $(this).next().toggle();
});
