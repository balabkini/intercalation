function roadmap() {
    this.AjaxResult = '';
    this.url = 'bitrix/road.php';
    this.preloader = $('#page-preloader');
    this.roadtype = '';
this.prioryDeal=0;
    this.dealList = [];
}


roadmap.prototype.init = function () {

};


roadmap.prototype.getList = function () {

    this.roadtype = $('#road').val();
    this.prioryDeal = $('#dealnumber').val();
    var param = {
        action: 'getRoad',
        auth: road.auth,
        roadtype: road.roadtype,
        prioryDeal:road.prioryDeal
    }
    this.sendAjax(param);
    var getProdfas = [];
    var table = ''
    var deals = this.AjaxResult.result;
    if(this.AjaxResult.result===null){
        $('#roadList tbody').html('Сделок нет');
        return false;

    }
    var totalsumm = this.AjaxResult.totalSumm;
    for (rowz in deals) {
        getProdfas.push(['crm.deal.productrows.get', {ID: deals[rowz].ID}])


    }

    BX24.callBatch(getProdfas, function (result) {
        console.log(result);
        road.dealList = [];
        for (rowz in deals) {
            deals[rowz].product = result[rowz].answer.result;
        }
        for (rowz in deals) {
            var ul = '<ul>'
            console.log(deals[rowz]);

            for (pr in deals[rowz].product) {
                ul += '<li>' + deals[rowz].product[pr].PRODUCT_NAME + ' ' + deals[rowz].product[pr].QUANTITY + ' ' + deals[rowz].product[pr].MEASURE_NAME + '</li>'
            }
            ul += '</ul>';

            for(nl in deals[rowz]){
                if(deals[rowz][nl]===null){
                    deals[rowz][nl] = '-';
                }
            }


            road.dealList.push(deals[rowz].ID);
            table += '<tr><td>'+deals[rowz].priory+'</td>' +
                '<td><a href="https://razvigor.bitrix24.ru/crm/deal/show/' + deals[rowz].ID + '/">' + deals[rowz].TITLE + '</a></td>' +
                '<td>' + deals[rowz].city + ' '+deals[rowz].address+'</td><td>'+deals[rowz].CompanyName+'</td><td>'+deals[rowz].contFace+' '+deals[rowz].phone+'</td>' +
                '<td>'+deals[rowz].assignet_name+'</td><td>'+deals[rowz].UF_CRM_1482481266+'</td><td>' + ul + '</td></tr> ';


        }
console.log(road.dealList);
        $('.totalsumm').html(totalsumm+' р.');
        $('#roadList tbody').html(table);


    });


}

roadmap.prototype.updateDeal = function () {
var arEntityBatch = [];
    for (deal in road.dealList){
        arEntityBatch.push(['crm.deal.update', {
            ID: road.dealList[deal],
            fields:{
                STAGE_ID:2
            }

        }]);
    }



    BX24.callBatch(arEntityBatch,

        function (result) {
            console.log(result);

        });


    // var param = {
    //     action: 'updateDeal',
    //     auth: road.auth,
    //     dealList:road.dealList
    // }
    // this.sendAjax(param);
}


roadmap.prototype.sendAjax = function (param) {
    var result = '';
    $.ajax({
        url: this.url, // куда отправляем
        type: "post", // метод передачи
        dataType: "json", // тип передачи данных
        async: false,
        beforeSend: function () {
        },
        data: param,
        success: function (data) {
            result = data;
        }
    });
    this.AjaxResult = result;
    return false;
};