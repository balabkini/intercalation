<?require_once (__DIR__.'/crest.php');?>

<html>
<head>
	<link rel="stylesheet" type="text/css" media="screen" href="style.css"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<title>Лид в Дело</title>
</head>
<body>


<?
$calendar1 = date('Y-m-d\T').'00:00';
$calendar2 = date('Y-m-d\T').'23:59';
$action = false;

if(isset($_POST["leadtodeal"])){
    $calendar1 = $_POST["calendar1"];
    $calendar2 = $_POST["calendar2"];
	$action = true;
}
?>


<section class="content">
<div style="background-color: #288ee2; padding-left: 5%; margin-bottom: 3%; padding-top: 1%; padding-bottom: 1%; color:azure"><h3>Фильтр</h3></div>
<div class="filter" style="margin-left: 30px;">
<form action="" method="post" >
	<input type="hidden" name="leadtodeal" value="leadtodeal">

    <div class="form-group">
		<label for="cal1">Дата начала</label>
        <input type="datetime-local" name="calendar1" id="cal1" value=<?=$calendar1?> tabindex="2" required>
	</div>
    <div class="form-group">
		<label for="cal2">Дата окончания</label>
        <input type="datetime-local" name="calendar2" id="cal2" value=<?=$calendar2?> tabindex="3" required>
    </div>

	<div class="form-group">
		<input class="btn btn-primary" type="submit" value="Создать дела">
	</div>
</form>
</div>

<div style="background-color: #288ee2; padding-left: 5%; margin-bottom: 3%; padding-top: 1%; padding-bottom: 1%; color:azure"><h3>Добавить поля из Лида в Дела</h3></div>
<div class="filter" style="margin-left: 30px;">
<form action="" method="post" >
	<input type="hidden" name="newfield" value="newfield">
	<div class="form-group">
		<input class="btn btn-primary" type="submit" value="Создать поля">
	</div>
</form>
</div>
</section>



<?
if(isset($_POST["newfield"])){
	
$result_uf = CRest::call('crm.lead.userfield.list', 
	[
		'order' => [
	],
		'filter' => [
	]
]);
$res_uf = $result_uf['result'];
/*
echo '<pre>';
print_r($res_uf);
echo '</pre>';
*/


$result_f = CRest::call('crm.lead.fields', []);
$res_f = $result_f['result'];
/*
echo '<pre>';
print_r($res_f);
echo '</pre>';
*/


foreach ($res_uf as $value){

$new_field = array(
	'ENTITY_ID' => 'CRM_DEAL',
	'USER_TYPE_ID' => $value['USER_TYPE_ID'],
	'FIELD_NAME' => $value['FIELD_NAME'],
	'LIST_FILTER_LABEL' => $res_f[$value['FIELD_NAME']]['filterLabel'],
	'LIST_COLUMN_LABEL' => $res_f[$value['FIELD_NAME']]['listLabel'],
	'EDIT_FORM_LABEL' => $res_f[$value['FIELD_NAME']]['formLabel'],
	//'ERROR_MESSAGE' => $value[''],
	//'HELP_MESSAGE' => $value[''],
	'MULTIPLE' => $value['MULTIPLE'],
	'MANDATORY' => $value['MANDATORY'],
	'SHOW_FILTER' => $value['SHOW_FILTER'],
	//'SETTINGS' => $value[''],
	//'LIST' => $value['']
);

if($value['LIST']){
	$l = array();
	foreach($value['LIST'] as $val){
		$l[] = array('VALUE' => $val['VALUE']);
	}
	$new_field = array_merge($new_field, array('LIST' => $l));
}

$new_field = array_merge($new_field, array('SETTINGS' => $value['SETTINGS']));
	
/*
	echo '<pre>';
	print_r($res_f[$value['FIELD_NAME']]);
	echo '</pre>';
	
	echo '<pre>***';
	print_r($new_field);
	echo '</pre>';
*/
	
	$result_a = CRest::call('crm.deal.userfield.add',
	[
		'fields' => $new_field
	]);
	$res_a = $result_a['result'];
	/*
	echo '<pre>';
	print_r($res_a);
	echo '</pre>';
	*/
	if($result_a['result'])
	echo 'Поле ['.$value['FIELD_NAME'].'] создано<br>';
	else
	echo $result_a['error_description'].'<br>';

}

}
?>



<?
$result = CRest::call('crm.lead.userfield.list',[]);
$res = $result['result'];
echo '<pre>';
print_r($res);
echo '</pre>';

$result = CRest::call('crm.deal.userfield.list',[]);
$res = $result['result'];
echo '<pre>';
print_r($res);
echo '</pre>';	
?>



<?php
if($action){

//	ПОЛУЧАЕМ ЛИДЫ

	$result = CRest::call('crm.lead.list',
		[
			'order' => [
				//$field2 => "ASC",
		],
			'filter' => [
				'>=DATE_CREATE' => $calendar1,
				'<=DATE_CREATE' => $calendar2,
				'STATUS_ID' => 'IN_PROCESS',
		],
			'select' => [
				'UF_*',
				'*',
		],
			//'start' => $next,
	]);

	$res_leads = $result['result'];
	
	if(count($res_leads)==0){
		echo 'В выбранном диапазоне нет лидов';
		return;
	}

echo '<pre>';
print_r($res_leads);
echo '</pre>';

//	СПИСОК ПОЛЬЗОВАТЕЛЬСКИХ ПОЛЕЙ

$result_uf = CRest::call('crm.lead.userfield.list', 
	[
		'order' => [
	],
		'filter' => [
	],
]);
$res_uf = $result_uf['result'];

echo '<pre>';
print_r($res_uf);
echo '</pre>';

//	СПИСОК ТАЙМЛАЙНА

$next = 0;
$finish = false;
$res_timeline = [];
while (!$finish)
{
	$result_timeline = CRest::call('crm.timeline.bindings.list',
		[
			'filter' => [
			'OWNER_ID' => ['*'],
		],
			'start' => $next,
	]);
	if(!$result_timeline['error']){
		$res_timeline = array_merge($res_timeline, $result_timeline['result']);
		if(isset($result_timeline['next']))
		$next = $result_timeline['next'];
		else
		$finish = true;
	}else{
		$finish = true;
		echo '<pre style="color: red;">'.$result_timeline['error'].'</pre>';
	}
}
echo '<pre>';
print_r($res_timeline);
echo '</pre>';


foreach ($res_leads as $lead){
	
	$el = $lead;
	
	echo 'Обработка лида: [#'.$el['ID'].'] '.$el['TITLE'].'<br>';

	$timeline = array();
	foreach ($res_timeline as $value){
		if (($value['ENTITY_TYPE'] == 'lead') AND ($value['ENTITY_ID'] == $el['ID']))
		$timeline[$el['ID']][] = $value['OWNER_ID'];
	}
	echo '<pre>';
	//print_r($timeline);
	echo '</pre>';

	//	КОММЕНТАРИИ ТАЙМЛАЙНА

		$result_comm_list = CRest::call('crm.timeline.comment.list',
			[
			   'filter' => [
				   'ENTITY_ID' => $el['ID'],
				   'ENTITY_TYPE' => 'lead',
				],
			   'select' => [
					'ID', 
					'COMMENT', 
					'FILES',
					'AUTHOR_ID',
				]
			]);

		$res_comm_list = $result_comm_list['result'];

	echo '<pre>';
	print_r($res_comm_list);
	echo '</pre>';

	//	ОБРАБОТКА КОНТАКТОВ

	$contact_id = 0;
	echo 'Обработка контактов<br>';
	if ($el['CONTACT_ID'] <> 0) {
		$contact_id = $el['CONTACT_ID'];
	}else{
		$result_con = CRest::call('crm.contact.add',
		[
			'fields' =>
			[
				'NAME' => $el['NAME'],
				'SECOND_NAME' => $el['SECOND_NAME'],
				'LAST_NAME' => $el['LAST_NAME'],
				'PHONE' => $el['PHONE'],
				'POST' => $el['POST'],
				'ADDRESS' => $el['ADDRESS'],
				'ADDRESS_2' => $el['ADDRESS_2'],
				'ADDRESS_CITY' => $el['ADDRESS_CITY'],
				'ADDRESS_POSTAL_CODE' => $el['ADDRESS_POSTAL_CODE'],
				'ADDRESS_REGION' => $el['ADDRESS_REGION'],
				'ADDRESS_PROVINCE' => $el['ADDRESS_PROVINCE'],
				'ADDRESS_COUNTRY' => $el['ADDRESS_COUNTRY'],
				'ADDRESS_COUNTRY_CODE' => $el['ADDRESS_COUNTRY_CODE'],
				'OPENED' => 'Y',
				'ASSIGNED_BY_ID' => $el['ASSIGNED_BY_ID'],
				'TYPE_ID' => 'CLIENT',
				'SOURCE_ID' => $el['SOURCE_ID'],
			],
			'params' => 
			[
				'REGISTER_SONET_EVENT' => 'Y'
			]
		]);

		$contact_id = $result_con['result'];
		
	echo '<pre>';
	print_r($contact_id);
	echo '</pre>';
	}

	//	ОБРАБОТКА КОМПАНИИ

	echo 'Обработка компании<br>';
	$company_id = 0;
	if ($el['COMPANY_ID'] <> 0) {
		$company_id = $el['COMPANY_ID'];
	}else{
		$result_com = CRest::call('crm.company.add',
		[
			'fields' =>
			[ 
				'TITLE' => $el['COMPANY_TITLE'],
				'ASSIGNED_BY_ID' => $el['ASSIGNED_BY_ID'],
			],
			'params' => 
			[
				'REGISTER_SONET_EVENT' => 'Y' 
			]	
		]);
		
		$company_id = $result_com['result'];

	echo '<pre>';
	print_r($company_id);
	echo '</pre>';	
	}

	//	ОБРАБОТКА МЕСЕНДЖЕРОВ

	//echo 'Обработка месенджеров';
	//echo $el['IM'].'<br>';
	
	//
	
	$uf = array();
	foreach($res_uf as $val){
		$uf[$val['FIELD_NAME']] = $el[$val['FIELD_NAME']];
	}
	echo '<pre>';
	print_r($uf);
	echo '</pre>';	

	//	ДОБАВЛЯЕМ ДЕЛО

		echo 'Добавляем дело: '.$el['TITLE'].'<br>';
		
		$fields = [
			'TITLE' => $el['TITLE'], 
			'STAGE_ID' => 'NEW', 					
			'COMPANY_ID' => $company_id,
			'CONTACT_ID' => $contact_id,
			'OPENED' => 'Y', 
			'ASSIGNED_BY_ID' => $el['ASSIGNED_BY_ID'],
			'CURRENCY_ID' => 'RUB', 
			'OPPORTUNITY' => $el['OPPORTUNITY'],
			'COMMENTS' => $el['COMMENTS'],
		];
		
		$fields = array_merge($fields, $uf);
		
		echo '<pre>';
		print_r($fields);
		echo '</pre>';
		
		/*$result_deal = CRest::call('crm.deal.add',
		[
			'fields' => $fields,
			'params' => 
			[
				'REGISTER_SONET_EVENT' => 'Y' 
			]	
		]);*/

		$res_deal = 0;
		$res_deal = $result_deal['result'];

	echo '<pre>';
	print_r($res_deal);
	echo '</pre>';

	//	ДОБАВЛЯЕМ СВЯЗИ ТАЙМЛАЙНА

	if(isset($timeline[$el['ID']])){
		echo 'Добавляем связи таймлайна <br>';
		$tl = $timeline[$el['ID']];
		if(count($tl)>0){
			foreach ($tl as $value){
				$result = CRest::call('crm.timeline.bindings.bind',
					[
					   'fields' => [
							'ENTITY_ID' => $res_deal,
							'ENTITY_TYPE' => 'deal',
							'OWNER_ID' => $value,
						]
					]);

				$res = $result['result'];

				echo '<pre>';
				print_r($res);
				echo '</pre>';
			}
		}
	}

	//	ДОБАВЛЯЕМ КОММЕНТАРИИ ТАЙМЛАЙНА

	if(count($res_comm_list)>0){
		echo 'Добавляем комментарии таймлайна <br>';
		foreach ($res_comm_list as $value){
			$result_comm = CRest::call('crm.timeline.comment.add',
				[
				   'fields' => [
					   'ENTITY_ID' => $res_deal,
					   'ENTITY_TYPE' => 'deal',
					   'COMMENT' => $value['COMMENT'],
					   'AUTHOR_ID' => $value['AUTHOR_ID'],
					]
				]);

			$res_comm = $result_comm['result'];
			
			echo '<pre>';
			print_r($res_comm);
			echo '</pre>';
		}
	}

	echo '-----------------------------<br>';

}

}
?>

</body>
</html>