<?php
$deal_id = $_REQUEST['data']['FIELDS']['ID'];

$result_src = executeREST('crm.deal.get',array(
  "ID" => $deal_id, 
  ));
$src_date=$result_src['result']['UF_CRM_1632727449204'];
if($src_date){
	$src_final_date=$result_src['result']['UF_CRM_1641906274631'];
	$final_date=date('Y-m-d', strtotime('-3 day', strtotime($src_date)));  
	if($final_date!=date('Y-m-d', strtotime($src_final_date))){
			$result = executeREST('crm.deal.update',array(
			  "ID" => $deal_id, 
			  "fields" => array(
				"UF_CRM_1641906274631" => $final_date,
			  ), 
			  "params" => array(
				"REGISTER_SONET_EVENT" => "N"
			  ), 
			));
			file_put_contents('result.txt', print_r('deal:'.$deal_id.'          '.$final_date.' ! = '.date('Y-m-d', strtotime($src_final_date)), true));
	}
}

function executeREST($method, $params) {

    $queryUrl = 'https://brenom.bitrix24.ru/rest/3577/z2k7eoiyongn1g6j/'.$method.'.json';
    $queryData = http_build_query($params);

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $queryUrl,
        CURLOPT_POSTFIELDS => $queryData,
    ));

    $result = curl_exec($curl);
    curl_close($curl);

    return json_decode($result, true);

}

