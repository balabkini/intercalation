<?php
// Собственно название функции говорит за себя.
// убираем две двойные :: являющиеся разделителем
function cleartext($text) {
    return str_replace(
        array("::"),
        array('||'),
        htmlspecialchars(trim($text))
    );
}

//Проверяем есть ли переменные на добавление
if(isset($_POST['mess']) && $_POST['mess']!="" && $_POST['mess']!=" ")
{
	//Стартуем сессию для записи логина пользователя
	session_start();

	$user= $_SESSION['SESS_AUTH'];
	//Переменная с логином пользователя
	$username = $user[LOGIN]."[".$user[FIRST_NAME]."]";

	//Принимаем переменную сообщения
	$mess=htmlspecialchars($_POST['mess']);

	// Проверка (и убирание) вселенского зла (волшебных кавычек)
	$mess = get_magic_quotes_gpc() ? stripslashes($mess) : $mess;
		
	// обработка поста для записи в БД
	$post = sprintf("%s::%s::%s\n",
		cleartext($username),
		cleartext($mess),
		time()
	);
	// Добавление поста в базу.
	// (функция file_put_contents появилась только в РНР5, по этому на РНР4 гостевуха работать не будет)
	// Думаю излишне напоминать что на *nix системах файл БД должен существовать и быть доступным для записи.
	file_put_contents('chat.txt', $post, FILE_APPEND);
}
?>