//<script type="text/javascript">
	//Загружаем библиотеку JQuery
	//google.load("jquery", "1.3.2");
	//google.load("jqueryui", "1.7.2");
	

	//Функция отправки сообщения
	function send()
	{
		//Считываем сообщение из поля ввода с id mess_to_add
		var mess=$("#mess_to_send").val();
		//Очищаем форму ввода сообщения
		$("#mess_to_send").val('');
		// Отсылаем паметры
       $.ajax({
                type: "POST",
                url: "add_mess.php",
                data:{'mess' : mess},
                // Выводим то что вернул PHP
                success: function(html)
				{
					//Если все успешно, загружаем сообщения
					load_messes();
                },
				error: function(xhr, status)
				{
					alert('Ошибка');
					$("#messages").append('<div style="color:red;">Ошибка, сообщение не отправлено</div>');
					$("#mess_to_send").val(mess);
					//Прокручиваем блок вниз(если сообщений много)
					var h = $("#messages")[0].scrollHeight;
					$("#messages").scrollTop(h);
				}
        });
	}
	
	//Функция загрузки сообщений
	var idmess = 0;
	var messloading = false;
	function load_messes()
	{
		if (messloading==false)
		{
			messloading=true;
			$.ajax({
                type: "POST",
                url:  "load_messes.php",
                data: {'idmess' : idmess},
                // Выводим то что вернул PHP
                success: function(html)
				{
					
					if (html!=="not new")
					{
					
					//Выводим что вернул нам php
					var obj0 = jQuery.parseJSON(html);
					var len = obj0.arr.length;
					for (var i = 0; i < len; i++)
					{
						var obj = obj0.arr[i];
						var admtxt='';
						if (obj.status==1)
						admtxt='<div class="admin_panel_msq"><div onclick="del_msg(\''+obj.uid+'\',\'1\');" class="b_del_l_msg"></div>'+
						'<div onclick="del_msg(\''+obj.id+'\',\'0\');" class="b_del_msg"></div></div>';
						var ndate = new Date(obj.date*1000);
						th = ndate.getHours();
						tm = ndate.getMinutes();
						time = (th < 10 ? '0' : '') + th + ':' + (tm < 10 ? '0' : '') + tm;
						if (obj.type == 0)
						{
							if (obj.msgview == 1)
							{
							
								if (obj.idto == 0)
								{
									var nick='<span class="chatUserFrom"><a href="javascript:insNick(\''+obj.login+'\',\', \');" style="color: '+obj.nikcol+'"><b>'+obj.login+'</b></a>:</span>';
								}
								else
								{
									logto = "<b><font color='#A41C1C'> → "+obj.logto+"</font></b>";
									var nick='<span style="background-color: #ffb53f; border-radius: 3px; text-align: center; margin-right: 5px;">ЛС</span>'+
									'<span class="chatUserFrom"><a href="javascript:SetNickTo(\''+obj.uid+'\',\''+obj.login+'\');" style="color: '+obj.nikcol+'"><b>'+obj.login+'</b>'+logto+'</a>:</span>';
								}
								
								if (obj.msgcol=='000000')
								msg='<span class="chatText">'+obj.msg+'</span>'
								else
								msg='<span class="Text" style="color:#'+obj.msgcol+'">'+obj.msg+'</span>';
							
								var text='<div class="chatMessage">'+
								'<div class="chatTime"><font color="gray" size="1">['+time+']</font></div>'+
								nick+
								msg+
								'</div>';
								$("#messages").append('<div id="id'+obj.id+'">'+admtxt+text+'</div>');
							}
							else
							{
							
								if (obj.idto == 0)
								{
									var nick='<a href="javascript:insNick(\''+obj.login+'\',\', \');" style="color: '+obj.nikcol+'"><b>'+obj.login+'</b></a>:&nbsp;';
								}
								else
								{
									var logto = "<b><font color='#A41C1C'> → "+obj.logto+"</font></b>";
									var nick='<span style="background-color: #ffb53f; border-radius: 3px; text-align: center; margin-right: 5px;">ЛС</span>'+
									'<a href="javascript:SetNickTo(\''+obj.uid+'\',\''+obj.login+'\');" style="color: '+obj.nikcol+'"><b>'+obj.login+'</b>'+logto+'</a>:&nbsp;';
								}
							
								var text='<div class="'+obj.al+'">'+
								'<div class="mescode_container">'+
								'<div class="mescode_mes">'+
								'<div class="mesbtbtbt">'+
								'<div class="mes_container">'+
								'<div class="mescode_mes_container">'+
								'<img src="'+obj.ava+'" class="img-ava32">'+
								'</div>'+
								'<div class="mescode_postedby">'+
								'<strong>'+
								nick+
								'</strong>'+
								'<font color="gray" size="1">['+time+']&nbsp;</font>'+
								'</div>'+
								'<div class="message">'+
								obj.msg+
								'</div></div></div></div></div></div>';
								$("#messages").append('<div id="id'+obj.id+'">'+admtxt+text+'</div>');
							}
						}
						else if (obj.type == 5)
						{
							$("#id"+obj.msg).remove();
						}
						else
						{
							var text='<div class="chatMsgSystem">'+
							'<div class="chatTime"><font color="gray" size="1">['+time+']</font></div>'+
							'<span class="chatUserFrom"></span>'+
							'<span class="chatText"><b>'+obj.login+'</b> '+obj.msg+'</span>'+
							'</div>';
							$("#messages").append('<div id="id'+obj.id+'">'+admtxt+text+'</div>');
						}
						idmess=obj.id;
					}
					
					
					//Прокручиваем блок вниз(если сообщений много)
					var h = $("#messages")[0].scrollHeight;
					$("#messages").scrollTop(h);
					
					$("#chatAudio")[0].play();
					
				
					}
					messloading=false;
                },
				error: function(xhr, status)
				{
					//alert('Ошибка');
					messloading=false;
				}
			});
		}
	}
	
	$(function()
	{
		
		$('<audio id="chatAudio"><source src="../chat/sound/notify.ogg" type="audio/ogg"><source src="../chat/sound/notify.mp3" type="audio/mpeg"><source src="../chat/sound/notify.wav" type="audio/wav"></audio>').appendTo('body');
	
		insNick = function (text1, text2, edit)
		{
			el = document.getElementById('mess_to_send');
			var element = el;
			var str = element.value;
			element.value = text1 + text2 + str;
			el.focus();
		}
	
		insTag = function (text1, text2, edit)
		{
			el = document.getElementById('mess_to_send');
			if ((document.selection))
			{
				//el.content.focus();
				el.document.selection.createRange().text = text1+document.form.document.selection.createRange().text+text2;
			}
			else if(el.selectionStart != undefined && ((edit && edit === true) || !edit)) 
			{
				var element    = el;
				var str     = element.value;
				var str_len = element.value.length;
				var end = element.selectionEnd;
				var start    = element.selectionStart;
				var length    = element.selectionEnd - element.selectionStart;
				element.value = str.substr(0, start) + text1 + str.substr(start, length) + text2 + str.substr(start + length);
			}
			else if(el.selectionStart != undefined && edit && edit !== 'true')
			{
				var element    = el;
				var str     = element.value;
				var start    = element.selectionStart;
				var length    = element.selectionEnd - element.selectionStart;
				element.value = str.substr(0, start) + text1 + edit + text2 + str.substr(start + length);
			}
			else if (el.selectionStart == undefined && edit && edit !== true)
			{
				document.form.content.value += text1+edit+text2;
			}
			else document.form.content.value += text1+text2;
			//el.focus();
			if(edit === true)
			{
				el.selectionStart = start + text1.length;
				el.selectionEnd = el.selectionStart;
			}
		} 
 
		get_obj_prop = function(el)
		{
			var offset = el.offset();
			var size = new Object;
			size.height = el.outerHeight();
			size.width = el.outerWidth();
			return (
			{
				offset: offset,
				size: size
			});
		}
	  
		close = function(id)
		{
			var el = $('#' + id);
			el.remove();
		}
				
		var html_ms="";
		show_s = function(prop)
		{
			var top = -364/*prop.offset.top - prop.size.height - 361*/;
			var left = 6/*prop.offset.left*/;
			var tag = '<div id="popup" style="position:absolute;top:' + top + 'px;left:' + left + 'px;" />';
			$('#editorchat').append(tag);
			
			if (html_ms=="")
			{
				$.ajax(
				{
					url: "menu_smiles.php",
					cache: false,
					success: function(html)
					{
						html_ms=html;
						$('#popup').append(html_ms);
					}
				});
			}
			else
			{			
				$('#popup').append(html_ms);
			}
		}
	  
		show_smiles = function(shown)
		{
			$('#popup').remove();
			if(shown == true)
			{
				var el = $('#bsmiles');
				var prop = get_obj_prop(el);
				show_s(prop);
				return void(0);
			}
		}
	  
		/*$('#mess_to_send').focus(function()
		{
			$('#popup').remove();
		});
		
		show_as = function(prop)
		{
			var top = prop.offset.top;
			var left = prop.offset.left;
			var tag = '<div id="popup" style="z-index: 1; position:absolute;top:' + top + ';left:' + left + ';" />';
			$('#addstat').append(tag);
			$.ajax(
			{
				url: "form_edit_status.php",
				cache: false,
				success: function(html)
				{
					$('#popup').html(html);
				}
			});
		}
	  
		show_add_status = function(shown)
		{
			$('#popup').remove();
			if(shown == true)
			{
				var el = $('#baddstat');
				var prop = get_obj_prop(el);
				show_as(prop);
				return void(0);
			}
		}*/
	});
	
//</script>


    //<script>     
        $(document).ready(function()
		{  
			//При загрузке страницы подгружаем сообщения
			load_messes();
			//Ставим цикл на каждые три секунды
			setInterval('load_messes()',3000);
        });  
    //</script>  
	
//<script>
	function spoiler(obj)
	{		
		if (obj.parentNode.getElementsByTagName('div')[0].style.display != '')
		{
			obj.parentNode.getElementsByTagName('div')[0].style.display = '';
			obj.innerText = 'свернуть';
		}
		else
		{
			obj.parentNode.getElementsByTagName('div')[0].style.display = 'none';
			obj.innerText = 'показать';
		}
	}
//</script>