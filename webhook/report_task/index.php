<?require_once (__DIR__.'/crest.php');?>

<html>
<head>
	<link rel="stylesheet" type="text/css" media="screen" href="style.css"/>
	<title>Отчет по задачам</title>
</head>
<body>

<?
if(isset($_POST["item"])){
	$tablename = "Отчет по задачам";
    $calendar1 = $_POST["calendar1"];
    $calendar2 = $_POST["calendar2"];
}else{
	$tablename = "Отчет";
	$calendar1 = date('Y-m-d\Th:m');
	$calendar2 = date('Y-m-d\Th:m');
}
?>

<style>
    .filtr {
    	padding: 10px;
		display: grid;
		width: 350px;
	}
</style>

<form action="" method="post" class="report">
    <div class="filtr filtr1">
		<label for="cal1">Дата начала</label>
        <input type="datetime-local" name="calendar1" id="cal1" value=<?=$calendar1?>>
		<label for="cal2">Дата окончания</label>
        <input type="datetime-local" name="calendar2" id="cal2" value=<?=$calendar2?>>
    </div>

    <div class="filtr filtr2">
		<label for="field1">Поле категорий</label>
        <select name="field1" id="field1">

        <?php

            $result = CRest::call('crm.'.$item.'.fields');

            $res = $result['result'];

            foreach ($res as $key => $value){
                echo "<option value=\"".$key."\"".($field1 == $key ? ' selected="selected"' : '').">".$value['title']."</option>";
            }
           
         ?>
        </select>
    </div>

    <div class="filtr filtr3">
		<label for="field2">Поле диапазона</label>
        <select name="field2" id="field2">
        <?php
            $res = $result['result'];
            foreach ($res as $key => $value){
                echo "<option value=\"".$key."\"".($field2 == $key ? ' selected="selected"' : '').">".$value['title']."</option>";
            }
	    ?>        
        </select>
    </div>

    <input type="submit" value="Применить">
</form>


<?php
/*
$next = 0;
$finish = false;
$res = [];
while (!$finish)
{
*/
	$result = CRest::call('tasks.task.list',
		[
			'order' => [
				'ID' => "ASC",
		],
			'filter' => [
				'>=DATE_CREATE' => $calendar1,
				'<=DATE_CREATE' => $calendar2,
		],
			'select' => [
				'ID',
				'TITLE',
				'STATUS',
				'TIME_SPENT_IN_LOGS',	// затраченное время из истории изменений;
				'DATE_START',	// дата старта;
				'CREATED_DATE',	// дата создания;
				'CHANGED_DATE',	// дата последнего изменения;
				'CLOSED_DATE',	// дата завершения;
				'START_DATE_PLAN',	// плановое начало;
				'END_DATE_PLAN',	// плановое завершение;
				'DEADLINE',	// крайний срок;
				'REAL_STATUS',	// статус задачи. Константы отражающие статусы задач:
				'STATUS_COMPLETE',	// флаг завершенности задачи;
				'DURATION_PLAN',	// затрачено (план);
				'DURATION_FACT',	// затрачено (фактически);
				'DURATION_TYPE',	// тип единицы измерения в планируемой длительности: days, hours или minutes
				'CREATED_BY',	// постановщик;
				'RESPONSIBLE_ID',	// исполнитель;
				'CREATED_BY_LAST_NAME',	// постановщик; 
				'RESPONSIBLE_LAST_NAME',	// ответственный; 
				'GROUP_ID',	// рабочая группа. 
				'TIME_ESTIMATE',	// затраченное время;
				'REPLICATE', 	// повторяемая задача;
		],
			'start' => $next,
	]);
	$res = $result['result'];
	
	$tasks = $res['tasks'];
/*
	$res = array_merge($res, $result['result']);
	if(isset($result['next']))
	$next = $result['next'];
	else
	$finish = true;
}
*/

echo '<pre>';
print_r($res);
echo '</pre>';



	$result = CRest::call('sonet_group.get',
		[
			'ORDER' => [
				'NAME' => "ASC",
		],
			'FILTER' => [],
	]);
	$groups = $result['result'];
	
echo '<pre>';
print_r($result);
echo '</pre>';	



$field1 = 'responsibleId';
$field2 = 'groupId';
$field3 = 'id';
$f1= array();
$f2= array();
$f = array();
$f3= array();
foreach ($tasks as $key => $value){
	if(in_array($value[$field2], $f1)==false)
	$f1[] = $value[$field2];
	
	$f2[$value[$field2]][] = $value[$field3];
	
	$f[$value[$field1]][$value[$field2]]['durationPlan'] += intval($value['durationPlan']);
	$f[$value[$field1]][$value[$field2]]['durationFact'] += intval($value['durationFact']);
	if(isset($value['closedDate']))
	$f[$value[$field1]][$value[$field2]]['closedDateLast'] = $value['closedDate'];

	$f3[$value[$field1]][$value[$field3]]['durationPlan'] = $value['durationPlan'];
	$f3[$value[$field1]][$value[$field3]]['durationFact'] = $value['durationFact'];
}

echo '<pre>';
print_r($f1);
print_r($f2);
print_r($f);
print_r($f3);
echo '</pre>';

$rows = $f;
?>


<?php
$len = count($f1);
$sum = 0;
echo "<table border='1'><caption>".$tablename."</caption>";
	echo "<tr><th  rowspan=2>".$catname."</th><th colspan=".$len.">Интервалы</th></tr>";
	echo "<tr>";
	//echo "<th>".$catname."</th>";
	foreach ($f1 as $k => $v){
		echo "<th>".$v."</th>";
	}
	echo "</tr>";
     	foreach ($rows as $key => $row) :
	     echo "<tr>";
		echo "<th>".$key."</th>";

		foreach ($f2 as $k => $v){

			if (array_key_exists($k, $row)) {
				echo "<td>".$row[$k]."</td>";
			}else{
				echo "<td></td>";
			}

		}

	     echo "</tr>";
     	endforeach;
	echo "<tr>";
	echo "<td>Всего</td>";
	foreach ($f2 as $k => $v){
		$sum=$sum+$v;
		echo "<td>".$v."</td>";
	}
	echo "</tr>";
	echo "<tr><td>Общее кол-во</td><td colspan=".$len.">".$sum."</td></tr>";
echo "</table>";
?>

</body>
</html>