<?php


/*
$statusTask = executeREST('task.stages.get', array(
    'entityId' => 9,
    'isAdmin' => true
));
*/


$titleTask = executeREST('tasks.task.get', array(
    'taskId' => intval($_REQUEST['data']['FIELDS_AFTER']['ID']),
    'select' => array('TITLE', 'RESPONSIBLE_ID', 'UF_CRM_TASK')
));


//postToChat(185, print_r($titleTask, true));

if (stristr($titleTask['result']['task']['title'], 'Выдача готовой продукции'))

{

    $changeTask = executeREST('task.stages.movetask', array(
        'id' => intval($_REQUEST['data']['FIELDS_AFTER']['ID']),
        'stageId' => 135
    ));

     
    postToChat($titleTask['result']['task']['responsible']['id'], 
        'Выдайте продукцию клиенту', 
        array(
            "ID" => $titleTask['result']['task']['responsible']['id'],
            "BLOCKS" => Array(
                Array("IMAGE" => Array(
                    "LINK" => "https://intercalation.ru/webhook/krafika/2.png"
                )),
                Array("LINK" => Array(
                    "NAME" => $titleTask['result']['task']['title'],
                    "DESC" => "Пройдите в задачу и выполните ее",
                    "LINK" => "https://".$_REQUEST['auth']['domain']."/workgroups/group/9/tasks/task/view/".$_REQUEST['data']['FIELDS_AFTER']['ID']."/"
               )),
        )
    )
);                 
}

if (stristr($titleTask['result']['task']['title'], 'Передать полученный образец заказчику'))

{

    $changeTask = executeREST('task.stages.movetask', array(
        'id' => intval($_REQUEST['data']['FIELDS_AFTER']['ID']),
        'stageId' => 135
    ));


    postToChat($titleTask['result']['task']['responsible']['id'], 
        'Передать полученный образец заказчику', 
        array(
            "ID" => $titleTask['result']['task']['responsible']['id'],
            "BLOCKS" => Array(
                Array("IMAGE" => Array(
                    "LINK" => "https://intercalation.ru/webhook/krafika/1.jpg"
                )),
                Array("LINK" => Array(
                    "NAME" => $titleTask['result']['task']['title'],
                    "DESC" => "Пройдите в задачу и выполните ее",
                    "LINK" => "https://".$_REQUEST['auth']['domain']."/workgroups/group/9/tasks/task/view/".$_REQUEST['data']['FIELDS_AFTER']['ID']."/"
               )),
        )
    )
);               
    
}

function executeREST($method, $params) {

    $queryUrl = 'https://krafika.bitrix24.ru/rest/185/zzefmlix6pc7nles/'.$method.'.json';
    $queryData = http_build_query($params);

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $queryUrl,
        CURLOPT_POSTFIELDS => $queryData,
    ));

    $result = curl_exec($curl);
    curl_close($curl);

    return json_decode($result, true);

}


function postToChat($user, $message, $attach = array()) {

    $queryUrl = 'https://krafika.bitrix24.ru/rest/185/zzefmlix6pc7nles/im.message.add.json';
    $queryData = http_build_query(
        array(
            "USER_ID" => $user,
            "MESSAGE" => $message,
            "ATTACH" => $attach
        )
    );

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $queryUrl,
        CURLOPT_POSTFIELDS => $queryData,
    ));

    $result = curl_exec($curl);
    curl_close($curl);

    return json_decode($result, true);

}
