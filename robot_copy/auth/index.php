<?
//define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

if (isset($_REQUEST["backurl"]) && strlen($_REQUEST["backurl"])>0) 
	LocalRedirect($backurl);

$APPLICATION->SetTitle("Авторизация");
?>

</header>

<?$APPLICATION->IncludeComponent(
	"bitrix:main.auth.form", 
	".default", 
	array(
		"AUTH_FORGOT_PASSWORD_URL" => "/robot/auth/forget.php",
		"AUTH_REGISTER_URL" => "/robot/auth/registration.php",
		"AUTH_SUCCESS_URL" => "/robot/",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>


<div id="auth-form" class="bg-popup">
		<div class="bg-flex">
			<div class="form-popup">
				<img id="auth-close" src="<?=SITE_TEMPLATE_PATH?>/img/close.png" alt="" class="close">
				<div class="form-wrap">
					<h1>Авторизация</h1>
					<input type="text" placeholder="Логин">
					<input type="text" placeholder="Пароль">
					<p><a id="regisr-link" class="form-link">Регистрация</a></p>
					<p><a id="btn-restore-form" class="form-link">Забыли пароль?</a></p> <!-- href="" -->
					<button id="auth" class="center">Войти</button>
				</div>
			</div>
		</div>
	</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>