<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("keywords", "crm, сайт, автоматизация, бизнес процессы, обучение, касса, маркировка, интернет магазин, лендинг, рассылка, внедрение, битрикс 24, домен, хостинг, сервер");
$APPLICATION->SetPageProperty("title", "Внедрение");
$APPLICATION->SetPageProperty("tags", "crm, сайт, автоматизация, бизнес процессы, обучение, касса, маркировка, интернет магазин, лендинг, рассылка, битрикс24");
$APPLICATION->SetPageProperty("description", "Внедрение в 3 этапа");
$APPLICATION->SetTitle("Центр современных технологий \"Внедрение\"");
?>

</header>

		<div class="container">
			<img class="babl-1" src="<?=SITE_TEMPLATE_PATH?>/img/babl-1.png" alt="">
			<img class="babl-2" src="<?=SITE_TEMPLATE_PATH?>/img/babl-2.png" alt="">
			<img class="babl-3" src="<?=SITE_TEMPLATE_PATH?>/img/babl-3.png" alt="">
			<img class="idle-1" src="<?=SITE_TEMPLATE_PATH?>/img/idle-1.png" alt="">
			<div class="content">
				<div class="row">
					<div class="slogan">
						<h1 align="left" style="font-weight: 500">Внедр<span style="font-weight: 700">и</span>&#x301;ть-</h1>
						<p><span aling="left">это Вкоренить, </span>заставить укрепиться в чем-нибудь, прочно войти куда-нибудь.</p>
						<button id="btn-send-form">Закажи внедрение</button>
					</div>
					<div class="slogan-img">
						<img src="<?=SITE_TEMPLATE_PATH?>/img/idle-1.png" alt="">
						<div class="danger"><img id="auth-danger" src="<?=SITE_TEMPLATE_PATH?>/img/danger.png" alt=""></div>
					</div>
				</div>
				<div id="s3d-1" class="slider-3d">
					<div class="bg-item-three">
						<div class="item-three">
							<a id="btn-send-form"><div class="ellipse"><img src="<?=SITE_TEMPLATE_PATH?>/img/img-item-1.png" alt=""></div></a>
							<a href="
							https://www.intercalation.ru/novosti/etapy-vnedreniya/proniknovenie"><h2>Проникновение</h2></a>
							<p>Что обозначает слово «проникновение». Проникать – это попадать внутрь, в среду, и распространяться там.</p>
						</div>
					</div>
					<div class="bg-item-three">
						<div class="item-three">
							<a id="btn-send-form"><div class="ellipse"><img src="<?=SITE_TEMPLATE_PATH?>/img/img-item-2.png" alt=""></div></a>
							<a href="https://www.intercalation.ru/novosti/etapy-vnedreniya/ukorenenie"><h2>Укоренение</h2></a>
							<p>Укоренить – это дать прочную установку, которая сможет прижиться и укрепиться. </p>
						</div>
					</div>
					<div class="bg-item-three">
						<div class="item-three">
							<a id="btn-send-form"><div class="ellipse"><img src="<?=SITE_TEMPLATE_PATH?>/img/img-item-3.png" alt=""></div></a>
							<a href="https://www.intercalation.ru/novosti/etapy-vnedreniya/zapolnenie"><h2>Заполнение</h2></a>
							<p>Заполнить – значит занять целиком, действовать для наполнения. </p>
						</div>
					</div>
				</div>
				<div class="titles">
					<div class="title-image">
						<img src="<?=SITE_TEMPLATE_PATH?>/img/idle-2.png" alt="">
					</div>
					<div class="title">

<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "inc1",
		"EDIT_TEMPLATE" => ""
	)
);?>

<?/*
						<a href=""><h2>CMS: 1С-Битрикс Управление сайтом</h2></a>
						<p>Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.Но высокотехнологичная концепция общественного уклада, .</p>
						<a href=""><h3>Сайты 24</h3></a>
						<p>Но высокотехнологичная концепция общественного уклада,</p>
						<a href=""><h3>Сайты и Лэндинги</h3></a>
						<p>Но высокотехнологичная концепция общественного уклада,</p>
						<a href=""><h3>Интернет магазин</h3></a>
						<p>Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.</p>
*/?>

					</div>
				</div>

				<div class="row">
					

<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "inc2",
		"EDIT_TEMPLATE" => ""
	)
);?>


<?/*
						<div class="item-six">
							<a href=""><div class="ellipse-min"><img src="<?=SITE_TEMPLATE_PATH?>/img/img-item-1.png" alt=""></div></a>
							<a href=""><h3>CRM</h3></a>
							<p>Коонцепция общественного уклада.</p>
						</div>
						<div class="item-six">
							<a href=""><div class="ellipse-min"><img src="<?=SITE_TEMPLATE_PATH?>/img/img-item-1.png" alt=""></div></a>
							<a href=""><h3>Маркетинг</h3></a>
							<p>Коонцепция общественного уклада.</p>
						</div>
						<div class="item-six">
							<a href=""><div class="ellipse-min"><img src="<?=SITE_TEMPLATE_PATH?>/img/img-item-1.png" alt=""></div></a>
							<a href=""><h3>Аналитика</h3></a>
							<p>Коонцепция общественного уклада.</p>
						</div>
						<div class="item-six">
							<a href=""><div class="ellipse-min"><img src="<?=SITE_TEMPLATE_PATH?>/img/img-item-1.png" alt=""></div></a>
							<a href=""><h3>Виртуализация</h3></a>
							<p>Коонцепция общественного уклада.</p>
						</div>
						<div class="item-six">
							<a href=""><div class="ellipse-min"><img src="<?=SITE_TEMPLATE_PATH?>/img/img-item-1.png" alt=""></div></a>
							<a href=""><h3>Управление</h3></a>
							<p>Коонцепция общественного уклада.</p>
						</div>
						<div class="item-six">
							<a href=""><div class="ellipse-min"><img src="<?=SITE_TEMPLATE_PATH?>/img/img-item-1.png" alt=""></div></a>
							<a href=""><h3>Дизайн</h3></a>
							<p>Коонцепция общественного уклада.</p>
						</div>
*/?>						
					
					
				</div>

				
			</div>
		</div>

		<div class="slider">
			<div class="slider-wrap">
				<div class="slider-image">
					<img src="<?=SITE_TEMPLATE_PATH?>/img/composite_big.png" alt="">
				</div>
				<div class="slider-content">
					<h1><a id="btn-send-form" href="https://www.intercalation.ru/lp/composite/">Композитный сайт</a></h1>
					<p>Уникальная технология производства сайтов объединяет в себе высокую скорость загрузки статического сайта и все возможности динамического сайта.<br>
					Пользователь мгновенно получает контент страницы.</p>
				</div>	
			</div>
			<div class="slider-wrap">
				<div class="slider-image">
					<img src="<?=SITE_TEMPLATE_PATH?>/img/banner-1.png" alt="">
				</div>
				<div class="slider-content">
					<h1><a id="btn-send-form">Отдел продаж за 1 месяц на Битрикс 24</a></h1>
					<p>Закажи Облачный портал Битрикс 24 с тарифом Базовый за 2490 руб в месяц и получи Внедрение отдела продаж со скидкой 50%</p>
				</div>	
			</div>

			<div class="slider-wrap">
				<div class="slider-image">
					<img src="<?=SITE_TEMPLATE_PATH?>/img/banner-2.png" alt="">
				</div>
				<div class="slider-content">
					<h1><a id="btn-send-form">Готовый интернет магазин</a></h1>
					<p>Каждый третий интернет-магазин в России сделан на «1С-Битрикс: Управление сайтом». Хотите начать зарабатывать уже сегодня?
					</p>
				</div>	
			</div>

			<div class="slider-wrap">
				<div class="slider-image">
					<img src="<?=SITE_TEMPLATE_PATH?>/img/banner-3.png" alt="">
				</div>
				<div class="slider-content">
					<h1><a id="btn-send-form">Скидка на облачко Битрикс 24</a></h1>
					<p> Для всех клиентов при покупке любого  тарифа облачного сервиса «Битрикс24» на 12 месяцев Скидка 30% и на 24 месяца Скидка 40%.</p>
				</div>	
			</div>

			<div class="slider-wrap">
				<div class="slider-image">
					<img src="<?=SITE_TEMPLATE_PATH?>/img/site_domain.png" alt="">
				</div>
				<div class="slider-content">
					<h1><a id="btn-send-form">Домен .SITE бесплатно!</a></h1>
					<p>Мы хотим, чтобы вы всегда оставались на связи и поэтому дарим вам бесплатную регистрацию веб-адреса в зоне .SITE на 1 год.</p>
				</div>	
			</div>

			<div class="slider-wrap">
				<div class="slider-image">
					<img src="<?=SITE_TEMPLATE_PATH?>/img/online_without_borders_1080x1080.jpg" alt="">
				</div>
				<div class="slider-content">
					<h1><a id="btn-send-form">ON LINE без границ!</a></h1>
					<p>Сейчас — лучший момент для старта онлайн-торговли! Не упусти момент!!!</p>
				</div>	
			</div>

		</div>
<?/*
		<div class="slider-wrap">
				<div class="slider-image">
					<img src="<?=SITE_TEMPLATE_PATH?>/img/idle-4.png" alt="">
				</div>
				<div class="slider-content">
					<h1>АКЦИЯ АКЦИЯ БАННЕР3 С АКЦИЕЙ</h1>
					<p>Условия акции, какой-нибудь текст или утп. Условия акции, какой-нибудь текст или утп. Условия акции, какой-нибудь текст или утп.</p>
				</div>	
		</div>
*/?>

		<div class="container">



			<div class="partners">


<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"partner", 
	array(
		"COMPONENT_TEMPLATE" => "partner",
		"IBLOCK_TYPE" => "information",
		"IBLOCK_ID" => "12",
		"NEWS_COUNT" => "15",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "TIMESTAMP_X",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "REFER",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"STRICT_SECTION_CHECK" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Главная",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
);?>


<?/*
				
				<div class="partner">
					<a href="https://www.bitrix24.ru/?p=9137365"><img  class="ellipse"
						src="<?=SITE_TEMPLATE_PATH?>/img/5.png"></a>
					<a href="https://www.bitrix24.ru/?p=9137365"><h3>1С Битрикс</h3></a>
				</div>

				<div class="partner">
					<a href="https://www.moysklad.ru/register/?p=2019-1186"><img class="ellipse"src="<?=SITE_TEMPLATE_PATH?>/img/2.png"></a>
					<a href="https://www.moysklad.ru/register/?p=2019-1186"><h3>Мойсклад</h3></a>
				</div>
				<div class="partner">
					<a href="https://roistat.com/ru/?ref=2isxqjhq"><img class="ellipse"src="<?=SITE_TEMPLATE_PATH?>/img/9.png"></a>
					<a href="https://roistat.com/ru/?ref=2isxqjhq"><h3>Roistat</h3></a>
				</div>

				<div class="partner">
					<a href="https://scloud.ru/?ref=mO87i">
						<img class="ellipse" 
						src="<?=SITE_TEMPLATE_PATH?>/img/1.png"	>
					</a>
					<a href="https://scloud.ru/?ref=mO87i"><h3>1С Облако</h3></a>
				</div>
				
				<div class="partner">
					<a href="https://app.pact.im/signup?ref=a45fdc33548242be5b0e19313cb3a82d"><img class="ellipse"
						src="<?=SITE_TEMPLATE_PATH?>/img/10.png"></a>
					<a href="https://app.pact.im/signup?ref=a45fdc33548242be5b0e19313cb3a82d"><h3>Pact</h3></a>
				</div>
				
				<div class="partner">
					<a href="https://sporina.icu/"><img  class="ellipse"
						src="<?=SITE_TEMPLATE_PATH?>/img/4.png"></a>
					<a href="https://sporina.icu/"><h3>Спорина</h3></a>
				</div>
				
				<div class="partner">
					<a href="https://www.reg.ru/?rlink=reflink-97505"><img class="ellipse"src="<?=SITE_TEMPLATE_PATH?>/img/7.png"></a>
					<a href="https://www.reg.ru/?rlink=reflink-97505"><h3>Reg.ru</h3></a>
				</div>
				

				<div class="partner">
					<a href="https://icons8.ru/"><img  class="ellipse"
						src="<?=SITE_TEMPLATE_PATH?>/img/6.png"></a>
					<a href="https://icons8.ru/"><h3>ИКОНКИ</h3></a>
				</div>

				<div class="partner">
					<a href="https://forms.amocrm.ru/wrxwxv"><img class="ellipse" src="<?=SITE_TEMPLATE_PATH?>/img/8.png"></a>
					<a href="https://forms.amocrm.ru/wrxwxv"><h3>amoCRM</h3></a>
				</div>
*/?>



			</div>




			<div class="row">
				<div id="reg" class="items-3">
					<div class="reg">
						<h1>Присоединяйся к команде лучших</h1>
						<p>Вливайся в ряды современных и информативно подкованных людей. Будь на острие прогресса.</p>
						<button id="btn-regisr-form">Регистрация</button>
					</div>
				</div>
			</div>
			







			<div id="s3d-2" class="slider-3d">
				<div class="bg-item-three">
					<div class="item-three">
						<a href="https://www.intercalation.ru/lp/composite/"><h1>Композитный сайт</h1></a>
						<a href="https://www.intercalation.ru/lp/composite/"><img src="<?=SITE_TEMPLATE_PATH?>/img/composite_big.png" alt=""></a>
						<p>Уникальная технология производства сайтов. Пользователь мгновенно получает контент страницы.</p>
						<p></p>
					</div>
				</div>
				<div class="bg-item-three">
					<div class="item-three">
						<a href="https://www.intercalation.ru/produkty/oblachnye-resheniya/"><h1>Технологии</h1></a>
						<a href="https://www.intercalation.ru/uslugi/nastroyka-i-ustanovka/nastroyka-vds"><h2>Облачные решения</h2></a>

						<p>Облачные технологии – технологии распределенной обработки цифровых данных,
						с помощью которых компьютерные ресурсы предоставляются интернет-пользователю как онлайн-сервис.</p>
						<p></p>
					</div>
				</div>
				<!--
				<div class="bg-item-three">
					<div class="item-three">
						<a href="https://bitrix24public.com/sporina.bitrix24.ru/form/26_tsentr_sovremennykh_tekhnologiy_vnedrenie/sk2wl8/"><h1>1С Битрикс</h1></a>
						<a href="https://www.intercalation.ru/uslugi/razrabotka-/korporativnyy-portal-organizatsii"><h2>Управление сайтом</h2></a>
						<a href="https://www.intercalation.ru/uslugi/razrabotka-/internet-magazin"><img src="<?=SITE_TEMPLATE_PATH?>/img/platforma-1s-bitriks.png" alt=""></a>
<?/* <a href=""><img src="<?=SITE_TEMPLATE_PATH?>/img/rect.png" alt=""></a> */?>
						<p>Сайты на платформе «1С-Битрикс» — это удобство, надежность и высокая посещаемость.</p>
					</div>
				</div>
				-->
				<div class="bg-item-three">
					<div class="item-three">
						<a href="https://www.intercalation.ru/lp/b24/"><h1>Внедрение Битрикс 24</h1></a>
						<?/*<a href="https://www.intercalation.ru/uslugi/vnedrenie/"><h2>Базовые пакеты</h2></a>
						*/?>
						<ul>
							<a href="https://www.intercalation.ru/uslugi/vnedrenie/base-intercalation-crm"><li>CRM</li></a>
							<a href="https://www.intercalation.ru/uslugi/vnedrenie/avtomatizatsiya-crm"><li>Автоматизация и Повторные продажи</li></a>
							<a href="https://www.intercalation.ru/uslugi/vnedrenie/zadachi-i-kommunikatsii"><li>Задачи и коммуникации</li></a>
							<a href="https://www.intercalation.ru/uslugi/vnedrenie/crm-plyus-temnyy-magazin"><li>CRM и Магазин</li></a>
							<a href="https://www.intercalation.ru/uslugi/nastroyka-i-ustanovka/nastroyka-skvoznoy-analitiki"><li>Сквозная аналитика</li></a>
						</ul>
					</div>
				</div>
			</div>


			
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => ""
	)
);?>
<?/*
			<div class="items-2">
				<div class="item-two">
					<a href=""><div class="item-empty"></div></a>
					<div class="item-subtext">Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.</div>
				</div>
				<div class="item-two">
					<a href=""><div class="item-empty"></div></a>
					<div class="item-subtext">Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.</div>
				</div>
				<div class="item-two">
					<a href=""><div class="item-empty"></div></a>
					<div class="item-subtext">Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.</div>
				</div>
				<div class="item-two">
					<a href=""><div class="item-empty"></div></a>
					<div class="item-subtext">Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.</div>
				</div>
			</div>

			<img src="<?=SITE_TEMPLATE_PATH?>/img/1.jpg" alt="">
*/?>
			
<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');?>