<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001649214887';
$dateexpire = '001685214887';
$ser_content = 'a:2:{s:7:"CONTENT";s:7668:"
<div>


		<p class="news-item" id="bx_3218110189_330">

									<h1><a href="/novosti/mir-programnogo-obespecheniya/v-rossii-poyavitsya-pervyy-marketpleys-otechestvennogo-softa">В России появится первый маркетплейс отечественного софта</a></h1>
					
					<h3>22.03.2022</h3>
		
		
									<div class="item-emty">
					<a href="/novosti/mir-programnogo-obespecheniya/v-rossii-poyavitsya-pervyy-marketpleys-otechestvennogo-softa">
						<img 
							src="/upload/iblock/d52/6tad5o9kg6lsvoasw7u92fdv3vp88ct8.jpg"
							alt="В России появится первый маркетплейс отечественного софта"
							title="В России появится первый маркетплейс отечественного софта"/>
					</a>
				</div>
					
		

		

					<p>Первый маркетплейс российского программного обеспечения планируется запустить в мае.</p>
							<div style="clear:both"></div>
							</p>
		<p class="news-item" id="bx_3218110189_326">

									<h1><a href="/novosti/mir-programnogo-obespecheniya/v-rostekhe-sozdadut-programmu-dlya-predskazaniya-mitingov-i-besporyadkov-">В «Ростехе» создадут программу для предсказания митингов и беспорядков </a></h1>
					
					<h3>17.02.2022</h3>
		
		
									<div class="item-emty">
					<a href="/novosti/mir-programnogo-obespecheniya/v-rostekhe-sozdadut-programmu-dlya-predskazaniya-mitingov-i-besporyadkov-">
						<img 
							src="/upload/iblock/7c2/y51r68i2tumlctw6rmci9ddk4zp2l33a.jpg"
							alt="В «Ростехе» создадут программу для предсказания митингов и беспорядков "
							title="В «Ростехе» создадут программу для предсказания митингов и беспорядков "/>
					</a>
				</div>
					
		

		

					<p>Система будет прогнозировать возникновение беспорядков и их эскалацию на основе публикаций в СМИ и соцсетях и данных «умных» камер. Разработка ведется в рамках проекта «Безопасный город»</p>
							<div style="clear:both"></div>
							</p>
		<p class="news-item" id="bx_3218110189_321">

									<h1><a href="/novosti/mir-programnogo-obespecheniya/operatsionnaya-sistema-phantom-ot-rossiyskogo-razrabotchika">Операционная система Phantom от российского разработчика</a></h1>
					
					<h3>26.01.2022</h3>
		
		
									<div class="item-emty">
					<a href="/novosti/mir-programnogo-obespecheniya/operatsionnaya-sistema-phantom-ot-rossiyskogo-razrabotchika">
						<img 
							src="/upload/iblock/56e/8mitaxn9ajugadkff6mtagkuoxdd77zt.png"
							alt="Операционная система Phantom от российского разработчика"
							title="Операционная система Phantom от российского разработчика"/>
					</a>
				</div>
					
		

		

					<p>По словам автора проекта, ОС Phantom предназначается, в первую очередь, для промышленного использования. Она может заинтересовать банки, военных, возможно, операторов дата-центров и энергетиков.</p>
							<div style="clear:both"></div>
							</p>
		<p class="news-item" id="bx_3218110189_319">

									<h1><a href="/novosti/mir-programnogo-obespecheniya/avtomatizatsiya-nalogovoy-sluzhby-oboydetsya-gosudarstvu-v-30-milliardov">Автоматизация налоговой службы обойдется государству в 30 миллиардов</a></h1>
					
					<h3>19.01.2022</h3>
		
		
									<div class="item-emty">
					<a href="/novosti/mir-programnogo-obespecheniya/avtomatizatsiya-nalogovoy-sluzhby-oboydetsya-gosudarstvu-v-30-milliardov">
						<img 
							src="/upload/iblock/a28/wm70wx0o0pafheixumkukatbmxariqk4.jpg"
							alt="Автоматизация налоговой службы обойдется государству в 30 миллиардов"
							title="Автоматизация налоговой службы обойдется государству в 30 миллиардов"/>
					</a>
				</div>
					
		

		

					<p>На развитие АИС «Налог-3», обеспечивающей автоматизацию Федеральной налоговой службы, до 2024 г. планируется выделить 30,1 млрд руб. В рамках намеченных мероприятий предполагается наладить межведомственный обмен данными об актах гражданского состояния, составе семьи и доходах физических лиц, создать внутренний информационный ресурс участников инвестиционной деятельности и изменить шаблоны налоговой отчетности с учетом появления новой категории налогоплательщиков.</p>
							<div style="clear:both"></div>
							</p>
		<p class="news-item" id="bx_3218110189_311">

									<h1><a href="/novosti/mir-programnogo-obespecheniya/v-rossii-zapushchen-analog-github-i-gitlab-dlya-khraneniya-koda-i-raboty-s-nim">В России запущен аналог GitHub и GitLab для хранения кода и работы с ним</a></h1>
					
					<h3>13.01.2022</h3>
		
		
									<div class="item-emty">
					<a href="/novosti/mir-programnogo-obespecheniya/v-rossii-zapushchen-analog-github-i-gitlab-dlya-khraneniya-koda-i-raboty-s-nim">
						<img 
							src="/upload/iblock/21b/hb7anmyn2quv4g8e6tamc2e3puerduzp.png"
							alt="В России запущен аналог GitHub и GitLab для хранения кода и работы с ним"
							title="В России запущен аналог GitHub и GitLab для хранения кода и работы с ним"/>
					</a>
				</div>
					
		

		

					<p>Компания «Ресолют» завершила бета-тестирование и объявила о широкой доступности GitFlic — первого в России облачного сервиса для хостинга исходного кода программных решений и их совместной разработки. Платформа подходит как для размещения проектов Open Source, так и для хранения приватных репозиториев.</p>
							<div style="clear:both"></div>
							</p>

	<br />
<font class="text">Новости 


	1 - 5 из 35<br /></font>

	<font class="text">

			Начало&nbsp;|&nbsp;Пред.&nbsp;|
	
	
					<b>1</b>
					
					<a href="/novosti/mir-programnogo-obespecheniya/?PAGEN_1=2">2</a>
					
					<a href="/novosti/mir-programnogo-obespecheniya/?PAGEN_1=3">3</a>
					
					<a href="/novosti/mir-programnogo-obespecheniya/?PAGEN_1=4">4</a>
					
					<a href="/novosti/mir-programnogo-obespecheniya/?PAGEN_1=5">5</a>
						|

			<a href="/novosti/mir-programnogo-obespecheniya/?PAGEN_1=2">След.</a>&nbsp;|
		<a href="/novosti/mir-programnogo-obespecheniya/?PAGEN_1=7">Конец</a>
	



</font>
</div>
";s:4:"VARS";a:2:{s:8:"arResult";a:8:{s:2:"ID";s:1:"1";s:14:"IBLOCK_TYPE_ID";s:11:"information";s:13:"LIST_PAGE_URL";s:47:"#SITE_DIR#/information/index.php?ID=#IBLOCK_ID#";s:15:"NAV_CACHED_DATA";N;s:4:"NAME";s:14:"Новости";s:7:"SECTION";a:1:{s:4:"PATH";a:1:{i:0;a:59:{s:2:"ID";s:1:"1";s:3:"~ID";s:1:"1";s:11:"TIMESTAMP_X";s:19:"2020-01-25 20:55:21";s:12:"~TIMESTAMP_X";s:19:"2020-01-25 20:55:21";s:11:"MODIFIED_BY";s:1:"1";s:12:"~MODIFIED_BY";s:1:"1";s:11:"DATE_CREATE";s:19:"2020-01-25 20:55:21";s:12:"~DATE_CREATE";s:19:"2020-01-25 20:55:21";s:10:"CREATED_BY";s:1:"1";s:11:"~CREATED_BY";s:1:"1";s:9:"IBLOCK_ID";s:1:"1";s:10:"~IBLOCK_ID";s:1:"1";s:17:"IBLOCK_SECTION_ID";N;s:18:"~IBLOCK_SECTION_ID";N;s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:13:"GLOBAL_ACTIVE";s:1:"Y";s:14:"~GLOBAL_ACTIVE";s:1:"Y";s:4:"SORT";s:3:"500";s:5:"~SORT";s:3:"500";s:4:"NAME";s:52:"Мир програмного обеспечения";s:5:"~NAME";s:52:"Мир програмного обеспечения";s:7:"PICTURE";s:1:"1";s:8:"~PICTURE";s:1:"1";s:11:"LEFT_MARGIN";s:1:"1";s:12:"~LEFT_MARGIN";s:1:"1";s:12:"RIGHT_MARGIN";s:1:"2";s:13:"~RIGHT_MARGIN";s:1:"2";s:11:"DEPTH_LEVEL";s:1:"1";s:12:"~DEPTH_LEVEL";s:1:"1";s:11:"DESCRIPTION";s:0:"";s:12:"~DESCRIPTION";s:0:"";s:16:"DESCRIPTION_TYPE";s:4:"text";s:17:"~DESCRIPTION_TYPE";s:4:"text";s:18:"SEARCHABLE_CONTENT";s:54:"МИР ПРОГРАМНОГО ОБЕСПЕЧЕНИЯ
";s:19:"~SEARCHABLE_CONTENT";s:54:"МИР ПРОГРАМНОГО ОБЕСПЕЧЕНИЯ
";s:4:"CODE";s:29:"mir-programnogo-obespecheniya";s:5:"~CODE";s:29:"mir-programnogo-obespecheniya";s:6:"XML_ID";N;s:7:"~XML_ID";N;s:6:"TMP_ID";N;s:7:"~TMP_ID";N;s:14:"DETAIL_PICTURE";s:1:"2";s:15:"~DETAIL_PICTURE";s:1:"2";s:15:"SOCNET_GROUP_ID";N;s:16:"~SOCNET_GROUP_ID";N;s:13:"LIST_PAGE_URL";s:9:"/novosti/";s:14:"~LIST_PAGE_URL";s:9:"/novosti/";s:16:"SECTION_PAGE_URL";s:39:"/novosti/mir-programnogo-obespecheniya/";s:17:"~SECTION_PAGE_URL";s:39:"/novosti/mir-programnogo-obespecheniya/";s:14:"IBLOCK_TYPE_ID";s:11:"information";s:15:"~IBLOCK_TYPE_ID";s:11:"information";s:11:"IBLOCK_CODE";s:4:"news";s:12:"~IBLOCK_CODE";s:4:"news";s:18:"IBLOCK_EXTERNAL_ID";N;s:19:"~IBLOCK_EXTERNAL_ID";N;s:11:"EXTERNAL_ID";N;s:12:"~EXTERNAL_ID";N;s:16:"IPROPERTY_VALUES";a:0:{}}}}s:8:"ELEMENTS";a:5:{i:0;i:330;i:1;i:326;i:2;i:321;i:3;i:319;i:4;i:311;}s:16:"IPROPERTY_VALUES";a:0:{}}s:18:"templateCachedData";a:5:{s:13:"additionalCSS";s:86:"/local/templates/robot/components/bitrix/news/news/bitrix/news.list/.default/style.css";s:9:"frameMode";b:1;s:8:"__NavNum";i:1;s:17:"__currentCounters";a:1:{s:28:"bitrix:system.pagenavigation";i:1;}s:13:"__editButtons";a:10:{i:0;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"330";i:2;N;i:3;s:31:"Изменить Новость";i:4;a:0:{}}i:1;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"330";i:2;N;i:3;s:29:"Удалить Новость";i:4;a:1:{s:7:"CONFIRM";N;}}i:2;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"326";i:2;N;i:3;s:31:"Изменить Новость";i:4;a:0:{}}i:3;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"326";i:2;N;i:3;s:29:"Удалить Новость";i:4;a:1:{s:7:"CONFIRM";N;}}i:4;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"321";i:2;N;i:3;s:31:"Изменить Новость";i:4;a:0:{}}i:5;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"321";i:2;N;i:3;s:29:"Удалить Новость";i:4;a:1:{s:7:"CONFIRM";N;}}i:6;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"319";i:2;N;i:3;s:31:"Изменить Новость";i:4;a:0:{}}i:7;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"319";i:2;N;i:3;s:29:"Удалить Новость";i:4;a:1:{s:7:"CONFIRM";N;}}i:8;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"311";i:2;N;i:3;s:31:"Изменить Новость";i:4;a:0:{}}i:9;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"311";i:2;N;i:3;s:29:"Удалить Новость";i:4;a:1:{s:7:"CONFIRM";N;}}}}}}';
return true;
?>