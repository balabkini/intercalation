<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001648666296';
$dateexpire = '001684666296';
$ser_content = 'a:2:{s:7:"CONTENT";s:32785:"<div class="news-list">


	
<pre>Array
(
    [ID] => 333
    [~ID] => 333
    [IBLOCK_ID] => 36
    [~IBLOCK_ID] => 36
    [IBLOCK_SECTION_ID] => 
    [~IBLOCK_SECTION_ID] => 
    [NAME] => Программист-разработчик
    [~NAME] => Программист-разработчик
    [ACTIVE_FROM] => 
    [~ACTIVE_FROM] => 
    [TIMESTAMP_X] => 31.03.2022 00:32:56
    [~TIMESTAMP_X] => 31.03.2022 00:32:56
    [DETAIL_PAGE_URL] => /company/vacanciescompany/?ELEMENT_ID=333
    [~DETAIL_PAGE_URL] => /company/vacanciescompany/?ELEMENT_ID=333
    [LIST_PAGE_URL] => /company/vacanciescompany/
    [~LIST_PAGE_URL] => /company/vacanciescompany/
    [DETAIL_TEXT] => 
    [~DETAIL_TEXT] => 
    [DETAIL_TEXT_TYPE] => text
    [~DETAIL_TEXT_TYPE] => text
    [PREVIEW_TEXT] => 
    [~PREVIEW_TEXT] => 
    [PREVIEW_TEXT_TYPE] => text
    [~PREVIEW_TEXT_TYPE] => text
    [PREVIEW_PICTURE] => 
    [~PREVIEW_PICTURE] => 
    [LANG_DIR] => /
    [~LANG_DIR] => /
    [CODE] => programmist-razrabotchik
    [~CODE] => programmist-razrabotchik
    [EXTERNAL_ID] => 333
    [~EXTERNAL_ID] => 333
    [IBLOCK_TYPE_ID] => information
    [~IBLOCK_TYPE_ID] => information
    [IBLOCK_CODE] => Vakansii
    [~IBLOCK_CODE] => Vakansii
    [IBLOCK_EXTERNAL_ID] => 
    [~IBLOCK_EXTERNAL_ID] => 
    [LID] => ro
    [~LID] => ro
    [EDIT_LINK] => /bitrix/admin/iblock_element_edit.php?IBLOCK_ID=36&type=information&ID=333&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Fcompany%2Fvacanciescompany%2F%3Fclear_cache%3DY
    [DELETE_LINK] => /bitrix/admin/iblock_list_admin.php?IBLOCK_ID=36&type=information&lang=ru&action=delete&ID=E333&return_url=%2Fcompany%2Fvacanciescompany%2F%3Fclear_cache%3DY
    [DISPLAY_ACTIVE_FROM] => 
    [FIELDS] => Array
        (
        )

    [PROPERTIES] => Array
        (
            [trebovanija] => Array
                (
                    [ID] => 32
                    [IBLOCK_ID] => 36
                    [NAME] => Требования
                    [ACTIVE] => Y
                    [SORT] => 500
                    [CODE] => trebovanija
                    [DEFAULT_VALUE] => 
                    [PROPERTY_TYPE] => S
                    [ROW_COUNT] => 1
                    [COL_COUNT] => 30
                    [LIST_TYPE] => L
                    [MULTIPLE] => Y
                    [XML_ID] => 
                    [FILE_TYPE] => 
                    [MULTIPLE_CNT] => 5
                    [LINK_IBLOCK_ID] => 0
                    [WITH_DESCRIPTION] => N
                    [SEARCHABLE] => N
                    [FILTRABLE] => N
                    [IS_REQUIRED] => N
                    [VERSION] => 1
                    [USER_TYPE] => 
                    [USER_TYPE_SETTINGS] => 
                    [HINT] => 
                    [~NAME] => Требования
                    [~DEFAULT_VALUE] => 
                    [VALUE_ENUM] => 
                    [VALUE_XML_ID] => 
                    [VALUE_SORT] => 
                    [VALUE] => Array
                        (
                            [0] => Знание HTML, CSS, JS, PHP, MySql, Git, Python, Битрикс;
                            [1] => Логическое мышление;
                            [2] => Начальные знания системного администрирования;
                            [3] => Начальные знания основ теоретического программирования, ООП;
                            [4] => Желание работать и обучаться.
                        )

                    [PROPERTY_VALUE_ID] => Array
                        (
                            [0] => 1034
                            [1] => 1035
                            [2] => 1036
                            [3] => 1037
                            [4] => 1038
                        )

                    [DESCRIPTION] => Array
                        (
                            [0] => 
                            [1] => 
                            [2] => 
                            [3] => 
                            [4] => 
                        )

                    [~VALUE] => Array
                        (
                            [0] => Знание HTML, CSS, JS, PHP, MySql, Git, Python, Битрикс;
                            [1] => Логическое мышление;
                            [2] => Начальные знания системного администрирования;
                            [3] => Начальные знания основ теоретического программирования, ООП;
                            [4] => Желание работать и обучаться.
                        )

                    [~DESCRIPTION] => Array
                        (
                            [0] => 
                            [1] => 
                            [2] => 
                            [3] => 
                            [4] => 
                        )

                )

            [objazannosti] => Array
                (
                    [ID] => 33
                    [IBLOCK_ID] => 36
                    [NAME] => Обязанности
                    [ACTIVE] => Y
                    [SORT] => 500
                    [CODE] => objazannosti
                    [DEFAULT_VALUE] => 
                    [PROPERTY_TYPE] => S
                    [ROW_COUNT] => 1
                    [COL_COUNT] => 30
                    [LIST_TYPE] => L
                    [MULTIPLE] => Y
                    [XML_ID] => 
                    [FILE_TYPE] => 
                    [MULTIPLE_CNT] => 5
                    [LINK_IBLOCK_ID] => 0
                    [WITH_DESCRIPTION] => N
                    [SEARCHABLE] => N
                    [FILTRABLE] => N
                    [IS_REQUIRED] => N
                    [VERSION] => 1
                    [USER_TYPE] => 
                    [USER_TYPE_SETTINGS] => 
                    [HINT] => 
                    [~NAME] => Обязанности
                    [~DEFAULT_VALUE] => 
                    [VALUE_ENUM] => 
                    [VALUE_XML_ID] => 
                    [VALUE_SORT] => 
                    [VALUE] => Array
                        (
                            [0] => Разработка программного обеспечения
                            [1] => Написания и редактирование компонентов
                        )

                    [PROPERTY_VALUE_ID] => Array
                        (
                            [0] => 1039
                            [1] => 1040
                        )

                    [DESCRIPTION] => Array
                        (
                            [0] => 
                            [1] => 
                        )

                    [~VALUE] => Array
                        (
                            [0] => Разработка программного обеспечения
                            [1] => Написания и редактирование компонентов
                        )

                    [~DESCRIPTION] => Array
                        (
                            [0] => 
                            [1] => 
                        )

                )

            [uslovija] => Array
                (
                    [ID] => 34
                    [IBLOCK_ID] => 36
                    [NAME] => Условия
                    [ACTIVE] => Y
                    [SORT] => 500
                    [CODE] => uslovija
                    [DEFAULT_VALUE] => 
                    [PROPERTY_TYPE] => S
                    [ROW_COUNT] => 1
                    [COL_COUNT] => 30
                    [LIST_TYPE] => L
                    [MULTIPLE] => Y
                    [XML_ID] => 
                    [FILE_TYPE] => 
                    [MULTIPLE_CNT] => 5
                    [LINK_IBLOCK_ID] => 0
                    [WITH_DESCRIPTION] => N
                    [SEARCHABLE] => N
                    [FILTRABLE] => N
                    [IS_REQUIRED] => N
                    [VERSION] => 1
                    [USER_TYPE] => 
                    [USER_TYPE_SETTINGS] => 
                    [HINT] => 
                    [~NAME] => Условия
                    [~DEFAULT_VALUE] => 
                    [VALUE_ENUM] => 
                    [VALUE_XML_ID] => 
                    [VALUE_SORT] => 
                    [VALUE] => Array
                        (
                            [0] => Заработная плата 2 раза в месяц;
                            [1] => 5-дневная рабочая неделя;
                            [2] => Комфортные условия труда.
                        )

                    [PROPERTY_VALUE_ID] => Array
                        (
                            [0] => 1041
                            [1] => 1042
                            [2] => 1043
                        )

                    [DESCRIPTION] => Array
                        (
                            [0] => 
                            [1] => 
                            [2] => 
                        )

                    [~VALUE] => Array
                        (
                            [0] => Заработная плата 2 раза в месяц;
                            [1] => 5-дневная рабочая неделя;
                            [2] => Комфортные условия труда.
                        )

                    [~DESCRIPTION] => Array
                        (
                            [0] => 
                            [1] => 
                            [2] => 
                        )

                )

            [oplata] => Array
                (
                    [ID] => 35
                    [IBLOCK_ID] => 36
                    [NAME] => Оплата
                    [ACTIVE] => Y
                    [SORT] => 500
                    [CODE] => oplata
                    [DEFAULT_VALUE] => 
                    [PROPERTY_TYPE] => S
                    [ROW_COUNT] => 1
                    [COL_COUNT] => 30
                    [LIST_TYPE] => L
                    [MULTIPLE] => N
                    [XML_ID] => 
                    [FILE_TYPE] => 
                    [MULTIPLE_CNT] => 5
                    [LINK_IBLOCK_ID] => 0
                    [WITH_DESCRIPTION] => N
                    [SEARCHABLE] => N
                    [FILTRABLE] => N
                    [IS_REQUIRED] => N
                    [VERSION] => 1
                    [USER_TYPE] => 
                    [USER_TYPE_SETTINGS] => 
                    [HINT] => 
                    [~NAME] => Оплата
                    [~DEFAULT_VALUE] => 
                    [VALUE_ENUM] => 
                    [VALUE_XML_ID] => 
                    [VALUE_SORT] => 
                    [VALUE] => 100 — 500 ₽ в час
                    [PROPERTY_VALUE_ID] => 1044
                    [DESCRIPTION] => 
                    [~VALUE] => 100 — 500 ₽ в час
                    [~DESCRIPTION] => 
                )

            [obrazovanie] => Array
                (
                    [ID] => 36
                    [IBLOCK_ID] => 36
                    [NAME] => Образование
                    [ACTIVE] => Y
                    [SORT] => 500
                    [CODE] => obrazovanie
                    [DEFAULT_VALUE] => 
                    [PROPERTY_TYPE] => S
                    [ROW_COUNT] => 1
                    [COL_COUNT] => 30
                    [LIST_TYPE] => L
                    [MULTIPLE] => N
                    [XML_ID] => 
                    [FILE_TYPE] => 
                    [MULTIPLE_CNT] => 5
                    [LINK_IBLOCK_ID] => 0
                    [WITH_DESCRIPTION] => N
                    [SEARCHABLE] => N
                    [FILTRABLE] => N
                    [IS_REQUIRED] => N
                    [VERSION] => 1
                    [USER_TYPE] => 
                    [USER_TYPE_SETTINGS] => 
                    [HINT] => 
                    [~NAME] => Образование
                    [~DEFAULT_VALUE] => 
                    [VALUE_ENUM] => 
                    [VALUE_XML_ID] => 
                    [VALUE_SORT] => 
                    [VALUE] => не ниже среднего
                    [PROPERTY_VALUE_ID] => 1045
                    [DESCRIPTION] => 
                    [~VALUE] => не ниже среднего
                    [~DESCRIPTION] => 
                )

        )

    [DISPLAY_PROPERTIES] => Array
        (
            [obrazovanie] => Array
                (
                    [ID] => 36
                    [IBLOCK_ID] => 36
                    [NAME] => Образование
                    [ACTIVE] => Y
                    [SORT] => 500
                    [CODE] => obrazovanie
                    [DEFAULT_VALUE] => 
                    [PROPERTY_TYPE] => S
                    [ROW_COUNT] => 1
                    [COL_COUNT] => 30
                    [LIST_TYPE] => L
                    [MULTIPLE] => N
                    [XML_ID] => 
                    [FILE_TYPE] => 
                    [MULTIPLE_CNT] => 5
                    [LINK_IBLOCK_ID] => 0
                    [WITH_DESCRIPTION] => N
                    [SEARCHABLE] => N
                    [FILTRABLE] => N
                    [IS_REQUIRED] => N
                    [VERSION] => 1
                    [USER_TYPE] => 
                    [USER_TYPE_SETTINGS] => 
                    [HINT] => 
                    [~NAME] => Образование
                    [~DEFAULT_VALUE] => 
                    [VALUE_ENUM] => 
                    [VALUE_XML_ID] => 
                    [VALUE_SORT] => 
                    [VALUE] => не ниже среднего
                    [PROPERTY_VALUE_ID] => 1045
                    [DESCRIPTION] => 
                    [~VALUE] => не ниже среднего
                    [~DESCRIPTION] => 
                    [DISPLAY_VALUE] => не ниже среднего
                )

            [objazannosti] => Array
                (
                    [ID] => 33
                    [IBLOCK_ID] => 36
                    [NAME] => Обязанности
                    [ACTIVE] => Y
                    [SORT] => 500
                    [CODE] => objazannosti
                    [DEFAULT_VALUE] => 
                    [PROPERTY_TYPE] => S
                    [ROW_COUNT] => 1
                    [COL_COUNT] => 30
                    [LIST_TYPE] => L
                    [MULTIPLE] => Y
                    [XML_ID] => 
                    [FILE_TYPE] => 
                    [MULTIPLE_CNT] => 5
                    [LINK_IBLOCK_ID] => 0
                    [WITH_DESCRIPTION] => N
                    [SEARCHABLE] => N
                    [FILTRABLE] => N
                    [IS_REQUIRED] => N
                    [VERSION] => 1
                    [USER_TYPE] => 
                    [USER_TYPE_SETTINGS] => 
                    [HINT] => 
                    [~NAME] => Обязанности
                    [~DEFAULT_VALUE] => 
                    [VALUE_ENUM] => 
                    [VALUE_XML_ID] => 
                    [VALUE_SORT] => 
                    [VALUE] => Array
                        (
                            [0] => Разработка программного обеспечения
                            [1] => Написания и редактирование компонентов
                        )

                    [PROPERTY_VALUE_ID] => Array
                        (
                            [0] => 1039
                            [1] => 1040
                        )

                    [DESCRIPTION] => Array
                        (
                            [0] => 
                            [1] => 
                        )

                    [~VALUE] => Array
                        (
                            [0] => Разработка программного обеспечения
                            [1] => Написания и редактирование компонентов
                        )

                    [~DESCRIPTION] => Array
                        (
                            [0] => 
                            [1] => 
                        )

                    [DISPLAY_VALUE] => Array
                        (
                            [0] => Разработка программного обеспечения
                            [1] => Написания и редактирование компонентов
                        )

                )

            [oplata] => Array
                (
                    [ID] => 35
                    [IBLOCK_ID] => 36
                    [NAME] => Оплата
                    [ACTIVE] => Y
                    [SORT] => 500
                    [CODE] => oplata
                    [DEFAULT_VALUE] => 
                    [PROPERTY_TYPE] => S
                    [ROW_COUNT] => 1
                    [COL_COUNT] => 30
                    [LIST_TYPE] => L
                    [MULTIPLE] => N
                    [XML_ID] => 
                    [FILE_TYPE] => 
                    [MULTIPLE_CNT] => 5
                    [LINK_IBLOCK_ID] => 0
                    [WITH_DESCRIPTION] => N
                    [SEARCHABLE] => N
                    [FILTRABLE] => N
                    [IS_REQUIRED] => N
                    [VERSION] => 1
                    [USER_TYPE] => 
                    [USER_TYPE_SETTINGS] => 
                    [HINT] => 
                    [~NAME] => Оплата
                    [~DEFAULT_VALUE] => 
                    [VALUE_ENUM] => 
                    [VALUE_XML_ID] => 
                    [VALUE_SORT] => 
                    [VALUE] => 100 — 500 ₽ в час
                    [PROPERTY_VALUE_ID] => 1044
                    [DESCRIPTION] => 
                    [~VALUE] => 100 — 500 ₽ в час
                    [~DESCRIPTION] => 
                    [DISPLAY_VALUE] => 100 — 500 ₽ в час
                )

            [trebovanija] => Array
                (
                    [ID] => 32
                    [IBLOCK_ID] => 36
                    [NAME] => Требования
                    [ACTIVE] => Y
                    [SORT] => 500
                    [CODE] => trebovanija
                    [DEFAULT_VALUE] => 
                    [PROPERTY_TYPE] => S
                    [ROW_COUNT] => 1
                    [COL_COUNT] => 30
                    [LIST_TYPE] => L
                    [MULTIPLE] => Y
                    [XML_ID] => 
                    [FILE_TYPE] => 
                    [MULTIPLE_CNT] => 5
                    [LINK_IBLOCK_ID] => 0
                    [WITH_DESCRIPTION] => N
                    [SEARCHABLE] => N
                    [FILTRABLE] => N
                    [IS_REQUIRED] => N
                    [VERSION] => 1
                    [USER_TYPE] => 
                    [USER_TYPE_SETTINGS] => 
                    [HINT] => 
                    [~NAME] => Требования
                    [~DEFAULT_VALUE] => 
                    [VALUE_ENUM] => 
                    [VALUE_XML_ID] => 
                    [VALUE_SORT] => 
                    [VALUE] => Array
                        (
                            [0] => Знание HTML, CSS, JS, PHP, MySql, Git, Python, Битрикс;
                            [1] => Логическое мышление;
                            [2] => Начальные знания системного администрирования;
                            [3] => Начальные знания основ теоретического программирования, ООП;
                            [4] => Желание работать и обучаться.
                        )

                    [PROPERTY_VALUE_ID] => Array
                        (
                            [0] => 1034
                            [1] => 1035
                            [2] => 1036
                            [3] => 1037
                            [4] => 1038
                        )

                    [DESCRIPTION] => Array
                        (
                            [0] => 
                            [1] => 
                            [2] => 
                            [3] => 
                            [4] => 
                        )

                    [~VALUE] => Array
                        (
                            [0] => Знание HTML, CSS, JS, PHP, MySql, Git, Python, Битрикс;
                            [1] => Логическое мышление;
                            [2] => Начальные знания системного администрирования;
                            [3] => Начальные знания основ теоретического программирования, ООП;
                            [4] => Желание работать и обучаться.
                        )

                    [~DESCRIPTION] => Array
                        (
                            [0] => 
                            [1] => 
                            [2] => 
                            [3] => 
                            [4] => 
                        )

                    [DISPLAY_VALUE] => Array
                        (
                            [0] => Знание HTML, CSS, JS, PHP, MySql, Git, Python, Битрикс;
                            [1] => Логическое мышление;
                            [2] => Начальные знания системного администрирования;
                            [3] => Начальные знания основ теоретического программирования, ООП;
                            [4] => Желание работать и обучаться.
                        )

                )

            [uslovija] => Array
                (
                    [ID] => 34
                    [IBLOCK_ID] => 36
                    [NAME] => Условия
                    [ACTIVE] => Y
                    [SORT] => 500
                    [CODE] => uslovija
                    [DEFAULT_VALUE] => 
                    [PROPERTY_TYPE] => S
                    [ROW_COUNT] => 1
                    [COL_COUNT] => 30
                    [LIST_TYPE] => L
                    [MULTIPLE] => Y
                    [XML_ID] => 
                    [FILE_TYPE] => 
                    [MULTIPLE_CNT] => 5
                    [LINK_IBLOCK_ID] => 0
                    [WITH_DESCRIPTION] => N
                    [SEARCHABLE] => N
                    [FILTRABLE] => N
                    [IS_REQUIRED] => N
                    [VERSION] => 1
                    [USER_TYPE] => 
                    [USER_TYPE_SETTINGS] => 
                    [HINT] => 
                    [~NAME] => Условия
                    [~DEFAULT_VALUE] => 
                    [VALUE_ENUM] => 
                    [VALUE_XML_ID] => 
                    [VALUE_SORT] => 
                    [VALUE] => Array
                        (
                            [0] => Заработная плата 2 раза в месяц;
                            [1] => 5-дневная рабочая неделя;
                            [2] => Комфортные условия труда.
                        )

                    [PROPERTY_VALUE_ID] => Array
                        (
                            [0] => 1041
                            [1] => 1042
                            [2] => 1043
                        )

                    [DESCRIPTION] => Array
                        (
                            [0] => 
                            [1] => 
                            [2] => 
                        )

                    [~VALUE] => Array
                        (
                            [0] => Заработная плата 2 раза в месяц;
                            [1] => 5-дневная рабочая неделя;
                            [2] => Комфортные условия труда.
                        )

                    [~DESCRIPTION] => Array
                        (
                            [0] => 
                            [1] => 
                            [2] => 
                        )

                    [DISPLAY_VALUE] => Array
                        (
                            [0] => Заработная плата 2 раза в месяц;
                            [1] => 5-дневная рабочая неделя;
                            [2] => Комфортные условия труда.
                        )

                )

        )

    [IPROPERTY_VALUES] => Array
        (
        )

)
Array
(
    [IBLOCK_TYPE] => information
    [IBLOCK_ID] => 36
    [NEWS_COUNT] => 20
    [SORT_BY1] => ACTIVE_FROM
    [SORT_ORDER1] => DESC
    [SORT_BY2] => SORT
    [SORT_ORDER2] => ASC
    [FIELD_CODE] => Array
        (
        )

    [PROPERTY_CODE] => Array
        (
            [0] => obrazovanie
            [1] => objazannosti
            [2] => oplata
            [3] => trebovanija
            [4] => uslovija
        )

    [DETAIL_URL] => /company/vacanciescompany/?ELEMENT_ID=#ELEMENT_ID#
    [SECTION_URL] => /company/vacanciescompany/?SECTION_ID=#SECTION_ID#
    [IBLOCK_URL] => /company/vacanciescompany/
    [DISPLAY_PANEL] => 
    [SET_TITLE] => 1
    [SET_LAST_MODIFIED] => 
    [MESSAGE_404] => 
    [SET_STATUS_404] => N
    [SHOW_404] => N
    [FILE_404] => 
    [INCLUDE_IBLOCK_INTO_CHAIN] => 1
    [CACHE_TYPE] => A
    [CACHE_TIME] => 36000000
    [CACHE_FILTER] => 
    [CACHE_GROUPS] => Y
    [DISPLAY_TOP_PAGER] => 
    [DISPLAY_BOTTOM_PAGER] => 
    [PAGER_TITLE] => Вакансии
    [PAGER_TEMPLATE] => .default
    [PAGER_SHOW_ALWAYS] => 
    [PAGER_DESC_NUMBERING] => 
    [PAGER_DESC_NUMBERING_CACHE_TIME] => 36000
    [PAGER_SHOW_ALL] => 
    [PAGER_BASE_LINK_ENABLE] => N
    [PAGER_BASE_LINK] => 
    [PAGER_PARAMS_NAME] => 
    [DISPLAY_DATE] => Y
    [DISPLAY_NAME] => Y
    [DISPLAY_PICTURE] => Y
    [DISPLAY_PREVIEW_TEXT] => Y
    [PREVIEW_TRUNCATE_LEN] => 0
    [ACTIVE_DATE_FORMAT] => d.m.Y
    [USE_PERMISSIONS] => 
    [GROUP_PERMISSIONS] => Array
        (
            [0] => 1
        )

    [FILTER_NAME] => 
    [HIDE_LINK_WHEN_NO_DETAIL] => 
    [CHECK_DATES] => 1
    [~IBLOCK_TYPE] => information
    [~IBLOCK_ID] => 36
    [~NEWS_COUNT] => 20
    [~SORT_BY1] => ACTIVE_FROM
    [~SORT_ORDER1] => DESC
    [~SORT_BY2] => SORT
    [~SORT_ORDER2] => ASC
    [~FIELD_CODE] => Array
        (
            [0] => 
            [1] => 
        )

    [~PROPERTY_CODE] => Array
        (
            [0] => obrazovanie
            [1] => objazannosti
            [2] => oplata
            [3] => trebovanija
            [4] => uslovija
            [5] => 
        )

    [~DETAIL_URL] => /company/vacanciescompany/?ELEMENT_ID=#ELEMENT_ID#
    [~SECTION_URL] => /company/vacanciescompany/?SECTION_ID=#SECTION_ID#
    [~IBLOCK_URL] => /company/vacanciescompany/
    [~DISPLAY_PANEL] => 
    [~SET_TITLE] => Y
    [~SET_LAST_MODIFIED] => N
    [~MESSAGE_404] => 
    [~SET_STATUS_404] => N
    [~SHOW_404] => N
    [~FILE_404] => 
    [~INCLUDE_IBLOCK_INTO_CHAIN] => Y
    [~CACHE_TYPE] => A
    [~CACHE_TIME] => 36000000
    [~CACHE_FILTER] => N
    [~CACHE_GROUPS] => Y
    [~DISPLAY_TOP_PAGER] => N
    [~DISPLAY_BOTTOM_PAGER] => N
    [~PAGER_TITLE] => Вакансии
    [~PAGER_TEMPLATE] => .default
    [~PAGER_SHOW_ALWAYS] => N
    [~PAGER_DESC_NUMBERING] => N
    [~PAGER_DESC_NUMBERING_CACHE_TIME] => 36000
    [~PAGER_SHOW_ALL] => N
    [~PAGER_BASE_LINK_ENABLE] => N
    [~PAGER_BASE_LINK] => 
    [~PAGER_PARAMS_NAME] => 
    [~DISPLAY_DATE] => Y
    [~DISPLAY_NAME] => Y
    [~DISPLAY_PICTURE] => Y
    [~DISPLAY_PREVIEW_TEXT] => Y
    [~PREVIEW_TRUNCATE_LEN] => 
    [~ACTIVE_DATE_FORMAT] => d.m.Y
    [~USE_PERMISSIONS] => N
    [~GROUP_PERMISSIONS] => 
    [~FILTER_NAME] => 
    [~HIDE_LINK_WHEN_NO_DETAIL] => N
    [~CHECK_DATES] => Y
    [PARENT_SECTION] => 0
    [INCLUDE_SUBSECTIONS] => 1
    [SET_BROWSER_TITLE] => Y
    [SET_META_KEYWORDS] => Y
    [SET_META_DESCRIPTION] => Y
    [ADD_SECTIONS_CHAIN] => 1
    [STRICT_SECTION_CHECK] => 
    [CHECK_PERMISSIONS] => 1
)
</pre>

<div class="card-jobs">
				<h1>Менеджер по продажам</h1>
				<p class="price-jobs">
					 25 000 - 50 000 ₽
				</p>
				<p class="text-jobs">
					 Среднее образование и стаж работы 3-5 лет.<br>
					<strong>Обязанности:</strong>
				</p>
				<ul class="pills">
					<li>Деловые встречи и переговоры, проведение презентаций;</li>
					<li>Консультация по ассортименту услуг компании и проводимым акциям;</li>
					<li>Отрабатывает возражения, отказы, сомнения;</li>
					<li>Организация маркетинговых мероприятий;</li>
					<li>БЕЗ ПОИСКА КЛИЕНТОВ, клиентская база предоставляется;</li>
					<li>Работа в CRM ,работа с теплыми клиентами.</li>
				</ul>
				<p class="text-jobs"><strong>Требования:</strong></p>
				<ul class="pills">
					<li>Навыки ведения переговоров;</li>
					<li>Грамотная устная и письменная речь;</li>
					<li>Навыки проведения презентаций; Рассмотрим молодого специалиста Условия: Заработная плата 2 раза в месяц; 5-дневная рабочая неделя; Комфортные условия труда;</li>
					<li>Активность, нацеленность на результат, грамотная речь, умение убеждать;</li>
					<li>Умение преодолевать возражения;</li>
					<li>Готовность к обучению, стремление к саморазвитию;</li>
					<li>Коммуникабельность, позитивный настрой, доброжелательность, ответственность, внимательность;</li>
					<li>Рассмотрим молодого специалиста.</li>
				</ul>
				<p class="text-jobs"><strong>Условия:</strong></p>
				<ul class="pills">
					<li>Заработная плата 2 раза в месяц;</li>
					<li>5-дневная рабочая неделя; </li>
					<li>Комфортные условия труда.</li>
				</ul>
				<p><button id="btn-send2-form">Откликнуться</button></p>
				<p>
				</p>
			</div>


	<p class="news-item" id="bx_3218110189_333">
													<a href="/company/vacanciescompany/?ELEMENT_ID=333"><b>Программист-разработчик</b></a><br />
																<small>
			Образование:&nbsp;
							не ниже среднего						</small><br />
					<small>
			Обязанности:&nbsp;
							Разработка программного обеспечения&nbsp;/&nbsp;Написания и редактирование компонентов						</small><br />
					<small>
			Оплата:&nbsp;
							100 — 500 ₽ в час						</small><br />
					<small>
			Требования:&nbsp;
							Знание HTML, CSS, JS, PHP, MySql, Git, Python, Битрикс;&nbsp;/&nbsp;Логическое мышление;&nbsp;/&nbsp;Начальные знания системного администрирования;&nbsp;/&nbsp;Начальные знания основ теоретического программирования, ООП;&nbsp;/&nbsp;Желание работать и обучаться.						</small><br />
					<small>
			Условия:&nbsp;
							Заработная плата 2 раза в месяц;&nbsp;/&nbsp;5-дневная рабочая неделя;&nbsp;/&nbsp;Комфортные условия труда.						</small><br />
			</p>
</div>
";s:4:"VARS";a:2:{s:8:"arResult";a:7:{s:2:"ID";s:2:"36";s:14:"IBLOCK_TYPE_ID";s:11:"information";s:13:"LIST_PAGE_URL";s:47:"#SITE_DIR#/information/index.php?ID=#IBLOCK_ID#";s:15:"NAV_CACHED_DATA";N;s:4:"NAME";s:16:"Вакансии";s:7:"SECTION";b:0;s:8:"ELEMENTS";a:1:{i:0;i:333;}}s:18:"templateCachedData";a:4:{s:13:"additionalCSS";s:90:"/local/templates/robot/components/bitrix/news/vakansii/bitrix/news.list/.default/style.css";s:9:"frameMode";b:1;s:17:"__currentCounters";a:1:{s:28:"bitrix:system.pagenavigation";i:1;}s:13:"__editButtons";a:2:{i:0;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"333";i:2;s:206:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=36&type=information&ID=333&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Fcompany%2Fvacanciescompany%2F%3Fclear_cache%3DY";i:3;s:33:"Изменить вакансию";i:4;a:0:{}}i:1;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"333";i:2;s:157:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=36&type=information&lang=ru&action=delete&ID=E333&return_url=%2Fcompany%2Fvacanciescompany%2F%3Fclear_cache%3DY";i:3;s:31:"Удалить вакансию";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}}}}}';
return true;
?>