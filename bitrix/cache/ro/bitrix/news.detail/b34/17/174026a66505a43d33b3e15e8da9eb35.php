<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001649333141';
$dateexpire = '001685333141';
$ser_content = 'a:2:{s:7:"CONTENT";s:26158:"

<div>

			<h3>Аспро: Маркет - адаптивный интернет-магазин</h3>	
	
	<div class="b-box">
			<img
			src="/upload/iblock/1b2/1b244b0c836840e072740f9c855b2a2a.png"
			alt="Аспро: Маркет - адаптивный интернет-магазин"
			title="Аспро: Маркет - адаптивный интернет-магазин"
			/>
		</div>

		
	
				<h4>Описание</h4>
 <img width="800" src="https://aspro.ru/upload/images/marketplace/market/bunner_top.png" height="100" border="0"><br>
 <br>
 Аспро: Маркет&nbsp;- это&nbsp;универсальный интернет-магазин для продажи любых видов товаров. Решение прекрасно адаптировано для продажи одежды, электроники, бытовой техники, стройматериалов, детских товаров, оборудования, продуктов питания и прочих товаров.<br>
 &nbsp;<br>
 Основные преимущества решения:
<ul>
	<li>Адаптивная верстка для компьютеров, планшетов и мобильных устройств;<br>
 </li>
	<li>Поддерживает PHP7 для еще более высокой производительности и увеличения скорости загрузки;<br>
 </li>
	<li>Полное соответствие последним стандартам&nbsp;Google Mobile Friendly&nbsp;(<a href="https://www.google.com/webmasters/tools/mobile-friendly/?hl=ru&url=http%3A%2F%2Fmarket.aspro-demo.ru%2F" target="_blank">результаты проверки решения</a>);<br>
 </li>
	<li>Более 100 различных вариантов оформления;<br>
 </li>
	<li>Поддержка торговых предложений: цвет, размер, фасовка и т.п. (<a href="http://market.aspro-demo.ru/catalog/odezhda/muzhskaya_odezhda/" target="_blank">пример реализации</a>);<br>
 </li>
	<li>Полноценная работа с единицами измерения (шт., кв.м., кг. и т.д.);<br>
 </li>
	<li>Поддержка валют - управление валютами и курсами валют;<br>
 </li>
	<li>Оптимизирован для SEO-продвижения: решение поддерживает шаблоны meta-тэгов для автоматического создания уникальных title, description, keywords и т.д.<br>
 </li>
</ul>
 <img width="800" src="https://aspro.ru/upload/images/marketplace/next/you_are_welcome_title_01.png" height="92" border="0"><br>
<table>
<tbody>
<tr>
	<td>
 <img width="47" src="https://aspro.ru/upload/images/marketplace/next/contacts_icn_01.png" height="28" border="0">
	</td>
	<td>
		 Электронная почта:<br>
 <a href="mailto:info@aspro.ru" target="_blank">info@aspro.ru</a>
	</td>
	<td>
 <img width="125" src="https://aspro.ru/upload/images/marketplace/next/contacts_icn_02.png" height="28" border="0">
	</td>
	<td>
		 Телефон для связи:<br>
		 8 (800) 500-47-11 (звонок бесплатный)
	</td>
	<td>
 <img width="82" src="https://aspro.ru/upload/images/marketplace/next/contacts_icn_03.png" height="28" border="0">
	</td>
	<td>
		 Техническая поддержка:<br>
 <a href="https://aspro.ru/support/" target="_blank">https://aspro.ru/support/</a>
	</td>
</tr>
</tbody>
</table>
 <br>
 <img width="800" src="https://aspro.ru/upload/images/marketplace/next/techsupport_01.png" height="201" border="0"><br>
<table>
<tbody>
<tr>
	<td>
		<table>
		<tbody>
		<tr>
			<td>
				 Для всех пользователей с активной лицензией решений Аспро доступна мгновенная поддержка в режиме онлайн – через чат в админке сайта.
			</td>
		</tr>
		</tbody>
		</table>
 <br>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table>
		<tbody>
		<tr>
			<td>
				<p>
					 Актуальные новинки<br>
				</p>
				<p>
					 Обзор обновления —&nbsp;<a href="https://aspro.ru/news/5991/?utm_source=marketplace&utm_medium=description&utm_campaign=vm_update_market_175_29102020&utm_term=obzor" target="_blank">версия 1.7.5</a>:
				</p>
				<p>
				</p>
				<ul>
					<li>Компактный умный фильтр для удобного подбора товаров на десктопах и мобильных устройствах.<br>
 </li>
					<li>Баннер с наездом на шапку и прозрачный верх для расширенного отображения.<br>
 </li>
					<li>Картинки разделов в выпадающем меню шапки для навигации.<br>
 </li>
				</ul>
				<p>
					 Обзор обновления —&nbsp;<a href="https://aspro.ru/news/5886/?utm_source=marketplace&utm_medium=description&utm_campaign=ks_250820_update_market_1_7_4&utm_term=obzor" target="_blank">версия 1.7.4</a>:
				</p>
				<p>
				</p>
				<ul>
					<li>Перемещение блока «Популярные категории» в каталоге для удобной навигации.<br>
 </li>
					<li>Раздел «Отзывы» для повышения доверия клиентов.<br>
 </li>
					<li>Несколько телефонов в разделе «Контакты», чтобы пользователь мог легко связаться с вами.<br>
 </li>
					<li>Возможность управлять настройками блока «Ранее вы смотрели».<br>
 </li>
				</ul>
				<p>
					 Обзор обновления —&nbsp;<a href="https://aspro.ru/company/news/5711/?utm_source=marketplace&utm_medium=description&utm_campaign=ks_080420_update_market_1_7_0&utm_term=obzor" target="_blank">версия 1.7.0</a>:
				</p>
				<ul>
					<li>Упрощенная привязка товаров для настройки перекрестных продаж.<br>
 </li>
					<li>Предсказания в карточке товара.<br>
 </li>
					<li>Обновленная версия Google reCAPTCHA.<br>
 </li>
					<li>Возможность скрывать недоступные товары в поиске в шапке сайта.<br>
 </li>
					<li>Установка ссылок на Pinterest, Tik Tok, Яндекс.Дзен, Snapchat.<br>
 </li>
					<li>Возможность не заливать логотип в цвет сайта.<br>
 </li>
				</ul>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
 <br>
<table>
<tbody>
<tr>
	<td>
	</td>
</tr>
</tbody>
</table>
 <img width="800" src="https://aspro.ru/upload/images/marketplace/next/docs_01.png" height="201" border="0"><br>
 <br>
 <br>
 <br>
 <a href="https://aspro.ru/docs/course/index.php?COURSE_ID=5&INDEX=Y&utm_source=marketplace&utm_medium=description&utm_campaign=market&utm_term=doc" target="_blank"><img width="260" src="https://aspro.ru/upload/images/marketplace/next/docs_pic_01.png" height="296" border="0"></a><a href="https://aspro.ru/kb/aspro.mshop/?utm_source=marketplace&utm_medium=description&utm_campaign=market&utm_term=bz" target="_blank"><img width="264" src="https://aspro.ru/upload/images/marketplace/next/docs_pic_02.png" height="296" border="0"></a><a href="https://www.youtube.com/channel/UCvNfOlv6ELNl95oi_Q8xooA?view_as=subscriber" target="_blank"><img width="260" src="https://aspro.ru/upload/images/marketplace/next/docs_pic_03.png" height="296" border="0"></a><br>
 Более 300 полезных материалов!<br>
 <br>
 <br>
 <img width="800" src="https://aspro.ru/upload/images/marketplace/market/adaptive.png" height="436" border="0"><br>
 Для пользователей редакции&nbsp;1С-Битрикс: Бизнес&nbsp;доступны дополнительные возможности решения:
<ul>
	<li>Поддержка&nbsp;<a href="http://market.aspro-demo.ru/catalog/elektroinstrument/perforatory/" target="_blank">различных типов цен</a>&nbsp;- Оптовая, Розничная, Партнерская с возможностью назначения их отдельным группам пользователей;<br>
 </li>
	<li>Поддержка&nbsp;<a href="http://www.1c-bitrix.ru/products/cms/features/tradecatalog.php#tab-bandles-link!bandl" target="_blank">наборов и комплектов</a>&nbsp;(<a href="http://market.aspro-demo.ru/catalog/sport/velosipedy/295/" target="_blank">пример набора</a>,&nbsp;<a href="http://market.aspro-demo.ru/catalog/foto_i_video/fotoapparaty/514/" target="_blank">пример комплекта</a>);<br>
 </li>
	<li>Возможность отображения остатков в разрезе складов (<a href="http://market.aspro-demo.ru/catalog/avtoelektronika/gps_navigatory/196/" target="_blank">пример на вкладке “Наличие по складам</a>”)<br>
 </li>
</ul>
 Поддержка всех новинок 1С-Битрикс:
<ul>
	<li>Поддержка сервиса&nbsp;<a href="http://www.1c-bitrix.ru/products/cms/features/bigdata.php" target="_blank">1C-Битрикс BigData</a>. Блок “Персональные рекомендации” на главной странице, в карточке товара и в корзине;<br>
 </li>
	<li><a href="http://www.1c-bitrix.ru/products/cms/new/index.php#d71" target="_blank">Оформление заказа D7</a>;<br>
 </li>
	<li>Умный фильтр с поддержкой&nbsp;<a href="http://www.1c-bitrix.ru/products/cms/features/tradecatalog.php#facet" target="_blank">Фасетного индекса</a>&nbsp;- мгновенный поиск по каталогу;<br>
 </li>
	<li><a href="http://www.1c-bitrix.ru/products/cms/new/#tab-puls-link" target="_blank">Пульс конверсии</a>&nbsp;для отслеживания ключевых показателей эффективности вашего магазина;<br>
 </li>
	<li>Поддержка технологии&nbsp;<a href="http://www.1c-bitrix.ru/products/cms/new/index.php#tab-compozite-link" target="_blank">Автокомпозит</a>&nbsp;для ускорения сайта в сотни раз;<br>
 </li>
	<li>Быстрый запуск интернет-магазина за 1 час с помощью&nbsp;<a href="http://www.1c-bitrix.ru/products/cms/new/#tab-wizard-link" target="_blank">Мастера настройки интернет-магазина</a>;<br>
 </li>
	<li>Быстрый морфологический поиск по технологии&nbsp;<a href="http://www.1c-bitrix.ru/products/cms/features/search.php" target="_blank">Sphinx</a>;<br>
 </li>
	<li>Поддержка интеграции с бесплатной облачной CRM&nbsp;<a href="http://www.1c-bitrix.ru/products/cms/features/crm.php" target="_blank">1С-Битрикс24</a>;<br>
 </li>
	<li>Бесплатное&nbsp;<a href="http://www.1c-bitrix.ru/products/mobile/adm.php" target="_blank">мобильное приложение</a>&nbsp;для администратора магазина;<br>
 </li>
</ul>
 Настраивайте решение под Ваш фирменный стиль<br>
 В любой момент Вы можете легко с помощью панели управления изменять настройки сайта и выбирать из:
<ul>
	<li>Неограниченного числа цветовых схем - используйте любой базовый цвет;<br>
 </li>
	<li>Поддержка шрифтов Google Fonts;<br>
 </li>
	<li>8-ми типов расположения главного меню;<br>
 </li>
	<li>3-х видов центрального баннера;<br>
 </li>
	<li>2-х видов блоков магазинов на главной странице;<br>
 </li>
	<li>2-х типов отображения торговых предложений;<br>
 </li>
	<li>2-х видов расположения умного фильтра;<br>
 </li>
	<li>2-х типов корзины - классическая в шапке сайта или летающая.<br>
 </li>
</ul>
 Попробуйте различные настройки решения в&nbsp;<a href="http://market.aspro-demo.ru/" target="_blank">демо-версии</a>!<br>
 <img width="800" src="https://aspro.ru/upload/images/marketplace/market/options.png" height="736" border="0"><br>
 Продающая главная страница<br>
 Главная страница решения спроектирована таким образом, чтобы максимально увеличить ваши продажи:
<ul>
	<li>Центральный баннер для самых важных акций;<br>
 </li>
	<li>Блок ключевых преимуществ компании;<br>
 </li>
	<li>Блок дополнительных баннеров в стиле Masonry;<br>
 </li>
	<li>Блок спецпредложений по товарам из каталога;<br>
 </li>
	<li>Обязательный блок “Хит, Советуем, Новинки, Акции”;<br>
 </li>
	<li>Линейка Брендов;<br>
 </li>
	<li>Новостная лента;<br>
 </li>
	<li>Блок Действующих акций;<br>
 </li>
	<li>Модуль подписки на рассылку;<br>
 </li>
	<li>Адресный блок со списком магазинов;<br>
 </li>
	<li>Текстовый блок для размещения SEO-текста;<br>
 </li>
	<li>Модуль недавно просмотренных товаров.<br>
 </li>
</ul>
 Функциональный каталог товаров<br>
 Для любого интернет-магазина каталог товаров - это самый важный раздел сайта. Мы уделили пристальное внимание каждой детали, чтобы Вашим клиентам было максимально удобно совершать покупки.<br>
 <br>
 В решении реализованы:
<ul>
	<li>три вида отображения товаров: плиткой, таблицей или прайс-листом, при этом для каждой категории товаров вы можете задать свой вид отображения по-умолчанию;<br>
 </li>
	<li>информер наличия товара с тремя видами отображения: &nbsp;индикатор наличия, числовой остаток, строковый остаток (в наличии/под заказ, остаток числом, много/мало/достаточно);<br>
 </li>
	<li>функциональный умный фильтр с подсветкой только возможных сочетаний значений свойств;<br>
 </li>
	<li>поддержка свойств-справочников (highload-инфоблоки) с отображением иконок в умном фильтре;<br>
 </li>
	<li>возможность сравнения товаров по свойствам;<br>
 </li>
	<li>отложенные товары.<br>
 </li>
</ul>
<table>
<tbody>
<tr>
	<td>
		 NEW!&nbsp;Новый модуль ранее просмотренных товаров, который не создает нагрузку на базу данных и загружается мгновенно.
	</td>
</tr>
</tbody>
</table>
 <br>
 <br>
 Удобная карточка товара<br>
 Страница товара спроектирована для обеспечения максимальной конверсии:
<ul>
	<li>Крупная фотография товара с фотогалереей дополнительных изображений;<br>
 </li>
	<li>Быстрый просмотр;<br>
 </li>
	<li>2 вида отображения страницы: с табами и без;<br>
 </li>
	<li>Компактный блок покупки с ценами и индикатором наличия;<br>
 </li>
	<li>Отображение наличия по складам отдельно для каждого торгового предложения (<a href="http://kshop.aspro-demo.ru/catalog/napolnye_pokrytiya/plitka/145/" target="_blank">пример реализации</a>);<br>
 </li>
	<li>Модули “Сопутствующие товары” и “Аксессуары” (<a href="http://market.aspro-demo.ru/catalog/foto_i_video/fotoapparaty/275/" target="_blank">пример реализации</a>);<br>
 </li>
	<li>Блок дополнительной информации с подробным описанием и &nbsp;характеристиками.<br>
 </li>
</ul>
 <img width="800" src="https://aspro.ru/upload/images/marketplace/market/detail.png" height="457" border="0"><br>
 Дополнительно реализованы:
<ul>
	<li>Отзывы о товаре;<br>
 </li>
	<li>Модуль “Недавно просмотренные товары”;<br>
 </li>
	<li>Вставка видео-описания товара Youtube, Vimeo и др. (<a href="http://kshop.aspro-demo.ru/catalog/foto_i_video/fotoapparaty/209/" target="_blank">пример реализации</a>);<br>
 </li>
	<li>Связь с разделом Услуги, теперь можно предлагать сопутствующие услуги прямо в карточке товара (<a href="http://market.aspro-demo.ru/catalog/foto_i_video/fotoapparaty/275/" target="_blank">пример реализации</a>);<br>
 </li>
</ul>
<ul>
	<li>Возможность прикрепления&nbsp;<a href="http://market.aspro-demo.ru/catalog/foto_i_video/fotoapparaty/275/" target="_blank">файлов для скачивания</a>&nbsp;(инструкции к товарам, сертификаты, каталоги);<br>
 </li>
	<li>Интеграция с каталогом Брендов, в карточке товара отображается логотип производителя товара.<br>
 </li>
</ul>
 Удобная летающая корзина<br>
 Возможность использования летающей корзины из коробки, в которой все данные обновляются по технологии AJAX без перезагрузки страницы:<br>
<ul>
	<li>Изменение количества товаров на лету;<br>
 </li>
	<li>Управление отложенными товарами;<br>
 </li>
	<li>Быстрое оформленик заказа в 1 клик без регистрации на сайте с указанием только имени и номера телефона;<br>
 </li>
	<li>Возможность указания минимальной суммы покупки.<br>
 </li>
</ul>
 <img width="800" src="https://aspro.ru/upload/images/marketplace/market/basket.png" height="496" border="0"><br>
 2 вида оформления заказа: с открытыми и закрытыми блоками.<br>
 <br>
 Широкие маркетинговые возможности:
<ul>
	<li>Стикеры на товары Новинка, Акция, Хит продаж, Рекомендуем;<br>
 </li>
	<li>Удобные баннерные позиции по всему сайту;<br>
 </li>
	<li>Модуль “Купить в 1 клик”;<br>
 </li>
	<li>Отображение старой и новой цены, а также размера скидки;<br>
 </li>
	<li><a href="https://market.aspro-demo.ru/landings/sportivnaya-odezhda-nike/" target="_blank">Посадочные страницы</a>&nbsp;с подборками товаров из каталога;<br>
 </li>
	<li>Специальный раздел “<a href="http://market.aspro-demo.ru/sale/" target="_blank">Акции на товары</a>”.<br>
 </li>
</ul>
<table>
<tbody>
<tr>
	<td>
		 Увеличьте выручку на 10-30% с помощью&nbsp;перекрестных продаж!&nbsp;Привязывайте товары к новостям, статьям, акциям и услугам, чтобы увеличить конверсию интернет-магазина. &nbsp;
	</td>
</tr>
</tbody>
</table>
 <br>
 <br>
 <br>
 <br>
 Готовые шаблоны триггерных писем<br>
 <br>
 В поставку включены шаблоны для триггерных рассылок, которые легко адаптируются под ваш фирменный стиль. Самые важные письма уже включены в поставку – с готовым универсальным текстом, версткой, кнопками и ссылками:<br>
<ul>
	<li>письмо с информацией о заказе<br>
 </li>
	<li>для забытой корзины (с промокодом на скидку и без него)<br>
 </li>
	<li>запрос отзыва на Яндекс.Маркет<br>
 </li>
	<li>реактивация пользователей<br>
 </li>
	<li>шаблон для акций и распродаж с привязкой к соответствующему разделу<br>
 </li>
	<li>общий шаблон.<br>
 </li>
</ul>
 Запустите цепочки писем в несколько кликов и увеличьте конверсию вашего интернет-магазина еще на 30%!<br>
 <br>
 <br>
 Расширенная интеграция с сервисом аналитики Яндекс.Метрика<br>
 <br>
 Настройте систему аналитики в несколько кликов и без программиста, повышайте эффективность своей рекламы и конверсию сайта!<br>
<p>
</p>
<p>
</p>
<p>
</p>
<ul>
	<li>
	<p>
		 Основные цели выведены в админку решения. Вам остается лишь настроить их на стороне Яндекс.Метрики
	</p>
 </li>
	<li>
	<p>
		 Общие и отдельные цели для веб-форм «Товар под заказ», «Задать вопрос», «Обратная связь», «Резюме», «Заказать услугу» и «Заказать звонок»
	</p>
 </li>
	<li>
	<p>
		 Цели корзины, покупки в 1 клик, быстрого и полного заказа.
	</p>
 </li>
</ul>
<p>
</p>
<p>
 <a href="https://aspro.ru/company/news/1886/?utm_source=marketplace&utm_medium=description&utm_campaign=ym_integration_blog_market" target="_blank">Подробности в блоге Аспро</a>
</p>
 Полная поддержка интеграции сайта с 1С: Предприятие:
<ul>
	<li>Обмен информацией о товарах (описание, фотографии, свойства);<br>
 </li>
	<li>Передача цен и остатков;<br>
 </li>
	<li>Обмен информацией о заказах;<br>
 </li>
	<li><a href="http://www.1c-bitrix.ru/products/cms/features/realtime.php#tab-realtime-link" target="_blank">Real-time обмен с 1С</a>.<br>
 </li>
</ul>
 Поддержка всех способов оплаты и доставки:
<ul>
	<li>Автоматизированные: Почта России, EMS, UPS, DHL, СПСР и др.<br>
 </li>
	<li>Настраиваемые: Самовывоз, Доставка курьером и д.р.<br>
 </li>
	<li>Оплата с помощью Visa, MasterCard, Яндекс.Деньги, WebMoney, PayPal и другие службы.<br>
 </li>
</ul>
 Разделы корпоративного сайта<br>
 Кроме непосредственно интернет-магазина мы дополнительно проработали разделы для представления вашей компании:
<ul>
	<li>Сотрудники:&nbsp;<a href="http://market.aspro-demo.ru/company/staff/" target="_blank">http://market.aspro-demo.ru/company/staff/</a><br>
 </li>
	<li>Вакансии:&nbsp;<a href="http://market.aspro-demo.ru/company/jobs/" target="_blank">http://market.aspro-demo.ru/company/jobs/</a><br>
 </li>
	<li>Новости:&nbsp;<a href="http://market.aspro-demo.ru/company/news/" target="_blank">http://market.aspro-demo.ru/company/news/</a><br>
 </li>
	<li>Магазины:&nbsp;<a href="http://market.aspro-demo.ru/contacts/stores/" target="_blank">http://market.aspro-demo.ru/contacts/stores/</a><br>
 </li>
	<li>Вопрос-ответ:&nbsp;<a href="http://market.aspro-demo.ru/info/faq/" target="_blank">http://market.aspro-demo.ru/info/faq/</a><br>
 </li>
	<li>и многие другие: О компании, Статьи, Помощь, Условия оплаты, Условия доставки, Гарантия на товар, Контакты.</li>
</ul>
 <br>		<div style="clear:both"></div>
	<br />
	
		услуга:&nbsp;
					<a href="/uslugi/razrabotka/internet-magazin/">Интернет магазин</a>				<br />
	
		цена:&nbsp;
					22900				<br />
		
	<button id="btn-send-form">Заказать</button>

</div>
";s:4:"VARS";a:2:{s:8:"arResult";a:12:{s:2:"ID";s:3:"211";s:9:"IBLOCK_ID";s:1:"4";s:4:"NAME";s:79:"Аспро: Маркет - адаптивный интернет-магазин";s:17:"IBLOCK_SECTION_ID";s:2:"21";s:6:"IBLOCK";a:92:{s:2:"ID";s:1:"4";s:3:"~ID";s:1:"4";s:11:"TIMESTAMP_X";s:19:"02.07.2021 17:02:21";s:12:"~TIMESTAMP_X";s:19:"02.07.2021 17:02:21";s:14:"IBLOCK_TYPE_ID";s:11:"information";s:15:"~IBLOCK_TYPE_ID";s:11:"information";s:3:"LID";s:2:"ro";s:4:"~LID";s:2:"ro";s:4:"CODE";s:6:"tovary";s:5:"~CODE";s:6:"tovary";s:8:"API_CODE";s:6:"tovary";s:9:"~API_CODE";s:6:"tovary";s:4:"NAME";s:12:"Товары";s:5:"~NAME";s:12:"Товары";s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:4:"SORT";s:3:"300";s:5:"~SORT";s:3:"300";s:13:"LIST_PAGE_URL";s:27:"/information/index.php?ID=4";s:14:"~LIST_PAGE_URL";s:27:"/information/index.php?ID=4";s:15:"DETAIL_PAGE_URL";s:50:"#SITE_DIR#/produkty/#SECTION_CODE#/#ELEMENT_CODE#/";s:16:"~DETAIL_PAGE_URL";s:50:"#SITE_DIR#/produkty/#SECTION_CODE#/#ELEMENT_CODE#/";s:16:"SECTION_PAGE_URL";s:35:"#SITE_DIR#/produkty/#SECTION_CODE#/";s:17:"~SECTION_PAGE_URL";s:35:"#SITE_DIR#/produkty/#SECTION_CODE#/";s:18:"CANONICAL_PAGE_URL";s:28:"/information/detail.php?ID=4";s:19:"~CANONICAL_PAGE_URL";s:28:"/information/detail.php?ID=4";s:7:"PICTURE";N;s:8:"~PICTURE";N;s:11:"DESCRIPTION";s:0:"";s:12:"~DESCRIPTION";s:0:"";s:16:"DESCRIPTION_TYPE";s:4:"text";s:17:"~DESCRIPTION_TYPE";s:4:"text";s:7:"RSS_TTL";s:2:"24";s:8:"~RSS_TTL";s:2:"24";s:10:"RSS_ACTIVE";s:1:"Y";s:11:"~RSS_ACTIVE";s:1:"Y";s:15:"RSS_FILE_ACTIVE";s:1:"N";s:16:"~RSS_FILE_ACTIVE";s:1:"N";s:14:"RSS_FILE_LIMIT";N;s:15:"~RSS_FILE_LIMIT";N;s:13:"RSS_FILE_DAYS";N;s:14:"~RSS_FILE_DAYS";N;s:17:"RSS_YANDEX_ACTIVE";s:1:"N";s:18:"~RSS_YANDEX_ACTIVE";s:1:"N";s:6:"XML_ID";N;s:7:"~XML_ID";N;s:6:"TMP_ID";s:32:"414a068f54a8dc1f64d7b2db7a2b1966";s:7:"~TMP_ID";s:32:"414a068f54a8dc1f64d7b2db7a2b1966";s:13:"INDEX_ELEMENT";s:1:"Y";s:14:"~INDEX_ELEMENT";s:1:"Y";s:13:"INDEX_SECTION";s:1:"Y";s:14:"~INDEX_SECTION";s:1:"Y";s:8:"WORKFLOW";s:1:"N";s:9:"~WORKFLOW";s:1:"N";s:7:"BIZPROC";s:1:"N";s:8:"~BIZPROC";s:1:"N";s:15:"SECTION_CHOOSER";s:1:"L";s:16:"~SECTION_CHOOSER";s:1:"L";s:9:"LIST_MODE";s:0:"";s:10:"~LIST_MODE";s:0:"";s:11:"RIGHTS_MODE";s:1:"S";s:12:"~RIGHTS_MODE";s:1:"S";s:16:"SECTION_PROPERTY";s:1:"N";s:17:"~SECTION_PROPERTY";s:1:"N";s:14:"PROPERTY_INDEX";s:1:"N";s:15:"~PROPERTY_INDEX";s:1:"N";s:7:"VERSION";s:1:"1";s:8:"~VERSION";s:1:"1";s:17:"LAST_CONV_ELEMENT";s:1:"0";s:18:"~LAST_CONV_ELEMENT";s:1:"0";s:15:"SOCNET_GROUP_ID";N;s:16:"~SOCNET_GROUP_ID";N;s:16:"EDIT_FILE_BEFORE";s:0:"";s:17:"~EDIT_FILE_BEFORE";s:0:"";s:15:"EDIT_FILE_AFTER";s:0:"";s:16:"~EDIT_FILE_AFTER";s:0:"";s:13:"SECTIONS_NAME";s:14:"Разделы";s:14:"~SECTIONS_NAME";s:14:"Разделы";s:12:"SECTION_NAME";s:12:"Раздел";s:13:"~SECTION_NAME";s:12:"Раздел";s:13:"ELEMENTS_NAME";s:12:"Товары";s:14:"~ELEMENTS_NAME";s:12:"Товары";s:12:"ELEMENT_NAME";s:10:"Товар";s:13:"~ELEMENT_NAME";s:10:"Товар";s:7:"REST_ON";s:1:"N";s:8:"~REST_ON";s:1:"N";s:11:"EXTERNAL_ID";N;s:12:"~EXTERNAL_ID";N;s:8:"LANG_DIR";s:1:"/";s:9:"~LANG_DIR";s:1:"/";s:11:"SERVER_NAME";s:16:"intercalation.ru";s:12:"~SERVER_NAME";s:16:"intercalation.ru";}s:13:"LIST_PAGE_URL";s:10:"/produkty/";s:14:"~LIST_PAGE_URL";s:10:"/produkty/";s:11:"SECTION_URL";s:25:"/produkty/shablony-aspro/";s:7:"SECTION";a:1:{s:4:"PATH";a:1:{i:0;a:31:{s:2:"ID";s:2:"21";s:3:"~ID";s:2:"21";s:4:"CODE";s:14:"shablony-aspro";s:5:"~CODE";s:14:"shablony-aspro";s:6:"XML_ID";N;s:7:"~XML_ID";N;s:11:"EXTERNAL_ID";N;s:12:"~EXTERNAL_ID";N;s:9:"IBLOCK_ID";s:1:"4";s:10:"~IBLOCK_ID";s:1:"4";s:17:"IBLOCK_SECTION_ID";N;s:18:"~IBLOCK_SECTION_ID";N;s:4:"SORT";s:3:"500";s:5:"~SORT";s:3:"500";s:4:"NAME";s:25:"Шаблоны Аспро";s:5:"~NAME";s:25:"Шаблоны Аспро";s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:11:"DEPTH_LEVEL";s:1:"1";s:12:"~DEPTH_LEVEL";s:1:"1";s:16:"SECTION_PAGE_URL";s:25:"/produkty/shablony-aspro/";s:17:"~SECTION_PAGE_URL";s:25:"/produkty/shablony-aspro/";s:14:"IBLOCK_TYPE_ID";s:11:"information";s:15:"~IBLOCK_TYPE_ID";s:11:"information";s:11:"IBLOCK_CODE";s:6:"tovary";s:12:"~IBLOCK_CODE";s:6:"tovary";s:18:"IBLOCK_EXTERNAL_ID";N;s:19:"~IBLOCK_EXTERNAL_ID";N;s:13:"GLOBAL_ACTIVE";s:1:"Y";s:14:"~GLOBAL_ACTIVE";s:1:"Y";s:16:"IPROPERTY_VALUES";a:0:{}}}}s:16:"IPROPERTY_VALUES";a:0:{}s:11:"TIMESTAMP_X";s:19:"13.01.2021 05:59:47";s:9:"META_TAGS";a:4:{s:5:"TITLE";s:79:"Аспро: Маркет - адаптивный интернет-магазин";s:13:"BROWSER_TITLE";s:0:"";s:8:"KEYWORDS";s:0:"";s:11:"DESCRIPTION";s:0:"";}}s:18:"templateCachedData";a:3:{s:13:"additionalCSS";s:90:"/local/templates/robot/components/bitrix/news/uslugi/bitrix/news.detail/.default/style.css";s:9:"frameMode";b:1;s:8:"__NavNum";i:1;}}}';
return true;
?>