<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001649233996';
$dateexpire = '001685233996';
$ser_content = 'a:2:{s:7:"CONTENT";s:8643:"

<div>

			<h3>Сервер HP 360 g5</h3>	
	
	<div class="b-box">
			<img
			src="/upload/iblock/b38/b38b9baea7173ae8aec05f345fc47fa6.gif"
			alt="Сервер HP 360 g5"
			title="Сервер HP 360 g5"
			/>
		</div>

		
	
				<ul>
	<h3>Основные технические характеристики ProLiant DL360 G5.</h3>
 <br>
	<ul>
	</ul>
 <br>
	<ul>
	</ul>
 <br>
	<ul>
	</ul>
	<table cellspacing="1" cellpadding="4">
	<tbody>
	<tr>
		<td align="left">
			<p align="left">
				 Процессор: Четырёхъядерные и Двухъядерные процессор Intel® Xeon® ( с шина FSB 1333 и FSB 1066);
			</p>
			<p align="left">
				 Кэш-память:
			</p>
			<ul>
				<li>Кэш-память второго уровня 4 Мб (1 x 4 Мб) - семейство процессора Intel® Xeon® 5100<br>
 </li>
				<li>Кэш-память второго уровня 8 Мб (2 x 4 Мб) - семейство процессора Intel® Xeon® 5300<br>
 </li>
				<li>Кэш-память второго уровня 4 Мб (2 x 2 Мб) - семейство процессора Intel® Xeon® 5000<br>
 </li>
			</ul>
			<p align="left">
				 Чипсет: Intel® 5000P
			</p>
			<p align="left">
				 Тип памяти: PC2-5300 с полной буферизацией DIMM (DDR2-667); чередованием адресов 4:1; зеркалированием памяти; и онлайновым подключением резервной памяти
			</p>
			<p align="left">
				 Слоты для памяти : 8 слота DIMM
			</p>
			<p align="left">
				 Максимальная память : 32 Гб
			</p>
			<p align="left">
				 Привод жёсткого диска: Без накопителей; (6) активных отсека для приводов: модели E5345 4 Гб, 5160 2 Гб и 5150 2 Гб; Без накопителей; (4) активных отсека, для (6) отсеков имеется опциональный кабель связи с системой хранения (PN 399546-B21): модели E5335 2 Гб, E5320 1 Гб, 5160 1 Гб, 5150 1 Гб, 5140 1 Гб и 5060 1 Гб; Без накопителей; (4) активных отсека для приводов: E5310 1 Гб; 5130 1 Гб; модели 5120 1 Гб и 5110 1 Гб
			</p>
			<p align="left">
				 Внутреннее устройство хранения данных: Serial-Attached SCSI (SAS) с возможностью горячей замены:
			</p>
			<ul>
				<li>876 Гб SAS (6 x 146 Гб, SAS);<br>
 </li>
				<li>Serial ATA (SATA) с возможностью горячей замены: 360 Гб SATA (6 x 60 Гб SATA)<br>
 </li>
			</ul>
			<p align="left">
				 Внутренние дисковые отсеки: До 6 отсеков для жёстких дисков SAS/SATA с возможностью горячей замены
			</p>
			<p align="left">
				 Дисковод для гибких дисков: Тонкий дисковод для гибких дисков 1,44 Мб
			</p>
			<p align="left">
				 Оптические приводы: Опция DVD-ROM, DVD/CD-RW Combo (стандарт в высокопроизводительных моделях), DVD-RW или тонкий накопитель на дискетах
			</p>
			<p align="left">
				 Функции питания: Дополнительно приобретаемый блок питания 700 Вт (избыточность 1+ 1)
			</p>
			<p align="left">
				 Порт ввода-вывода:
			</p>
			<ul>
				<li>Клавиатура: 1;<br>
 </li>
				<li>Мышь: 1;<br>
 </li>
				<li>Последовательный: 1;<br>
 </li>
				<li>Видео: 1;<br>
 </li>
				<li>порты USB 2.0: 4 всего – 2 сзади, 1 спереди, 1 внутренний;<br>
 </li>
				<li>Сетевой RJ-45: 2;<br>
 </li>
				<li>порт дистанционного управления iLO 2: 1<br>
 </li>
				<li>Сетевой интерфейс: Встроенные многофункциональные гигабитные серверные адаптеры NC373i с сетевой картой TCP/IP Offload Engine, включая поддержку Accelerated iSCSI и RDMA посредством опционального комплекта лицензий ProLiant Essentials<br>
 </li>
			</ul>
			<p>
				 Слот расширения: Два слота расширения: (1) полноразмерный слот по высоте и длине; (1) низкопрофильный слот; видеопорт на передней или задней панели
			</p>
			<p>
				 Совместимые операционные системы: Microsoft® Windows® 2000 Server, Microsoft® Windows® Server 2003, Novell NetWare, Linux (Red Hat, SuSE), Solaris 10 (32 или 64 разряда), VMware Virtualization Software
			</p>
			<p>
				 Функции управления:
			</p>
			<ul>
				<li>HP Systems Insight Manager;<br>
 </li>
				<li>SmartStart, утилита RBSU;<br>
 </li>
				<li>Резервная возможность дистанционной перепрошивки ПЗУ;<br>
 </li>
				<li>Встроенный журнал управления (IML);<br>
 </li>
				<li>Регистрация состояния сервера;<br>
 </li>
				<li>ASR-2 (Automatic Server Recovery-2);<br>
 </li>
				<li>Дисплей System Insight (SID);<br>
 </li>
				<li>Функция динамического восстановления секторов и отслеживания параметров накопителя (с контроллером Smart Array);<br>
 </li>
				<li>Загрузка с горячим резервированием;<br>
 </li>
				<li>Утилита выключения питания;<br>
 </li>
				<li>Профилактическая гарантия (охватывает процессоры, жёсткие диски SAS и память)<br>
 </li>
			</ul>
			<p>
				 Управление безопасностью:
			</p>
			<ul>
				<li>Пароль на включение питания;<br>
 </li>
				<li>Пароль клавиатуры;<br>
 </li>
				<li>Контроль дисковода;<br>
 </li>
				<li>Контроль загрузки с дискеты;<br>
 </li>
				<li>QuickLock, режим сетевого сервера;<br>
 </li>
				<li>Съёмные приводы для компакт-дисков и дискет;<br>
 </li>
				<li>Контроль последовательного интерфейса;<br>
 </li>
				<li>Пароль администратора;<br>
 </li>
				<li>Блокировка конфигурации диска;<br>
 </li>
				<li>Средство Integrated Lights-Out обеспечивает 12 настраиваемых учётных записей пользователей и шифрование по протоколу SSL;<br>
 </li>
				<li>Integrated Lights-Out можно отключить через общие настройки;<br>
 </li>
				<li>Решение iLO Advance Pack поддерживает интеграцию служб каталогов и аутентификацию по двум параметрам<br>
 </li>
			</ul>
			<p>
				 Обслуживание: В стандартный комплект поставки входят направляющие для обслуживания в стойке; дополнительно приобретается мобильный кронштейн для прокладки кабелей<br>
 <br>
				 Габариты: 42,62 x 70,49 x 4,32 см
			</p>
			<p>
				 Вес:
			</p>
			<ul>
				<li>Максимум (установлено шесть жёстких дисков, два блока питания и два процессора): 16,78 кг;<br>
 </li>
				<li>Без жёстких дисков, с одним блоком питания и одним процессором: 12,43 кг<br>
 </li>
			</ul>
		</td>
	</tr>
	</tbody>
	</table>
 <br>
</ul>
<p>
</p>		<div style="clear:both"></div>
	<br />
	
		услуга:&nbsp;
					<a href="/uslugi/nastroyka-i-ustanovka/nastroyka-servera/">Настройка сервера</a>				<br />
	
		цена:&nbsp;
					25000				<br />
		
	<button id="btn-send-form">Заказать</button>

</div>
";s:4:"VARS";a:2:{s:8:"arResult";a:12:{s:2:"ID";s:3:"146";s:9:"IBLOCK_ID";s:1:"4";s:4:"NAME";s:22:"Сервер HP 360 g5";s:17:"IBLOCK_SECTION_ID";s:2:"18";s:6:"IBLOCK";a:92:{s:2:"ID";s:1:"4";s:3:"~ID";s:1:"4";s:11:"TIMESTAMP_X";s:19:"02.07.2021 17:02:21";s:12:"~TIMESTAMP_X";s:19:"02.07.2021 17:02:21";s:14:"IBLOCK_TYPE_ID";s:11:"information";s:15:"~IBLOCK_TYPE_ID";s:11:"information";s:3:"LID";s:2:"ro";s:4:"~LID";s:2:"ro";s:4:"CODE";s:6:"tovary";s:5:"~CODE";s:6:"tovary";s:8:"API_CODE";s:6:"tovary";s:9:"~API_CODE";s:6:"tovary";s:4:"NAME";s:12:"Товары";s:5:"~NAME";s:12:"Товары";s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:4:"SORT";s:3:"300";s:5:"~SORT";s:3:"300";s:13:"LIST_PAGE_URL";s:27:"/information/index.php?ID=4";s:14:"~LIST_PAGE_URL";s:27:"/information/index.php?ID=4";s:15:"DETAIL_PAGE_URL";s:50:"#SITE_DIR#/produkty/#SECTION_CODE#/#ELEMENT_CODE#/";s:16:"~DETAIL_PAGE_URL";s:50:"#SITE_DIR#/produkty/#SECTION_CODE#/#ELEMENT_CODE#/";s:16:"SECTION_PAGE_URL";s:35:"#SITE_DIR#/produkty/#SECTION_CODE#/";s:17:"~SECTION_PAGE_URL";s:35:"#SITE_DIR#/produkty/#SECTION_CODE#/";s:18:"CANONICAL_PAGE_URL";s:28:"/information/detail.php?ID=4";s:19:"~CANONICAL_PAGE_URL";s:28:"/information/detail.php?ID=4";s:7:"PICTURE";N;s:8:"~PICTURE";N;s:11:"DESCRIPTION";s:0:"";s:12:"~DESCRIPTION";s:0:"";s:16:"DESCRIPTION_TYPE";s:4:"text";s:17:"~DESCRIPTION_TYPE";s:4:"text";s:7:"RSS_TTL";s:2:"24";s:8:"~RSS_TTL";s:2:"24";s:10:"RSS_ACTIVE";s:1:"Y";s:11:"~RSS_ACTIVE";s:1:"Y";s:15:"RSS_FILE_ACTIVE";s:1:"N";s:16:"~RSS_FILE_ACTIVE";s:1:"N";s:14:"RSS_FILE_LIMIT";N;s:15:"~RSS_FILE_LIMIT";N;s:13:"RSS_FILE_DAYS";N;s:14:"~RSS_FILE_DAYS";N;s:17:"RSS_YANDEX_ACTIVE";s:1:"N";s:18:"~RSS_YANDEX_ACTIVE";s:1:"N";s:6:"XML_ID";N;s:7:"~XML_ID";N;s:6:"TMP_ID";s:32:"414a068f54a8dc1f64d7b2db7a2b1966";s:7:"~TMP_ID";s:32:"414a068f54a8dc1f64d7b2db7a2b1966";s:13:"INDEX_ELEMENT";s:1:"Y";s:14:"~INDEX_ELEMENT";s:1:"Y";s:13:"INDEX_SECTION";s:1:"Y";s:14:"~INDEX_SECTION";s:1:"Y";s:8:"WORKFLOW";s:1:"N";s:9:"~WORKFLOW";s:1:"N";s:7:"BIZPROC";s:1:"N";s:8:"~BIZPROC";s:1:"N";s:15:"SECTION_CHOOSER";s:1:"L";s:16:"~SECTION_CHOOSER";s:1:"L";s:9:"LIST_MODE";s:0:"";s:10:"~LIST_MODE";s:0:"";s:11:"RIGHTS_MODE";s:1:"S";s:12:"~RIGHTS_MODE";s:1:"S";s:16:"SECTION_PROPERTY";s:1:"N";s:17:"~SECTION_PROPERTY";s:1:"N";s:14:"PROPERTY_INDEX";s:1:"N";s:15:"~PROPERTY_INDEX";s:1:"N";s:7:"VERSION";s:1:"1";s:8:"~VERSION";s:1:"1";s:17:"LAST_CONV_ELEMENT";s:1:"0";s:18:"~LAST_CONV_ELEMENT";s:1:"0";s:15:"SOCNET_GROUP_ID";N;s:16:"~SOCNET_GROUP_ID";N;s:16:"EDIT_FILE_BEFORE";s:0:"";s:17:"~EDIT_FILE_BEFORE";s:0:"";s:15:"EDIT_FILE_AFTER";s:0:"";s:16:"~EDIT_FILE_AFTER";s:0:"";s:13:"SECTIONS_NAME";s:14:"Разделы";s:14:"~SECTIONS_NAME";s:14:"Разделы";s:12:"SECTION_NAME";s:12:"Раздел";s:13:"~SECTION_NAME";s:12:"Раздел";s:13:"ELEMENTS_NAME";s:12:"Товары";s:14:"~ELEMENTS_NAME";s:12:"Товары";s:12:"ELEMENT_NAME";s:10:"Товар";s:13:"~ELEMENT_NAME";s:10:"Товар";s:7:"REST_ON";s:1:"N";s:8:"~REST_ON";s:1:"N";s:11:"EXTERNAL_ID";N;s:12:"~EXTERNAL_ID";N;s:8:"LANG_DIR";s:1:"/";s:9:"~LANG_DIR";s:1:"/";s:11:"SERVER_NAME";s:16:"intercalation.ru";s:12:"~SERVER_NAME";s:16:"intercalation.ru";}s:13:"LIST_PAGE_URL";s:10:"/produkty/";s:14:"~LIST_PAGE_URL";s:10:"/produkty/";s:11:"SECTION_URL";s:23:"/produkty/oborudovanie/";s:7:"SECTION";a:1:{s:4:"PATH";a:1:{i:0;a:31:{s:2:"ID";s:2:"18";s:3:"~ID";s:2:"18";s:4:"CODE";s:12:"oborudovanie";s:5:"~CODE";s:12:"oborudovanie";s:6:"XML_ID";N;s:7:"~XML_ID";N;s:11:"EXTERNAL_ID";N;s:12:"~EXTERNAL_ID";N;s:9:"IBLOCK_ID";s:1:"4";s:10:"~IBLOCK_ID";s:1:"4";s:17:"IBLOCK_SECTION_ID";N;s:18:"~IBLOCK_SECTION_ID";N;s:4:"SORT";s:3:"500";s:5:"~SORT";s:3:"500";s:4:"NAME";s:24:"Оборудование";s:5:"~NAME";s:24:"Оборудование";s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:11:"DEPTH_LEVEL";s:1:"1";s:12:"~DEPTH_LEVEL";s:1:"1";s:16:"SECTION_PAGE_URL";s:23:"/produkty/oborudovanie/";s:17:"~SECTION_PAGE_URL";s:23:"/produkty/oborudovanie/";s:14:"IBLOCK_TYPE_ID";s:11:"information";s:15:"~IBLOCK_TYPE_ID";s:11:"information";s:11:"IBLOCK_CODE";s:6:"tovary";s:12:"~IBLOCK_CODE";s:6:"tovary";s:18:"IBLOCK_EXTERNAL_ID";N;s:19:"~IBLOCK_EXTERNAL_ID";N;s:13:"GLOBAL_ACTIVE";s:1:"Y";s:14:"~GLOBAL_ACTIVE";s:1:"Y";s:16:"IPROPERTY_VALUES";a:0:{}}}}s:16:"IPROPERTY_VALUES";a:0:{}s:11:"TIMESTAMP_X";s:19:"05.08.2020 12:49:19";s:9:"META_TAGS";a:4:{s:5:"TITLE";s:22:"Сервер HP 360 g5";s:13:"BROWSER_TITLE";s:0:"";s:8:"KEYWORDS";s:0:"";s:11:"DESCRIPTION";s:0:"";}}s:18:"templateCachedData";a:3:{s:13:"additionalCSS";s:90:"/local/templates/robot/components/bitrix/news/uslugi/bitrix/news.detail/.default/style.css";s:9:"frameMode";b:1;s:8:"__NavNum";i:1;}}}';
return true;
?>