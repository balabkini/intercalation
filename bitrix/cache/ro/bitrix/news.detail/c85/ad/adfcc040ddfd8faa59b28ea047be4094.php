<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001649053350';
$dateexpire = '001685053350';
$ser_content = 'a:2:{s:7:"CONTENT";s:8249:"
<!-- <div class="col-full"> -->

<div>
		<div class="b-box">
		<img
			src="/upload/iblock/371/8bwrj9yuisi2p3v002e7t8s79pxhf9x0.png"
			alt="В России запущен аналог GitHub и GitLab для хранения кода и работы с ним"
			title="В России запущен аналог GitHub и GitLab для хранения кода и работы с ним"
			/>
	</div>

	
			<h1>В России запущен аналог GitHub и GitLab для хранения кода и работы с ним</h1>
	
			<h3>13.01.2022</h3>
	
	

				Сервис GitFlic построен на базе системы контроля версий Git и является аналогом популярных американских площадок GitHub и GitLab, которые в данный момент времени, следуя санкциям США, ограничили доступ к своим ресурсам для некоторых российских компаний и вузов. GitFlic нивелирует риски возможного отключения России от зарубежных хранилищ кода и предлагает отечественным софтверным компаниям альтернативу в выборе поставщика услуг для размещения исходного кода своих технологических решений. При этом хранение данных осуществляется в расположенных на территории РФ сертифицированных дата-центрах, соответствующих требованиям надёжности уровня Tier III по классификации Uptime Institute.<br>
 <br>
 <img width="750" alt="gitflic2911-2.png" src="/upload/medialibrary/1d7/ux9y3xb88wrzecpyteburp9kimneeczb.png" height="481" title="gitflic2911-2.png"><br>
 <br>
 В основу GitFlic положена единая экосистема управления проектами — все функциональные части сервиса по факту являются одним целым, что позволяет упростить и автоматизировать процесс программирования и управления разработкой софтверных продуктов. Сервис поддерживает работу в командах (как публичных, так и приватных), позволяет назначать пользователям роли в проекте и гибко конфигурировать права доступа, создавать запросы на слияние веток и использовать для дополнительной защиты аккаунта двухфакторную авторизацию от Google. Платформа также предлагает инструменты для создания публичных и закрытых репозиториев. Открытые хранилища кода отлично подходят для размещения исходных кодов и готовых библиотек свободно распространяемого ПО. Приватные проекты доступны только по приглашению, а для подключения к ним необходим SSH-ключ, обеспечивающий простой, но при этом очень безопасный способ входа в систему.<br>
 <br>
 <img width="750" alt="gitflic2911-2plus.png" src="/upload/medialibrary/b93/3rec7mzhtr021e92o04jtjnwr6ewz3h8.png" height="475" title="gitflic2911-2plus.png"><br>
 <br>
 В скором времени GitFlic сможет предложить ещё больше функций и инструментов для полного цикла разработки ПО. В частности, в сервисе станут доступны средства контроля и управления процессами разработки (трекер задач), статический анализ кода и система отслеживания ошибок, встроенные инструменты непрерывной интеграции и развёртывания (CI/CD), собственная wiki-система, механизм рассылки уведомлений в Telegram, возможность отмечать комментариями участки кода, требующие дополнительного внимания, внутренний мессенджер, автозапуск приложений в облаке, а также прочие востребованные в профессиональной среде функции.<br>
 <br>
 <img width="750" alt="gitflic2911-3.png" src="/upload/medialibrary/2d2/5qp31x60dd3kzv5xrxbifkl95buhtp39.png" height="486" title="gitflic2911-3.png"><br>
 <br>
 Отличительной особенностью GitFlic является ценовая политика предоставляемых услуг, стоимость которых существенно ниже, чем у зарубежных конкурентов. На данный момент в сервисе доступны два тарифных плана: бесплатный (подходит для Open Source-проектов и приватных репозиториев с командой до 5 человек) и платный стоимостью 250 рублей в месяц за пользователя (подходит для размещения приватных репозиториев в команде более 5 человек, при этом количество пользователей в команде не ограничено). Линейка тарифов будет расширяться по мере внедрения новых функций GitFlic.<br>
 <br>
 <img width="750" alt="gitflic2911-4.png" src="/upload/medialibrary/eb4/hg93yw7gr4hebb9l46bq66juq3fxzt1v.png" height="485" title="gitflic2911-4.png"><br>
 <br>
 В компании «Ресолют» уверены, что в перспективе GitFlic станет не только платформой для хранения кода и работы с ним, а полноценным сообществом разработчиков и просто людей, которые любят заниматься программированием, как в качестве хобби, так и основного заработка. Именно в объединении профессионального сообщества и неравнодушных к миру Open Source людей заключатся ключевая парадигма GitFlic.<br>
 <br>
Ну и, наконец, ещё одной важной составляющей сервиса является возможность использования GitFlic в образовательной среде — университетах и вузах, имеющих кафедры программирования или разработки ПО. Платформа может применяться с целью автоматизации процессов обучения основам программирования и позволяет решать задачи, связанные с выдачей студентам практических заданий и их последующей оценкой, проверкой готовых решений на работоспособность, качество и уникальность использованного кода (так называемый анализ на плагиат). Формирование сообщества разработчиков должно начинаться с учебной скамьи, убеждены разработчики GitFlic.		<div style="clear:both"></div>
	<br />
	





";s:4:"VARS";a:2:{s:8:"arResult";a:12:{s:2:"ID";s:3:"311";s:9:"IBLOCK_ID";s:1:"1";s:4:"NAME";s:119:"В России запущен аналог GitHub и GitLab для хранения кода и работы с ним";s:17:"IBLOCK_SECTION_ID";s:1:"1";s:6:"IBLOCK";a:92:{s:2:"ID";s:1:"1";s:3:"~ID";s:1:"1";s:11:"TIMESTAMP_X";s:19:"24.08.2021 07:16:47";s:12:"~TIMESTAMP_X";s:19:"24.08.2021 07:16:47";s:14:"IBLOCK_TYPE_ID";s:11:"information";s:15:"~IBLOCK_TYPE_ID";s:11:"information";s:3:"LID";s:2:"ro";s:4:"~LID";s:2:"ro";s:4:"CODE";s:4:"news";s:5:"~CODE";s:4:"news";s:8:"API_CODE";s:4:"news";s:9:"~API_CODE";s:4:"news";s:4:"NAME";s:14:"Новости";s:5:"~NAME";s:14:"Новости";s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:4:"SORT";s:3:"400";s:5:"~SORT";s:3:"400";s:13:"LIST_PAGE_URL";s:27:"/information/index.php?ID=1";s:14:"~LIST_PAGE_URL";s:27:"/information/index.php?ID=1";s:15:"DETAIL_PAGE_URL";s:49:"#SITE_DIR#/information/detail.php?ID=#ELEMENT_ID#";s:16:"~DETAIL_PAGE_URL";s:49:"#SITE_DIR#/information/detail.php?ID=#ELEMENT_ID#";s:16:"SECTION_PAGE_URL";s:39:"#SITE_DIR#/novosti/#SECTION_CODE_PATH#/";s:17:"~SECTION_PAGE_URL";s:39:"#SITE_DIR#/novosti/#SECTION_CODE_PATH#/";s:18:"CANONICAL_PAGE_URL";s:0:"";s:19:"~CANONICAL_PAGE_URL";s:0:"";s:7:"PICTURE";N;s:8:"~PICTURE";N;s:11:"DESCRIPTION";s:0:"";s:12:"~DESCRIPTION";s:0:"";s:16:"DESCRIPTION_TYPE";s:4:"text";s:17:"~DESCRIPTION_TYPE";s:4:"text";s:7:"RSS_TTL";s:2:"24";s:8:"~RSS_TTL";s:2:"24";s:10:"RSS_ACTIVE";s:1:"Y";s:11:"~RSS_ACTIVE";s:1:"Y";s:15:"RSS_FILE_ACTIVE";s:1:"N";s:16:"~RSS_FILE_ACTIVE";s:1:"N";s:14:"RSS_FILE_LIMIT";N;s:15:"~RSS_FILE_LIMIT";N;s:13:"RSS_FILE_DAYS";N;s:14:"~RSS_FILE_DAYS";N;s:17:"RSS_YANDEX_ACTIVE";s:1:"N";s:18:"~RSS_YANDEX_ACTIVE";s:1:"N";s:6:"XML_ID";N;s:7:"~XML_ID";N;s:6:"TMP_ID";s:32:"42dcb3cac9cf1c29a600097d4e5cd7ec";s:7:"~TMP_ID";s:32:"42dcb3cac9cf1c29a600097d4e5cd7ec";s:13:"INDEX_ELEMENT";s:1:"Y";s:14:"~INDEX_ELEMENT";s:1:"Y";s:13:"INDEX_SECTION";s:1:"Y";s:14:"~INDEX_SECTION";s:1:"Y";s:8:"WORKFLOW";s:1:"N";s:9:"~WORKFLOW";s:1:"N";s:7:"BIZPROC";s:1:"N";s:8:"~BIZPROC";s:1:"N";s:15:"SECTION_CHOOSER";s:1:"L";s:16:"~SECTION_CHOOSER";s:1:"L";s:9:"LIST_MODE";s:0:"";s:10:"~LIST_MODE";s:0:"";s:11:"RIGHTS_MODE";s:1:"S";s:12:"~RIGHTS_MODE";s:1:"S";s:16:"SECTION_PROPERTY";s:1:"N";s:17:"~SECTION_PROPERTY";s:1:"N";s:14:"PROPERTY_INDEX";s:1:"N";s:15:"~PROPERTY_INDEX";s:1:"N";s:7:"VERSION";s:1:"1";s:8:"~VERSION";s:1:"1";s:17:"LAST_CONV_ELEMENT";s:1:"0";s:18:"~LAST_CONV_ELEMENT";s:1:"0";s:15:"SOCNET_GROUP_ID";N;s:16:"~SOCNET_GROUP_ID";N;s:16:"EDIT_FILE_BEFORE";s:0:"";s:17:"~EDIT_FILE_BEFORE";s:0:"";s:15:"EDIT_FILE_AFTER";s:0:"";s:16:"~EDIT_FILE_AFTER";s:0:"";s:13:"SECTIONS_NAME";s:14:"Разделы";s:14:"~SECTIONS_NAME";s:14:"Разделы";s:12:"SECTION_NAME";s:12:"Раздел";s:13:"~SECTION_NAME";s:12:"Раздел";s:13:"ELEMENTS_NAME";s:14:"Новости";s:14:"~ELEMENTS_NAME";s:14:"Новости";s:12:"ELEMENT_NAME";s:14:"Новость";s:13:"~ELEMENT_NAME";s:14:"Новость";s:7:"REST_ON";s:1:"N";s:8:"~REST_ON";s:1:"N";s:11:"EXTERNAL_ID";N;s:12:"~EXTERNAL_ID";N;s:8:"LANG_DIR";s:1:"/";s:9:"~LANG_DIR";s:1:"/";s:11:"SERVER_NAME";s:16:"intercalation.ru";s:12:"~SERVER_NAME";s:16:"intercalation.ru";}s:13:"LIST_PAGE_URL";s:9:"/novosti/";s:14:"~LIST_PAGE_URL";s:9:"/novosti/";s:11:"SECTION_URL";s:39:"/novosti/mir-programnogo-obespecheniya/";s:7:"SECTION";a:1:{s:4:"PATH";a:1:{i:0;a:31:{s:2:"ID";s:1:"1";s:3:"~ID";s:1:"1";s:4:"CODE";s:29:"mir-programnogo-obespecheniya";s:5:"~CODE";s:29:"mir-programnogo-obespecheniya";s:6:"XML_ID";N;s:7:"~XML_ID";N;s:11:"EXTERNAL_ID";N;s:12:"~EXTERNAL_ID";N;s:9:"IBLOCK_ID";s:1:"1";s:10:"~IBLOCK_ID";s:1:"1";s:17:"IBLOCK_SECTION_ID";N;s:18:"~IBLOCK_SECTION_ID";N;s:4:"SORT";s:3:"500";s:5:"~SORT";s:3:"500";s:4:"NAME";s:52:"Мир програмного обеспечения";s:5:"~NAME";s:52:"Мир програмного обеспечения";s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:11:"DEPTH_LEVEL";s:1:"1";s:12:"~DEPTH_LEVEL";s:1:"1";s:16:"SECTION_PAGE_URL";s:39:"/novosti/mir-programnogo-obespecheniya/";s:17:"~SECTION_PAGE_URL";s:39:"/novosti/mir-programnogo-obespecheniya/";s:14:"IBLOCK_TYPE_ID";s:11:"information";s:15:"~IBLOCK_TYPE_ID";s:11:"information";s:11:"IBLOCK_CODE";s:4:"news";s:12:"~IBLOCK_CODE";s:4:"news";s:18:"IBLOCK_EXTERNAL_ID";N;s:19:"~IBLOCK_EXTERNAL_ID";N;s:13:"GLOBAL_ACTIVE";s:1:"Y";s:14:"~GLOBAL_ACTIVE";s:1:"Y";s:16:"IPROPERTY_VALUES";a:0:{}}}}s:16:"IPROPERTY_VALUES";a:1:{s:18:"ELEMENT_META_TITLE";s:48:"GITаналог программирование";}s:11:"TIMESTAMP_X";s:19:"13.01.2022 15:52:55";s:9:"META_TAGS";a:4:{s:5:"TITLE";s:119:"В России запущен аналог GitHub и GitLab для хранения кода и работы с ним";s:13:"BROWSER_TITLE";s:48:"GITаналог программирование";s:8:"KEYWORDS";s:0:"";s:11:"DESCRIPTION";s:0:"";}}s:18:"templateCachedData";a:3:{s:13:"additionalCSS";s:88:"/local/templates/robot/components/bitrix/news/news/bitrix/news.detail/.default/style.css";s:9:"frameMode";b:1;s:8:"__NavNum";i:1;}}}';
return true;
?>