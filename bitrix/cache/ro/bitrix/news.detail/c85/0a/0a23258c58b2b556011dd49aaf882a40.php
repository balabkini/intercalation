<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001649135852';
$dateexpire = '001685135852';
$ser_content = 'a:2:{s:7:"CONTENT";s:4524:"
<!-- <div class="col-full"> -->

<div>
		<div class="b-box">
		<img
			src="/upload/iblock/378/6orsv9it9zadm5sfnh1ek7dl39us7ba5.jpg"
			alt="Yandex Cloud повышает цены на облачные сервисы с 13 апреля"
			title="Yandex Cloud повышает цены на облачные сервисы с 13 апреля"
			/>
	</div>

	
			<h1>Yandex Cloud повышает цены на облачные сервисы с 13 апреля</h1>
	
			<h3>22.03.2022</h3>
	
	

				<p>
22 марта 2022 года облачная платформа Yandex.Cloud объявила о повышении стоимости услуг своих сервисов с 13 апреля. Рост цен на популярные сервисы будет от 40% до 60%.<br>
<table>
<tr><th>Сервис Yandex Cloud</th><th>Диапазон изменения цен</th></tr>
<tr><td>Compute Cloud</td><td>+40–60%</td></tr>
<tr><td>Object Storage</td><td>+60%</td></tr>
<tr><td>Network Load Balancer</td><td>+40%</td></tr>
<tr><td>Application Load Balancer</td><td>+60%</td></tr>
<tr><td>Cloud CDN</td><td>+60%</td></tr>
<tr><td>Virtual Private Cloud</td><td>+60%</td></tr>
<tr><td>Cloud DNS</td><td>+44–60%</td></tr>
<tr><td>Managed Service for Kubernetes</td><td>+60%</td></tr>
<tr><td>Container Registry</td><td>+60%</td></tr>
<tr><td>Managed Service for PostgreSQL</td><td>+40–60%</td></tr>
<tr><td>Managed Service for ClickHouse</td><td>+40–60%</td></tr>
<tr><td>Managed Service for MySQL</td><td>+33–60%</td></tr>
<tr><td>Managed Service for Redis</td><td>+60%</td></tr>
<tr><td>Managed Service for MongoDB</td><td>+33–80%</td></tr>
<tr><td>Managed Service for Elasticsearch</td><td>+60%</td></tr>
<tr><td>Managed Service for Apache Kafka®</td><td>+40–60%</td></tr>
<tr><td>Managed Service for SQL Server</td><td>+60%</td></tr>
<tr><td>Managed Service for Greenplum®</td><td>+40–60%</td></tr>
<tr><td>Data Proc</td><td>+33–60%</td></tr>
<tr><td>Yandex Database</td><td>+60%</td></tr>
<tr><td>Cloud Functions</td><td>+60%</td></tr>
<tr><td>Message Queue</td><td>+60%</td></tr>
<tr><td>Yandex API Gateway</td><td>+60%</td></tr>
<tr><td>Yandex IoT Core</td><td>+60%</td></tr>
<tr><td>Serverless Containers</td><td>+60%</td></tr>
<tr><td>DataSphere</td><td>+60%</td></tr>
<tr><td>SpeechKit</td><td>+5–10%</td></tr>
<tr><td>Translate</td><td>+10%</td></tr>
<tr><td>Vision</td><td>+8%</td></tr>
<tr><td>Yandex Key Management Service</td><td>+60%</td></tr>
<tr><td>Yandex Monitoring</td><td>+40%</td></tr>
</table>
<br>
Яндекс пояснил повышение цен нестабильной ситуацией в мире и экономике, которая оказывает беспрецедентное влияние на бизнес всех технологических компаний и вынуждает Yandex.Cloud пересмотреть стоимость услуг в рублях и тенге. Цены в долларах останутся без изменений.<br>
<br>
По данным Яндекса, основные причины данного шага — это значительное повышение цен у поставщиков и производителей на покупку нового сетевого и серверного оборудования из-за роста курса валют, а также сложности в логистике поставок и закупок.<br>
<br>
Компания уточнила, что все согласованные ранее с клиентами специальные условия и акции продолжают действовать в соответствии с договоренностями без изменений по тарифам.<br>
<br>
Яндекс напомнил, что за все время существования платформы Yandex Cloud компания ни разу не повышала цены на вычислительные мощности и сервисы. По ее расчетам, стоимость каждого нового поколения виртуальных машин на базе более современных процессоров в пересчёте на одно ядро становилась только ниже для пользователей и партнеров.
</p>		<div style="clear:both"></div>
	<br />
	





";s:4:"VARS";a:2:{s:8:"arResult";a:12:{s:2:"ID";s:3:"332";s:9:"IBLOCK_ID";s:1:"1";s:4:"NAME";s:94:"Yandex Cloud повышает цены на облачные сервисы с 13 апреля";s:17:"IBLOCK_SECTION_ID";s:1:"7";s:6:"IBLOCK";a:92:{s:2:"ID";s:1:"1";s:3:"~ID";s:1:"1";s:11:"TIMESTAMP_X";s:19:"24.08.2021 07:16:47";s:12:"~TIMESTAMP_X";s:19:"24.08.2021 07:16:47";s:14:"IBLOCK_TYPE_ID";s:11:"information";s:15:"~IBLOCK_TYPE_ID";s:11:"information";s:3:"LID";s:2:"ro";s:4:"~LID";s:2:"ro";s:4:"CODE";s:4:"news";s:5:"~CODE";s:4:"news";s:8:"API_CODE";s:4:"news";s:9:"~API_CODE";s:4:"news";s:4:"NAME";s:14:"Новости";s:5:"~NAME";s:14:"Новости";s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:4:"SORT";s:3:"400";s:5:"~SORT";s:3:"400";s:13:"LIST_PAGE_URL";s:27:"/information/index.php?ID=1";s:14:"~LIST_PAGE_URL";s:27:"/information/index.php?ID=1";s:15:"DETAIL_PAGE_URL";s:49:"#SITE_DIR#/information/detail.php?ID=#ELEMENT_ID#";s:16:"~DETAIL_PAGE_URL";s:49:"#SITE_DIR#/information/detail.php?ID=#ELEMENT_ID#";s:16:"SECTION_PAGE_URL";s:39:"#SITE_DIR#/novosti/#SECTION_CODE_PATH#/";s:17:"~SECTION_PAGE_URL";s:39:"#SITE_DIR#/novosti/#SECTION_CODE_PATH#/";s:18:"CANONICAL_PAGE_URL";s:0:"";s:19:"~CANONICAL_PAGE_URL";s:0:"";s:7:"PICTURE";N;s:8:"~PICTURE";N;s:11:"DESCRIPTION";s:0:"";s:12:"~DESCRIPTION";s:0:"";s:16:"DESCRIPTION_TYPE";s:4:"text";s:17:"~DESCRIPTION_TYPE";s:4:"text";s:7:"RSS_TTL";s:2:"24";s:8:"~RSS_TTL";s:2:"24";s:10:"RSS_ACTIVE";s:1:"Y";s:11:"~RSS_ACTIVE";s:1:"Y";s:15:"RSS_FILE_ACTIVE";s:1:"N";s:16:"~RSS_FILE_ACTIVE";s:1:"N";s:14:"RSS_FILE_LIMIT";N;s:15:"~RSS_FILE_LIMIT";N;s:13:"RSS_FILE_DAYS";N;s:14:"~RSS_FILE_DAYS";N;s:17:"RSS_YANDEX_ACTIVE";s:1:"N";s:18:"~RSS_YANDEX_ACTIVE";s:1:"N";s:6:"XML_ID";N;s:7:"~XML_ID";N;s:6:"TMP_ID";s:32:"42dcb3cac9cf1c29a600097d4e5cd7ec";s:7:"~TMP_ID";s:32:"42dcb3cac9cf1c29a600097d4e5cd7ec";s:13:"INDEX_ELEMENT";s:1:"Y";s:14:"~INDEX_ELEMENT";s:1:"Y";s:13:"INDEX_SECTION";s:1:"Y";s:14:"~INDEX_SECTION";s:1:"Y";s:8:"WORKFLOW";s:1:"N";s:9:"~WORKFLOW";s:1:"N";s:7:"BIZPROC";s:1:"N";s:8:"~BIZPROC";s:1:"N";s:15:"SECTION_CHOOSER";s:1:"L";s:16:"~SECTION_CHOOSER";s:1:"L";s:9:"LIST_MODE";s:0:"";s:10:"~LIST_MODE";s:0:"";s:11:"RIGHTS_MODE";s:1:"S";s:12:"~RIGHTS_MODE";s:1:"S";s:16:"SECTION_PROPERTY";s:1:"N";s:17:"~SECTION_PROPERTY";s:1:"N";s:14:"PROPERTY_INDEX";s:1:"N";s:15:"~PROPERTY_INDEX";s:1:"N";s:7:"VERSION";s:1:"1";s:8:"~VERSION";s:1:"1";s:17:"LAST_CONV_ELEMENT";s:1:"0";s:18:"~LAST_CONV_ELEMENT";s:1:"0";s:15:"SOCNET_GROUP_ID";N;s:16:"~SOCNET_GROUP_ID";N;s:16:"EDIT_FILE_BEFORE";s:0:"";s:17:"~EDIT_FILE_BEFORE";s:0:"";s:15:"EDIT_FILE_AFTER";s:0:"";s:16:"~EDIT_FILE_AFTER";s:0:"";s:13:"SECTIONS_NAME";s:14:"Разделы";s:14:"~SECTIONS_NAME";s:14:"Разделы";s:12:"SECTION_NAME";s:12:"Раздел";s:13:"~SECTION_NAME";s:12:"Раздел";s:13:"ELEMENTS_NAME";s:14:"Новости";s:14:"~ELEMENTS_NAME";s:14:"Новости";s:12:"ELEMENT_NAME";s:14:"Новость";s:13:"~ELEMENT_NAME";s:14:"Новость";s:7:"REST_ON";s:1:"N";s:8:"~REST_ON";s:1:"N";s:11:"EXTERNAL_ID";N;s:12:"~EXTERNAL_ID";N;s:8:"LANG_DIR";s:1:"/";s:9:"~LANG_DIR";s:1:"/";s:11:"SERVER_NAME";s:16:"intercalation.ru";s:12:"~SERVER_NAME";s:16:"intercalation.ru";}s:13:"LIST_PAGE_URL";s:9:"/novosti/";s:14:"~LIST_PAGE_URL";s:9:"/novosti/";s:11:"SECTION_URL";s:21:"/novosti/ya-i-tsifra/";s:7:"SECTION";a:1:{s:4:"PATH";a:1:{i:0;a:31:{s:2:"ID";s:1:"7";s:3:"~ID";s:1:"7";s:4:"CODE";s:11:"ya-i-tsifra";s:5:"~CODE";s:11:"ya-i-tsifra";s:6:"XML_ID";N;s:7:"~XML_ID";N;s:11:"EXTERNAL_ID";N;s:12:"~EXTERNAL_ID";N;s:9:"IBLOCK_ID";s:1:"1";s:10:"~IBLOCK_ID";s:1:"1";s:17:"IBLOCK_SECTION_ID";N;s:18:"~IBLOCK_SECTION_ID";N;s:4:"SORT";s:3:"500";s:5:"~SORT";s:3:"500";s:4:"NAME";s:16:"Я и цифра";s:5:"~NAME";s:16:"Я и цифра";s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:11:"DEPTH_LEVEL";s:1:"1";s:12:"~DEPTH_LEVEL";s:1:"1";s:16:"SECTION_PAGE_URL";s:21:"/novosti/ya-i-tsifra/";s:17:"~SECTION_PAGE_URL";s:21:"/novosti/ya-i-tsifra/";s:14:"IBLOCK_TYPE_ID";s:11:"information";s:15:"~IBLOCK_TYPE_ID";s:11:"information";s:11:"IBLOCK_CODE";s:4:"news";s:12:"~IBLOCK_CODE";s:4:"news";s:18:"IBLOCK_EXTERNAL_ID";N;s:19:"~IBLOCK_EXTERNAL_ID";N;s:13:"GLOBAL_ACTIVE";s:1:"Y";s:14:"~GLOBAL_ACTIVE";s:1:"Y";s:16:"IPROPERTY_VALUES";a:0:{}}}}s:16:"IPROPERTY_VALUES";a:0:{}s:11:"TIMESTAMP_X";s:19:"22.03.2022 12:46:46";s:9:"META_TAGS";a:4:{s:5:"TITLE";s:94:"Yandex Cloud повышает цены на облачные сервисы с 13 апреля";s:13:"BROWSER_TITLE";s:0:"";s:8:"KEYWORDS";s:0:"";s:11:"DESCRIPTION";s:0:"";}}s:18:"templateCachedData";a:3:{s:13:"additionalCSS";s:88:"/local/templates/robot/components/bitrix/news/news/bitrix/news.detail/.default/style.css";s:9:"frameMode";b:1;s:8:"__NavNum";i:1;}}}';
return true;
?>