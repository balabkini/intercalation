<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001649135860';
$dateexpire = '001685135860';
$ser_content = 'a:2:{s:7:"CONTENT";s:3298:"
<!-- <div class="col-full"> -->

<div>
		<div class="b-box">
		<img
			src="/upload/iblock/f2c/bmfct1n62avbtqnthl6wqjb4rqqx8oac.jpg"
			alt="QED предлагает кабель HDMI 2.1, поддерживающий передачу видео 8K на расстояние до 20 метров"
			title="QED предлагает кабель HDMI 2.1, поддерживающий передачу видео 8K на расстояние до 20 метров"
			/>
	</div>

	
			<h1>QED предлагает кабель HDMI 2.1, поддерживающий передачу видео 8K на расстояние до 20 метров</h1>
	
			<h3>17.02.2022</h3>
	
	

				<p>
Британская компания QED, специализирующаяся на аудио- и видеотехнике, представила оптический кабель HDMI, обеспечивающий возможность передачи сжатого видео с максимальным потоком 48 Гбит/с (10K при 120 Гц) на расстояние до 20 метров. Этот кабель с сертификацией HDMI 2.1 предназначен для современных AV-инсталляций. Он поддерживает все новшества HDMI, включая несжатое видео 48 Гбит/с с разрешением 8K при кадровой частоте 60 Гц, звук Dolby Atmos и технологию HDR (High Dynamic Range).<br><br>
В описании изделия производитель отмечает сертификацию Reaction to Fire, подтверждающую, что оболочка кабеля при горении мало дымит и не выделяет галогены.<br><br>
Для кабеля выбрана инновационная гибридная конструкция с медными проводами и оптическими волокнами, предотвращающая потери данных при передаче на большие расстояния, но при этом позволяющая обойтись без внешнего источника питания.<br><br>

Кабель, совместимый с HDCP 2.2, CEC, DDC, HDR, HEC и eARC, имеет соответствующую маркировку аккредитованной лаборатории HDMI Forum. Чтобы проверить подлинность кабеля QED Ultra Speed  Active Optical HDMI, производитель предлагает отсканировать QR-код на этикетке с помощью официального приложения HDMI.<br><br>

Кабель QED Performance Optical Ultra High Speed HDMI уже доступен по цене 139,95 фунтов стерлингов за кабель длиной 7,5 м, 159,95 фунтов стерлингов за кабель длиной 10 м, 179,95 фунтов стерлингов за кабель длиной 12 м, 199,95 фунтов стерлингов за кабель длиной 15 м и 249,95 фунтов стерлингов — за кабель длиной 20 м.<br>
</p>		<div style="clear:both"></div>
	<br />
	





";s:4:"VARS";a:2:{s:8:"arResult";a:12:{s:2:"ID";s:3:"327";s:9:"IBLOCK_ID";s:1:"1";s:4:"NAME";s:154:"QED предлагает кабель HDMI 2.1, поддерживающий передачу видео 8K на расстояние до 20 метров";s:17:"IBLOCK_SECTION_ID";s:1:"2";s:6:"IBLOCK";a:92:{s:2:"ID";s:1:"1";s:3:"~ID";s:1:"1";s:11:"TIMESTAMP_X";s:19:"24.08.2021 07:16:47";s:12:"~TIMESTAMP_X";s:19:"24.08.2021 07:16:47";s:14:"IBLOCK_TYPE_ID";s:11:"information";s:15:"~IBLOCK_TYPE_ID";s:11:"information";s:3:"LID";s:2:"ro";s:4:"~LID";s:2:"ro";s:4:"CODE";s:4:"news";s:5:"~CODE";s:4:"news";s:8:"API_CODE";s:4:"news";s:9:"~API_CODE";s:4:"news";s:4:"NAME";s:14:"Новости";s:5:"~NAME";s:14:"Новости";s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:4:"SORT";s:3:"400";s:5:"~SORT";s:3:"400";s:13:"LIST_PAGE_URL";s:27:"/information/index.php?ID=1";s:14:"~LIST_PAGE_URL";s:27:"/information/index.php?ID=1";s:15:"DETAIL_PAGE_URL";s:49:"#SITE_DIR#/information/detail.php?ID=#ELEMENT_ID#";s:16:"~DETAIL_PAGE_URL";s:49:"#SITE_DIR#/information/detail.php?ID=#ELEMENT_ID#";s:16:"SECTION_PAGE_URL";s:39:"#SITE_DIR#/novosti/#SECTION_CODE_PATH#/";s:17:"~SECTION_PAGE_URL";s:39:"#SITE_DIR#/novosti/#SECTION_CODE_PATH#/";s:18:"CANONICAL_PAGE_URL";s:0:"";s:19:"~CANONICAL_PAGE_URL";s:0:"";s:7:"PICTURE";N;s:8:"~PICTURE";N;s:11:"DESCRIPTION";s:0:"";s:12:"~DESCRIPTION";s:0:"";s:16:"DESCRIPTION_TYPE";s:4:"text";s:17:"~DESCRIPTION_TYPE";s:4:"text";s:7:"RSS_TTL";s:2:"24";s:8:"~RSS_TTL";s:2:"24";s:10:"RSS_ACTIVE";s:1:"Y";s:11:"~RSS_ACTIVE";s:1:"Y";s:15:"RSS_FILE_ACTIVE";s:1:"N";s:16:"~RSS_FILE_ACTIVE";s:1:"N";s:14:"RSS_FILE_LIMIT";N;s:15:"~RSS_FILE_LIMIT";N;s:13:"RSS_FILE_DAYS";N;s:14:"~RSS_FILE_DAYS";N;s:17:"RSS_YANDEX_ACTIVE";s:1:"N";s:18:"~RSS_YANDEX_ACTIVE";s:1:"N";s:6:"XML_ID";N;s:7:"~XML_ID";N;s:6:"TMP_ID";s:32:"42dcb3cac9cf1c29a600097d4e5cd7ec";s:7:"~TMP_ID";s:32:"42dcb3cac9cf1c29a600097d4e5cd7ec";s:13:"INDEX_ELEMENT";s:1:"Y";s:14:"~INDEX_ELEMENT";s:1:"Y";s:13:"INDEX_SECTION";s:1:"Y";s:14:"~INDEX_SECTION";s:1:"Y";s:8:"WORKFLOW";s:1:"N";s:9:"~WORKFLOW";s:1:"N";s:7:"BIZPROC";s:1:"N";s:8:"~BIZPROC";s:1:"N";s:15:"SECTION_CHOOSER";s:1:"L";s:16:"~SECTION_CHOOSER";s:1:"L";s:9:"LIST_MODE";s:0:"";s:10:"~LIST_MODE";s:0:"";s:11:"RIGHTS_MODE";s:1:"S";s:12:"~RIGHTS_MODE";s:1:"S";s:16:"SECTION_PROPERTY";s:1:"N";s:17:"~SECTION_PROPERTY";s:1:"N";s:14:"PROPERTY_INDEX";s:1:"N";s:15:"~PROPERTY_INDEX";s:1:"N";s:7:"VERSION";s:1:"1";s:8:"~VERSION";s:1:"1";s:17:"LAST_CONV_ELEMENT";s:1:"0";s:18:"~LAST_CONV_ELEMENT";s:1:"0";s:15:"SOCNET_GROUP_ID";N;s:16:"~SOCNET_GROUP_ID";N;s:16:"EDIT_FILE_BEFORE";s:0:"";s:17:"~EDIT_FILE_BEFORE";s:0:"";s:15:"EDIT_FILE_AFTER";s:0:"";s:16:"~EDIT_FILE_AFTER";s:0:"";s:13:"SECTIONS_NAME";s:14:"Разделы";s:14:"~SECTIONS_NAME";s:14:"Разделы";s:12:"SECTION_NAME";s:12:"Раздел";s:13:"~SECTION_NAME";s:12:"Раздел";s:13:"ELEMENTS_NAME";s:14:"Новости";s:14:"~ELEMENTS_NAME";s:14:"Новости";s:12:"ELEMENT_NAME";s:14:"Новость";s:13:"~ELEMENT_NAME";s:14:"Новость";s:7:"REST_ON";s:1:"N";s:8:"~REST_ON";s:1:"N";s:11:"EXTERNAL_ID";N;s:12:"~EXTERNAL_ID";N;s:8:"LANG_DIR";s:1:"/";s:9:"~LANG_DIR";s:1:"/";s:11:"SERVER_NAME";s:16:"intercalation.ru";s:12:"~SERVER_NAME";s:16:"intercalation.ru";}s:13:"LIST_PAGE_URL";s:9:"/novosti/";s:14:"~LIST_PAGE_URL";s:9:"/novosti/";s:11:"SECTION_URL";s:32:"/novosti/sovremennye-tehnologii/";s:7:"SECTION";a:1:{s:4:"PATH";a:1:{i:0;a:31:{s:2:"ID";s:1:"2";s:3:"~ID";s:1:"2";s:4:"CODE";s:22:"sovremennye-tehnologii";s:5:"~CODE";s:22:"sovremennye-tehnologii";s:6:"XML_ID";N;s:7:"~XML_ID";N;s:11:"EXTERNAL_ID";N;s:12:"~EXTERNAL_ID";N;s:9:"IBLOCK_ID";s:1:"1";s:10:"~IBLOCK_ID";s:1:"1";s:17:"IBLOCK_SECTION_ID";N;s:18:"~IBLOCK_SECTION_ID";N;s:4:"SORT";s:3:"500";s:5:"~SORT";s:3:"500";s:4:"NAME";s:43:"Современные технологии";s:5:"~NAME";s:43:"Современные технологии";s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:11:"DEPTH_LEVEL";s:1:"1";s:12:"~DEPTH_LEVEL";s:1:"1";s:16:"SECTION_PAGE_URL";s:32:"/novosti/sovremennye-tehnologii/";s:17:"~SECTION_PAGE_URL";s:32:"/novosti/sovremennye-tehnologii/";s:14:"IBLOCK_TYPE_ID";s:11:"information";s:15:"~IBLOCK_TYPE_ID";s:11:"information";s:11:"IBLOCK_CODE";s:4:"news";s:12:"~IBLOCK_CODE";s:4:"news";s:18:"IBLOCK_EXTERNAL_ID";N;s:19:"~IBLOCK_EXTERNAL_ID";N;s:13:"GLOBAL_ACTIVE";s:1:"Y";s:14:"~GLOBAL_ACTIVE";s:1:"Y";s:16:"IPROPERTY_VALUES";a:0:{}}}}s:16:"IPROPERTY_VALUES";a:0:{}s:11:"TIMESTAMP_X";s:19:"17.02.2022 13:35:13";s:9:"META_TAGS";a:4:{s:5:"TITLE";s:154:"QED предлагает кабель HDMI 2.1, поддерживающий передачу видео 8K на расстояние до 20 метров";s:13:"BROWSER_TITLE";s:0:"";s:8:"KEYWORDS";s:0:"";s:11:"DESCRIPTION";s:0:"";}}s:18:"templateCachedData";a:3:{s:13:"additionalCSS";s:88:"/local/templates/robot/components/bitrix/news/news/bitrix/news.detail/.default/style.css";s:9:"frameMode";b:1;s:8:"__NavNum";i:1;}}}';
return true;
?>