
; /* Start:"a:4:{s:4:"full";s:83:"/local/templates/robot/components/bitrix/menu/top.multi/script.min.js?1648540322409";s:6:"source";s:65:"/local/templates/robot/components/bitrix/menu/top.multi/script.js";s:3:"min";s:69:"/local/templates/robot/components/bitrix/menu/top.multi/script.min.js";s:3:"map";s:69:"/local/templates/robot/components/bitrix/menu/top.multi/script.map.js";}"*/
var jsvhover=function(){var e=document.getElementById("vertical-multilevel-menu");if(!e)return;var t=e.getElementsByTagName("li");for(var n=0;n<t.length;n++){t[n].onmouseover=function(){this.className+=" jsvhover"};t[n].onmouseout=function(){this.className=this.className.replace(new RegExp(" jsvhover\\b"),"")}}};if(window.attachEvent)window.attachEvent("onload",jsvhover);
/* End */
;
; /* Start:"a:4:{s:4:"full";s:60:"/local/js/reaspekt/reaspekt.geobase/script.js?16485403224682";s:6:"source";s:45:"/local/js/reaspekt/reaspekt.geobase/script.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
(function(window, document, $, undefined){
    "use strict";
   
    var paramsDefault = {
        height : "250",
        width : "500",
        ajax  : {
            dataType : 'html',
            headers  : { 'X-reaspektPopupBox': true }
        },
        content : null,
        fixedPosition : false
    };
    var params = {
        htmlPopup : '<div class="ReaspektPopupOverlay"></div><div id="ReaspektPopupBody"><div class="ReaspektClosePosition"><div id="ReaspektCloseBtn"></div></div><div id="ReaspektPopupContainer">��������...</div></div>',
        objPopupIdBody : '#ReaspektPopupBody',
        objPopupIdOverlay : '.ReaspektPopupOverlay',
        objPopupIdCloseBtn : '#ReaspektCloseBtn',
        objPopupIdContainer : '#ReaspektPopupContainer',
		activeClassBodyReaspekt : 'activeClassBodyReaspekt'
    };
    var methods = {
        init : function( options ) {
            
            
            return this.click(function(element){
                var obClass = $(this);
				paramsDefault['href'] = obClass.data('reaspektmodalbox-href') || obClass.attr('href');
				
				var settings = $.extend($.ReaspektModalBox, paramsDefault, options);
                
                methods.addHtmlTemplate(settings);
                
                
                if (!settings.fixedPosition) {
                    $(window).bind('resize.ReaspektPopupOverlay', $.proxy( methods.rePosition, this) );
                    methods.rePosition();
                }
            });
        },
        
        //��������� Div`s
        addHtmlTemplate : function(settings) {
            methods.closeReaspektPopup();
			$('body').append(params.htmlPopup);
            $('body').addClass(params.activeClassBodyReaspekt);
            methods.addContainerData(settings);
        },
        
        //Add data in popup html
        addContainerData : function(settings) {
            //Add event click close button
            $(params.objPopupIdCloseBtn).bind("click", function(e){
                e.preventDefault();
                
                methods.closeReaspektPopup();
            });
            
            //Add event click overlay
            $(params.objPopupIdOverlay).bind("click", function(e){
                e.preventDefault();
                
                methods.closeReaspektPopup();
            });
            
            methods._loadAjax(settings);
        },
        
        //Close popup
        closeReaspektPopup : function() {
            $(window).unbind('resize.ReaspektPopupOverlay');
            $('body').removeClass(params.activeClassBodyReaspekt);
            $(params.objPopupIdBody).remove();
            $(params.objPopupIdOverlay).remove();
        },
        
        rePosition : function() {
            
            $(params.objPopupIdBody).css("top", Math.max(0, (($(window).height() - $(params.objPopupIdBody).outerHeight()) / 2) + $(window).scrollTop()) + "px");
            
            $(params.objPopupIdBody).css("left", Math.max(0, (($(window).width() - $(params.objPopupIdBody).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
        },
        
        _loadAjax: function (settings) {
           if (settings.href) {
                $.ajax($.extend({}, settings.ajax, {
                    url: settings.href,
                    error: function (jqXHR, textStatus) {
                        console.log(jqXHR);
                        console.log(textStatus);
                    },
                    success: function (data, textStatus) {
                        if (textStatus === 'success') {
                            settings.content = data;

                            methods._afterLoad(settings);
                        }
                    }
                }));
           } else {
               console.log('Error, not atribute href or data-reaspektmodalbox-href');
           }
		},
        
        _afterLoad: function (settings) {
            $(params.objPopupIdContainer).html(settings.content);
            
            methods.rePosition();
        }
    };

    $.fn.ReaspektModalBox = function( method ) {

        // ������ ������ ������
        if ( methods[method] ) {
          return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
          return methods.init.apply( this, arguments );
        } else {
          $.error( '����� � ������ ' +  method + ' �� ���������� ��� jQuery.ReaspektModalBox' );
        } 
    };
    
})(window, document, jQuery);
/* End */
;; /* /local/templates/robot/components/bitrix/menu/top.multi/script.min.js?1648540322409*/
; /* /local/js/reaspekt/reaspekt.geobase/script.js?16485403224682*/

//# sourceMappingURL=template_443bf85b80f042a0183608e93b6a9240.map.js