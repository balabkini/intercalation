<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>

			<footer>
				<div class="footer-left">
					
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"AREA_FILE_RECURSIVE" => "Y"
	),
	false
);
?>
				</div>
					

				<img class="down" src="<?=SITE_TEMPLATE_PATH?>/img/down.png" src="" alt="">
				
				<div id="s3d-3" class="footer-center">

					<div class="footer-item">
						<p><a href="https://www.bitrix24.ru/?p=9137365">Битрикс 24</a></p>
						<p><a href="https://www.bitrix24.ru/?p=9137365">CMS:1С Битрикс</a></p>
						<p><a href="https://www.moysklad.ru/register/?p=2019-1186">Маркировка</a></p>
					</div>
					<div class="footer-item">
						<p><a href="https://sporina.bitrix24.ru/pub/form/14_vysshee_obrazovanie_i_kursy_povysheniya_kvalifikatsii/pu8ua7/">Обучение</a></p>
						<p><a href="https://uprav.ru/mini-mba/?partnerid=48209">MiniMBA</a></p>
						<p><a href="https://www.intercalation.ru/robot/uslugi/dizayn/">Дизайн</a></p>
					</div>
					<div class="footer-item">
						<p><a href="https://www.reg.ru/?rlink=reflink-97505">Домен</a></p>
						<p><a href="https://www.reg.ru/?rlink=reflink-97505">Хостинг</a></p>
						<p><a href="https://www.reg.ru/?rlink=reflink-97505">Облачный сервер</a></p>
					</div>

<?/*
					<div class="footer-item">
						<p><a href="">Пункт футера</a></p>
						<p><a href="">Пункт футера</a></p>
						<p><a href="">Пункт футера</a></p>
						<p><a href="">Пункт футера</a></p>
						<p><a href="">Пункт футера</a></p>
						<p><a href="">Пункт футера</a></p>
					</div>
					<div class="footer-item">
						<p><a href="">Пункт футера</a></p>
						<p><a href="">Пункт футера</a></p>
						<p><a href="">Пункт футера</a></p>
						<p><a href="">Пункт футера</a></p>
						<p><a href="">Пункт футера</a></p>
					</div>
					<div class="footer-item">
						<p><a href="">Пункт футера</a></p>
						<p><a href="">Пункт футера</a></p>
						<p><a href="">Пункт футера</a></p>
						<p><a href="">Пункт футера</a></p>
					</div>
*/?>
				</div>

				<div class="footer-right">
					<a href=""><h1>Внедрение</h1></a>
					<div class="socials">
						<a href="https://vk.com/event184258233"><img src="<?=SITE_TEMPLATE_PATH?>/img/vk.png" alt=""></a>
						<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/img/inst.png" alt=""></a>
						<a href="mailto:info@intercalation.ru"><img src="<?=SITE_TEMPLATE_PATH?>/img/mail.png" alt=""></a>
					</div>
					<button id="fbtn-send-form">Обратная связь</button>
				</div>
			</footer>
		</div>
	</div>

	<script src="<?=SITE_TEMPLATE_PATH?>/js/jq.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/slick/slick.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/script.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/quiz.js"></script>

<script>
(function(w,d,u){
var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
})(window,document,'https://cdn-ru.bitrix24.ru/b10018921/crm/tag/call.tracker.js');
</script>
	
</body>
</html>		