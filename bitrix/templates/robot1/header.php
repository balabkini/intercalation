<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
			
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">

	<meta name="og:url" content="https://intercalation.ru" />
	<meta name="og:title" content="Современные технологии - Внедрение в три этапа" />
	<meta name="og:image" content="<?=SITE_TEMPLATE_PATH?>/img/ss.png" />
	<meta name="og:image:width" content="150"/>
	<meta name="og:image:height" content="50"/>


	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="yandex-verification" content="246f82aa51171df2" />

	<link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/img/favicon.png" type="image/x-icon">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/style.css">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/pop-ups.css">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/quiz.css">

	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/slick/slick.css"/>
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/slick/slick-theme.css"/>
	
	<title><?$APPLICATION->ShowTitle("Внедрение");?></title>
	<?$APPLICATION->ShowHead();?>

	<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(65279116, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/65279116" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145272881-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-145272881-2');
</script>
<!-- /Google Analytics counter -->



</head>
<body>

<div id="panel">
	<?$APPLICATION->ShowPanel();?>
</div>


<form action="<?=SITE_TEMPLATE_PATH?>/send.php" method="post">
	<div id="send-form" class="bg-popup">
		<div class="bg-flex">
			<div class="form-popup">
				<img id="send-close" src="<?=SITE_TEMPLATE_PATH?>/img/close.png" alt="" class="close">
				<div class="form-wrap">
					<img src="<?=SITE_TEMPLATE_PATH?>/img/rocket.png" class="rocket" alt="">
					<input type="text" name="fio" placeholder="Иванов Иван Иванович" required>
					<input type="text" name="tel"placeholder="Телефон">
					<input type="text" name="email" placeholder="Email" required>
					<textarea name="content" id="" cols="10" rows="5" placeholder="Введите текст..."></textarea>
					<button id="send" class="center">Отправить</button>
				</div>
			</div>
		</div>
	</div>
</form>

<form action="<?=SITE_TEMPLATE_PATH?>/send2.php" method="post">
	<div id="send2-form" class="bg-popup">
		<div class="bg-flex">
			<div class="form-popup">
				<img id="send2-close" src="<?=SITE_TEMPLATE_PATH?>/img/close.png" alt="" class="close">
				<div class="form-wrap">
					<img src="<?=SITE_TEMPLATE_PATH?>/img/rocket.png" class="rocket" alt="">
					<input type="text" name="fio" placeholder="Иванов Иван Иванович" required>
					<input type="text" name="tel"placeholder="Телефон">

					
					<button id="send" class="center">Откликнуться</button>
				</div>
			</div>
		</div>
	</div>
</form>

<form action="<?=SITE_TEMPLATE_PATH?>/send3.php" method="post">
 <div id="send3-form" class="bg-popup">
    <div class="bg-flex">
        <div class="form-popup">
			<img id="send3-close" src="<?=SITE_TEMPLATE_PATH?>/img/close.png" alt="" class="close">
            <div class="form-wrap">
				<img src="<?=SITE_TEMPLATE_PATH?>/img/rocket.png" class="rocket" alt="">
                <div class="quiz-header" id="header">
                    <!-- Заголовок вопроса -->
                    <h2 class="title">Загружаем вопрос...</h2>
        
                    <!-- Результаты викторины -->
                    <input type="text" placeholder="" value="Иванов Иван Иванович">
                    <input type="text" placeholder="Телефон">
                    <button id="send3" class="center">Откликнуться</button>

                        <h2>%title%</h2>
                        <h3>%tel%</h3>
                               
                </div>
                <ul class="quiz-list" id="list">
                    <li>
                        <label>
                            <input type="radio" class="answer" name="answer" />
                            <span>Ответ...</span>
                        </label>
                    </li>
                    <li>
                        <label>
                            <input type="radio" class="answer" name="answer" />
                            <span>Ответ...</span>
                        </label>
                    </li>
                </ul>
                <button class="quiz-submit submit" id="for">Вперед</button>
                <button class="quiz-submit2 submit" id="back">Назад</button>
				<img src="<?=SITE_TEMPLATE_PATH?>/img/begin-quiz.png" class="image-quiz" id="img1">
                <img src="<?=SITE_TEMPLATE_PATH?>/img/end-quiz.png" class="image-quiz" id="img2">
            </div>
        </div>
    </div>
</div>
</form>

	<div id="auth-form" class="bg-popup">
		<div class="bg-flex">
			<div class="form-popup">
				<img id="auth-close" src="<?=SITE_TEMPLATE_PATH?>/img/close.png" alt="" class="close">
				<div class="form-wrap">
					<h1>Авторизация</h1>
					<input type="text" placeholder="Логин">
					<input type="text" placeholder="Пароль">
					<p><a id="regisr-link" class="form-link">Регистрация</a></p>
					<p><a id="btn-restore-form" class="form-link">Забыли пароль?</a></p> <!-- href="" -->
					<button id="auth" class="center">Войти</button> 
				</div>
			</div>
		</div>
	</div>
	<div id="regisr-form" class="bg-popup">
		<div class="bg-flex">
			<div class="form-popup">
				<img id="regisr-close" src="<?=SITE_TEMPLATE_PATH?>/img/close.png" alt="" class="close">
				<div class="form-wrap">
					<h1>Регистрация</h1>
					<input type="text" placeholder="Логин">
					<input type="text" placeholder="Пароль">
					<input type="text" placeholder="Email">
					<input type="text" placeholder="Телефон">
					<p><a id="auth-link" class="form-link">Авторизация</a></p>
					<button id="regisr" class="center">Регистрация</button>
				</div>
			</div>
		</div>
	</div>
	<div id="restore-form" class="bg-popup">
		<div class="bg-flex">
			<div class="form-popup">
				<img id="restore-close" src="<?=SITE_TEMPLATE_PATH?>/img/close.png" alt="" class="close">
				<div class="form-wrap">
					<img id="restore-back" src="<?=SITE_TEMPLATE_PATH?>/img/back.png" alt="" class="back">
					<h1>Восстановление пароля</h1>
					<input type="text" placeholder="Логин">
					<input type="text" placeholder="Email">
					<input type="text" placeholder="Номер телефона">
					<p>&nbsp;</p><p>&nbsp;</p>
					<button id="restore" class="center">Восстановить</button>
				</div>
			</div>
		</div> 
	</div>
	<div class="babls">
		<header>
			
			<div class="logo">
				<a href="https://intercalation.ru">
			<?/*	<h1><a href="">Внедрение</a></h1>*/?>
				<img src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" 
					alt=""
					height = "38px"
					width = "210px">
				</a>
			</div>
			


<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"top.multi", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "3",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "N",
		"COMPONENT_TEMPLATE" => "top.multi"
	),
	false
);?>



<?/*

			<nav>
				<div>
					<a href="" class="nav-link__active">Главная</a>
				</div>
				<div>
					<a class="nav-link">Подменю</a>
					<div class="sub-menu">
						<div class="bx"><div></div></div>
						<img src="<?=SITE_TEMPLATE_PATH?>/img/back.png" alt="" class="back">
						<div class="sub-link">Текст</div>
						<div class="sub-link">Текст</div>
						<div class="sub-link">Текст1</div>
						<div class="sub2-menu">
							<div class="bx2"><div></div></div>
							<img src="<?=SITE_TEMPLATE_PATH?>/img/back.png" alt="" class="back">
							<div class="sub2-link">подТекст1</div>
							<div class="sub2-link">подТекст1</div>
							<div class="sub2-link">подТекст1</div>	
						</div>
						<div class="sub-link">Текст</div>
						<div class="sub-link">Текст2</div>
						<div class="sub2-menu">
							<div class="bx2"><div></div></div>
							<img src="<?=SITE_TEMPLATE_PATH?>/img/back.png" alt="" class="back">
							<div class="sub2-link">подТекст2</div>
							<div class="sub2-link">подТекст2</div>
							<div class="sub2-link">подТекст2</div>
							<div class="sub2-link">подТекст2</div>
							<div class="sub2-link">подТекст2</div>
							<div class="sub2-link">подТекст2</div>
							<div class="sub2-link">подТекст2</div>
						</div>
					</div>
				</div>
				<div>
					<a href="" class="nav-link">Левый</a>
				</div>
				<div>
					<a href="" class="nav-link">Правый</a>
				</div>
				<div>
					<a href="" class="nav-link">Два</a>
				</div>
				<div>
					<a href="" class="nav-link">Нету</a>
				</div>
				<div id="socials" class="socials">
					<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/img/vk.png" alt=""></a>
					<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/img/inst.png" alt=""></a>
					<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/img/mail.png" alt=""></a>
				</div>
			</nav>

*/?>

			<div class="auth">
				<img id="burger" src="<?=SITE_TEMPLATE_PATH?>/img/burger.png" alt="">
				<button id="btn-auth-form">Войти</button>
			</div>




<?/* Метод вызывающий авторизацию через popup
<?$APPLICATION->IncludeFile(
 	"/robot/auth/index.php",
 			Array(),
 			Array("MODE"=>"php")
);?>			
*/?>
		