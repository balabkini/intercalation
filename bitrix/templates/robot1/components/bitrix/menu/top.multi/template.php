<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<nav>
<?
$previousLevel = 0;
foreach($arResult as $arItem):?>


	<?if ($arItem["DEPTH_LEVEL"] == 2):?>
		
		<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
			<?=str_repeat("</div>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
		<?endif?>

	<?else:?>

			<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
				<?=str_repeat("</div>",($previousLevel));?>
			<?endif?>

	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		
		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<div><a <?/* href="<?=$arItem["LINK"]?>" */?>
				class="<?if ($arItem["SELECTED"]):?>nav-link__active<?else:?>nav-link<?endif?>"><?=$arItem["TEXT"]?></a>
				<?/*<div class="root-item-2">*/?>
				<div class="sub-menu">
						<div class="bx"><div></div></div>
						<img src="<?=SITE_TEMPLATE_PATH?>/img/back.png" alt="" class="back">
		<?else:?>

				<?if ($arItem["DEPTH_LEVEL"] == 2):?>

					<div class="sub-link"><?=$arItem["TEXT"]?></div>

				
				<div class="sub2-menu">
					<div class="bx2"><div></div></div>
						<img src="<?=SITE_TEMPLATE_PATH?>/img/back.png" alt="" class="back">

				<?else:?>

					<div class="sub3-link"><a href="<?=$arItem["LINK"]?> 
						current<?if ($arItem["SELECTED"]):?>sub2-link<?endif?>">
							<?=$arItem["TEXT"]?></a></div>

				<?endif?>


		<?endif?>

	<?else:?>
		
		<?if ($arItem["PERMISSION"] > "D"):?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<div><a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>nav-link__active<?else:?>nav-link<?endif?>"><?=$arItem["TEXT"]?></a></div>
			<?else:?>
				<div <?if ($arItem["DEPTH_LEVEL"] == 2):?> 
					class="sub-link"<?else:?>class="sub2-link"<?endif?>>
					<a href="<?=$arItem["LINK"]?>"
						<?if ($arItem["DEPTH_LEVEL"] == 2):?> 
							class="sub-link"<?else:?>class="sub2-link"<?endif?>>
								<?=$arItem["TEXT"]?></a>
				</div>
				
			<?endif?>

		<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>


<?endforeach?>


				<div id="socials" class="socials">
					<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/img/vk.png" alt=""></a>
					<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/img/inst.png" alt=""></a>
					<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/img/mail.png" alt=""></a>
				</div>
</nav>


<?endif?>