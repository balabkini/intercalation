<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>


<?/*
			<nav>
				<div>
					<a href="" class="nav-link__active">Главная</a>
				</div>
				
				<div>
					<a class="nav-link">Подменю</a>
					<div class="sub-menu">
						<div class="bx"><div></div></div>
						<img src="<?=SITE_TEMPLATE_PATH?>/img/back.png" alt="" class="back">
						<div class="sub-link">Текст</div>
						<div class="sub2-menu">
							<div class="bx2"><div></div></div>
							<img src="img/back.png" alt="" class="back">
							<div class="sub2-link">подТекст</div>
							<div class="sub2-link">подТекст</div>
							<div class="sub2-link">подТекст</div>
							<div class="sub2-link">подТекст</div>
							<div class="sub2-link">подТекст</div>
						</div>
						<div class="sub-link">Текст</div>
						<div class="sub-link">Текст</div>
						<div class="sub-link">Текст</div>
						<div class="sub-link">Текст</div>
					</div>
				</div>


				<div>
					<a href="" class="nav-link">Левый</a>
				</div>
				<div>
					<a href="" class="nav-link">Правый</a>
				</div>
				<div>
					<a href="" class="nav-link">Два</a>
				</div>
				<div>
					<a href="" class="nav-link">Нету</a>
				</div>
				<div id="socials" class="socials">
					<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/img/vk.png" alt=""></a>
					<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/img/inst.png" alt=""></a>
					<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/img/mail.png" alt=""></a>
				</div>
			</nav>

*/?>



<nav>

	
				
<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>

<div>

	<?if($arItem["SELECTED"]):?>
		
		<a href="<?=$arItem["LINK"]?>" class="nav-link__active"><?=$arItem["TEXT"]?></a>

	<?else:?>
		<a href="<?=$arItem["LINK"]?>" class="nav-link"><?=$arItem["TEXT"]?></a>
		
	<?endif?>

</div>

<?endforeach?>

	

	<div id="socials" class="socials">
		<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/img/vk.png" alt=""></a>
		<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/img/inst.png" alt=""></a>
		<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/img/mail.png" alt=""></a>
	</div>

</nav>



<?endif?>