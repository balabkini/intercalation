const questions = [
	{
		question: "Вопрос 1",
		answers: [
			"CRM-системы", 
			"Сайты и Интернет-магазин", 
			"Разработка ПО", 
			"Системное администрирование" ],
		
	},
	{
		question: "Вопрос 2(CRM)",
		answers: [
			"Первичное внедрение",
			"Настройка или доработка существующей CRM"
		],
		
	},
	{
		question: "Вопрос 2(Сайты)",
		answers: [
			"Сайт Визитка",
			"Лендинг",
			"Интернет магазин"
		],
		
	},
	{
		question: "Вопрос 2(ПО)",
		answers: [
			"Интеграция между системами по API",
			"Разработка приложения к CRM",
			"Интеграция с 1С",
			"Разработка квизов"
		],
		
	},
	{
		question: "Вопрос 2(администрирование)",
		answers: [
			"Настройка сетевого оборудования",
			"Настройка серверов",
			"Настройка прочего оборудования",
			"Прокладка сетевых кабелей"
		],
		
	},
	{
		question: "Вопрос 3",
		answers: [
			"Индивидуальное решение",
			"Шаблонное решение"
		],
		
	},
	
];

let questionIndex = 0;

const headerContainer = document.querySelector('#header');
const listContainer = document.querySelector('#list');
const submitBtn = document.querySelector('#for');
const submit2Btn = document.querySelector('#back');

setTimeout(function() {
	document.getElementById('send3-form').style.display = 'block';
 }, 3000);

clearPage();
showQuestion();
submitBtn.onclick = checkAnswer;
submit2Btn.onclick = backAnswer;
//listContainer.onclick = checkAnswer; //для перехода без кнопки вперед

function clearPage(){
	headerContainer.innerHTML = '';
	listContainer.innerHTML = '';
}

function showQuestion(){
	console.log('showQuestion');
	//Вопрос
	const headerTemplate = `<h2 class="title">%title%</h2>`;//шаблон
	const title = headerTemplate.replace('%title%', questions[questionIndex]['question']);//заменяем шаблон

	headerContainer.innerHTML = title;//применяем

	//Ответы
	let answerNumber = 1;

	for (answerText of questions[questionIndex]['answers']){
		console.log(answerText);

			const questionTemplate = 
			`<li>
				<label>
					<input type="radio" value="%number%" class="answer" name="answer"/>
					<span>%answer%</span>
				</label>
			</li>
			`;
			
		
		const answerHTML = questionTemplate
							.replace('%answer%', answerText)
							.replace('%number%', answerNumber);

		listContainer.innerHTML = listContainer.innerHTML + answerHTML;
		if (questionIndex === 0) {document.getElementById('img1').style.display = 'block';} else {document.getElementById('img1').style.display = 'none';}
		document.getElementById('img2').style.display = 'none';
		answerNumber++;
	}
}

function checkAnswer(){
	const checkedRadio = listContainer.querySelector('input[type="radio"]:checked'); //поиск выбранной радиокнопки
	console.log(checkedRadio);

	//если ответ не выбран, ничего не делаем, выход из функции
	if (!checkedRadio){
		submitBtn.blur();
		return
	}
	
	//узнаем номер ответа пользователя
	let userAnswer = parseInt(checkedRadio.value);
	console.log(userAnswer);

	console.log(questions[questionIndex]);
	if (questionIndex !== questions.length){
		
		if (questions[questionIndex]['question'] === 'Вопрос 1'){
			//document.querySelector('#img1').style.display = "block";
			switch (userAnswer){
				case 1: console.log("Выбран вопрос 1 и ответ CRM"); v1 = "CRM"; questionIndex++; break;
				case 2: console.log("Выбран вопрос 1 и ответ Сайты"); v1 = "Сайты"; questionIndex = questionIndex + 2; break;
				case 3: console.log("Выбран вопрос 1 и ответ ПО"); v1 = "ПО"; questionIndex = questionIndex + 3; break;
				case 4: console.log("Выбран вопрос 1 и ответ Администрирование"); v1 = "Администрирование"; questionIndex = questionIndex + 4; break;
			}
			v3 = "";
			console.log("Выбор", v1);
			clearPage();
			showQuestion();
		}

		else if (questions[questionIndex]['question'] === 'Вопрос 2(CRM)'){			
			switch (userAnswer){	
				case 1: console.log("Выбран вопрос 2 и ответ Первичное внедрение"); v2 = " Первичное внедрение"; questionIndex = questionIndex + 4; break;
				case 2: console.log("Выбран вопрос 2 и ответ Настройка или доработка существующей CRM"); v2 = " Настройка или доработка существующей CRM"; questionIndex = questionIndex + 4; break;
			}
			v3 = "";
			console.log("Выбор", v1, v2, v3);
			clearPage();
			showResults();
		}
		
		else if (questions[questionIndex]['question'] === 'Вопрос 2(Сайты)'){
			switch (userAnswer){	
				case 1: console.log("Выбран вопрос 2 и ответ Сайт Визитка"); v2 = " Сайт Визитка"; questionIndex = questionIndex + 3; break;
				case 2: console.log("Выбран вопрос 2 и ответ Лендинг"); v2 = " Лендинг"; questionIndex = questionIndex + 3; break;
				case 3: console.log("Выбран вопрос 2 и ответ Интернет магазин"); v2 = " Интернет магазин"; questionIndex = questionIndex + 3; break;
			}
			console.log("Выбор", v1, v2);
			clearPage();
			showQuestion();
		}

		else if (questions[questionIndex]['question'] === 'Вопрос 2(ПО)'){
			switch (userAnswer){	
				case 1: console.log("Выбран вопрос 2 и ответ Интеграция между системами по API"); v2 = " Интеграция между системами по API"; questionIndex = questionIndex + 2; break;
				case 2: console.log("Выбран вопрос 2 и ответ Разработка приложения к CRM"); v2 = " Разработка приложения к CRM"; questionIndex = questionIndex + 2; break;
				case 3: console.log("Выбран вопрос 2 и ответ Интеграция с 1С"); v2 = " Интеграция с 1С"; questionIndex = questionIndex + 2; break;
				case 4: console.log("Выбран вопрос 2 и ответ Разработка квизов"); v2 = " Разработка квизов"; questionIndex = questionIndex + 2; break;
			}
			v3 = "";
			console.log("Выбор", v1, v2, v3);
			clearPage();
			showResults();
		}

		else if (questions[questionIndex]['question'] === 'Вопрос 2(администрирование)'){
			switch (userAnswer){	
				case 1: console.log("Выбран вопрос 2 и ответ Настройка сетевого оборудования"); v2 = " Настройка сетевого оборудования"; questionIndex++; break;
				case 2: console.log("Выбран вопрос 2 и ответ Настройка серверов"); v2 = "Настройка серверов"; questionIndex++; break;
				case 3: console.log("Выбран вопрос 2 и ответ Настройка прочего оборудования"); v2 = " Настройка прочего оборудования"; questionIndex++; break;
				case 4: console.log("Выбран вопрос 2 и ответ Прокладка сетевых кабелей"); v2 = " Прокладка сетевых кабелей"; questionIndex++; break;
			}
			v3 = "";
			console.log("Выбор", v1, v2, v3);
			clearPage();
			showResults();
		}

		else if (questions[questionIndex]['question'] === 'Вопрос 3'){
			switch (userAnswer){	
				case 1: console.log("Выбран вопрос 3 и ответ Индивидуальное решение"); v3 = " Индивидуальное решение"; questionIndex = questionIndex + 1; break;
				case 2: console.log("Выбран вопрос 3 и ответ Шаблонное решение"); v3 = " Шаблонное решение"; questionIndex = questionIndex + 1; break;
			}
			console.log("Выбор", v1, v2, v3);
			clearPage();
			showResults();
		}
		
		//clearPage();
		//showQuestion();

	}

	else {
		clearPage();
		showResults();
	}

}

function backAnswer(){

	submitBtn.innerText = 'Отправить';
	if (questionIndex !== questions.length - 6){

		if (questions[questionIndex]['question'] === 'Вопрос 2(CRM)'){
			 questionIndex = questionIndex - 1;
			
			//console.log("Выбор", v1, v2);
		}
		
		else if (questions[questionIndex]['question'] === 'Вопрос 2(Сайты)'){
			 questionIndex = questionIndex - 2;
			
			//console.log("Выбор", v1, v2);
		}

		else if (questions[questionIndex]['question'] === 'Вопрос 2(ПО)'){
			 questionIndex = questionIndex - 3;
		
		}

		else if (questions[questionIndex]['question'] === 'Вопрос 2(администрирование)'){

			questionIndex = questionIndex - 4;
		}

		else if (questions[questionIndex]['question'] === 'Вопрос 3'){
			if (v1 === "CRM"){
				questionIndex = questionIndex - 4;
			}
			else if (v1 === "Сайты"){
				questionIndex = questionIndex - 3;
			}
			else if (v1 === "ПО"){
				questionIndex = questionIndex - 2;
			}
			else questionIndex--;
		}

		clearPage();
		showQuestion();
		submitBtn.innerText = 'Вперед';

	}

	else {
		
		clearPage();
		showResults();
	}

}

function showResults(){
	console.log('showResults start');

	const resultsTemplate = 
	`	
		<h3>Оставьте данные</h3><br>
		<input type="text" id="fio-quiz" placeholder="Иванов Иван Иванович" required><br>
		<input type="text" id="tel-quiz" placeholder="Телефон" required><br>
		<label id="label">%result%</label><br>
		<button id="send3" class="center">Отправить</button>
	`;
	let result = v1 + ', ' + v2 + ', ' + v3;
	const final = resultsTemplate.replace('%result%', result);

	headerContainer.innerHTML = final;
	//document.getElementById('#for').style.display = 'block';
	document.getElementById('img2').style.display = 'block';
	document.getElementById('for').style.display = 'none';
	document.getElementById('back').style.display = 'none';
	//submitBtn.blur();
	//submitBtn.innerText = 'Отправить';

	var res = document.querySelector("#label"),
	text = res.lastChild.textContent.trim();
	//alert(text);
	$.ajax({
		url: 'send3.php',
		type: 'POST',
		dataType: 'json',
		data: {
			text: text,
	
		},
		success: function(){
		console.log("Переменная передана в json")
		}, error: function(){
		console.log('ERROR');
			}
		})

}
