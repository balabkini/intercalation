<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>

		<footer id="fourth">
			<div class="container">
				<div class="findUs">
					
					<div class="findUsBlock size">
						<h4>Контакты:</h4>
						<div class="phone">
							<p><span>тел:</span> +7 913 607 30 30 </p>
						</div>
						<div class="phone eMail">
							<p><span>e-mail:</span>info@sporina.icu</p>
						</div>
						<div class="phone whatsApp">
							<p><span>Whats'app & Viber:</span>+7 913 964 33 88</p>
						</div>
						<a href="http://vk.com/sporinaomsk"><img src="<?=SITE_TEMPLATE_PATH?>/img/vk.png" alt="VK"></a>
						<a href="http://facebook.com/sporinaomsk"><img src="<?=SITE_TEMPLATE_PATH?>/img/facebook.png" alt="facebook"></a>
						<a href="http://youtube.com/channel/UC930-nTvxcuRZy0J744THNg"><img src="<?=SITE_TEMPLATE_PATH?>/img/youtube.png" alt="youtube"></a>
					</div>

					<div class="findUsBlock">
						<h4>Адрес:</h4>
						<p>г. Омск ул 2-я Солнечная, 39 оф.44</p>
						<p>Почтовый индекс: 644073</p>
						
					</div>

					<div class="findUsBlock">
						<h4>Время работы:</h4>
						<p><span>Пн-Пт:</span>09:00-18:00</p>
						<p><span>Сб:</span>10:00-13:00</p>
						<p><span>Вск:</span>Выходной</p>
					</div>

				</div>
			</div>
		</footer>

		<div class="underFoot">
			<div class="container">
				<div class="footLogo">
					<a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" width="186" height="35" alt="logo"></a>
				</div>
			<!-- Copyright -->
				<div id="copyright">&copy; Untitled. All rights reserved. 2020 | Design: 
					<a href="">Спорина</a>
				</div>				

			</div>
		</div>
	</div>

<?/*
<script>
        (function(w,d,u){
                var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn.bitrix24.ru/b10018921/crm/site_button/loader_4_m0os1e.js');
</script>
<script>
	(function(w,d,u){
		var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
		var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
	})(window,document,'https://cdn.bitrix24.ru/b10018921/crm/tag/call.tracker.js');
</script>
*/?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145272881-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-145272881-1');
</script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(54749740, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/54749740" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>