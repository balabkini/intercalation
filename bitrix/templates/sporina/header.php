<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
			

<!DOCTYPE HTML>
<html>
 <head>
	<!--"Ниже приведенная строчка делает сайт адаптивным"-->
<?/*<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">*/?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	
	<title><?$APPLICATION->ShowTitle("Спорина");?></title>
		<?$APPLICATION->ShowHead();?>

<link rel="icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/sporina.css">

<link href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i,900,900i&amp;subset=cyrillic" rel="stylesheet">

</head>

<body>

<div id="panel">
	<?$APPLICATION->ShowPanel();?>
</div>

		<header>
			<div class="container">
				<div class="head_block">

					<div class="header_logo">
		        		<a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" width="186" height="35" alt="">	</a>
		    		</div>
		    		
	
<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"top", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "2",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "N",
		"COMPONENT_TEMPLATE" => "top"
	),
	false
);?>
				
					

				
</div>

			   
				<style>
				.button-large {
    background-image: -moz-linear-gradient(top, #0A36B9, #0A36B9);
    background-image: -webkit-linear-gradient(top, #0A36B9, #0A36B9);
    background-image: -ms-linear-gradient(top, #0A36B9, #0A36B9);
    background-image: linear-gradient(top, #0A36B9, #0A36B9);
    display: inline-block;
    background-color: black;
    color: white;
    text-decoration: none;
    font-size: 1em;
    height: 2em;
    line-height: 2.125em;
    font-weight: 300;
    padding: 0 45px;
    outline: 0;
    border-radius: 10px;
    box-shadow: inset 0px 0px 0px 1px rgba(0, 0, 0, 0.75), inset 0px 2px 0px 0px rgba(255, 192, 192, 0.5), inset 0px 0px 0px 2px rgba(255, 96, 96, 0.85), 3px 3px 3px 1px rgba(0, 0, 0, 0.15);
    text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.5);
  }
 	
				</style>	
	<a href="http://intercalation.ru/sporina/auth/" class="button-large">Войти</a>

				</div> 
		</header>
		