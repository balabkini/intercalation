<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<!DOCTYPE HTML>
<!--
	Halcyonic by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<!--"Ниже приведенная строчка делает сайт адаптивным"-->
		 <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
		<!--<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />-->
		<title><?$APPLICATION->ShowTitle();?></title>
		<?$APPLICATION->ShowHead();?>
		<link rel="shortcut icon" type="image/x-icon" href="/favicon1.ico" /> 
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/assets/css/main.css" />
	</head>
	<body>

		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>
		
		<div id="page-wrapper">
			

			<!-- Header -->
				<section id="header">
					<div class="container">
						<div class="row">
							<div class="col-12">

																
								<!-- Logo -->
									<h1><a a href="http://www.intercalation.ru/lp" id="logo">Внедрение</a></h1>
									<!-- Nav -->
								
<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"top", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "N",
		"COMPONENT_TEMPLATE" => "top"
	),
	false
);?>


								</div>
							</div>
			    		</div>

</section>