<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>

<!-- Footer -->
				<section id="footer">
					<div class="container">
						<div class="row">
							<div class="col-8 col-12-medium">
								<!-- Links -->
									<section>
										<h2>Внимание</h2>
											<div class="row">
											    <div class="col-3 col-6-medium col-12-small">
<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"footer_menu", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "N",
		"COMPONENT_TEMPLATE" => "footer_menu"
	),
	false
);?>
<?/*
													<ul class="link-list last-child">
														<li><a href="#">Neque amet dapibus</a></li>
														<li><a href="#">Sed mattis quis rutrum</a></li>
														<li><a href="#">Accumsan suspendisse</a></li>
														<li><a href="#">Eu varius vitae magna</a></li>
													</ul>
*/?>
												</div>
												<div class="col-3 col-12-small">

<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "footer_news", Array(
	"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"COUNT_ELEMENTS" => "N",	// Показывать количество элементов в разделе
		"COUNT_ELEMENTS_FILTER" => "CNT_ACTIVE",	// Показывать количество
		"FILTER_NAME" => "sectionsFilter",	// Имя массива со значениями фильтра разделов
		"IBLOCK_ID" => "1",	// Инфоблок
		"IBLOCK_TYPE" => "information",	// Тип инфоблока
		"SECTION_CODE" => "",	// Код раздела
		"SECTION_FIELDS" => array(	// Поля разделов
			0 => "",
			1 => "",
		),
		"SECTION_ID" => $_REQUEST["SECTION_ID"],	// ID раздела
		"SECTION_URL" => "/novosti/#SECTION_CODE_PATH#/",	// URL, ведущий на страницу с содержимым раздела
		"SECTION_USER_FIELDS" => array(	// Свойства разделов
			0 => "",
			1 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "2",	// Максимальная отображаемая глубина разделов
		"VIEW_MODE" => "LIST",
		"COMPONENT_TEMPLATE" => "news"
	),
	false
);?><br>
												</div>

												<div class="col-3 col-6-medium col-12-small">

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"footer_news", 
	array(
		"ACTIVE_DATE_FORMAT" => "j M Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "/stati/#ELEMENT_CODE#/",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "-",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "5",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Статьи",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "NAME",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "footer_news"
	),
	false
);?><br>
												</div>

												<div class="col-3 col-12-small">
													<ul class="link-list last-child">
														
														<li><a href="#">Neque amet dapibus</a></li>
														<li><a href="#">Sed mattis quis rutrum</a></li>
														<li><a href="#">Accumsan suspendisse</a></li>
														<li><a href="#">Eu varius vitae magna</a></li>
													</ul>
												</div>

											</div>
								    </section> 
							</div>
							
							<div class="col-4 col-12-medium imp-medium">

								<!-- Blurb -->
									<section>
										<h2>Свяжитесь с нами</h2>


<?$APPLICATION->IncludeComponent("altasib:feedback.form", "footer_form", Array(
	"ACTIVE_ELEMENT" => "Y",	// Активировать элемент после добавления
		"ADD_HREF_LINK" => "Y",	// Добавлять ссылку на страницу, с которой были отправлены данные формы
		"ADD_LEAD" => "N",	// Добавлять лид на корпоративном портале Битрикс24
		"ALX_IBLOCK_ELEMENT_LINK" => "",
		"ALX_LINK_POPUP" => "N",	// Открывать во всплывающем окне
		"ALX_LOAD_PAGE" => "N",
		"ALX_NAME_LINK" => "Напишите нам",
		"ALX_POPUP_TITLE" => "",
		"BBC_MAIL" => "",	// Дополнительные E-mail получателей почтовых уведомлений (через запятую)
		"CAPTCHA_TYPE" => "default",
		"CATEGORY_SELECT_NAME" => "Выберите категорию",	// Название списка разделов
		"CHANGE_CAPTCHA" => "N",
		"CHECKBOX_TYPE" => "CHECKBOX",	// Выберите тип чекбоксов
		"CHECK_ERROR" => "Y",	// Выдавать сообщения об ошибках
		"COLOR_OTHER" => "#FF0000",	// Другой цвет, в формате #XXXXXX
		"COLOR_SCHEME" => "BRIGHT",	// Цветовая схема
		"COLOR_THEME" => "",	// Цвет темы
		"EVENT_TYPE" => "ALX_FEEDBACK_FORM",	// Тип почтового события
		"FB_TEXT_NAME" => "",	// Название поля «Текст сообщения» [FEEDBACK_TEXT]
		"FB_TEXT_SOURCE" => "PREVIEW_TEXT",	// Где сохранять поле «Текст сообщения»
		"FORM_ID" => "1",	// ID Формы
		"IBLOCK_ID" => "5",	// Инфоблок
		"IBLOCK_TYPE" => "altasib_feedback",	// Тип инфоблока
		"INPUT_APPEARENCE" => array(	// Внешний вид полей
			0 => "DEFAULT",
		),
		"JQUERY_EN" => "jquery",	// Подключать jQuery Битрикс
		"LINK_SEND_MORE_TEXT" => "Отправить ещё одно сообщение",	// Текст ссылки для отправки другого сообщения
		"LOCAL_REDIRECT_ENABLE" => "N",	// Выполнять при успешном добавлении сообщения переход на другую страницу
		"MASKED_INPUT_PHONE" => array(	// Поля, обрабатываемые маской ввода телефона
			0 => "PHONE",
		),
		"MESSAGE_OK" => "Ваше сообщение было успешно отправлено",	// Текст, выводимый при успешном добавлении сообщения
		"NAME_ELEMENT" => "ALX_DATE",	// Сделать именем элемента
		"NOT_CAPTCHA_AUTH" => "Y",
		"POPUP_ANIMATION" => "0",
		"PROPERTY_FIELDS" => array(	// Поля для заполнения
			0 => "FIO",
			1 => "EMAIL",
			2 => "FEEDBACK_TEXT",
		),
		"PROPERTY_FIELDS_REQUIRED" => array(	// Обязательные для заполнения
			0 => "FIO",
			1 => "EMAIL",
			2 => "FEEDBACK_TEXT",
		),
		"PROPS_AUTOCOMPLETE_EMAIL" => array(	// Поля подстановки E-mail пользователя
			0 => "EMAIL",
		),
		"PROPS_AUTOCOMPLETE_NAME" => array(	// Поля подстановки ФИО пользователя
			0 => "FIO",
		),
		"PROPS_AUTOCOMPLETE_PERSONAL_PHONE" => array(	// Поля подстановки телефона пользователя
			0 => "PHONE",
		),
		"PROPS_AUTOCOMPLETE_VETO" => "Y",	// Запретить изменение полей автоподстановки для зарегистрированных пользователей
		"REQUIRED_SECTION" => "N",	// Обязательный выбор категории
		"SECTION_FIELDS_ENABLE" => "N",	// Использовать различный набор полей для разделов
		"SECTION_MAIL_ALL" => "a.nechaykin@sporina.icu",	// Для всех категорий
		"SEND_IMMEDIATE" => "Y",	// Отправлять почтовое сообщение немедленно, без записи в таблицу b_event
		"SEND_MAIL" => "N",	// Отправлять подтверждение о принятии обращения отправителю, если указан e-mail
		"SHOW_LINK_TO_SEND_MORE" => "Y",	// Показывать ссылку на отправку другого сообщения
		"SHOW_MESSAGE_LINK" => "Y",	// Добавлять ссылку просмотра сообщения в письме администратору
		"SPEC_CHAR" => "N",	// Блокировать отправку формы, если в данных (кроме поля email) имеются спецсимволы
		"USERMAIL_FROM" => "N",	// Подставлять обратный адрес в письме администратору из поля E-mail, заполненного пользователем
		"USER_CONSENT" => "N",	// Запрашивать согласие
		"USER_CONSENT_ID" => "0",	// Соглашение
		"USER_CONSENT_INPUT_LABEL" => "",	// Текст описания чекбокса (оставьте пустым для значения по-умолчанию)
		"USER_CONSENT_IS_CHECKED" => "Y",	// Галка по умолчанию проставлена
		"USER_CONSENT_IS_LOADED" => "N",	// Загружать текст сразу
		"USE_CAPTCHA" => "N",	// Использовать CAPTCHA
		"WIDTH_FORM" => "50%",	// Ширина формы (% или px)
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>


										
<?/*									
<?$APPLICATION->IncludeComponent(
	"slam:easyform", 
	".default", 
	array(
		"CATEGORY_EMAIL_PLACEHOLDER" => "",
		"CATEGORY_EMAIL_TITLE" => "Ваш E-mail",
		"CATEGORY_EMAIL_TYPE" => "email",
		"CATEGORY_EMAIL_VALIDATION_ADDITIONALLY_MESSAGE" => "data-bv-emailaddress-message=\"E-mail введен некорректно\"",
		"CATEGORY_EMAIL_VALIDATION_MESSAGE" => "Обязательное поле",
		"CATEGORY_EMAIL_VALUE" => "",
		"CATEGORY_MESSAGE_PLACEHOLDER" => "",
		"CATEGORY_MESSAGE_TITLE" => "Сообщение",
		"CATEGORY_MESSAGE_TYPE" => "textarea",
		"CATEGORY_MESSAGE_VALIDATION_ADDITIONALLY_MESSAGE" => "",
		"CATEGORY_MESSAGE_VALUE" => "",
		"CATEGORY_PHONE_INPUTMASK" => "N",
		"CATEGORY_PHONE_INPUTMASK_TEMP" => "+7 (999) 999-9999",
		"CATEGORY_PHONE_PLACEHOLDER" => "",
		"CATEGORY_PHONE_TITLE" => "Мобильный телефон",
		"CATEGORY_PHONE_TYPE" => "tel",
		"CATEGORY_PHONE_VALIDATION_ADDITIONALLY_MESSAGE" => "",
		"CATEGORY_PHONE_VALUE" => "",
		"CATEGORY_TITLE_PLACEHOLDER" => "",
		"CATEGORY_TITLE_TITLE" => "Ваше имя",
		"CATEGORY_TITLE_TYPE" => "text",
		"CATEGORY_TITLE_VALIDATION_ADDITIONALLY_MESSAGE" => "",
		"CATEGORY_TITLE_VALUE" => "",
		"CREATE_SEND_MAIL" => "",
		"DISPLAY_FIELDS" => array(
			0 => "TITLE",
			1 => "EMAIL",
			2 => "MESSAGE",
			3 => "",
		),
		"EMAIL_BCC" => "",
		"EMAIL_FILE" => "N",
		"EMAIL_TO" => "",
		"ENABLE_SEND_MAIL" => "Y",
		"ERROR_TEXT" => "Произошла ошибка. Сообщение не отправлено.",
		"EVENT_MESSAGE_ID" => array(
		),
		"FIELDS_ORDER" => "TITLE,EMAIL,MESSAGE",
		"FORM_AUTOCOMPLETE" => "Y",
		"FORM_ID" => "FORM5",
		"FORM_NAME" => "Форма обратной связи 6",
		"FORM_SUBMIT_VALUE" => "Отправить",
		"FORM_SUBMIT_VARNING" => "Нажимая на кнопку \"#BUTTON#\", вы даете согласие на обработку <a target=\"_blank\" href=\"#\">персональных данных</a>",
		"HIDE_ASTERISK" => "N",
		"HIDE_FIELD_NAME" => "N",
		"HIDE_FORMVALIDATION_TEXT" => "N",
		"INCLUDE_BOOTSRAP_JS" => "Y",
		"MAIL_SUBJECT_ADMIN" => "#SITE_NAME#: Сообщение из формы обратной связи",
		"OK_TEXT" => "Ваше сообщение отправлено. Мы свяжемся с вами в течение 2х часов",
		"REQUIRED_FIELDS" => array(
		),
		"SEND_AJAX" => "Y",
		"SHOW_MODAL" => "N",
		"TITLE_SHOW_MODAL" => "Спасибо!",
		"USE_BOOTSRAP_CSS" => "Y",
		"USE_BOOTSRAP_JS" => "N",
		"USE_CAPTCHA" => "N",
		"USE_FORMVALIDATION_JS" => "Y",
		"USE_IBLOCK_WRITE" => "N",
		"USE_JQUERY" => "N",
		"USE_MODULE_VARNING" => "Y",
		"WIDTH_FORM" => "500px",
		"_CALLBACKS" => "",
		"COMPONENT_TEMPLATE" => ".default",
		"EMAIL_SEND_FROM" => "N"
	),
	false
);?>
*/?>
										
<?/*
											<p>Duis neque nisi, dapibus sed mattis quis, rutrum accumsan sed. Suspendisse eu
											varius nibh. Suspendisse vitae magna eget odio amet mollis. Duis neque nisi,
											dapibus sed mattis quis, sed rutrum accumsan sed. Suspendisse eu varius nibh
											lorem ipsum amet dolor sit amet lorem ipsum consequat gravida justo mollis.</p>*/?>


									</section>

							</div>
							
						</div>
					</div>
				</section>

			<!-- Copyright -->
				<div id="copyright">&copy; Untitled. All rights reserved. 2020 | Design: 
					<a href="https://sporina.icu">Спорина</a>
				</div>

		</div>

		<!-- Scripts -->
			<script src="<?=SITE_TEMPLATE_PATH?>/assets/js/jssor.slider-28.0.0.min.js" 
				type="text/javascript"></script>
			<script src="<?=SITE_TEMPLATE_PATH?>/assets/js/jquery.min.js"></script>
			<script src="<?=SITE_TEMPLATE_PATH?>/assets/js/browser.min.js"></script>
			<script src="<?=SITE_TEMPLATE_PATH?>/assets/js/breakpoints.min.js"></script>
			<script src="<?=SITE_TEMPLATE_PATH?>/assets/js/util.js"></script>
			<script src="<?=SITE_TEMPLATE_PATH?>/assets/js/main.js"></script>

	</body>
</html>