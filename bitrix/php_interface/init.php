<?
use Bitrix\Main\Diag\Debug;

//include("include/redirects_v4.php");
include("include/excel-parser.php");



function pre($data, $public = false, $toFile = false)
{
    global $USER;

    if ($toFile) {
        Debug::writeToFile(print_r($data, 1));
    }
    return $USER->isAdmin() || $public ? '<pre>' . print_r($data, true) . '</pre>' : '';
}
?>