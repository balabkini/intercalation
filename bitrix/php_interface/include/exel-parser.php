<?
/**
 * Функция проверяет существование файла
 * Ln(nF$(:WY98r'a>
 * isPdfExistsInUpload($val);
 * isPdfExistsInUpload($val, 'description');
*/
function isPdfExistsInUpload($article = '', $mode = 'file')
{
	if (!strlen($article)) return;

	$r = [];
	$files = [
		'catalogpr' => [
			'file' => '/upload/tmp/import/catalogpr/' . $article . '.pdf',
			'description' => 'Каталог Производителя'
		],
		'cert' => [
			'file' => '/upload/tmp/import/cert/' . $article . '.pdf',
			'description' => 'Сертификаты'
		],

		'passport' => [
			'file' => '/upload/tmp/import/passport/' . $article . '.pdf',
			'description' => 'Паспорт'
		]
	];

	foreach ($files as $file) {
		if (file_exists($_SERVER['DOCUMENT_ROOT'] . $file['file'])) {
			$r[] = $file[$mode];
		}
	}
	
	return count($r) ? implode(';', $r) : '';
}

/**
 * Удаление файлов импорта после загрузки через функцию isPdfExistsInUpload()
*/
AddEventHandler("esol.importexportexcel", "OnEndImport", "deleteImportFiles");
function deleteImportFiles($r, $data)
{
	$dir = $_SERVER['DOCUMENT_ROOT'] . '/upload/tmp/import';
    if (is_dir($dir)) {
    	\Bitrix\Main\IO\Directory::deleteDirectory($dir);
    }
}