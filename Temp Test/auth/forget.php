<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Забыли пароль");
?>



<!-- Content -->
				<section id="content">
					<div class="container">
						<div class="row">
							<div class="col-9 col-12-medium">

								<!-- Main Content -->
									<section>
										<header>
											<h2>Восстановление пароля</h2>
											<h3>
<?$APPLICATION->IncludeComponent(
	"bitrix:system.auth.forgotpasswd",
	"",
	Array(
		"FORGOT_PASSWORD_URL" => "/auth/forget.php",
		"PROFILE_URL" => "/personal/",
		"REGISTER_URL" => "/auth/registration.php",
		"SHOW_ERRORS" => "N"
	)
);?>	
											</h3>
										</header>	


									
										
									</section>

							</div>
							<div class="col-3 col-12-medium">
<!-- Sidebar -->
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"COMPONENT_TEMPLATE" => ".default",
		"AREA_FILE_RECURSIVE" => "Y"
	),
	false
);?>

								
									
							</div>
						</div>
					</div>
				</section>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>