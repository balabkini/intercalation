<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Регистрация пользователя");
?>



<!-- Content -->
				<section id="content">
					<div class="container">
						<div class="row">
							<div class="col-9 col-12-medium">

								<!-- Main Content -->
									<section>
										<header>
											<h2>Регистрация пользователя</h2>
											<h3>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.register",
	"",
	Array(
		"AUTH" => "Y",
		"REQUIRED_FIELDS" => array(),
		"SET_TITLE" => "Y",
		"SHOW_FIELDS" => array(),
		"SUCCESS_PAGE" => "",
		"USER_PROPERTY" => array(),
		"USER_PROPERTY_NAME" => "",
		"USE_BACKURL" => "Y"
	)
);?>
											</h3>

										</header>


									</section>

							</div>
							<div class="col-3 col-12-medium">
<!-- Sidebar -->
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"COMPONENT_TEMPLATE" => ".default",
		"AREA_FILE_RECURSIVE" => "Y"
	),
	false
);?>

								
									
							</div>
						</div>
					</div>
				</section>




<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>