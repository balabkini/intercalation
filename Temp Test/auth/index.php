<?
//define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

if (isset($_REQUEST["backurl"]) && strlen($_REQUEST["backurl"])>0) 
	LocalRedirect($backurl);

$APPLICATION->SetTitle("Авторизация");
?>
			<!-- Content -->
				<section id="content">
					<div class="container">
						<div class="row">
							<!-- Main Content -->
							<div class="col-9 col-12-medium">
									<section>
										<header>
											<h2>Авторизация</h2>
											<h3>
<?$APPLICATION->IncludeComponent("bitrix:system.auth.form", "robot.auth", Array(
	"FORGOT_PASSWORD_URL" => "/auth/forget.php",	// Страница забытого пароля
		"PROFILE_URL" => "/personal/",	// Страница профиля
		"REGISTER_URL" => "/auth/registration.php",	// Страница регистрации
		"SHOW_ERRORS" => "Y",	// Показывать ошибки
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>


											</h3>
										</header>
					<p>Какой то текст</p>
									</section>
							</div>



							<!-- Sidebar -->
							<div class="col-3 col-12-medium">
									<section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"COMPONENT_TEMPLATE" => ".default",
		"AREA_FILE_RECURSIVE" => "Y"
	),
	false
);?>



									</section>
							</div>

						</div>
					</div>
				</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>