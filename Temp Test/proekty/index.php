<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Проекты");
?>


Все расскажеми покажем <br>

<?/*
Может кому пригодится. Как побороть нижеописанные баги.

1. Не загружаются базы при установке.
Нужно переименовать функцию CArchiver в __construct
Функция находится в файле bitrix/modules/reaspekt.geobase/admin/reaspekt_geobase_update_ipgeobase.php

2. Не закрывается окно выбора города, при нажатии на "Да".
В файле bitrix/modules/reaspekt.geobase/classes/general/geoip.php вместо
$APPLICATION->set_cookie("REASPEKT_GEOBASE_SAVE", "Y", time() + 86400); // 60*60*24
пишем:
setcookie("BITRIX_SM_REASPEKT_GEOBASE_SAVE", "Y", time() + 31104000, "/");

3. Не пропадает иконка загрузки.
Комментируем
$.fn.ReaspektModalBox('rePosition'); на строке 122 в файле local/components/reaspekt/reaspekt.geoip/templates/.default/script.js

*/?>

<?$APPLICATION->IncludeComponent(
	"reaspekt:reaspekt.geoip", 
	"mytown", 
	array(
		"CHANGE_CITY_MANUAL" => "Y",
		"COMPONENT_TEMPLATE" => "mytown"
	),
	false
);?>



 <?$APPLICATION->IncludeComponent(
	"altasib:feedback.form",
	"",
	Array(
		"ACTIVE_ELEMENT" => "Y",
		"ADD_HREF_LINK" => "Y",
		"ADD_LEAD" => "N",
		"ALX_LINK_POPUP" => "N",
		"BBC_MAIL" => "",
		"CATEGORY_SELECT_NAME" => "Выберите категорию",
		"CHECKBOX_TYPE" => "CHECKBOX",
		"CHECK_ERROR" => "Y",
		"COLOR_SCHEME" => "BRIGHT",
		"EVENT_TYPE" => "ALX_FEEDBACK_FORM",
		"FB_TEXT_NAME" => "",
		"FB_TEXT_SOURCE" => "PREVIEW_TEXT",
		"FORM_ID" => "1",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "altasib_feedback",
		"INPUT_APPEARENCE" => array("DEFAULT"),
		"JQUERY_EN" => "jquery",
		"LINK_SEND_MORE_TEXT" => "Отправить ещё одно сообщение",
		"LOCAL_REDIRECT_ENABLE" => "N",
		"MASKED_INPUT_PHONE" => array("PHONE"),
		"MESSAGE_OK" => "Ваше сообщение было успешно отправлено",
		"NAME_ELEMENT" => "ALX_DATE",
		"PROPERTY_FIELDS" => array("FIO","EMAIL","FEEDBACK_TEXT"),
		"PROPERTY_FIELDS_REQUIRED" => array("FEEDBACK_TEXT"),
		"PROPS_AUTOCOMPLETE_EMAIL" => array("EMAIL"),
		"PROPS_AUTOCOMPLETE_NAME" => array("FIO"),
		"PROPS_AUTOCOMPLETE_PERSONAL_PHONE" => array("PHONE"),
		"PROPS_AUTOCOMPLETE_VETO" => "N",
		"REQUIRED_SECTION" => "N",
		"SECTION_FIELDS_ENABLE" => "N",
		"SECTION_MAIL_ALL" => "a.nechaykin@sporina.icu",
		"SEND_IMMEDIATE" => "Y",
		"SEND_MAIL" => "N",
		"SHOW_LINK_TO_SEND_MORE" => "Y",
		"SHOW_MESSAGE_LINK" => "Y",
		"SPEC_CHAR" => "N",
		"USERMAIL_FROM" => "N",
		"USER_CONSENT" => "N",
		"USER_CONSENT_ID" => "0",
		"USER_CONSENT_INPUT_LABEL" => "",
		"USER_CONSENT_IS_CHECKED" => "Y",
		"USER_CONSENT_IS_LOADED" => "N",
		"USE_CAPTCHA" => "Y",
		"WIDTH_FORM" => "50%"
	)
);?>

<script type="text/javascript">(function(i,m,p,a,c,t){c.ire_o=p;c[p]=c[p]||function(){(c[p].a=c[p].a||[]).push(arguments)};t=a.createElement(m);var z=a.getElementsByTagName(m)[0];t.async=1;t.src=i;z.parentNode.insertBefore(t,z)})('//d.impactradius-event.com/P-A2203312-b1cc-4eb6-ba49-ff86dd15d6461.js','script','impactStat',document,window);impactStat('transformLinks');impactStat('trackImpression');</script>



 <?$APPLICATION->IncludeComponent(
	"slam:easyform",
	"",
	Array(
		"CATEGORY_EMAIL_PLACEHOLDER" => "",
		"CATEGORY_EMAIL_TITLE" => "Ваш E-mail",
		"CATEGORY_EMAIL_TYPE" => "email",
		"CATEGORY_EMAIL_VALIDATION_ADDITIONALLY_MESSAGE" => "data-bv-emailaddress-message=\"E-mail введен некорректно\"",
		"CATEGORY_EMAIL_VALIDATION_MESSAGE" => "Обязательное поле",
		"CATEGORY_EMAIL_VALUE" => "",
		"CATEGORY_MESSAGE_PLACEHOLDER" => "",
		"CATEGORY_MESSAGE_TITLE" => "Сообщение",
		"CATEGORY_MESSAGE_TYPE" => "textarea",
		"CATEGORY_MESSAGE_VALIDATION_ADDITIONALLY_MESSAGE" => "",
		"CATEGORY_MESSAGE_VALUE" => "",
		"CATEGORY_PHONE_INPUTMASK" => "N",
		"CATEGORY_PHONE_INPUTMASK_TEMP" => "+7 (999) 999-9999",
		"CATEGORY_PHONE_PLACEHOLDER" => "",
		"CATEGORY_PHONE_TITLE" => "Мобильный телефон",
		"CATEGORY_PHONE_TYPE" => "tel",
		"CATEGORY_PHONE_VALIDATION_ADDITIONALLY_MESSAGE" => "",
		"CATEGORY_PHONE_VALUE" => "",
		"CATEGORY_TITLE_PLACEHOLDER" => "",
		"CATEGORY_TITLE_TITLE" => "Ваше имя",
		"CATEGORY_TITLE_TYPE" => "text",
		"CATEGORY_TITLE_VALIDATION_ADDITIONALLY_MESSAGE" => "",
		"CATEGORY_TITLE_VALUE" => "",
		"CREATE_SEND_MAIL" => "",
		"DISPLAY_FIELDS" => array("TITLE","EMAIL","PHONE","MESSAGE",""),
		"EMAIL_BCC" => "",
		"EMAIL_FILE" => "N",
		"EMAIL_TO" => "",
		"ENABLE_SEND_MAIL" => "Y",
		"ERROR_TEXT" => "Произошла ошибка. Сообщение не отправлено.",
		"EVENT_MESSAGE_ID" => array(),
		"FIELDS_ORDER" => "TITLE,EMAIL,PHONE,MESSAGE",
		"FORM_AUTOCOMPLETE" => "Y",
		"FORM_ID" => "FORM5",
		"FORM_NAME" => "Форма обратной связи 6",
		"FORM_SUBMIT_VALUE" => "Отправить",
		"FORM_SUBMIT_VARNING" => "Нажимая на кнопку \"#BUTTON#\", вы даете согласие на обработку <a target=\"_blank\" href=\"#\">персональных данных</a>",
		"HIDE_ASTERISK" => "N",
		"HIDE_FIELD_NAME" => "N",
		"HIDE_FORMVALIDATION_TEXT" => "N",
		"INCLUDE_BOOTSRAP_JS" => "Y",
		"MAIL_SUBJECT_ADMIN" => "#SITE_NAME#: Сообщение из формы обратной связи",
		"OK_TEXT" => "Ваше сообщение отправлено. Мы свяжемся с вами в течение 2х часов",
		"REQUIRED_FIELDS" => array("EMAIL"),
		"SEND_AJAX" => "Y",
		"SHOW_MODAL" => "N",
		"TITLE_SHOW_MODAL" => "Спасибо!",
		"USE_BOOTSRAP_CSS" => "Y",
		"USE_BOOTSRAP_JS" => "N",
		"USE_CAPTCHA" => "N",
		"USE_FORMVALIDATION_JS" => "Y",
		"USE_IBLOCK_WRITE" => "N",
		"USE_JQUERY" => "N",
		"USE_MODULE_VARNING" => "Y",
		"WIDTH_FORM" => "500px",
		"_CALLBACKS" => ""
	)
);?>



<a href="https://shutterstock.7eer.net/c/2203312/586711/1305" id="586711"><img src="//a.impactradius-go.com/display-ad/1305-586711" border="0" alt="" width="300" height="250"/></a><img height="0" width="0" src="//shutterstock.7eer.net/i/2203312/586711/1305" style="position:absolute;visibility:hidden;" border="0" />


<br>

<h3 id="253413"><a href="https://shutterstock.7eer.net/c/2203312/253413/1305">Save 20% off image and footage products</a></h3>
<img height="0" width="0" src="//shutterstock.7eer.net/i/2203312/253413/1305" style="position:absolute;visibility:hidden;" border="0" />

<h3 id="44350"><a href="https://shutterstock.7eer.net/c/2203312/44350/1305">Low Cost Stock Images</a></h3>
<img height="0" width="0" src="//shutterstock.7eer.net/i/2203312/44350/1305" style="position:absolute;visibility:hidden;" border="0" />


<?$APPLICATION->IncludeComponent(
	"wsrubi:barcode", 
	"barcode", 
	array(
		"COMPONENT_TEMPLATE" => "barcode",
		"TYPE" => "Codabar",
		"VAL" => "01",
		"IMAGE_TYPE" => "jpeg",
		"RENDER_FORMAT" => "image"
	),
	false
);?>

<h3 id="44350"><a href="https://shutterstock.7eer.net/c/2203312/44350/1305">Low Cost Stock Images</a></h3>
<img height="0" width="0" src="//shutterstock.7eer.net/i/2203312/44350/1305" style="position:absolute;visibility:hidden;" border="0" />

<a href="https://shutterstock.7eer.net/c/2203312/54701/1305" id="54701"><img src="//a.impactradius-go.com/display-ad/1305-54701" border="0" alt="" width="588" height="90"/></a><img height="0" width="0" src="//shutterstock.7eer.net/i/2203312/54701/1305" style="position:absolute;visibility:hidden;" border="0" />

<html>
<head>
	<title>Shutterstock</title>
	<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type" />
</head>

<body marginheight="0" topmargin="0" marginwidth="0" leftmargin="0" bgcolor="#EBEFEE">
<table cellspacing="0" border="0" style="background: #EBEFEE;" cellpadding="0" width="100%">
<tr>
<td valign="top">
                    <!-- content -->
                    <table cellspacing="0" border="0" align="center" style="background: #EBEFEE;" cellpadding="0" width="650">
                    <tr>
                    <td bgcolor="#EBEFEE" width="650" valign="top"><table cellspacing="0" border="0" align="center" cellpadding="0" width="650">
                    <tr>
                    <td width="650" height="48" bgcolor="#EBEFEE" style="font-family:Arial, Helvetica, sans-serif; font-size:30px; color:#ff0000"><a href="https://shutterstock.7eer.net/c/2203312/108513/1305" style="font-family:Arial, Helvetica, sans-serif; font-size:30px; color:#ff0000"><img src="http://d34kf23wmhy492.cloudfront.net/1305_691733745.jpg" alt="Shutterstock" width="650" height="48" border="0" style="display: block"></a></td>
                    </tr>
                    </table>
                    <table cellspacing="0" border="0" align="center" cellpadding="0" width="650">
                    <tr>
                    <td width="386" height="362" valign="top" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif; font-size:36px; color:#ff0000"><a href="https://shutterstock.7eer.net/c/2203312/108513/1305" style="font-family:Arial, Helvetica, sans-serif; font-size:36px; color:#ff0000"><img src="http://d34kf23wmhy492.cloudfront.net/1305_61882774.jpg" alt="Save 20%. Create cool stuff." width="386" height="362" border="0" style="display: block"></a></td>
                    <td width="240" height="362" valign="top" bgcolor="#ffffff"><table cellspacing="0" border="0" align="center" cellpadding="0" width="240">
                    <tr>
                    <td height="20" bgcolor="#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_910411200.gif" width="1" height="20" border="0" style="display: block"></td>
                    </tr>
                    </table>
                    <table cellspacing="0" border="0" align="center" cellpadding="0" width="240">
                    <tr>
                    <td bgcolor="#ffffff" align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:22px; color:#ff0000">Limited time offer:<br>                      <strong>Save 20%</strong> when you<br>
sign up today!</td>
<td width="10" bgcolor="#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_1184366706.gif" width="10" height="1" border="0" style="display: block"></td>
                    </tr>
                    </table>
                    <table cellspacing="0" border="0" align="center" cellpadding="0" width="240">
                    <tr>
                    <td height="20" bgcolor="#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_851505331.gif" width="1" height="20" border="0" style="display: block"></td>
                    </tr>
                    </table>
                    <table cellspacing="0" border="0" align="center" cellpadding="0" width="240">
                    <tr>
                    <td bgcolor="#ffffff" align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#ff0000; line-height:16px">&bull;</td>
                    <td width="5" bgcolor="#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_1083627688.gif" width="5" height="1" border="0" style="display: block"></td>
                    <td bgcolor="#ffffff" align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#666666; line-height:16px">30 million images and videos from 150 countries</td>
                    <td width="10" bgcolor="#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_1282576185.gif" width="10" height="1" border="0" style="display: block"></td>
                    </tr>
                    </table>
                    <table cellspacing="0" border="0" align="center" cellpadding="0" width="240">
                    <tr>
                    <td height="10" bgcolor="#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_2135178533.gif" width="1" height="10" border="0" style="display: block"></td>
                    </tr>
                    </table>
                    <table cellspacing="0" border="0" align="center" cellpadding="0" width="240">
                    <tr>
                    <td bgcolor="#ffffff" align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#ff0000; line-height:16px">&bull;</td>
                    <td width="5" bgcolor="#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_1802653636.gif" width="5" height="1" border="0" style="display: block"></td>
                    <td bgcolor="#ffffff" align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#666666; line-height:16px">100% royalty-free photos, icons, backgrounds, and illustrations</td>
                    <td width="10" bgcolor="#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_441985736.gif" width="10" height="1" border="0" style="display: block"></td>
                    </tr>
                    </table>
                    <table cellspacing="0" border="0" align="center" cellpadding="0" width="240">
                    <tr>
                    <td height="10" bgcolor="#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_1254469003.gif" width="1" height="10" border="0" style="display: block"></td>
                    </tr>
                    </table>
                    <table cellspacing="0" border="0" align="center" cellpadding="0" width="240">
                    <tr>
                    <td bgcolor="#ffffff" align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#ff0000; line-height:16px">&bull;</td>
                    <td width="5" bgcolor="#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_170834732.gif" width="5" height="1" border="0" style="display: block"></td>
                    <td bgcolor="#ffffff" align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#666666; line-height:16px">Simple pricing on subscriptions and single image plans</td>
                    <td width="10" bgcolor="#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_334471575.gif" width="10" height="1" border="0" style="display: block"></td>
                    </tr>
                    </table>
                    <table cellspacing="0" border="0" align="center" cellpadding="0" width="240">
                    <tr>
                    <td height="20" bgcolor="#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_1098765974.gif" width="1" height="20" border="0" style="display: block"></td>
                    </tr>
                    </table>
                    <table cellspacing="0" border="0" align="center" cellpadding="0" width="240">
                    <tr>
                    <td width="204" height="40" valign="middle" align="center" bgcolor="#ff0000" background="images_test1/media_bistro_Email_Test1_V1_btn.jpg" style="font-family:Arial, Helvetica, sans-serif; font-size:20px; color:#ffffff; font-weight:bold">
<a href="https://shutterstock.7eer.net/c/2203312/108513/1305" style="font-family:Arial, Helvetica, sans-serif; font-size:20px; color:#ffffff; font-weight:bold; text-decoration:none">Save Now</a></td>
                    <td width="36" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif; font-size:30px; color:#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_431504173.gif" width="36" height="1" border="0" style="display: block"></td>
                    </tr>
                    </table></td>
                    <td width="24" height="362" valign="top" bgcolor="#ffffff"><a href="https://shutterstock.7eer.net/c/2203312/108513/1305"><img src="http://d34kf23wmhy492.cloudfront.net/1305_1063672485.jpg" width="24" height="362" border="0" style="display: block"></a></td>
                    </tr>
                    </table>
                    <table cellspacing="0" border="0" align="center" cellpadding="0" width="650">
                    <tr>
                    <td width="217" height="112" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif; font-size:30px; color:#ffffff"><a href="https://shutterstock.7eer.net/c/2203312/108513/1305" style="font-family:Arial, Helvetica, sans-serif; font-size:30px; color:#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_643569520.jpg" width="217" height="112" border="0" style="display: block"></a></td>
                    <td width="216" height="112" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif; font-size:30px; color:#ffffff"><a href="https://shutterstock.7eer.net/c/2203312/108513/1305" style="font-family:Arial, Helvetica, sans-serif; font-size:30px; color:#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_1109839269.jpg" width="216" height="112" border="0" style="display: block"></a></td>
                    <td width="217" height="112" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif; font-size:30px; color:#ffffff"><a href="https://shutterstock.7eer.net/c/2203312/108513/1305" style="font-family:Arial, Helvetica, sans-serif; font-size:30px; color:#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_201579298.jpg" width="217" height="112" border="0" style="display: block"></a></td>
                    </tr>
                    </table>
                    <table cellspacing="0" border="0" align="center" cellpadding="0" width="650">
                    <tr>
                    <td width="26" height="95" valign="top" bgcolor="#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_423899164.jpg" width="26" height="95" border="0" style="display: block"></td>
                    <td bgcolor="#ffffff" valign="top" width="600"><table cellspacing="0" border="0" align="center" cellpadding="0" width="600">
                    <tr>
                    <td width="20" bgcolor="#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_1005385411.gif" width="20" height="1" border="0" style="display: block"></td>
                    <td bgcolor="#ffffff" align="center" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#666666; line-height:16px">20,000 new images<br>
added daily</td>
                    <td width="60" bgcolor="#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_1706257851.gif" width="60" height="1" border="0" style="display: block"></td>
                    <td bgcolor="#ffffff" align="center" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#666666; line-height:16px">12,000 new videos<br>
uploaded weekly </td>
                    <td width="40" bgcolor="#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_296894749.gif" width="40" height="1" border="0" style="display: block"></td>
                    <td bgcolor="#ffffff" align="center" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#666666; line-height:16px">Over 230,000 images<br>
downloaded daily</td>
                    <td width="20" bgcolor="#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_153225459.gif" width="20" height="1" border="0" style="display: block"></td>
                    </tr>
                    </table>
                    <table cellspacing="0" border="0" align="center" cellpadding="0" width="600">
                    <tr>
                    <td height="20" bgcolor="#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_1312600064.gif" width="1" height="20" border="0" style="display: block"></td>
                    </tr>
                    </table>
                    <table cellspacing="0" border="0" align="center" cellpadding="0" width="600">
                    <tr>
                    <td width="20" bgcolor="#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_746713000.gif" width="20" height="1" border="0" style="display: block"></td>
                    <td bgcolor="#ffffff" align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#666666">*Discount will be applied at checkout. Savings good on Standard License plans only. Good through XXXXXX, 2013 at 11:59 p.m. EST. Cannot be used for Enhanced Licenses or custom sales products.</td>
<td width="20" bgcolor="#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_10051829.gif" width="20" height="1" border="0" style="display: block"></td>
                    </tr>
                    </table></td>
                    <td width="24" height="95" valign="top" bgcolor="#ffffff"><img src="http://d34kf23wmhy492.cloudfront.net/1305_147761319.jpg" width="24" height="95" border="0" style="display: block"></td>
                    </tr>
                    </table>
                    <table cellspacing="0" border="0" align="center" cellpadding="0" width="650">
                    <tr>
                    <td width="650" height="101" bgcolor="#EBEFEE"><a href="https://shutterstock.7eer.net/c/2203312/108513/1305"><img src="http://d34kf23wmhy492.cloudfront.net/1305_1430864091.jpg" width="650" height="101" border="0" style="display: block"></a></td>
                    </tr>
                    </table>                   

                    <!--  / content -->
    </td>
                    </tr>
                    </table></td>
                    </tr>
                    </table>
<img height="0" width="0" src="http://shutterstock.7eer.net/i/2203312/108513/1305" style="position:absolute;visibility:hidden;" border="0" /></body>
</html>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>