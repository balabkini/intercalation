<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?><script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script> <script type="text/javascript">
	ymaps.ready(init);

function init () {
    var myMap = new ymaps.Map("banner", {
            center: [55.002072, 73.197631],
            zoom: 15,
            controls: []
        }, {
            searchControlProvider: 'yandex#search'
        }),

    // Создание макета содержимого хинта.
    // Макет создается через фабрику макетов с помощью текстового шаблона.
        HintLayout = ymaps.templateLayoutFactory.createClass( "<div class='my-hint'>" +
            "<b>{{ properties.object }}</b><br />" +
            "{{ properties.address }}" +
            "</div>", {
                // Определяем метод getShape, который
                // будет возвращать размеры макета хинта.
                // Это необходимо для того, чтобы хинт автоматически
                // сдвигал позицию при выходе за пределы карты.
                getShape: function () {
                    var el = this.getElement(),
                        result = null;
                    if (el) {
                        var firstChild = el.firstChild;
                        result = new ymaps.shape.Rectangle(
                            new ymaps.geometry.pixel.Rectangle([
                                [0, 0],
                                [firstChild.offsetWidth, firstChild.offsetHeight]
                            ])
                        );
                    }
                    return result;
                }
            }
        );

    var myPlacemark = new ymaps.Placemark([55.002072, 73.197631], {
        address: "г. Омск ул. 2-я Солнечная, 39",
        object: "Центр современных технологий Внедрение"
    }, {
        hintLayout: HintLayout
    });

    myMap.geoObjects.add(myPlacemark);
    myMap.behaviors
        // Отключаем часть включенных по умолчанию поведений:
        //  - drag - перемещение карты при нажатой левой кнопки мыши;
        //  - magnifier.rightButton - увеличение области, выделенной правой кнопкой мыши.
        .disable(['drag', 'rightMouseButtonMagnifier', 'scrollZoom'])
}
	</script> <style>
            .my-hint {
            display: inline-block;
            padding: 5px;
            height: 35px;
            position: relative;
            left: -10px;
            width: 195px;
            font-size: 11px;
            line-height: 17px;
            color: #333333;
            text-align: center;
            vertical-align: middle;
            background-color: #faefb6;
            border: 1px solid #CDB7B5;
            border-radius: 20px;
            font-family: Arial;
        }
</style> <?/*
<!--Banner -->
                <section id="header">
                    <div id="banner">
                        <div class="container">
                            <div class="row">
                                <div class="col-6 col-12-medium">
                                  <h2>Адрес:</h2><p>г. Омск ул. 2-я Солнечная, 39</p> 
                                </div>
                              <a href="#footer" class="button-large">Написать сообщение</a>
                            </div>
                        </div>
                    </div>
                </section>  
*/?> <style>
    #banner {
        background-image: url("../../images/banner.jpg");
        background-position: center center;
        background-size: cover;
        height: 28em;
        text-align: center;
        position: relative;
    }

        #banner header {
            position: absolute;
            bottom: 0;
            left: 0;
            width: 100%;
            background: #212121;
            background: rgba(27, 27, 27, 0.75);
            color: #fff;
            padding: 1.5em 0;
        }

            #banner header h2 {
                display: inline-block;
                margin: 0;
                font-size: 1.25em;
                vertical-align: middle;
            }

                #banner header h2 em {
                    opacity: 0.75;
                }

                #banner header h2 a {
                    border-bottom-color: rgba(255, 255, 255, 0.5);
                }

                    #banner header h2 a:hover {
                        border-bottom-color: transparent;
                    }

            #banner header .button {
                vertical-align: middle;
                margin-left: 1em;
            }
            #banner header {
                z-index: 1
            }
</style> <section id="banner">
<div>
	<h3>Адрес:г. Омск ул. 2-я Солнечная, 39</h3>
 <a href="#footer" class="button-large">Написать сообщение</a>
</div>
 </section>
<!-- Features --> <section id="features">
<div class="container">
	<div class="row">
		<div class="col-6 col-12-medium imp-medium">
			 <!-- Feature #1 --> <section>
			<h2>Контакты</h2>
			<p>
				 644074 г. Омск ул. 2-я Солнечная, 39 <br>
 <a href="mailto:+79136703030">+79136703030</a><br>
 <a href="mailto:info@intercalation.ru">info@intercalation.ru</a>
			</p>
			<p>
				 Время работы: пон - пт с 9-00 до 18-00
			</p>
 </section>
		</div>
		<div class="col-6 col-12-medium imp-medium">
			 <!-- Feature #2 --> <section>
			<h2>Центр современных технологий «Внедрение»</h2>
			<p>
				 ИНН 55070788585<br>
				 КПП 550701001<br>
				 ОГРН 1365547965549<br>
				 ОКАТО 45
			</p>
 </section>
		</div>
	</div>
</div>
 </section>
<div id="send-form" class="bg-popup">
	<div class="bg-flex">
		<div class="form-popup">
 <img src="img/close.png" id="send-close" alt="" class="close">
			<div class="form-wrap">
 <img src="img/rocket.png" class="rocket" alt=""> <input type="text" placeholder="" value="Иванов Иван Иванович"> <input type="text" placeholder="Телефон"> <textarea name="" id="" cols="10" rows="5" placeholder="Введите текст..."></textarea> <button id="send" class="center">Отправить</button>
			</div>
		</div>
	</div>
</div>
<div id="auth-form" class="bg-popup">
	<div class="bg-flex">
		<div class="form-popup">
 <img src="img/close.png" id="auth-close" alt="" class="close">
			<div class="form-wrap">
				<h1>Авторизация</h1>
 <input type="text" placeholder="Логин"> <input type="text" placeholder="Пароль">
				<p>
					<a id="regisr-link" class="form-link" >Регистрация</a>
				</p>
				<p>
					<a id="btn-restore-form" class="form-link" >Забыли пароль?</a>
				</p>
				 <!-- href="" --> <button id="auth" class="center">Войти</button>
			</div>
		</div>
	</div>
</div>
<div id="regisr-form" class="bg-popup">
	<div class="bg-flex">
		<div class="form-popup">
 <img src="img/close.png" id="regisr-close" alt="" class="close">
			<div class="form-wrap">
				<h1>Регистрация</h1>
 <input type="text" placeholder="Логин"> <input type="text" placeholder="Пароль"> <input type="text" placeholder="Телефон">
				<p>
					<a id="auth-link" class="form-link" >Авторизация</a>
				</p>
 <button id="regisr" class="center">Регистрация</button>
			</div>
		</div>
	</div>
</div>
<div id="restore-form" class="bg-popup">
	<div class="bg-flex">
		<div class="form-popup">
 <img src="img/close.png" id="restore-close" alt="" class="close">
			<div class="form-wrap">
 <img src="img/back.png" id="restore-back" alt="" class="back">
				<h1>Восстановление пароля</h1>
 <input type="text" placeholder="Логин"> <input type="text" placeholder="Номер телефона">
				<p>
					&nbsp;
				</p>
				<p>
					&nbsp;
				</p>
 <button id="restore" class="center">Восстановить</button>
			</div>
		</div>
	</div>
</div>
<div class="babls">
	<div>
		<div class="logo">
			<h1><a href="">Внедрение</a></h1>
		</div>
 <nav>
		<div>
 <a href="index.html" class="nav-link__active">Главная</a>
		</div>
		<div>
			 <a class="nav-link" >Подменю</a>
			<div class="sub-menu">
				<div class="bx">
					<div>
					</div>
				</div>
 <img src="img/back.png" alt="" class="back">
				<div class="sub-link">
					Текст
				</div>
				<div class="sub2-menu">
					<div class="bx2">
						<div>
						</div>
					</div>
 <img src="img/back.png" alt="" class="back">
					<div class="sub2-link">
						подТекст
					</div>
					<div class="sub2-link">
						подТекст
					</div>
					<div class="sub2-link">
						подТекст
					</div>
					<div class="sub2-link">
						подТекст
					</div>
					<div class="sub2-link">
						подТекст
					</div>
				</div>
				<div class="sub-link">
					Текст
				</div>
				<div class="sub-link">
					Текст
				</div>
				<div class="sub-link">
					Текст
				</div>
				<div class="sub-link">
					Текст
				</div>
			</div>
		</div>
		<div>
 <a href="left_sidebar.html" class="nav-link">Левый</a>
		</div>
		<div>
 <a href="right_sidebar.html" class="nav-link">Правый</a>
		</div>
		<div>
 <a href="sidebar-2.html" class="nav-link">Два</a>
		</div>
		<div>
 <a href="sidebar-0.html" class="nav-link">Нету</a>
		</div>
		<div id="socials" class="socials">
 <a href=""><img src="img/vk.png" alt=""></a> <a href=""><img src="img/inst.png" alt=""></a> <a href=""><img src="img/mail.png" alt=""></a>
		</div>
 </nav>
		<div class="auth">
 <img src="img/burger.png" id="burger" alt=""> <button id="btn-auth-form">Войти</button>
		</div>
	</div>
	<div class="container">
 <img src="img/babl-1.png" class="babl-1" alt=""> <img src="img/babl-2.png" class="babl-2" alt=""> <img src="img/babl-3.png" class="babl-3" alt=""> <img src="img/idle-1.png" class="idle-1" alt="">
		<div class="content">
			<div class="row">
				<div class="slogan">
					<h1 style="font-weight: 500" align="left">Внедр<span style="font-weight: 700">и</span>́ть-</h1>
					<p>
						<span aling="left">это Вкоренить, </span>заставить укрепиться в чем-нибудь, прочно войти куда-нибудь.
					</p>
 <button id="btn-send-form">Закажи внедрение</button>
				</div>
				<div class="slogan-img">
 <img src="img/idle-1.png" alt="">
					<div class="danger">
						<img src="img/danger.png" id="auth-danger" alt="">
					</div>
				</div>
			</div>
			<div id="s3d-1" class="slider-3d">
				<div class="bg-item-three">
					<div class="item-three">
 <a href="#">
						<div class="ellipse">
							<img src="img/img-item-1.png" alt="">
						</div>
						</a> <a href="#">
						<h2>Подзаголовок</h2>
						</a>
						<p>
							Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.
						</p>
					</div>
				</div>
				<div class="bg-item-three">
					<div class="item-three">
 <a href="#">
						<div class="ellipse">
							<img src="img/img-item-2.png" alt="">
						</div>
						</a> <a href="#">
						<h2>Подзаголовок</h2>
						</a>
						<p>
							Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.
						</p>
					</div>
				</div>
				<div class="bg-item-three">
					<div class="item-three">
 <a href="#">
						<div class="ellipse">
							<img src="img/img-item-3.png" alt="">
						</div>
						</a> <a href="#">
						<h2>Подзаголовок</h2>
						</a>
						<p>
							Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.
						</p>
					</div>
				</div>
			</div>
			<div id="slider1" class="items-3">
				<div class="item-three">
 <a href="#">
					<div class="ellipse">
						<img src="img/img-item-1.png" alt="">
					</div>
					</a> <a href="#">
					<h2>Подзаголовок</h2>
					</a>
					<p>
						Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.
					</p>
				</div>
				<div class="item-three">
 <a href="#">
					<div class="ellipse">
						<img src="img/img-item-2.png" alt="">
					</div>
					</a> <a href="#">
					<h2>Подзаголовок</h2>
					</a>
					<p>
						Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.
					</p>
				</div>
				<div class="item-three">
 <a href="#">
					<div class="ellipse">
						<img src="img/img-item-3.png" alt="">
					</div>
					</a> <a href="#">
					<h2>Подзаголовок</h2>
					</a>
					<p>
						Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.
					</p>
				</div>
			</div>
			<div class="titles">
				<div class="title-image">
 <img src="img/idle-2.png" alt="">
				</div>
				<div class="title">
 <a href="">
					<h2>Заголовок</h2>
					</a>
					<p>
						Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.Но высокотехнологичная концепция общественного уклада, .
					</p>
 <a href="">
					<h3>Подзаголовок</h3>
					</a>
					<p>
						Но высокотехнологичная концепция общественного уклада,
					</p>
 <a href="">
					<h3>Подзаголовок</h3>
					</a>
					<p>
						Но высокотехнологичная концепция общественного уклада,
					</p>
 <a href="">
					<h3>Подзаголовок</h3>
					</a>
					<p>
						Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.
					</p>
				</div>
			</div>
			<div class="row">
				<div class="items-6">
					<div class="item-six">
 <a href="">
						<div class="ellipse-min">
							<img src="img/img-item-1.png" alt="">
						</div>
						</a> <a href="">
						<h3>Подзаг</h3>
						</a>
						<p>
							Коонцепция общественного уклада.
						</p>
					</div>
					<div class="item-six">
 <a href="">
						<div class="ellipse-min">
							<img src="img/img-item-1.png" alt="">
						</div>
						</a> <a href="">
						<h3>Подзаг</h3>
						</a>
						<p>
							Коонцепция общественного уклада.
						</p>
					</div>
					<div class="item-six">
 <a href="">
						<div class="ellipse-min">
							<img src="img/img-item-1.png" alt="">
						</div>
						</a> <a href="">
						<h3>Подзаг</h3>
						</a>
						<p>
							Коонцепция общественного уклада.
						</p>
					</div>
					<div class="item-six">
 <a href="">
						<div class="ellipse-min">
							<img src="img/img-item-1.png" alt="">
						</div>
						</a> <a href="">
						<h3>Подзаг</h3>
						</a>
						<p>
							Коонцепция общественного уклада.
						</p>
					</div>
					<div class="item-six">
 <a href="">
						<div class="ellipse-min">
							<img src="img/img-item-1.png" alt="">
						</div>
						</a> <a href="">
						<h3>Подзаг</h3>
						</a>
						<p>
							Коонцепция общественного уклада.
						</p>
					</div>
					<div class="item-six">
 <a href="">
						<div class="ellipse-min">
							<img src="img/img-item-1.png" alt="">
						</div>
						</a> <a href="">
						<h3>Подзаг</h3>
						</a>
						<p>
							Коонцепция общественного уклада.
						</p>
					</div>
 <img src="img/idle-3.png" class="idle-3" alt="">
				</div>
				<div class="item-6-img">
					<img src="img/idle-3.png" alt="">
				</div>
			</div>
		</div>
	</div>
	<div class="slider">
		<div class="slider-wrap">
			<div class="slider-image">
 <img src="img/idle-4.png" alt="">
			</div>
			<div class="slider-content">
				<h1>АКЦИЯ АКЦИЯ БАННЕР С АКЦИЕЙ</h1>
				<p>
					Условия акции, какой-нибудь текст или утп. Условия акции, какой-нибудь текст или утп. Условия акции, какой-нибудь текст или утп.
				</p>
			</div>
		</div>
		<div class="slider-wrap">
			<div class="slider-image">
 <img src="img/idle-4.png" alt="">
			</div>
			<div class="slider-content">
				<h1>АКЦИЯ АКЦИЯ БАННЕР С АКЦИЕЙ</h1>
				<p>
					Условия акции, какой-нибудь текст или утп. Условия акции, какой-нибудь текст или утп. Условия акции, какой-нибудь текст или утп.
				</p>
			</div>
		</div>
		<div class="slider-wrap">
			<div class="slider-image">
 <img src="img/idle-4.png" alt="">
			</div>
			<div class="slider-content">
				<h1>АКЦИЯ АКЦИЯ БАННЕР С АКЦИЕЙ</h1>
				<p>
					Условия акции, какой-нибудь текст или утп. Условия акции, какой-нибудь текст или утп. Условия акции, какой-нибудь текст или утп.
				</p>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="partners">
			<div class="partner">
 <a href="">
				<div class="ellipse">
				</div>
				</a> <a href="">
				<h3>Партнер</h3>
				</a>
			</div>
			<div class="partner">
 <a href="">
				<div class="ellipse">
				</div>
				</a> <a href="">
				<h3>Партнер</h3>
				</a>
			</div>
			<div class="partner">
 <a href="">
				<div class="ellipse">
				</div>
				</a> <a href="">
				<h3>Партнер</h3>
				</a>
			</div>
			<div class="partner">
 <a href="">
				<div class="ellipse">
				</div>
				</a> <a href="">
				<h3>Партнер</h3>
				</a>
			</div>
			<div class="partner">
 <a href="">
				<div class="ellipse">
				</div>
				</a> <a href="">
				<h3>Партнер</h3>
				</a>
			</div>
			<div class="partner">
 <a href="">
				<div class="ellipse">
				</div>
				</a> <a href="">
				<h3>Партнер</h3>
				</a>
			</div>
			<div class="partner">
 <a href="">
				<div class="ellipse">
				</div>
				</a> <a href="">
				<h3>Партнер</h3>
				</a>
			</div>
			<div class="partner">
 <a href="">
				<div class="ellipse">
				</div>
				</a> <a href="">
				<h3>Партнер</h3>
				</a>
			</div>
		</div>
		<div class="row">
			<div id="reg" class="items-3">
				<div class="reg">
					<h1>Текст Текст Текст Текст Текст Текст</h1>
					<p>
						ТЕКСТ ТЕКСТ ТЕКСТ ТЕКСТ ТЕКСТ ТЕКСТ ТЕКСТ
					</p>
 <button id="btn-regisr-form">Регистрация</button>
				</div>
			</div>
		</div>
		<div id="s3d-2" class="slider-3d">
			<div class="bg-item-three">
				<div class="item-three">
 <a href="">
					<h1>Заголовок</h1>
					</a> <a href="">
					<h2>Подзаголовок</h2>
					</a>
					<p>
						Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.
					</p>
					<p>
						Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.
					</p>
				</div>
			</div>
			<div class="bg-item-three">
				<div class="item-three">
 <a href="">
					<h1>Заголовок</h1>
					</a> <a href="">
					<h2>Подзаголовок</h2>
					</a> <a href=""><img src="img/rect.png" alt=""></a>
					<p>
						Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.
					</p>
				</div>
			</div>
			<div class="bg-item-three">
				<div class="item-three">
 <a href="">
					<h1>Заголовок</h1>
					</a> <a href="">
					<h2>Подзаголовок</h2>
					</a>
					<ul>
 <a href="">
						<li>Тест</li>
						</a> <a href="">
						<li>Тест</li>
						</a> <a href="">
						<li>Тест</li>
						</a> <a href="">
						<li>Тест</li>
						</a> <a href="">
						<li>Тест</li>
						</a>
					</ul>
				</div>
			</div>
		</div>
		<div id="slider2" class="items-3">
			<div class="item-three">
 <a href="">
				<h1>Заголовок</h1>
				</a> <a href="">
				<h2>Подзаголовок</h2>
				</a>
				<p>
					Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи. Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.
				</p>
			</div>
			<div class="item-three">
 <a href="">
				<h1>Заголовок</h1>
				</a> <a href="">
				<h2>Подзаголовок</h2>
				</a> <a href=""><img src="img/rect.png" alt=""></a>
				<p>
					Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.
				</p>
			</div>
			<div class="item-three">
 <a href="">
				<h1>Заголовок</h1>
				</a> <a href="">
				<h2>Подзаголовок</h2>
				</a>
				<ul>
 <a href="">
					<li>Тест</li>
					</a> <a href="">
					<li>Тест</li>
					</a> <a href="">
					<li>Тест</li>
					</a> <a href="">
					<li>Тест</li>
					</a> <a href="">
					<li>Тест</li>
					</a>
				</ul>
			</div>
		</div>
		<div class="items-2">
			<div class="item-two">
				<div class="item-empty">
				</div>
				<div class="item-subtext">
					Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.
				</div>
			</div>
			<div class="item-two">
				<div class="item-empty">
				</div>
				<div class="item-subtext">
					Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.
				</div>
			</div>
			<div class="item-two">
				<div class="item-empty">
				</div>
				<div class="item-subtext">
					Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.
				</div>
			</div>
			<div class="item-two">
				<div class="item-empty">
				</div>
				<div class="item-subtext">
					Но высокотехнологичная концепция общественного уклада, а также свежий взгляд на привычные вещи.
				</div>
			</div>
		</div>
 <footer>
		<div class="footer-left">
 <a href=""><img src="img/footer-rect.png" alt=""></a>
		</div>
		<div class="footer-center">
			<div class="footer-item">
				<p>
					<a href="">Пункт футера</a>
				</p>
				<p>
					<a href="">Пункт футера</a>
				</p>
				<p>
					<a href="">Пункт футера</a>
				</p>
				<p>
					<a href="">Пункт футера</a>
				</p>
				<p>
					<a href="">Пункт футера</a>
				</p>
				<p>
					<a href="">Пункт футера</a>
				</p>
 <img src="img/down.png" class="down" alt="">
			</div>
			<div class="footer-item">
				<p>
					<a href="">Пункт футера</a>
				</p>
				<p>
					<a href="">Пункт футера</a>
				</p>
				<p>
					<a href="">Пункт футера</a>
				</p>
				<p>
					<a href="">Пункт футера</a>
				</p>
				<p>
					<a href="">Пункт футера</a>
				</p>
			</div>
			<div class="footer-item">
				<p>
					<a href="">Пункт футера</a>
				</p>
				<p>
					<a href="">Пункт футера</a>
				</p>
				<p>
					<a href="">Пункт футера</a>
				</p>
				<p>
					<a href="">Пункт футера</a>
				</p>
			</div>
			<div id="s3d-3" class="f-slider" style="width: 100%">
				<div>
					<div class="footer-item">
						<p>
							<a href="">Пункт футера</a>
						</p>
						<p>
							<a href="">Пункт футера</a>
						</p>
						<p>
							<a href="">Пункт футера</a>
						</p>
						<p>
							<a href="">Пункт футера</a>
						</p>
						<p>
							<a href="">Пункт футера</a>
						</p>
						<p>
							<a href="">Пункт футера</a>
						</p>
 <img src="img/down.png" class="down" alt="">
					</div>
				</div>
				<div>
					<div class="footer-item">
						<p>
							<a href="">Пункт футера</a>
						</p>
						<p>
							<a href="">Пункт футера</a>
						</p>
						<p>
							<a href="">Пункт футера</a>
						</p>
						<p>
							<a href="">Пункт футера</a>
						</p>
						<p>
							<a href="">Пункт футера</a>
						</p>
					</div>
				</div>
				<div>
					<div class="footer-item">
						<p>
							<a href="">Пункт футера</a>
						</p>
						<p>
							<a href="">Пункт футера</a>
						</p>
						<p>
							<a href="">Пункт футера</a>
						</p>
						<p>
							<a href="">Пункт футера</a>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-right">
 <a href="">
			<h1>Заголовок</h1>
			</a>
			<div class="socials">
 <a href=""><img src="img/vk.png" alt=""></a> <a href=""><img src="img/inst.png" alt=""></a> <a href=""><img src="img/mail.png" alt=""></a>
			</div>
 <button id="fbtn-send-form">Закажи внедрение</button>
		</div>
 </footer>
	</div>
</div>
	<script src="js/jq.js"></script>
	<script src="js/script.js"></script>
	<script type="text/javascript" src="slick/slick.min.js"></script>
	<script>

		if($(window).width() <= 375)
		{
			var ctrPdg = '9%';
			var k = 1;
		}
		else if($(window).width() <= 768)
		{
			var ctrPdg = '27%';
			var k = 2;
		}
		else
		{
			var ctrPdg = '0';
			var k = 3;
		}

		$('.slider-3d').slick({
			dots: true,
			infinite: true,
			accessibility: false,
			centerMode: true,
			centerPadding: ctrPdg,
			slidesToShow: 3,
			speed: 500,
		responsive: [{

		      breakpoint: 992,
		      settings: {
		        slidesToShow: 1,
		        arrows: false
		      }

		    }]
		});

		$('.f-slider').slick({
			dots: true,
			infinite: true,
			accessibility: false,
			centerMode: true,
			centerPadding: 0,
			slidesToShow: 3,
			speed: 500,
		responsive: [{

		      breakpoint: 992,
		      settings: {
		        slidesToShow: k,
		        arrows: false
		      }

		    }]
		});

		$('.slider').slick({
	      dots: true,
	      accessibility: true,
	      prevArrow: "<img class='arrow-left' src='img/arrow-left.png' class='prev' alt='1'>",
		  nextArrow: "<img class='arrow-right' src='img/arrow-right.png' class='next' alt='2'>",
	    });
	</script><? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>